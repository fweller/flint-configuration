" --------------------------------------------------
" NAME        : .vimrc
" DESCRIPTION : VimRC = VIM RunCon = Vi IMproved Runtime Control
" --------------------------------------------------

" --------------------------------------------------
" General Settings
" --------------------------------------------------

" Use Vim settings rather than Vi settings, must be listed first
set nocompatible

" Enable filetype plugins and indenting
filetype plugin on
filetype indent on

" Enable syntax highlighting
if !exists("g:syntax_on")
    syntax enable
endif

" --------------------------------------------------
" Display
" --------------------------------------------------

set title
set number          " Enable line numbering
set numberwidth=5
set visualbell      " No sounds
set noerrorbells
set encoding=utf-8  " Encoding shown in the terminal
set cursorline      " Show a visual line under the cursor
set showtabline=0   " Start OFF, toggle =2 to show tabline
set laststatus=2    " Always show all statuslines
set ruler           " Enable ruler
set showcmd         " Show incomplete cmds down the bottom
set showmode        " Show current mode down the bottom
" set gcr=a:blinkon0  " Disable cursor blink (uncomment if needed)

" --------------------------------------------------
" Scrolling
" --------------------------------------------------

set scrolloff=8      " Start scrolling when 8 lines away from margins
set sidescrolloff=16
set sidescroll=1

" --------------------------------------------------
" Input
" --------------------------------------------------

set mouse=a         " Enable mouse
set backspace=indent,eol,start  " Allow backspace in insert mode
set notimeout
set ttimeout

" --------------------------------------------------
" Remapping of Keys
" --------------------------------------------------

" Remap hjkl navigation keys to jkl;
nnoremap j h
nnoremap k j
nnoremap l k
nnoremap ; l

vnoremap j h
vnoremap k j
vnoremap l k
vnoremap ; l

" --------------------------------------------------
" Leader
" --------------------------------------------------

" Change leader to a comma
" let mapleader=","

" --------------------------------------------------
" Platform-Specific Configuration
" --------------------------------------------------

" Windows
if has('win32') || has('win64')
  let $HOME = $USERPROFILE . "\\.vim"
  set runtimepath+=$HOME
  set directory^=$HOME
  set backupdir=$HOME\\backup//
  set undodir=$HOME\\undo//
endif


" --------------------------------------------------
" History
" --------------------------------------------------

set history=1000    " Store lots of :cmdline history

" --------------------------------------------------
" File Saving
" --------------------------------------------------

set fileencoding=utf-8 " Written output encoding
set fileformats=unix,mac,dos

" --------------------------------------------------
" Backups and Swap Files
" --------------------------------------------------

silent !mkdir -p $HOME/.vim/backups > /dev/null 2>&1
set backupdir=$HOME/.vim/backups/
set backup
set writebackup
set noswapfile

" --------------------------------------------------
" Persistent Undo
" --------------------------------------------------

silent !mkdir -p $HOME/.vim/undo > /dev/null 2>&1
set undodir=$HOME/.vim/undo
set undofile
set undolevels=1000
set undoreload=10000

" --------------------------------------------------
" Completion
" --------------------------------------------------

" Uncomment and configure wildmenu and wildignore as needed
" set wildmenu
" set wildmode=list:longest
" set wildignore=*.o,*.obj,*${HOME}
" set wildignore+=*vim/backups*
" set wildignore+=*sass-cache*
" set wildignore+=*DS_Store*
" set wildignore+=vendor/rails/**
" set wildignore+=vendor/cache/**
" set wildignore+=*.gem
" set wildignore+=log/**
" set wildignore+=tmp/**
" set wildignore+=*.png,*.jpg,*.gif

" --------------------------------------------------
" Window Splitting and Buffers
" --------------------------------------------------

set splitbelow
set splitright

if exists('+splitkeep')
  set splitkeep=screen
endif

set hidden           " Remember undo after quitting
set switchbuf=useopen
set nostartofline    " Don't jump to col1 on switch buffer

" --------------------------------------------------
" Code Folding
" --------------------------------------------------

" Uncomment and configure folding as needed
" set foldmethod=indent
" set foldnestmax=10
" set nofoldenable
" set foldenable
" set foldlevelstart=10

" --------------------------------------------------
" Trailing Whitespace
" --------------------------------------------------

"set list
"set listchars=tab:→,trail:·,extends:»,precedes:«,nbsp:⣿

" --------------------------------------------------
" Diffing
" --------------------------------------------------

set fillchars+=diff:⣿
set diffopt=vertical,filler,iwhite

" --------------------------------------------------
" Input Auto Formatting
" --------------------------------------------------

set formatoptions=r,n,2,1

if has('patch-7.3.541')
  set formatoptions+=j
endif

" --------------------------------------------------
" Whitespace
" --------------------------------------------------

set nowrap
set nojoinspaces

" --------------------------------------------------
" Indenting
" --------------------------------------------------

set autoindent
set smartindent
set smarttab
set softtabstop=2
set tabstop=2
set shiftwidth=2
set expandtab

" --------------------------------------------------
" Tabbing
" --------------------------------------------------

set noshiftround

" --------------------------------------------------
" Match and Search
" --------------------------------------------------

set matchtime=1
set showmatch
set wrapscan
set incsearch
set hlsearch
set ignorecase
set smartcase

if exists('+tagcase') && has('patch-7.4.2230')
  set tagcase=followscs
endif

nnoremap <leader><space> :nohlsearch<CR>

" --------------------------------------------------
" Syntax
" --------------------------------------------------

set synmaxcol=512

" ----------------------------------------
" Filetype: markdown
" ----------------------------------------

let g:markdown_fenced_languages = ['javascript', 'js=javascript', 'javascriptreact', 'json', 'bash=sh', 'sh', 'vim', 'help']

" ----------------------------------------
" Filetype: python
" ----------------------------------------

let python_highlight_all = 1

" ----------------------------------------
" Filetype: sh
" ----------------------------------------

let g:is_bash = 1

" ----------------------------------------
" Filetype: vim
" ----------------------------------------

let g:vimsyn_embed = 'lpPr'

" --------------------------------------------------
" Plugins
" --------------------------------------------------

" Add your plugin manager and plugins here
" For example, using vim-plug:
" call plug#begin('~/.vim/plugged')
" Plug 'tpope/vim-sensible'
" call plug#end()

" --------------------------------------------------
" Autocommands
" --------------------------------------------------

" Add your autocommands here

" --------------------------------------------------
" Colors
" --------------------------------------------------

set background=dark

let g:gruvbox_contrast_dark = 'hard'
let g:material_theme_style  = 'darker'

silent! colorscheme gruvbox

" --------------------------------------------------
" Fonts
" --------------------------------------------------

set guifont=Noto\ Sans\ Mono\ 14

if has('gui_running')
  if has("mac") || has("macunix")
    set guifont=Noto\ Sans\ Mono:h14
  elseif has('win32') || has('win64')
    set guifont=Noto\ Sans\ Mono:h14
  elseif has("unix")
    set guifont=Noto\ Sans\ Mono\ 14
  endif
endif

" Set font size based on hostname (example)
" if hostname() == 'home-pc'
"     set guifont=Noto\ Sans\ Mono\ 14
" endif

" --------------------------------------------------
" Security
" --------------------------------------------------

" Disallow unsafe local vimrc commands
set secure

