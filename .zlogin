#!/bin/zsh
# ----------------------------------------
# NAME        : .zlogin
# DESCRIPTION : ZLogin = Z Shell Login
# ----------------------------------------
# For login shells. It is sourced on the start of a login shell but after .zshrc,
# if the shell is also interactive.
# This file is often used to start X using startx.
# Some systems start X on boot, so this file is not always very useful.
# ----------------------------------------

FILE=".zlogin"

# ----------------------------------------
# Handle debug

if [[ -f "$HOME/.debug" ]]; then
  date "+%Y-%m-%d %H:%M:%S-%N %Z $NAME" >> "$HOME/Logs/dotfiles.log"
fi

# ----------------------------------------
# Chain-loading

# The next dotfile to chain-load
nextfile="$HOME/.shellrc"

# Chain-load the next file if this is an interactive shell
if [ "${-#*i}" != "$-" ] && [ -f "$nextfile" ]; then
  . "$nextfile"
fi

