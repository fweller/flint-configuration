#!/bin/zsh
# ----------------------------------------
# NAME        : .zprofile
# DESCRIPTION : ZProfile = Z Shell Profile
# ----------------------------------------
# This script is for login shells.
# It is basically the same as .zlogin except that it's sourced before .zshrc
# whereas .zlogin is sourced after .zshrc.
# According to the zsh documentation, ".zprofile is meant as an alternative to
# .zlogin for ksh fans; the two are not intended to be used together,
# although this could certainly be done if desired."
# ----------------------------------------

NAME=".zprofile"

# Handle debug
if [ -f "$HOME/.debug" ]; then
  date "+%Y-%m-%d %H:%M:%S-%N %Z $NAME" >> "$HOME/Logs/dotfiles.log"
fi


