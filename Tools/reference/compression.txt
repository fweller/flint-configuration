##Compression

## Modern compression with built-in archiving
7z - Best compression

## Modern compression - no archiving
Zstandard
LZMA
LZ77 - This is the standard that most modern algorithms are based on

## Classic compression with built-in archiving
zip - Use for compatibility

## Classic compression
gzip
bzip2

## Classic archiving
tar

-----------------------------------------------
## 7z

Archive a file or a directory
$ 7za a archive.7z input

Decompress a file or a directory
$ 7za x archive.7z
Can use the -oTarget flag to specify a Target directory

Archive a file or directory and encrypt it
$ 7z a -p archive.7z input

-----------------------------------------------
## Zip

Archive a file or directory
$ zip -r output.zip input

Decompress a file or directory
unzip input.zip


-----------------------------------------------
## Zstandard


# zstd
Parallelized by default

Compress a file
$ zstd -z file

Decompress a file
$ zstd -d file.zst

Compress a directory
$ tar --zstd -cf directory.tar.zst directory/

Decompress a directory
$ tar --zstd -xf directory.tzr.zst


# pzstd - is not faster than zstd

Compress a file
$ pzstd -o output.zst file

Decompress a file
$ pzstd -d input.zst

-----------------------------------------------
