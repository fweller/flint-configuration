#!/bin/bash
# ----------------------------------------
# NAME        : generate_book_list.sh
# DESCRIPTION : Generates a CSV-like text file with book title, author, and Amazon URL.
# USAGE       : ./generate_book_list.sh
# DEPENDENCIES: Requires exiftool and a directory with .azw3 files.
# OUTPUT      : book_list.txt
# ----------------------------------------

# Ensure exiftool is installed
if ! command -v exiftool &> /dev/null; then
    echo "Error: exiftool is not installed. Install it and try again."
    exit 1
fi

# Output file
OUTPUT_FILE="book_list.txt"

# Create or clear the output file
echo "Generating book list..."
echo '"TITLE", "AUTHOR", "AMAZON LINK"' > "$OUTPUT_FILE"

# Loop through all .azw3 files in the directory
for file in *.azw3; do
    # Skip if no .azw3 files are found
    [[ -f "$file" ]] || continue
    
    echo "Processing: $file"

    # Extract metadata
    title=$(exiftool -s -s -s -BookName "$file")
    author=$(exiftool -s -s -s -Author "$file")
    asin=$(exiftool -s -s -s -ASIN "$file")

    # Format missing values
    title="${title:-Unknown Title}"
    author="${author:-Unknown Author}"
    asin="${asin:-N/A}"

    # Construct Amazon URL or "N/A"
    if [[ "$asin" != "N/A" ]]; then
        amazon_link="https://www.amazon.com/dp/$asin"
    else
        amazon_link="N/A"
    fi

    # Append to the output file
    echo "\"$title\", \"$author\", \"$amazon_link\"" >> "$OUTPUT_FILE"
done

echo "Book list saved to $OUTPUT_FILE"
