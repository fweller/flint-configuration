#!/bin/bash
# ----------------------------------------
# NAME        : extract_azw3_metadata.sh
# DESCRIPTION : Extracts book metadata from Kindle .azw3 files using exiftool.
# USAGE       : ./extract_azw3_metadata.sh file1.azw3 file2.azw3 ...
# DEPENDENCIES: Requires exiftool installed.
# ----------------------------------------

# Ensure exiftool is installed
if ! command -v exiftool &> /dev/null; then
    echo "Error: exiftool is not installed. Install it and try again."
    exit 1
fi

# Check if at least one file is provided
if [ "$#" -lt 1 ]; then
    echo "Usage: $0 file1.azw3 [file2.azw3 ...]"
    exit 1
fi

# Process each file
for file in "$@"; do
    if [[ ! -f "$file" ]]; then
        echo "Error: File not found - $file"
        continue
    fi

    echo "----------------------------------------"
    echo "Extracting metadata from: $file"
    
    # Extract relevant fields
    title=$(exiftool -s -s -s -BookName "$file")
    author=$(exiftool -s -s -s -Author "$file")
    publisher=$(exiftool -s -s -s -Publisher "$file")
    publish_date=$(exiftool -s -s -s -PublishDate "$file")
    asin=$(exiftool -s -s -s -ASIN "$file")
    language=$(exiftool -s -s -s -Language "$file")
    rights=$(exiftool -s -s -s -Rights "$file")
    file_size=$(exiftool -s -s -s -FileSize "$file")
    mod_date=$(exiftool -s -s -s -FileModificationDateTime "$file")

    # Display extracted metadata
    echo "Title         : ${title:-N/A}"
    echo "Author        : ${author:-N/A}"
    echo "Publisher     : ${publisher:-N/A}"
    echo "Publish Date  : ${publish_date:-N/A}"
    echo "ASIN          : ${asin:-N/A}"
    echo "Language      : ${language:-N/A}"
    echo "Rights        : ${rights:-N/A}"
    echo "File Size     : ${file_size:-N/A}"
    echo "Modified Date : ${mod_date:-N/A}"
    echo "----------------------------------------"
done

