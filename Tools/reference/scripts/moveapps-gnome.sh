#!/bin/sh

# This scipt only works with gnome AFAIK and is not appropriate for kde plasma
# However, this script could be rewritten to support kde plasma while
# still relying on wmctrl, which must be installed.
# Otherwise just rely on KDE Plasma scripting with kwin!
# https://develop.kde.org/docs/plasma/kwin/

# Purpose: Start my work related applications in the proper position
# Install: 
# ln "${HOME}/Tools/scripts/work/moveapps.sh" "${HOME}/Bin/moveapps"

# NOTE: wmctrl does not seem to be consistent about window positions
# I can not trust this application

# Run this command to see the exact position to specify
# wmctrl -lG

# Run this command to see what extra frame boundary is associated with the window
# $ xprop | grep EXTENT
# <click the GEdit window>

# Force 4 workspaces
wmctrl -n 4

# Gnome top bar height
TopBarHeight=130

# Monitor 1 is 4K UHD 16:9 mounted horizontally, centered on desktop
Monitor1Width=3840
Monitor1Height=2160
Monitor1AvailableWidth=$Monitor1Width
Monitor1AvailableHeight=$(( $Monitor1Height - $TopBarHeight ))
Monitor1CoordinateLeft=-60
Monitor1CoordinateRight=$(( $Monitor1CoordinateLeft + $Monitor1AvailableWidth ))
Monitor1CoordinateTop=0
Monitor1CoordinateBottom=$(( $Monitor1CoordinateTop + $Monitor1AvailableHeight ))
Monitor1AvailableWidthHalf=$(( $Monitor1AvailableWidth / 2))
Monitor1AvailableHeightHalf=$(( $Monitor1AvailableHeight / 2 ))
Monitor1CoordinateWidthHalf=$(( $Monitor1CoordinateLeft + $Monitor1AvailableWidthHalf ))
Monitor1CoordinateHeightHalf=$(( $Monitor1CoordinateTop + $Monitor1AvailableHeightHalf ))

# Monitor 2 is 4K UHD 16:9 mounted vertically, right side of desktop
Monitor2Width=2160
Monitor2Height=3840
Monitor2AvailableWidth=$Monitor2Width
Monitor2AvailableHeight=$(( $Monitor2Height - $TopBarHeight ))
Monitor2CoordinateLeft=$(( $Monitor1Width + 1 ))
Monitor2CoordinateRight=$(( $Monitor2CoordinateLeft + $Monitor2AvailableWidth ))
Monitor2CoordinateTop=0
Monitor2CoordinateBottom=$(( $Monitor2CoordinateTop + $Monitor2AvailableHeight ))
Monitor2AvailableWidthHalf=$(( $Monitor2Width / 2))
Monitor2AvailableHeightHalf=$(( ( $Monitor2AvailableHeight ) / 2 ))
Monitor2CoordinateWidthHalf=$(( $Monitor2CoordinateLeft + $Monitor2AvailableWidthHalf ))
Monitor2CoordinateHeightHalf=$(( $Monitor2CoordinateTop + $Monitor2AvailableHeightHalf ))

# Workspaces
WorkspaceCount=4
WorkspaceMain=1
WorkspaceWindows=2
WorkspaceWork=3
WorkspaceProject=4

# Gravity
Gravity=0

# Debug
#echo "--------------------------------------------"
#echo "TopBarHeight                  = $TopBarHeight"
#echo "--------------------------------------------"
#echo "Monitor1Width                 = $Monitor1Width"
#echo "Monitor1Height                = $Monitor1Height"
#echo "Monitor1AvailableWidth        = $Monitor1AvailableWidth"
#echo "Monitor1AvailableHeight       = $Monitor1AvailableHeight"
#echo "Monitor1CoordinateLeft        = $Monitor1CoordinateLeft"
#echo "Monitor1CoordinateRight       = $Monitor1CoordinateRight"
#echo "Monitor1CoordinateTop         = $Monitor1CoordinateTop"
#echo "Monitor1CoordinateBottom      = $Monitor1CoordinateBottom"
#echo "Monitor1AvailableWidthHalf    = $Monitor1AvailableWidthHalf"
#echo "Monitor1AvailableHeightHalf   = $Monitor1AvailableHeightHalf"
#echo "Monitor1CoordinateWidthHalf   = $Monitor1CoordinateWidthHalf"
#echo "Monitor1CoordinateHeightHalf  = $Monitor1CoordinateHeightHalf"
#echo "--------------------------------------------"
#echo "Monitor2Width                 = $Monitor2Width"
#echo "Monitor2Height                = $Monitor2Height"
#echo "Monitor2AvailableWidth        = $Monitor2AvailableWidth"
#echo "Monitor2AvailableHeight       = $Monitor2AvailableHeight"
#echo "Monitor2CoordinateLeft        = $Monitor2CoordinateLeft"
#echo "Monitor2CoordinateRight       = $Monitor2CoordinateRight"
#echo "Monitor2CoordinateTop         = $Monitor2CoordinateTop"
#echo "Monitor2CoordinateBottom      = $Monitor2CoordinateBottom"
#echo "Monitor2AvailableWidthHalf    = $Monitor2AvailableWidthHalf"
#echo "Monitor2AvailableHeightHalf   = $Monitor2AvailableHeightHalf"
#echo "Monitor2CoordinateWidthHalf   = $Monitor2CoordinateWidthHalf"
#echo "Monitor2CoordinateHeightHalf  = $Monitor2CoordinateHeightHalf"
#echo "--------------------------------------------"

#--------------------------------------------
#TopBarHeight                  = 130
#--------------------------------------------
#Monitor1Width                 = 3840
#Monitor1Height                = 2160
#Monitor1AvailableWidth        = 3840
#Monitor1AvailableHeight       = 2030
#Monitor1CoordinateLeft        = 0
#Monitor1CoordinateRight       = 3840
#Monitor1CoordinateTop         = 0
#Monitor1CoordinateBottom      = 2030
#Monitor1AvailableWidthHalf    = 1920
#Monitor1AvailableHeightHalf   = 1015
#Monitor1CoordinateWidthHalf   = 1920
#Monitor1CoordinateHeightHalf  = 1015
#--------------------------------------------
#Monitor2Width                 = 2160
#Monitor2Height                = 3840
#Monitor2AvailableWidth        = 2160
#Monitor2AvailableHeight       = 3710
#Monitor2CoordinateLeft        = 3841
#Monitor2CoordinateRight       = 6001
#Monitor2CoordinateTop         = 0
#Monitor2CoordinateBottom      = 3710
#Monitor2AvailableWidthHalf    = 1080
#Monitor2AvailableHeightHalf   = 1855
#Monitor2CoordinateWidthHalf   = 4921
#Monitor2CoordinateHeightHalf  = 1855



# -------------------------------------------------
# -------------------------------------------------
# Monitor 1 - Workspace 0

# Workspaces
WorkspaceCount=4
WorkspaceMain=1
WorkspaceWindows=2
WorkspaceWork=3
WorkspaceProject=4

MoveChrome() {
App="Chrome"
G=$Gravity
X=$(( $Monitor1CoordinateLeft - 20 ))
Y=$(( $Monitor1CoordinateTop - 20 ))
W=$(( $Monitor1AvailableWidthHalf + 33 ))
H=$(( $Monitor1AvailableHeight + 140 ))
Command="wmctrl -r $App -e $G,$X,$Y,$W,$H"
#echo $Command
Retval=$($Command)
#echo $Retval
}

MoveTerminal () {
App="Terminal"
G=$Gravity
X=$(( $Monitor1CoordinateWidthHalf + 32 ))
Y=$(( $Monitor1CoordinateTop - 26 ))
W=$(( $Monitor1AvailableWidthHalf + 68 )) 
H=$(( $Monitor1AvailableHeight + 150 ))
#X=3788
#Y=1496
#W=1972
#H=2164
Command="wmctrl -r $App -e $G,$X,$Y,$W,$H"
#echo $Command
Retval=$($Command)
#echo $Retval
}

#This needs to be fixed.  
MoveNautilus() {
App="Downloads"
#App="0x00e00007" #this worked
#App="Nautilus"
G=$Gravity
X=$(( $Monitor1CoordinateWidthHalf + 30 ))
Y=$(( $Monitor1CoordinateTop - 26 ))
W=$(( $Monitor1AvailableWidthHalf + 60 ))
H=$(( $Monitor1AvailableHeight  ))
#Command="wmctrl -i -r $App -e $G,$X,$Y,$W,$H"
Command="wmctrl -r $App -e $G,$X,$Y,$W,$H"
#echo $Command
Retval=$($Command)
#echo $Retval
}

# -------------------------------------------------
# -------------------------------------------------
# Monitor 2 - Workspace 0

MoveEvernote() {
App="Evernote Legacy"
G=$Gravity
X=$Monitor2CoordinateLeft
Y=$(( $Monitor2CoordinateTop - 37 ))
W=$Monitor2AvailableWidthHalf
H=$Monitor2AvailableHeightHalf
Command="wmctrl -r $App -e $G,$X,$Y,$W,$H"
#echo $Command
Retval=$($Command)
#echo $Retval
}

MoveSpotify() {
App="spotify"
G=$Gravity
X=$Monitor2CoordinateWidthHalf
Y=$(( $Monitor2CoordinateTop - 38 ))
W=$Monitor2AvailableWidthHalf
H=$Monitor2AvailableHeightHalf
#X=4930 
#Y=130  
#W=1080 
#H=1859
Command="wmctrl -r $App -e $G,$X,$Y,$W,$H"
#echo $Command
Retval=$($Command)
#echo $Retval
}

MoveTeams() {
App="Teams"
G=$Gravity
X=$Monitor2CoordinateLeft
Y=$(( $Monitor2CoordinateHeightHalf + 90 ))
W=$Monitor2AvailableWidth
H=$(( $Monitor2AvailableHeightHalf + 20 )) #100
#X=7680 
#Y=3888 
#W=2160 
#H=1896
Command="wmctrl -r $App -e $G,$X,$Y,$W,$H"
#echo $Command
Retval=$($Command)
#echo $Retval
}


# -------------------------------------------------
# -------------------------------------------------
# Monitor 1 - Workspace 1
# -------------------------------------------------

MoveVirtualbox() {
App="Virtualbox"
Workspace=1
G=$Gravity
X=$Monitor1CoordinateLeft
Y=$Monitor1CoordinateTop
W=$Monitor1AvailableWidth
H=$Monitor1AvailableHeight
Command="wmctrl -r $App -e $G,$X,$Y,$W,$H"
#echo $Command
Retval=$($Command)
#echo $Retval
Command="wmctrl -r $App -t $Workspace"
#echo $Command
Retval=$($Command)
#echo $Retval
}


#MoveChrome
#MoveTerminal
MoveNautilus
#MoveEvernote
#MoveSpotify
#MoveTeams
MoveVirtualbox


