#!/bin/bash
# install-fonts.sh
# Purpose: Install fonts that I like
# NOTE: Bash is required for using pushd, popd, and read.

# Variables used in this script
ScriptName="install-fonts.sh"
TaskCounter=0
CommandCounter=0
HomeDir=""
DownloadDir=""
FontsDir=""
ProjectName=""
ProjectURL=""
ProjectsDir=""
InstallURL=""
InstallDir=""

Usage() {
echo "Usage:"
echo "$ bash $ScriptName run    # Run the installer"
echo "$ bash $ScriptName debug  # Debug this script"
exit 1
}

InstallInfo() {
TaskCounter=$((TaskCounter + 1))
CommandCounter=0
echo "------------------------------------------------------------"
echo "------------------------------------------------------------"
echo "$TaskCounter-$CommandCounter: Installing $ProjectName"
echo ""
echo " Project : $ProjectURL"
echo "    From : $InstallURL"
echo "    To   : $InstallDir"
}

Command() {
CommandCounter=$((CommandCounter + 1))
echo ""
echo "------------------------------------------------------------"
echo "$TaskCounter-$CommandCounter: $1"
echo ""
eval "$1"
}

InstallHack() {
ProjectName="Hack Nerd Font"
ProjectURL="https://github.com/ryanoasis/nerd-fonts/tree/master/patched-fonts/Hack"
InstallURL="https://github.com/source-foundry/Hack/releases/download/v3.003/Hack-v3.003-ttf.zip"
InstallDir="$ProjectsDir/hack"
InstallInfo
Command "mkdir -p $InstallDir"
Command "pushd $DownloadDir"
Command "wget $InstallURL"
Command "unzip Hack-v3.003-ttf.zip -d $InstallDir"
Command "popd"
Command "pushd $InstallDir"
Command "pushd ttf"
#if [ -z "$DEBUG" ]; then Command "sudo cp *.ttf /usr/share/fonts/"; fi
Command "sudo cp *.ttf $FontsDir"
Command "popd"
Command "popd"
}

echo "------------------------------------------------------------"

# Parse the command line parameters
if [ $# -eq 0 ]; then
  Usage
fi
for arg do
  case $arg in
    debug|--debug)
      DEBUG="true"
      ;;
    run|--run)
      # Run normally
      ;;
    *)
      Usage
      ;;
  esac
done

# Configure for Debug or Run
if [ ! -z "$DEBUG" ]; then
  HomeDir="${HOME}/tmp"
  FontsDir="$HomeDir/fonts"
  echo "$ScriptName DEBUG Enabled."
else
  HomeDir="${HOME}"
  FontsDir="/usr/share/fonts"
  echo "$ScriptName RUN Enabled."
fi

DownloadDir="${HOME}/Downloads"
ProjectsDir="$HomeDir/Projects/Fonts"
echo ""
echo "    HomeDir     = $HomeDir"
echo "    DownloadDir = $DownloadDir"
echo "    ProjectsDir = $ProjectsDir"
echo "    FontsDir    = $FontsDir"
echo ""

Command "mkdir -p $DownloadDir"
Command "mkdir -p $FontsDir"
Command "mkdir -p $ProjectsDir"

# Call each individual font installer here
InstallHack

echo "------------------------------------------------------------"
echo ""
echo "Done!"
exit 0
