# MacOS Login Script

MacOS provides a way to run user scripts after login using several methods. One common approach is to use the ~/.bash_profile, ~/.zshrc, or ~/.bashrc file (depending on the shell you are using) to execute scripts when you open a new terminal session. However, if you want a script to run after login, regardless of whether a terminal session is started, you can use launchd with a launch agent.

Here's a step-by-step guide to setting up a script to run after login using launchd:

1. Create the Script: Write the script you want to run and save it somewhere, for example, ~/scripts/my_script.sh.

2. Make the Script Executable:

3. Create a Launch Agent: Create a plist file for the launch agent. Save this file as ~/Library/LaunchAgents/com.example.myscript.plist. Here is an example plist file:

```
<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE plist PUBLIC "-//Apple//DTD PLIST 1.0//EN" "http://www.apple.com/DTDs/PropertyList-1.0.dtd">
<plist version="1.0">
<dict>
    <key>Label</key>
    <string>com.example.myscript</string>
    <key>ProgramArguments</key>
    <array>
        <string>/bin/bash</string>
        <string>/Users/your_username/scripts/my_script.sh</string>
    </array>
    <key>RunAtLoad</key>
    <true/>
    <key>KeepAlive</key>
    <false/>
</dict>
</plist>
```


4. Load the Launch Agent: Load the launch agent using launchctl:

```
launchctl load ~/Library/LaunchAgents/com.example.myscript.plist
```

This plist file tells launchd to run your script after login. The RunAtLoad key ensures the script runs when the user logs in.

Replace your_username with your actual macOS username and adjust the script path as necessary.

If you need to edit the script later, make sure to unload the launch agent first, then reload it after making changes:

```
launchctl unload ~/Library/LaunchAgents/com.example.myscript.plist
# Edit your script or plist file
launchctl load ~/Library/LaunchAgents/com.example.myscript.plist
```

This setup ensures your script runs automatically after you log in to your macOS account.


