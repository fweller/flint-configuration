# Find files

# ncdu (NCurses Disk Usage)
# https://dev.yorhel.nl/ncdu
sudo apt install ncdu

If necessary, further speedup can be achieved by precaching ncdu -1xo- / | gzip >export.gz in a cronjob and later accessing it with  zcat export.gz | ncdu -f-, but obviously gives more outdated information.

# duc (Dude, Where are my Bytes)
# https://duc.zevv.nl/
sudo apt install duc

# Update database used by locate command
sudo updatedb

Directory Size

Get the size of a directory in Linux with du command
$ du -hc . | sort -rh | head -20
    du: It’s a command
    -h: Print sizes in human readable format (e.g., 1K, 234M, 2G)
    -c: Produce a grand total
    .: The path of directory
    sort -rh: Sort the results with numerical value
    head -20: Output the first 20 lines result
Other options for du
--max-depth=1

Find the size of a directory in Linux with ncdu command
$ ncdu /home/daygeek/Documents/

Check a directory size in Linux with tree command
$ tree --du -h /opt/ktube-media-downloader


