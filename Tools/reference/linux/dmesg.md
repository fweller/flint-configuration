# Dmesg

## Human
Will be human-readable and allow scrolling.  Color will be enabled by default.
`dmesg -H` or `dmesg --human`

## Follow
Will view dmesg in real-time.  Color will be enabled by default.
`dmesg --follow`

## Color
Dmesg usually removes color when piping the output.
To preserve color output while piping.
`dmesg --color=always | less`

