## nvidia GPU Stuff
# For nvidia who does not play nicely with open source
# NEVER PURCHASE AN NVIDIA GPU - EVER - ITS NOT WORTH IT

# X11 configuration file is
/etc/X11/xorg.conf
# xorg.conf is only read when the X server starts
# Deleting the file will make the system automatically figure things out on next reboot
# Note: xorg.conf uses decimal values while lspci displays hex values


# To start and stop the X11 server
# KDE Plasma sddm example
sudo service sddm stop
sudo service sddm start

# Configure the nvidia driver
sudo nvidia-xconfig    # 
sudo nvidia-settings   # Save X config file to /etc/X11/xorg.conf


# Remove all nvidia stuff from a system
su
apt list -i | grep nvidia
dpkg -l | grep nvidia
apt purge nvidia-driver-*
apt purge nvidia-*

# Confirm GPU exists
su
lspci -nn | egrep -i "3d|display|vga"
lspci | grep -e VGA

# Install nvidia stuff
su
apt update
apt install linux-headers-amd64   # Linux headers
apt install firmware-misc-nonfree # All firmware
apt install nvidia-driver         # nvidia driver
apt install nvidia-xconfig        # The NVIDIA X Configuration Tool
apt install nvidia-smi            # NVIDIA System Management Interface
nvidia-settings   # Save X config file to /etc/X11/xorg.conf
# a message may appear, this is normal:
# The free nouveau kernel module is currently loaded and conflicts with the non-free nvidia kernel module.
Power off the computer, power off any attached hubs or docks, then turn power back on


# Debugging
su
lshw -c video         # List hardare
lsmod | grep nvidia   # List nvidia drivers
lspci | grep VGA      # List all video hardware
lspci -vnnn | perl -lne 'print if /^\d+\:.+(\[\S+\:\S+\])/' | grep VGA  # Show nvidia on PCIe
lspci | grep ' VGA ' | cut -d" " -f 1 | xargs -i lspci -v -s {}         # Show nvidia on PCIe details
lsof /dev/nvidia*     # Is nvidia active?
inxi -G               # Graphics system information
xrandr                # List vido capabilities for all attached displays
xrandr --verbose      # Do it verbosely
xrandr --listproviders  # List GPUs
xrandr --listmonitors   # List Displays that are active
nvidia-xconfig --query-gpu-info   # Get info that nvidia knows about

# NVIDIA System Management Interface
# Query the GPU device state and privileges
# May not be fully capable with latest devices
sudo nvidia-smi


