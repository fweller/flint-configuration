# flatpak-commandline
## Be able to run flatpaks from the Linux command line

## To find installed flatpaks - using directory structure
find /var/lib/flatpak/app/* -maxdepth 0 -type d

## Example results
/var/lib/flatpak/app/com.axosoft.GitKraken
/var/lib/flatpak/app/com.spotify.Client
/var/lib/flatpak/app/com.visualstudio.code
/var/lib/flatpak/app/org.libreoffice.LibreOffice
/var/lib/flatpak/app/org.mozilla.firefox
/var/lib/flatpak/app/org.videolan.VLC

## To find installed flatpaks - using flatpak
flatpak list --columns=name,application

### Example results
Name                                                      Application ID
GitKraken                                                 com.axosoft.GitKraken
Spotify                                                   com.spotify.Client
Visual Studio Code                                        com.visualstudio.code
LibreOffice                                               org.libreoffice.LibreOffice
Firefox                                                   org.mozilla.firefox
VLC                                                       org.videolan.VLC

## To create a launcher application
Place a shell script in ${USER}/Bin

```
#!/bin/sh
flatpak run org.mozilla.firefox
```

chmod +x FILENAME
