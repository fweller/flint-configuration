# APT and DPKG

# Fix broken packages
https://linuxhint.com/apt_get_fix_missing_broken_packages/
apt update --fix-missing

# Fix aborted installation
dpkg --configure -a

# List of all installed packages
dpkg --get-selections

# List of all installed packages, but print only the package name
dpkg --get-selections | sed -n 's/[[:space:]]install$//p'

# List all installed packages with versions and details
dpkg -l

# To install or upgrade a package from a file
dpkg -i <FileName>

# To remove a package, leaving the configuration files
dpkg -r <PackageName>

# To purge a package, deleting the configuration files
dpkg -P <PackageName>

# To view all properties of a package
dpkg -s <PackageName>

# List package contents
dpkg -L <PackageName>

# List file contents
dpkg -c <FileName>

# Display which package provides this file
dpkg -S <FileName>

# List manually installed packages
zgrep -hE '^(Start-Date:|Commandline:)' $(ls -tr /var/log/apt/history.log*.gz ) | egrep -v 'aptdaemon|upgrade' | egrep -B1 '^Commandline:'

