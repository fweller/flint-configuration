# Network

# show ip addresses for all devices (a = address)
ip a

# show routing table (ro = route)
ip ro

# show interface statistics (st = stats)
ip st

# show network status (shows everything)
nmcli

# show devices
nmcli device status

# show details for a device
nmcli device show <Device>

# scan wifi
nmcli d wifi list

# show network routing table
netstat -nr

# query internet dns (domain name servers)
nslookup google.com

# print route packets trace to host
traceroute google.com

# show dns server in use
cat /etc/resolv.conf

# DNS lookup
dig google.com

# Reverse DNS lookup
dig -x 8.8.8.8

# dnsdiag package provides 3 tools: 
- dnsping
- dnstraceroute
- dnseval

# check for currently used DNS server
systemd-resolve --status | grep Current

# bulk ping a <Filename> list of ip addresses for DNS servers to look up <hostname>
dnseval -f <Filename> <hostname>
dnseval -f ${HOME}/Tools/reference/dns/dnslist-america.txt github.com

# dhcp release
sudo dhclient -r eth0 #release dhcp address, use -v for verbose

# dhcp obtain
sudo dhclient eth0 #obtain a new dhcp address, use -v for verbose

