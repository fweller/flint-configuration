# ----------------------------------------
# NAME        : script.txt
# DESCRIPTION : Text to prompt a GPT to write or review a script
# USAGE       : Share the below text with the GPT
# ----------------------------------------

Please review the script that I will post after these instructions:

1. The scripts must operate flawlessly in Linux, macOS, and WSL2 (Windows Subsystem for Linux 2). Additionally, compatibility with Cygwin, BSD, and UNIX is desirable but not required. Please make suggestions to maximize compatibility across these environments.
2. Incorporate corrections suggested by the shellcheck tool to enhance compatibility with the scripting language.
3. Focus on improving the scripts' resiliency, including error handling, exit status checks, input validation, quotation usage, logging, safety of temporary files, graceful exits, retry logic, resource limitation checking, and modular design.
4. Enhance both commenting and code readability so that a high-school student could understand the code and the comments.
5. Whenever possible, re-write the scripts in the POSIX shell instead of Bash to ensure maximum portability. But first you must confim that every section of the script can be made shell compatible, that there are no issues with what shell lacks compared to bash.
6. The scripts should include a header formatted as follows:
```
# ----------------------------------------
# NAME        : (populate this with the script name)
# DESCRIPTION : (provide a useful description for this script)
# USAGE       : (provide usage details here)
# ----------------------------------------
```
7. After the header, include in comments the prompt that led to the script being written.
8. If logging is necessary, the script will output to a logfile located in $HOME/Logs, named after the script, with a .log extension.
9. The script should include logic to create the $HOME/Logs directory if it doesn't already exist. If the script fails to create the $HOME/Logs directory, it should quit immediately.
10. The script should perform pre-checks for safety, such as ensuring that any file or directory it operates on exists before proceeding.
11. The script should check for appropriate permissions before attempting to access or modify files or directories, and handle permission errors gracefully by logging the error both on the screen and in the logfile.
12. User interaction will occur through the command line. Errors and any useful debug information should be echoed to the command line and also logged in the logfile.
13. If the script needs to create temporary files, they should be placed in $HOME/Tmp. The script should include logic to create the $HOME/Tmp directory if it doesn't already exist. If the script fails to create the $HOME/Tmp directory, it should quit immediately.
14. The preferred format for log entries is YYYYMMDD-HHMM:SS Message. The debug logs should include commands that the script will run, the success/fail status of those commands, and any other debug information deemed important for troubleshooting.
15. The script should attempt to clean up partial or temporary files that it created before quitting successfully. However, if the script quits due to failure, it should keep the partial or temporary files or directories to aid in debugging. If the script must quit due to failure, it should provide as much useful information to the user and into the logfile as possible.

