#!/bin/bash

# VM Name and location
VMname="Debian12"
VMdir="$HOME/VMs/$VMname"
VM="$HOME/VMs/$VMname/$VMname.qcow2"

# Installer
Installer="$HOME/Downloads/debian-12.6.0-amd64-netinst.iso"

# Display resolution (for guest OS)
DisplayRes="1920x1080"  # Set your preferred resolution for the VM

# Create the directory (if not already created)
mkdir -p "$VMdir"

# Step 2: Create the QCOW2 disk image
qemu-img create -f qcow2 "$VM" 32G

# Step 3: Run the QEMU command in windowed mode with specified resolution
qemu-system-x86_64 \
  -enable-kvm \
  -m 8G \
  -cpu host \
  -smp 2 \
  -drive file="$VM",format=qcow2,if=virtio \
  -cdrom "$Installer" \
  -boot order=d \
  -bios /usr/share/OVMF/OVMF_CODE.fd \
  -device virtio-net,netdev=net0 \
  -netdev user,id=net0 \
  -vga virtio \
  -display gtk,gl=on \
  -device virtio-gpu-pci \
  -monitor stdio \
  -device virtio-vga,xres=$(echo $DisplayRes | cut -d'x' -f1),yres=$(echo $DisplayRes | cut -d'x' -f2)

