#!/bin/bash

# Variables
Device="/mnt/SkyPenguin/"
Values="/dev/zero" # or /dev/urandom
Dashes="------------------------------------"

Output() {
  Message=$1
  NowTime="$(date -u +%s)"
  Elapsed="$((NowTime - StartTime))"
  ElapsedFormatted=$(printf "%06d" $Elapsed)
  echo "TIME: $ElapsedFormatted - $Message"
}

# Create a seconds counter
StartTime="$(date -u +%s)"

echo $Dashes
echo "nas-dd-test.sh"
date

echo $Dashes
Output "START Write performance short"
Filename="dd1gib.bin"; File="${Device}${Filename}"
Command="dd if=$Values of=$File bs=1GiB count=1 status=progress"
echo "$Command"
eval "$Command"
Output "STOP  Write performance short"

echo $Dashes
Output "START Write performance long"
Filename="dd10gib.bin"; File="${Device}${Filename}"
Command="dd if=$Values of=$File bs=1GiB count=10 status=progress"
echo "$Command"
eval "$Command"
Output "STOP  Write performance long"

echo $Dashes
Output "START Read performance short"
Filename="dd1gib.bin"; File="${Device}${Filename}"
Command="dd if=$File of=/dev/null status=progress"
echo "$Command"
eval "$Command"
Output "END  Read performance short"

echo $Dashes
Output "START Read performance long"
Filename="dd10gib.bin"; File="${Device}${Filename}"
Command="dd if=$File of=/dev/null status=progress"
echo "$Command"
eval "$Command"
Output "END  Read performance long"

echo $Dashes
Output "START Read performance short and potentially cached"
Filename="dd1gib.bin"; File="${Device}${Filename}"
Command="dd if=$File of=/dev/null status=progress"
for i in {1..10}
  do
    Output "START - Iteration $1"
    echo "$Command"
    eval "$Command"
    Output "STOP  - Iteration $1"
done
Output "STOP   Read performance short and potentially cached"

echo $Dashes
