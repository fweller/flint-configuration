#!/bin/bash
# Taken from Leonroy/sysbench-fileio.sh

# Usage
# PWD should be the NAS volume
# /home/flint/Tools/reference/benchmarks/storage/sysbench-fileio.sh


# Set to 2x RAM
FILE_TOTAL_SIZE="16G"

#Set to long enough to complete several runs
MAX_TIME="240"

#For random IO set to 4K otherwise set to 1M for sequential
FILE_BLOCK_SIZE="4K"

logdate=$(date +%F)

Output() {
  Message=$1
  NowTime="$(date -u +%s)"
  Elapsed="$((NowTime - StartTime))"
  ElapsedFormatted=$(printf "%06d" $Elapsed)
  echo "TIME: $ElapsedFormatted - $Message"
}

# Create a seconds counter
StartTime="$(date -u +%s)"

Output "START Preparing Test"
sysbench --test=fileio --file-total-size=$FILE_TOTAL_SIZE prepare
Output "STOP Preparing Test"

#echo "Running tests"
for run in 1 2 3; do
	for each in 1 4 8 16 32 64; do
		#echo "############## Running Test - Read/Write - Thread Number:" $each "- Run:" $run "##############"
		#sysbench --test=fileio --file-total-size=$FILE_TOTAL_SIZE --file-test-mode=rndrw --max-time=$MAX_TIME --max-requests=0 --file-block-size=$FILE_BLOCK_SIZE --num-threads=$each --file-fsync-all run > log-$logdate-readwrite-${each}T-${run}R.log

		#echo "############## Running Test - Write - Thread Number:" $each "- Run:" $run "##############"
    Output "START Write Test for Thread $each and Run $run"
		sysbench --test=fileio --file-total-size=$FILE_TOTAL_SIZE --file-test-mode=rndwr --max-time=$MAX_TIME --max-requests=0 --file-block-size=$FILE_BLOCK_SIZE --num-threads=$each --file-fsync-all run > log-$logdate-write-${each}T-${run}R.log
    Output "STOP  Write Test for Thread $each and Run $run"
	
		#echo "############## Running Test - Read - Thread Number:" $each "- Run:" $run "##############"
    Output "START Read Test for Thread $each and Run $run"
		sysbench --test=fileio --file-total-size=$FILE_TOTAL_SIZE --file-test-mode=rndrd --max-time=$MAX_TIME --max-requests=0 --file-block-size=$FILE_BLOCK_SIZE --num-threads=$each run > log-$logdate-read-${each}T-${run}R.log
    Output "STOP  Read Test for Thread $each and Run $run"
	done
done

#echo "Cleaning up"
Output "START Cleaning up"
sysbench --test=fileio --file-total-size=$FILE_TOTAL_SIZE cleanup
Output "STOP  Cleaning up"

