# PiBenchmarks

## Information

- [Jeff Geerling - Using PiBenchmarks.com for SBC disk performance testing](https://www.jeffgeerling.com/blog/2023/using-pibenchmarkscom-sbc-disk-performance-testing)
- [James Chambers - Raspberry Pi Storage Benchmarks + Benchmarking Script](https://jamesachambers.com/raspberry-pi-storage-benchmarks-2019-benchmarking-script/)
- [Storage.sh](https://raw.githubusercontent.com/TheRemote/PiBenchmarks/master/Storage.sh)
- [PiBenchmarks.com](https://pibenchmarks.com/) *Results*

## Install required packages

```bash
sudo apt install iozone3 lshw pciutils usbutils lsscsi bc \
 curl hwinfo hdparm nvme-cli dmidecode smartmontools fio \
 sdparm xxd libxml-dumper-perl --no-install-recommends -y
```

## Test the boot storage and upload results directly to PiBenchmarks.com

```bash
sudo ./Storage.sh
```

## Test a specific device and upload results directly to PiBenchmarks.com

```bash
sudo ./Storage.sh /path/to/mount/point
```


## Test a NAS and upload results directly to PiBenchmarks.com

I commented out line 89 `fstrim -av`

```bash
sudo ./StorageNAS.sh /path/to/mount/point
```



