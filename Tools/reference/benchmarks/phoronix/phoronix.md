# Phoronix-test-suite

## Information

- [phoronix-test-suite](https://www.phoronix-test-suite.com/)
- [openbenchmarking](https://openbenchmarking.org/)

Information

- The Phoronix Test Suite is the most comprehensive testing and benchmarking platform available for POSIX operating systems.
- [OpenBenchmarking.org](http://openbenchmarking.org/) also allows for conducting side-by-side result comparisons, a central location for storing and sharing test results, and collaborating over test data.
- [Phoronix Test Suite](https://www.phoronix-test-suite.com/)
- [OpenBenchmarking](https://openbenchmarking.org/)
- [GitHub](https://github.com/phoronix-test-suite/phoronix-test-suite)
- [Documentation](https://github.com/phoronix-test-suite/phoronix-test-suite/blob/master/documentation/phoronix-test-suite.md)
- [Man Page](https://www.mankier.com/1/phoronix-test-suite)
- [Tests](https://openbenchmarking.org/tests/pts)
- [Test Suites](https://openbenchmarking.org/suites/pts)


