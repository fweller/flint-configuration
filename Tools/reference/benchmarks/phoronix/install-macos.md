# Install on MacOS

There are two options, installing from Homebrew or from the downloaded script.
Homebrew has some strange directory issues.
The script can not be installed properly, but seems to run properly from where it was extracted.
Install Apple Xcode first

## Homebrew

brew install phoronix-test-suite
cd "$(brew --prefix phoronix-test-suite)/share/phoronix-test-suite"

## Download the universal script

cd ${HOME}/Builds
wget https://phoronix-test-suite.com/releases/phoronix-test-suite-10.8.4.tar.gz
tar xvf phoronix-test-suite-10.8.4.tar.gz
cd phoronix-test-suite
./phoronix-test-suite

