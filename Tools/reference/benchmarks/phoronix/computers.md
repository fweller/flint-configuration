# Computers that I have tested

Test Result Name Format: Title-YYYY-MM-DD

## These are all laptops or desktops

- Tesla
  - MacBook Pro M1 Pro
- [Turing](https://openbenchmarking.org/result/2301090-PHIL-TURING241)
  - Gigabyte X570 Aorus Master Motherboard, AMD Ryzen 9 5950X CPU, AMD Radeon RX 6900 XT GPU
- [ZBook](https://openbenchmarking.org/result/2301093-PHIL-ZBOOK2069)
  - HP ZBOOK 15 G6 Laptop with Intel Core i7-9850H Coffee Lake CPU
- [Archimedes](https://openbenchmarking.org/result/2301115-PHIL-ARCHIME39)
  - Apple MacBook Pro (Retina, 15-inch, Mid 2014), Intel Core i7-4870HQ, Intel Iris Pro + NVIDIA GeForce GT 750M
- [Tesla](https://openbenchmarking.org/result/2301091-PHIL-TESLA2009)
  - Gigabyte P55A-UD3 Motherboard, Intel Core i5-650 CPU, Gigabyte NVIDIA GeForce GTX 660 Ti GPU
- [Goat](https://openbenchmarking.org/result/2301092-PHIL-GOAT20273)
  - Dell 0U695R Laptop, Intel Core 2 Duo P8700, NVIDIA Quadro NVS 160M 255MB
- [DaVinci](https://openbenchmarking.org/result/2301090-PHIL-DAVINCI04)
  - Dell 0YRRCV XPS13 Laptop, Intel Core i5-10210U, Intel UHD GPU
- [Hedy](https://openbenchmarking.org/result/2301090-PHIL-HEDY20229)
  - Lenono 20HRCTO1WW ThinkPad X1 Carbon Laptop, Intel Core i7-7600U, Intel HD 620
- [Ada](https://openbenchmarking.org/result/2301128-PHIL-ADA202364)
  - Intel NUC7i5DNB, Intel Core i5-7300U, Intel HD 620

## These are all ARM or RISC-V SBC or EVK

- [Rock5](https://openbenchmarking.org/result/2301096-PHIL-ROCK52013)
  - Radxa ROCK 5B SBC with CPU Rockchip RK3588 SoC
- [RPi4](https://openbenchmarking.org/result/2301094-PHIL-RPI420270)
  - Raspberry Pi 4 Model B with Broadcom BCM2711 SoC
- [MX8MPlus](https://openbenchmarking.org/result/2301097-PHIL-MX8MPLU40)
  - NXP 8MPLUSLPD4-EVK i.MX8MPlus EVK board with NXP i.MX 8M Plus SoC
