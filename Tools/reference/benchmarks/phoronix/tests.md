# Tests that I consider interesting

## Standard Test Suite

- [RAMspeed](https://openbenchmarking.org/test/pts/ramspeed)
  - RAM, RAM performance (Linux)
- [Stream](https://openbenchmarking.org/test/pts/stream)
  - RAM, Memory bandwidth (Linux, MacOS, BSD)
- [MBW](https://openbenchmarking.org/test/pts/mbw&eval=6c5906ddce2c6a27e257f9c2355990d79c2e6654)
  - RAM, Memory bandwidth (Linux, MacOS, BSD)
- [CacheBench](https://openbenchmarking.org/test/pts/cachebench)
  - CPU, Memory and cache bandwidth (Linux, BSD, Windows)
- [encode-mp3](https://openbenchmarking.org/test/pts/encode-mp3)
  - CPU, Single-thread performance (Linux, MacOS, BSD, Windows)
- [C-Ray](https://openbenchmarking.org/test/pts/c-ray)
  - CPU, Multi-thread floating-point performance  (Linux, MacOS, BSD, Windows)
- [Stockfish](https://openbenchmarking.org/test/pts/stockfish)
  - CPU, Multi-thread performance (Linux, MacOS, BSD, Windows)
- [Core-latency](https://openbenchmarking.org/test/pts/core-latency)
  - CPU, Latency between all core combinations (Linux)
- [CoreMark](https://openbenchmarking.org/test/pts/coremark)
  - CPU, EEMBC CoreMark Multi-thread processor (Linux, MacOS, BSD, Windows)
- [Crypto++](https://openbenchmarking.org/test/pts/cryptopp)
  - CPU, Cryptographic algorithms (Linux, MacOS, BSD)
- [compress-7zip](https://openbenchmarking.org/test/pts/compress-7zip)
  - CPU, 7zip compression (Linux, MacOS, BSD, Windows)
- [Stress-NG](https://openbenchmarking.org/test/pts/stress-ng)
  - System, stress test (Linux, MacOS, BSD)
- [OSBench](https://openbenchmarking.org/test/pts/osbench)
  - System, OS system primitives (Linux, MacOS, BSD, Windows)
- [BYTE](https://openbenchmarking.org/test/pts/byte)
  - System, BYTE UNIX Benchmark (Linux, MacOS, BSD)
- [Cl-mem](https://openbenchmarking.org/test/pts/cl-mem)
  - GPU, OpenCL memory benchmark (Linux)
- [CLpeak](https://openbenchmarking.org/test/pts/clpeak)
  - GPU, OpenCL peak capabilities (Linux)
- [GLmark2](https://openbenchmarking.org/test/pts/glmark2-1.1.0)
  - GPU, OpenGL X11 2D performance (Linux) REMOVE?
- [JXRenderMark](https://openbenchmarking.org/test/pts/jxrendermark)
  - GPU, XRender X11 (Linux) rendered to backbuffer so wont see on screen

# Removed

- [GtkPerf](https://openbenchmarking.org/test/pts/gtkperf)
  - GPU, GTK2 frame rate (Linux) REMOVE? deprecated
- [QGears2](https://openbenchmarking.org/test/pts/qgears2)
  - GPU, QT4 rendering on OpenGL and XRender (Linux) REMOVE? Requires qmake - no MX9+

# Extra

- [x264](https://openbenchmarking.org/test/pts/x264)
  - CPU, x264 encoder (Linux, MacOS, BSD)
- [x265](https://openbenchmarking.org/test/pts/x265)
  - CPU, x265 encoder (Linux, MacOS, BSD)Requires substantial filesystem space
- [iozone](https://openbenchmarking.org/test/pts/iozone)
  - Disk, takes very long time to run (Linux, Windows) DEPRECIATED

# PC Test Suite to run in addition to standard test suite

- [scimark2](https://openbenchmarking.org/test/pts/scimark2)
  - CPU, Scientific and numerical computing  (Linux, MacOS, BSD, Windows)
- [GraphicsMagick](https://openbenchmarking.org/test/pts/graphics-magick)
  - CPU, OpenMP image (Linux, MacOS, BSD, Windows)
- [JohnTheRipper](https://openbenchmarking.org/test/pts/john-the-ripper)
  - CPU, processor password cracker (Linux, MacOS, BSD, Windows)
- [OpenSSL](https://openbenchmarking.org/test/pts/openssl)
  - CPU, RSA 4096-bit performance (Linux, MacOS, BSD, Windows)
- [build-php](https://openbenchmarking.org/test/pts/build-php)
  - CPU, compile PHP7 (Linux, MacOS, BSD)
- [build-linux-kernel](https://openbenchmarking.org/test/pts/build-linux-kernel)
  - CPU, compile linux kernel (Linux) Requires substantial filesystem space
- [GMIC](https://openbenchmarking.org/test/system/gmic)
  - System, image processing (Linux, BSD)
- [JuliaGPU](https://openbenchmarking.org/test/pts/juliagpu)
  - System, OpenCL and CPU AVX benchmark (Linux, MacOS)
- [mandelgpu](https://openbenchmarking.org/test/pts/mandelgpu)
  - GPU, OpenCL (Linux, MacOS, BSD)  
- [mandelbulbgpu](https://openbenchmarking.org/test/pts/mandelbulbgpu)
  - GPU, OpenCL (Linux, MacOS, BSD)
- [APITest](https://openbenchmarking.org/test/pts/apitest)
  - GPU, OpenGL 4 (Linux, MacOS) DEPRECIATED
- [GpuTest](https://openbenchmarking.org/test/pts/gputest)
  - GPU, OpenGL rendering workloads (Linux, Windows) (x86 only)
- [luxcorerender](https://openbenchmarking.org/test/pts/luxcorerender)
  - GPU, graphics rendering using CPU OpenCL Nvidia CUDA and Optix (Linux, MacOS, Windows) (x86 only)

# Special and useful for extra testing

- [sockperf](https://openbenchmarking.org/test/pts/sockperf)
  - Network, network socket API performance (Linux, MacOS, BSD)
- [iperf](https://openbenchmarking.org/test/pts/iperf)
  - Network, network bandwidth throughput, requires iperf server (Linux, MacOS, BSD, Windows)
- [netperf](https://openbenchmarking.org/test/pts/netperf)
  - Network, network bandwidth throughput, requires netperf server (Linux, MacOS, BSD, Windows)
- [fio](https://openbenchmarking.org/test/pts/fio)
  - Disk, disk benchmark using kernel AIO access library (Linux, MacOS, Windows)
- [dbench](https://openbenchmarking.org/test/pts/dbench)
  - Disk, filesystem calls, alternative to netbench (Linux)
- [OpenArena](https://openbenchmarking.org/test/pts/openarena)
  - GPU, 3D GPU gaming, 60 seconds per test (Linux, MacOS, Windows) (x86 only)
- [Xonotic](https://openbenchmarking.org/test/pts/xonotic)
  - GPU, 3D GPU gaming, 1 hour (Linux, MacOS, Windows) (x86 only)
- [unigine-heaven](https://openbenchmarking.org/test/pts/unigine-heaven)
  - GPU, OpenGL 3D GPU gaming, 15 minutes per test (Linux, Windows) (x86 only)
- [unigine-valley](https://openbenchmarking.org/test/pts/unigine-valley)
  - GPU, OpenGL 3 3D GPU gaming, 10 minutes (Linux, Windows) (x86 only)
- [mixbench](https://openbenchmarking.org/test/pts/mixbench)
  - GPU, OpenCL GPU compute, requires nvidia CUDA (Linux)
- [J2DBench](https://openbenchmarking.org/test/pts/j2dbench)
  - GPU, OpenGL Java (Linux, MacOS, BSD, Windows)
- [JGfxBat](https://openbenchmarking.org/test/pts/jgfxbat)
  - GPU, OpenGL Java basic acceptance (Linux, MacOS, BSD, Windows)
- [AtscEnc](https://openbenchmarking.org/test/pts/astcenc)
  - CPU, ATSC decompression compression using OpenGL OpenGL ES Vulkan (Linux, MacOS, Windows)
- [Himeno](https://openbenchmarking.org/test/pts/himeno)
  - CPU, linear solver pressure Poisson (Linux, MacOS, BSD)
- [caffe](https://openbenchmarking.org/test/pts/caffe)
  - System, Caffe deep learning framework on CPU (Linux)
- [TNN](https://openbenchmarking.org/test/pts/tnn-1.0.0)
  - System, neural network (Linux)

# Extra that I considered at one time

- [cryptopp](https://openbenchmarking.org/test/pts/cryptopp)
  - CPU, cryptography (Linux, MacOS, BSD)
- [QmlBench](https://openbenchmarking.org/test/pts/qmlbench)
  - System, QT5 QML framework (Linux, MacOS, BSD)
- [GFXBench](https://openbenchmarking.org/test/pts/gfxbench)
  - GPU, 2D OpenGL performance, requires special rights (Linux)
- [gtkperf](https://openbenchmarking.org/test/pts/gtkperf)
  - GPU, 2D GPU performance (Linux) DEPRECIATED but will run on ARM
