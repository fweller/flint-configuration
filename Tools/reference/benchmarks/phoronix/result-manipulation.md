# Test result manipulation commands performed in order

## List names of all result files

phoronix-test-suite    show-result

## Rename the identifier in each result file to be the same

phoronix-test-suite    rename-identifier-in-result-file

## Only if needed, remove individual results from result file

phoronix-test-suite    remove-result

## Merge all result files into one file

phoronix-test-suite    merge-results

## Rename the merged result file to something useful

phoronix-test-suite    rename-result-file

## Sort the result file

phoronix-test-suite    auto-sort-result-file

## Remove the empty identifiers in the result file

### 1 rename each identifier: one, two, three, etc....

phoronix-test-suite    rename-identifier-in-result-file           [Test Result]

### 2 Show the result, determine which identifiers can be deleted

phoronix-test-suite    show-result

### 3 Delete empty identifiers

phoronix-test-suite    remove-run-from-result-file                [Test Result]

### 4 Rename the identifier back to the original Title name

phoronix-test-suite    rename-identifier-in-result-file           [Test Result]

### 5 Check to make sure it is correct

phoronix-test-suite    show-result

## Change the Title and Description of the computer in the result file

phoronix-test-suite    edit-result-file

## Confirm that the result is all correct

phoronix-test-suite    show-result

## Upload to the OpenBenchmarking server

phoronix-test-suite    upload-result

## Store all the results in one archive that I can access later

Location: ``/SkyData/Computer/Benchmarks/``

tar cf - ~/.phoronix-test-suite/test-results/ | 7za a -si ~/Downloads/Computername-Benchmarks-Phoronix-YYYYMMDD.tar.7z
