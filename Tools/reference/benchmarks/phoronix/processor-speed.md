# Setting the CPU speed governor in Linux

- [cpufreq is being replaced by cpupower](https://manpages.debian.org/bookworm/cpufrequtils/cpufreq-info.1.en.html)
- [https://wiki.archlinux.org/title/CPU_frequency_scaling](https://wiki.archlinux.org/title/CPU_frequency_scaling)

get current state

```bash
sudo cpupower frequency-info
```

set to performance

```bash
sudo cpupower frequency-set -g performance
```

return to original

```bash
cpupower frequency-set -g governor
cpupower frequency-set -g powersave
```
