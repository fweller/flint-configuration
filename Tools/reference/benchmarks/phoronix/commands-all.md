# List of the most useful commands

SYSTEM

phoronix-test-suite    system-info
phoronix-test-suite    system-properties  
phoronix-test-suite    system-sensors

TESTING

phoronix-test-suite    benchmark             [Test | Suite | OpenBenchmarking ID | Test Result]  ...
phoronix-test-suite    estimate-install-time [Test | Suite | OpenBenchmarking ID | Test Result]
phoronix-test-suite    estimate-run-time     [Test | Suite | OpenBenchmarking ID | Test Result]
phoronix-test-suite    finish-run            [Test Result]
phoronix-test-suite    run                   [Test | Suite | OpenBenchmarking ID | Test Result]  ...

BATCH TESTING

phoronix-test-suite    batch-benchmark   [Test | Suite | OpenBenchmarking ID | Test Result]  ...
phoronix-test-suite    batch-install     [Test | Suite | OpenBenchmarking ID | Test Result]  ...
phoronix-test-suite    batch-run         [Test | Suite | OpenBenchmarking ID | Test Result]  ...
phoronix-test-suite    batch-setup

OPENBENCHMARKING.ORG

phoronix-test-suite    openbenchmarking-login
phoronix-test-suite    openbenchmarking-refresh
phoronix-test-suite    openbenchmarking-changes
phoronix-test-suite    openbenchmarking-refresh
phoronix-test-suite    openbenchmarking-repositories
phoronix-test-suite    openbenchmarking-uploads
phoronix-test-suite    upload-result                 [Test Result]

INFORMATION

phoronix-test-suite    info  [Test | Suite | OpenBenchmarking ID | Test Result]  
phoronix-test-suite    list-available-suites
phoronix-test-suite    list-available-tests
phoronix-test-suite    list-installed-suites
phoronix-test-suite    list-installed-tests
phoronix-test-suite    list-missing-dependencies
phoronix-test-suite    list-not-installed-tests
phoronix-test-suite    list-possible-dependencies
phoronix-test-suite    list-saved-results
phoronix-test-suite    list-test-status
phoronix-test-suite    list-test-usage
phoronix-test-suite    search  

RESULT MANAGEMENT

phoronix-test-suite    auto-sort-result-file                      [Test Result]
phoronix-test-suite    compare-results-to-baseline                [Test Result] [Test Result]
phoronix-test-suite    compare-results-two-way                    [Test Result]
phoronix-test-suite    edit-result-file                           [Test Result]
phoronix-test-suite    extract-from-result-file                   [Test Result]
phoronix-test-suite    keep-results-in-result-file                [Test Result]
phoronix-test-suite    merge-results                              [Test Result]  ...
phoronix-test-suite    remove-incomplete-results-from-result-file [Test Result]
phoronix-test-suite    remove-result                              [Test Result]
phoronix-test-suite    remove-result-from-result-file             [Test Result]
phoronix-test-suite    remove-results-from-result-file            [Test Result]
phoronix-test-suite    remove-run-from-result-file                [Test Result]
phoronix-test-suite    rename-identifier-in-result-file           [Test Result]
phoronix-test-suite    rename-result-file                         [Test Result]
phoronix-test-suite    reorder-result-file                        [Test Result]
phoronix-test-suite    show-result                                [Test Result]

RESULT ANALYSIS

phoronix-test-suite    analyze-run-times                [Test Result]
phoronix-test-suite    workload-topology                [Test Result]
phoronix-test-suite    upload-result                    [Test Result]
phoronix-test-suite    upload-test-profile
phoronix-test-suite    upload-test-suite                [Suite]

RESULT VIEWER

phoronix-test-suite    start-result-viewer       ***Very Useful to view with browser***

RESULT EXPORT

phoronix-test-suite    result-file-raw-to-csv           [Test Result]
phoronix-test-suite    result-file-to-csv               [Test Result]
phoronix-test-suite    result-file-to-html              [Test Result]
phoronix-test-suite    result-file-to-json              [Test Result]  
phoronix-test-suite    result-file-to-suite             [Test Result]
phoronix-test-suite    result-file-to-pdf               [Test Result]
phoronix-test-suite    result-file-to-text              [Test Result]

OTHER

phoronix-test-suite       commands
phoronix-test-suite       help
phoronix-test-suite       version
