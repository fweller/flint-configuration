# Tests to run in benchmark mode

Last updated 20220622

## Standard Test Suite

pts/ramspeed pts/stream pts/mbw pts/cachebench pts/encode-mp3 pts/c-ray pts/stockfish pts/core-latency pts/coremark pts/cryptopp pts/compress-7zip pts/stress-ng pts/osbench pts/byte pts/cl-mem pts/clpeak pts/glmark2 pts/gtkperf pts/jxrendermark

### Optional

pts/x264 pts/x265

## PC Test Suite

pts/scimark2 pts/graphics-magick pts/john-the-ripper pts/openssl pts/build-php pts/build-linux-kernel system/gmic pts/juliagpu pts/gputest pts/mandelgpu pts/mandelbulbgpu pts/apitest pts/luxcorerender

## Combined Standard and PC Test Suites

pts/ramspeed pts/stream pts/mbw pts/cachebench pts/encode-mp3 pts/c-ray pts/stockfish pts/core-latency pts/coremark pts/cryptopp pts/compress-7zip pts/x264 pts/x265 pts/stress-ng pts/osbench pts/byte pts/cl-mem pts/clpeak pts/glmark2 pts/qgears2 pts/gtkperf pts/jxrendermark  pts/scimark2 pts/graphics-magick pts/john-the-ripper pts/openssl pts/build-php pts/build-linux-kernel system/gmic pts/juliagpu pts/gputest pts/mandelgpu pts/mandelbulbgpu pts/apitest pts/luxcorerender

### Optional

pts/iozone

