# History of my test suites

## 20210628

### combined

pts/ramspeed pts/stream pts/mbw pts/cachebench pts/encode-mp3 pts/c-ray pts/stockfish pts/core-latency pts/coremark pts/cryptopp pts/compress-7zip pts/x264 pts/x265 pts/stress-ng pts/osbench pts/byte pts/cl-mem pts/clpeak pts/glmark2 pts/qgears2 pts/gtkperf pts/jxrendermark  pts/scimark2 pts/graphics-magick pts/john-the-ripper pts/openssl pts/build-php pts/build-linux-kernel system/gmic pts/juliagpu pts/gputest pts/mandelgpu pts/mandelbulbgpu pts/apitest pts/luxcorerender

### Standard Test Suite

pts/ramspeed pts/stream pts/mbw pts/cachebench pts/encode-mp3 pts/c-ray pts/stockfish pts/core-latency pts/coremark pts/cryptopp pts/compress-7zip pts/stress-ng pts/osbench pts/byte pts/cl-mem pts/clpeak pts/glmark2 pts/qgears2 pts/gtkperf pts/jxrendermark

### Optional

pts/x264 pts/x265

### PC Test Suite

pts/scimark2 pts/graphics-magick pts/john-the-ripper pts/openssl pts/build-php pts/build-linux-kernel system/gmic pts/juliagpu pts/gputest pts/mandelgpu pts/mandelbulbgpu pts/apitest pts/luxcorerender

### My old computer comparison test suite

pts/compress-7zip pts/apitest pts/c-ray pts/cachebench pts/core-latency pts/coremark pts/cryptopp system/gmic pts/gputest pts/graphics-magick pts/iozone pts/john-the-ripper pts/juliagpu pts/luxcorerender pts/mandelbulbgpu pts/mandelgpu pts/mbw pts/openssl pts/osbench pts/ramspeed pts/scimark2 pts/stream pts/stress-ng pts/build-linux-kernel pts/build-php pts/x264 pts/x265

### Long tests that should be run seperately

pts/iozone

### Fun tests

pts/openarena

## 20221128 Hedy mint 21 kernel 5.15

### install dependencies

sudo apt install php php-zip php-cli php-xml php-gd p7zip-full curl sqlite bzip2 sqlite opencl-headers ocl-icd-libopencl1 clinfo ocl-icd-opencl-dev gmic cpufrequtils

### install phoronix

wget https://phoronix-test-suite.com/releases/repo/pts.debian/files/phoronix-test-suite_10.8.4_all.deb
sudo dpkg -i phoronix-test-suite_10.8.4_all.deb
phoronix-test-suite system-sensors
phoronix-test-suite openbenchmarking-login
MONITOR=all
phoronix-test-suite batch-setup
phoronix-test-suite install
phoronix-test-suite install-dependencies
MONITOR=all phoronix-test-suite batch-benchmark
MONITOR=all phoronix-test-suite run

### Test 1: Standard Test Suite

pts/ramspeed pts/stream pts/mbw pts/cachebench pts/encode-mp3 pts/c-ray pts/stockfish pts/core-latency pts/coremark pts/cryptopp pts/compress-7zip pts/stress-ng pts/osbench pts/byte pts/cl-mem pts/clpeak pts/glmark2  pts/gtkperf pts/jxrendermark

### Test 2: My old computer comparison test suite

pts/compress-7zip pts/apitest pts/gputest pts/c-ray pts/cachebench pts/core-latency pts/coremark pts/cryptopp system/gmic pts/gputest pts/graphics-magick pts/juliagpu pts/luxcorerender pts/mandelbulbgpu pts/mandelgpu pts/mbw pts/openssl pts/osbench pts/ramspeed pts/scimark2 pts/stream pts/stress-ng pts/build-linux-kernel pts/build-php  

### removed

pts/iozone pts/john-the-ripper pts/x264 pts/x265 pts/openarena

### Test 3: new stuff

phoronix-test-suite install vulkan-compute system/gegl  graphics-magick betsy

[https://openbenchmarking.org/result/2211298-PHIL-202211204](https://openbenchmarking.org/result/2211298-PHIL-202211204)

[https://openbenchmarking.org/result/2212027-PHIL-202211286](https://openbenchmarking.org/result/2212027-PHIL-202211286)

## 20220622 turing ubuntu 22.04 lts

phoronix-test-suite openbenchmarking-login
phoronix-test-suite batch-setup
phoronix-test-suite install
phoronix-test-suite install-dependencies
phoronix-test-suite batch-benchmark
phoronix-test-suite run
Standard Test Suite

pts/ramspeed  pts/mbw pts/cachebench pts/encode-mp3 pts/c-ray pts/stockfish pts/core-latency pts/coremark pts/cryptopp pts/compress-7zip   pts/osbench pts/byte pts/cl-mem pts/clpeak pts/glmark2 pts/gtkperf pts/jxrendermark

error 1
pts/stress-ng
bug [https://bugs.launchpad.net/ubuntu-kernel-tests/+bug/1973673](https://bugs.launchpad.net/ubuntu-kernel-tests/%2Bbug/1973673)
LD stress-ng
/usr/bin/ld: cannot find -lapparmor: No such file or directory
need to wait for it to get brought in
error 2
pts/stream
another ld issue

PC test suite

pts/scimark2 pts/graphics-magick pts/john-the-ripper pts/openssl pts/build-php pts/build-linux-kernel system/gmic pts/juliagpu pts/gputest pts/mandelgpu pts/mandelbulbgpu pts/apitest pts/luxcorerender

phoronix-test-suite system-sensors

MONITOR=all phoronix-test-suite batch-benchmark  pts/ramspeed  pts/mbw pts/cachebench pts/encode-mp3 pts/c-ray pts/stockfish pts/core-latency pts/coremark pts/cryptopp pts/compress-7zip   pts/osbench pts/byte pts/cl-mem pts/clpeak pts/glmark2 pts/gtkperf pts/jxrendermark

[https://askubuntu.com/questions/1313098/cpu-frquency-scaling-broken-on-upgrade-from-ubuntu-20-04-20-10-intel-cpu](https://askubuntu.com/questions/1313098/cpu-frquency-scaling-broken-on-upgrade-from-ubuntu-20-04-20-10-intel-cpu)

## 20211119 Turing

## 20211117 ubuntu 21.10

Three tests fail to install on Ubuntu 21.10 due to missing Qt4 libraries libqt4-dev, libqt4-devel, qt4-devel
- pts/stream-1.3.3
- pts/qgears2-1.0.1
- pts/john-the-ripper-1.7.2

did this: sudo apt-get install qtbase5-dev qtchooser qt5-qmake qtbase5-dev-tools
and now qgears installed.  I will not bother to install qt4.

TEST 1: $ phoronix-test-suite batch-benchmark pts/ramspeed pts/stream pts/mbw pts/cachebench pts/encode-mp3 pts/c-ray pts/stockfish pts/core-latency pts/coremark pts/cryptopp pts/compress-7zip pts/x264 pts/x265 pts/stress-ng pts/osbench pts/byte pts/cl-mem pts/clpeak pts/glmark2 pts/qgears2 pts/gtkperf pts/jxrendermark

Results Uploaded To: [https://openbenchmarking.org/result/2111183-PHIL-202111103](https://openbenchmarking.org/result/2111183-PHIL-202111103)

OpenCL support seems to be unavailable.

[Installing AMDGPU-PRO drivers](https://linuxconfig.org/install-opencl-for-the-amdgpu-open-source-drivers-on-debian-and-ubuntu)

sudo apt install build-essential dkms
tar -xJpf amdgpu-pro-*.tar.xz
sudo dpkg -i ...
./amdgpu-install --opencl=rocr

ADDITIONAL TESTS (these are the slow ones):
pts/openarena
pts/gputest
pts/apitest
pts/iozone

20210628 ZBook

completed
pts/jxrendermark
pts/glmark2
pts/apitest (not completely finished) - run again
pts/qgears2
pts/cl-mem
pts/ramspeed
pts/mbw
pts/cachebench
pts/gputest
pts/gtkperf  
pts/osbench
pts/cryptopp
pts/byte  
pts/scimark2
pts/john-the-ripper
pts/luxcorerender

FIRST RUN

phoronix-test-suite batch-benchmark pts/encode-mp3 pts/c-ray pts/stockfish pts/core-latency pts/coremark pts/compress-7zip pts/x264 pts/x265 pts/stress-ng pts/clpeak   pts/graphics-magick pts/openssl pts/build-php pts/build-linux-kernel system/gmic pts/juliagpu pts/mandelgpu pts/mandelbulbgpu   pts/openarena

My old computer comparison test suite

pts/compress-7zip pts/apitest pts/c-ray pts/cachebench pts/core-latency pts/coremark pts/cryptopp system/gmic pts/gputest pts/graphics-magick pts/iozone pts/john-the-ripper pts/juliagpu pts/luxcorerender pts/mandelbulbgpu pts/mandelgpu pts/mbw pts/openssl pts/osbench pts/ramspeed pts/scimark2 pts/stream pts/stress-ng pts/build-linux-kernel pts/build-php pts/x264 pts/x265

TEST 1

pts/compress-7zip  pts/c-ray pts/cachebench pts/core-latency pts/coremark pts/cryptopp system/gmic  pts/graphics-magick  pts/john-the-ripper pts/juliagpu pts/luxcorerender pts/mandelbulbgpu pts/mandelgpu pts/mbw pts/openssl pts/osbench pts/ramspeed pts/scimark2 pts/stream pts/stress-ng pts/build-linux-kernel pts/build-php pts/x264 pts/x265

TEST2 pts/apitest

TEST3 pts/gputest

TEST4 pts/iozone

## 20210628 MX8MP EVK

pts/ramspeed pts/stream pts/mbw pts/cachebench pts/encode-mp3 pts/c-ray pts/stockfish pts/core-latency pts/coremark pts/cryptopp pts/compress-7zip pts/x264 pts/stress-ng pts/osbench pts/cl-mem pts/clpeak pts/glmark2 pts/jxrendermark  pts/scimark2 pts/graphics-magick pts/john-the-ripper pts/openssl pts/build-php system/gmic pts/juliagpu pts/mandelgpu pts/mandelbulbgpu  

Run separately

pts/iozone
pts/caffe

Did not install

pts/x265 - not enough space
pts/build-linux-kernel - not enough space
pts/qgears2 - error qmake not found, install qtchooser - no
pts/gtkperf - error can not guess build type you must specify one
pts/apitest - error during compile
pts/luxcorerender - x86 only
pts/gputest - x86 only

## pre-202106 Tests that I have run

## 20210627 Linux ARM

pts/ramspeed pts/cachebench pts/stream pts/encode-mp3 pts/c-Ray pts/stockfish pts/core-latency pts/coremark pts/cryptopp pts/stress-ng  pts/glmark2 pts/clpeak pts/cl-mem pts/juliagpu pts/qmlbench pte/qgears2 pts/x264 pts/x265

The following tests failed to properly run: pts/juliagpu-1.3.1: OpenCL Device: CPU

## 20210627 Linux x86

pts/stream pts/cachebench pts/ramspeed pts/stream pts/mbw pts/c-ray pts/coremark pts/core-latency pts/cryptopp pts/john-the-ripper pts/scimark2 pts/osbench pts/openssl pts/compress-7zip pts/build-php pts/stockfish system/gmic pts/graphics-magick pts/x264 pts/x265 pts/apitest pts/gputest pts/stress-ng pts/build-linux-kernel

Only run this if enough time available: pts/iozone

## 20200505 Linux

pts/stream pts/cachebench pts/ramspeed pts/stream pts/mbw pts/c-ray pts/coremark pts/core-latency pts/cryptopp pts/john-the-ripper pts/scimark2 pts/osbench pts/openssl pts/compress-7zip pts/build-php pts/stockfish system/gmic pts/graphics-magick pts/x264 pts/x265 pts/apitest pts/gputest pts/stress-ng pts/build-linux-kernel pts/iozone

## 20200426 Linux

pts/stream pts/cachebench pts/ramspeed pts/stream pts/mbw pts/c-ray pts/coremark pts/core-latency pts/cryptopp pts/john-the-ripper pts/scimark2 pts/osbench pts/openssl pts/compress-7zip pts/build-php pts/stockfish system/gmic pts/openarena pts/graphics-magick pts/x264 pts/x265 pts/apitest pts/gputest pts/mandelbulbgpu pts/juliagpu pts/stress-ng pts/build-linux-kernel pts/iozone

Removed: pts/luxcorerender, pts/mandelgpu

20200425 Linux

pts/gputest pts/luxcorerender pts/juliagpu pts/mandelgpu pts/mandelbulbgpu system/gmic pts/c-ray pts/cachebench pts/coremark pts/cryptopp pts/ramspeed pts/core-latency pts/compress-7zip pts/stress-ng pts/build-linux-kernel

## pre-202106 Notes

Multi-OS, Multi-HW

- GitHub [https://github.com/phoronix-test-suite/phoronix-test-suite](https://github.com/phoronix-test-suite/phoronix-test-suite)
- [https://github.com/phoronix-test-suite/test-profiles/tree/master/pts](https://github.com/phoronix-test-suite/test-profiles/tree/master/pts)
- Documentation [https://www.phoronix-test-suite.com/documentation/phoronix-test-suite.html](https://www.phoronix-test-suite.com/documentation/phoronix-test-suite.html)
- [https://www.mankier.com/1/phoronix-test-suite](https://www.mankier.com/1/phoronix-test-suite)
- OSX
  - [https://gist.github.com/anshula/728a76297e4a4ee7688d](https://gist.github.com/anshula/728a76297e4a4ee7688d)
  - does not work: brew install phoronix-test-suite
  - just download the code and run it
  - actually it does now, after i installed it from the download
  - sudo ./phoronix-test-suite system-info
  - sudo ./phoronix-test-suite openbenchmarking-login
  - sudo ./phoronix-test-suite list-available-tests #these are available
  - sudo ./phoronix-test-suite list-recommended-tests #these should work with OS
  - sudo ./phoronix-test-suite run pts/openssl
- OSX second attempt
  - install
    - wget [https://phoronix-test-suite.com/releases/phoronix-test-suite-9.6.0.tar.gz](https://phoronix-test-suite.com/releases/phoronix-test-suite-9.6.0.tar.gz)
    - sudo tar -xvf pho...
- Linux
  - [https://linuxize.com/post/how-to-install-php-on-ubuntu-18-04/](https://linuxize.com/post/how-to-install-php-on-ubuntu-18-04/)
  - sudo apt install phoronix-test-suite
  - other install method which actually works
    - wget [http://phoronix-test-suite.com/releases/repo/pts.debian/files/phoronix-test-suite_9.6.0_all.deb](http://phoronix-test-suite.com/releases/repo/pts.debian/files/phoronix-test-suite_9.6.0_all.deb)
    - wget [http://phoronix-test-suite.com/releases/repo/pts.debian/files/phoronix-test-suite_10.2.2_all.deb](http://phoronix-test-suite.com/releases/repo/pts.debian/files/phoronix-test-suite_10.2.2_all.deb)
    - sudo dpkg -i pho...
    - sudo apt-get install -f
    - sudo apt install php
    - sudo apt install php-zip
    - other install that works
      - wget [https://phoronix-test-suite.com/releases/phoronix-test-suite-9.6.0.tar.gz](https://phoronix-test-suite.com/releases/phoronix-test-suite-9.6.0.tar.gz)
      - tar -xvf pho...
- Tests
  - Mac and Linux
    - pts/c-ray
    - pts/cachebench
    - pts/coremark
    - pts/juliagpu
    - pts/mandelgpu
    - pts/mandelbulbgpu
    - pts/cryptopp
    - pts/iozone
    - pts/fio
    - pts/compress-7zip
    - pts/iperf #requires iperf server
    - pts/netperf #requires netperf server
    - pts/sockperf
  - Linux Only
    - pts/ramspeed
    - pts/dbench
    - pts/core-latency
    - pts/gputest
    - pts/luxcorerender
    - pts/dbench
    - pts/hdparm
    - pts/stress-ng
    - pts/build-linux-kernel
  - Suites
    - pts/motherboard
    - pts/netbook
    - pts/universe
    - pts/cpu-massive
    - pts/favorites

wget [https://phoronix-test-suite.com/releases/phoronix-test-suite-9.6.0.tar.gz](https://phoronix-test-suite.com/releases/phoronix-test-suite-9.6.0.tar.gz)
tar -xvf
sudo apt install php php-zip php-cli php-xml php-gd p7zip-full curl sqlite bzip2 sqlite
phoronix-test-suite openbenchmarking-login
phoronix-test-suite list-available-tests
phoronix-test-suite info
phoronix-test-suite install
phoronix-test-suite install-dependencies
phoronix-test-suite batch-setup
phoronix-test-suite batch-benchmark
phoronix-test-suite show-result  
phoronix-test-suite merge-results
phoronix-test-suite rename-result-file
phoronix-test-suite upload-result
phoronix-test-suite estimate-run-time
phoronix-test-suite list-installed-tests
phoronix-test-suite remove-installed-test

## 20210507 ARM testing

wget [http://phoronix-test-suite.com/releases/repo/pts.debian/files/phoronix-test-suite_10.2.2_all.deb](http://phoronix-test-suite.com/releases/repo/pts.debian/files/phoronix-test-suite_10.2.2_all.deb)
sudo dpkg -i phoronix-test-suite_10.2.2_all.deb
sudo apt-get install -f
sudo apt install php php-zip php-cli php-xml php-gd p7zip-full curl sqlite bzip2 sqlite
audo apt install opencl-headers ocl-icd-libopencl1 clinfo ocl-icd-opencl-dev
phoronix-test-suite openbenchmarking-login
phoronix-test-suite list-available-tests
phoronix-test-suite batch-setup
phoronix-test-suite install
phoronix-test-suite install-dependencies
phoronix-test-suite batch-benchmark

my old tests from last year

apt install gimp-gmic

pts/gputest pts/luxcorerender pts/juliagpu pts/mandelgpu pts/mandelbulbgpu system/gmic pts/c-ray pts/cachebench pts/coremark pts/cryptopp pts/ramspeed pts/core-latency pts/compress-7zip pts/stress-ng pts/build-linux-kernel

for x86:

pts/stream pts/cachebench pts/ramspeed pts/stream pts/mbw pts/c-ray pts/coremark pts/core-latency pts/cryptopp pts/john-the-ripper pts/scimark2 pts/osbench pts/openssl pts/compress-7zip pts/build-php pts/stockfish system/gmic pts/graphics-magick pts/x264 pts/x265 pts/apitest pts/gputest pts/stress-ng pts/build-linux-kernel pts/iozone

for x86 and all oss

pts/gputest pts/luxcorerender pts/juliagpu pts/mandelgpu pts/mandelbulbgpu system/gmic pts/c-ray pts/cachebench pts/coremark pts/cryptopp pts/ramspeed pts/core-latency pts/compress-7zip pts/stress-ng pts/build-linux-kernel

my new linux arm tests

phoronix-test-suite batch-benchmark pts/ramspeed pts/cachebench pts/stream pts/encode-mp3 pts/c-Ray pts/stockfish pts/core-latency pts/coremark pts/cryptopp pts/stress-ng  pts/glmark2 pts/clpeak pts/cl-mem pts/juliagpu pts/qmlbench pte/qgears2 pts/x264 pts/x265

The following tests failed to properly run:

- pts/juliagpu-1.3.1: OpenCL Device: CPU

## 20200425 Single Linux Test:

phoronix-test-suite batch-benchmark pts/gputest pts/luxcorerender pts/juliagpu pts/mandelgpu pts/mandelbulbgpu system/gmic pts/c-ray pts/cachebench pts/coremark pts/cryptopp pts/ramspeed pts/core-latency pts/compress-7zip pts/stress-ng pts/build-linux-kernel

## Next Test Platform

MEMORY

pts/stream pts/cachebench pts/ramspeed pts/stream pts/mbw

CPU

pts/osbench pts/c-ray pts/coremark pts/core-latency pts/cryptopp pts/scimark2 pts/openssl pts/compress-7zip pts/build-php pts/stockfish

OPTIONAL

pts/john-the-ripper

GRAPHICS

system/gmic pts/openarena pts/graphics-magick pts/x264 pts/x265

OpenGL

pts/apitest pts/gputest

OpenCL

pts/mandelbulbgpu pts/juliagpu

LINUX

pts/stress-ng pts/build-linux-kernel

DISK

pts/iozone

ALL TOGETHER

## 20200426 Single Linux Test

pts/stream pts/cachebench pts/ramspeed pts/stream pts/mbw pts/c-ray pts/coremark pts/core-latency pts/cryptopp pts/john-the-ripper pts/scimark2 pts/osbench pts/openssl pts/compress-7zip pts/build-php pts/stockfish system/gmic pts/openarena pts/graphics-magick pts/x264 pts/x265 pts/apitest pts/gputest pts/mandelbulbgpu pts/juliagpu pts/stress-ng pts/build-linux-kernel pts/iozone

REMOVED

pts/luxcorerender  
pts/mandelgpu

ALL TOGETHER

20200505 Single Linux Test

pts/stream pts/cachebench pts/ramspeed pts/stream pts/mbw pts/c-ray pts/coremark pts/core-latency pts/cryptopp pts/john-the-ripper pts/scimark2 pts/osbench pts/openssl pts/compress-7zip pts/build-php pts/stockfish system/gmic pts/graphics-magick pts/x264 pts/x265 pts/apitest pts/gputest pts/stress-ng pts/build-linux-kernel pts/iozone
