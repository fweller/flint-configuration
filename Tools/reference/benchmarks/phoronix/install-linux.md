# Installation in Linux

## Prerequisites

Need to confirm if these are still required

sudo apt install php php-zip php-cli php-xml php-gd p7zip-full curl sqlite bzip2 sqlite opencl-headers ocl-icd-libopencl1 clinfo ocl-icd-opencl-dev gmic cpufrequtils
sudo pip3 install cmake
sudo apt install qt5-qmake qt5*-dev qtdeclarative5-dev libqt5*-dev  qml-module-qtquick-controls

## Debian package install

wget https://phoronix-test-suite.com/releases/repo/pts.debian/files/phoronix-test-suite_10.8.4_all.deb
sudo dpkg -i phoronix-test-suite_10.8.4_all.deb
