" VimRC = Vim Runtime Control

" --------------------------------------------------
" General Settings

" Use Vim settings rather then Vi settings
" This must be first, because it changes other options as a side effect.
set nocompatible

set number      " Enable line numbering
set ruler       " Enable ruler
set mouse=a     " Enable mouse
set visualbell  " No sounds
set autoread    " Reload files changed outside vim
set showmatch   " Highlight matching parenthesis
set showcmd     " Show incomplete cmds down the bottom
set showmode    " Show current mode down the bottom
set lazyredraw  " Don't redraw screen if unnecessary
set encoding=utf8   " utf8
set gcr=a:blinkon0  " Disable cursor blink
set history=1000    " Store lots of :cmdline history
syntax on           " Turn on syntax highlighting
syntax enable       " Turn on syntax highlighting
set fileformat=unix " Linefeed only.
set ffs=unix,dos,mac  " Order for buffer.
set backspace=indent,eol,start " Allow backspace in insert mode

"let python_highlight_all = 1 " enable all Python syntax highlighting features 

" --------------------------------------------------
" Leader

" Change leader to a comma because the backslash is too far away
" That means all \x commands turn into ,x
"let mapleader=","


" --------------------------------------------------
" Commenting

" Disable auto commenting
"autocmd FileType * setlocal formatoptions-=c formatoptions-=r formatoptions-=o
"set formatoptions-=cro
"setlocal formatoptions-=cro
autocmd BufNewFile,BufRead * setlocal formatoptions-=cro

" --------------------------------------------------
" Backups and Swap Files

set backupdir='${HOME}/.vim/backups/'
set backup
set writebackup
set noswapfile
"set nobackup
"set nowb


" --------------------------------------------------
" Persistent Undo

" Keep undo history across sessions, by storing in file.
" Only works all the time.
if has('persistent_undo')
  silent !mkdir '${HOME}/.vim/backups' > /dev/null 2>&1
  set undodir='${HOME}/.vim/backups'
  set undofile
endif


" --------------------------------------------------
" Indentation

filetype plugin on
filetype indent on
set autoindent
set smartindent   "spaces for tab
set smarttab
set softtabstop=2 " 4 spaces for tab (python)
"set softtabstop=2 " 2 spaces for tab
set tabstop=2     " 4 spaces for tab (python)
"set tabstop=2     " 2 spaces for tab
set shiftwidth=2  "(python)
"set shiftwidth=2
set expandtab
set list listchars=tab:\ \ ,trail:· " Display tabs and trailing spaces visually
set nowrap    " Don't wrap lines
"set wraplines  " Wrap lines
set linebreak " Wrap lines at convenient points
"set cursorline  "show a visual line under the cursor


" --------------------------------------------------
" Folds

" Learn what these are before using them
"set foldmethod=indent  " Fold based on indent
"set foldnestmax=10     " Deepest fold
"set nofoldenable      " Dont fold by default
"set foldenable         " Fold by default
"set foldlevelstart=10  " Initial fold level


" --------------------------------------------------
" Completion

"set wildmenu "enable ctrl-n and ctrl-p to scroll thru matches
"set wildmode=list:longest
"set wildignore=*.o,*.obj,*~ "stuff to ignore when tab completing
"set wildignore+=*vim/backups*
"set wildignore+=*sass-cache*
"set wildignore+=*DS_Store*
"set wildignore+=vendor/rails/**
"set wildignore+=vendor/cache/**
"set wildignore+=*.gem
"set wildignore+=log/**
"set wildignore+=tmp/**
"set wildignore+=*.png,*.jpg,*.gif



" --------------------------------------------------
" Scrolling

set scrolloff=8 "Start scrolling when we're 8 lines away from margins
set sidescrolloff=15
set sidescroll=1


" --------------------------------------------------
" Searching

set incsearch   " Find the next match as we type the search
set hlsearch    " Highlight searches by default
set ignorecase  " Ignore case when searching...
set smartcase   " ...unless we type a capital
" turn off search highlight with comma space
nnoremap <leader><space> :nohlsearch<CR>


" --------------------------------------------------
" Colors

" Dark background always preferred
set background=dark
set bg=dark

let g:gruvbox_contrast_dark = 'hard'
silent! colorscheme koehler "fallback to this one
silent! colorscheme gruvbox "prefer this one

" Change colorscheme based on operating system
"if has("mac") || has("macunix")
  "colorscheme flintsolar
  "colorscheme blackboard
"  color koehler
"elseif has('win32') || has('win64')
"  color koehler
"  behave mswin
"elseif has("unix")
"  color koehler
"endif

" Change the background 
"if has('gui_running')
"  set background=light
"else
"  set background=dark
"endif

"let g:solarized_contrast="high"
"let g:solarized_visibility="high"

" --------------------------------------------------
" Fonts
" 20200409 assuming all my computers now have Consolas font installed
" To determine which font is presently loaded :set guifont?

if has('gui_running')
  "set lines=60 columns=108 linespace=0
  if has("mac") || has("macunix")
    set guifont=consolas:h14
  elseif has('win32') || has('win64')
"    set guifont=inconsolata\ medium\ 14
    set guifont=Consolas:h10
    set lines=60 columns=108 linespace=0
  elseif has("unix")
    "    set guifont=inconsolata\ medium\ 14
    "    set guifont=consolas\ 14
    "set a preference of fonts to use.  will use first available.
    set guifont=Consolas\ 13,Hack\ Nerd\ Font\ 13,Inconsolata\ 13,Ubuntu\ Mono\ 13
    set lines=57 columns=103 linespace=0
  endif
endif

" Set font size based on hostname
"  if hostname() == 'home-pc'
"      set guifont=...

" --------------------------------------------------
