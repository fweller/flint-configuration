# ----------------------------------------
# NAME        : aliases-general
# DESCRIPTION : Aliases for any OS or shell
# ----------------------------------------
# This file contains general aliases that can be used across different operating systems
# and shells, providing convenience and efficiency in common tasks.
# ----------------------------------------

# ----------------------------------------
# Directory Listings (ls)
# ----------------------------------------
alias ls='ls -hF'                   # List files with human-readable sizes, append indicator (e.g., / for directories)
alias ll='ls -FlAhp'                # Long listing, show almost all files, human-readable sizes, with file type indicator
alias lr='ls -rtFlAhp'              # List files sorted by modification time, reverse order, long format
alias lg='ll --git'                 # Long listing with git information (requires custom script or tool)
alias l='ls -lAhF'                  # Long listing, show almost all files, human-readable sizes, with file type indicator
alias lrt='ls -R | grep ":$" | sed -e "s/:$//" -e "s/[^-][^\/]*\//--/g" -e "s/^/   /" -e "s/-/|/" | less'
                                    # Recursively list directories, formatted as a tree
alias dir='ls -lAhF'                # Directory listing alias similar to 'dir' command in other systems

# ----------------------------------------
# File and Directory Size
# ----------------------------------------
#alias size='tree --du -hC | grep -E "├──|└──"'  # Display directory structure with sizes

# ----------------------------------------
# Overloaded Commands
# ----------------------------------------
alias cp='cp -iv'                   # Copy files interactively, with verbose output
alias df='df -hT'                   # Display disk usage with human-readable sizes, show filesystem type
alias grep='grep --color=auto'      # Highlight matched patterns with color
alias less='less -FSRXcr'           # Improved less with various options for better navigation
alias mv='mv -iv'                   # Move files interactively, with verbose output
alias mkdir='mkdir -pv'             # Create directories, including parent directories, with verbose output
#alias tree='tree --du -hC'         # Display directory structure with sizes (uncomment if needed)

# ----------------------------------------
# System CPU and Memory Usage
# ----------------------------------------
alias htop='htop -s PERCENT_CPU -H 0'  # Launch htop, sorted by CPU usage, with threads collapsed

# ----------------------------------------
# Shortcuts
# ----------------------------------------
alias c='clear'                    # Clear the terminal screen
alias h='history'                  # Display the command history
alias untar='tar -zxvf'            # Shortcut for extracting tar.gz files
alias cd..='cd ../'                # Go up one directory
alias cd...='cd ../../'            # Go up two directories
alias cd....='cd ../../../'        # Go up three directories
alias cd.....='cd ../../../../'    # Go up four directories
alias ..='cd ../'                  # Go up one directory
alias ...='cd ../../'              # Go up two directories
alias ....='cd ../../../'          # Go up three directories
alias .....='cd ../../../../'      # Go up four directories
alias ~='cd $HOME'                 # Shortcut to change directory to the home directory
alias path='echo $PATH | tr ":" "\n"'  # Display the PATH environment variable, one entry per line

# ----------------------------------------
# Git Aliases
# ----------------------------------------
alias gits='git status --untracked-files=no'  # Check git status without showing untracked files
alias gita='git add -u'                       # Add all modified tracked files to the staging area
alias gitc='git commit -m "saving"'           # Commit changes with a simple message "saving"
alias gitp='git push'                         # Push changes to th

