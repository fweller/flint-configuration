#!/bin/bash
ScriptName="$0"
ScriptDescription="Install Flint's Awesome Tools"
Line="--------------------------------------------------------------------------------"
echo ""; echo "${Line}"; echo "${ScriptName} START ${ScriptDescription}"; echo ""
Name="flint-configuration"
Project="https://gitlab.com/fweller/flint-configuration.git"
InstallDir="${HOME}" # Production
DownloadDir="${HOME}/Downloads"

# Debug: uncomment the next line
#InstallDir="${HOME}/Downloads/Install" # Test

Command() {
echo ""; echo "${ScriptName} COMMAND: $1"; echo ""
eval "$1"
}

# Git configuration
git config --global user.name "Flint Weller"
git config --global user.email "flint.weller@gmail.com"

# Prepare
Command "mkdir -p ${DownloadDir}"
Command "mkdir -p ${InstallDir}"

# Move to download dir
pushd "$DownloadDir" || exit

# Clone my project
Command "git clone ${Project}"

# Copy to InstallDir
Command "cp -r ${Name}/* ${Name}/.[!.]* ${Name}/..?* ${InstallDir}"

# Exit
popd || exit

echo ""; echo "${ScriptName} END ${ScriptDescription}"; echo "${Line}"; echo ""
exit
