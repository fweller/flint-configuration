#!/bin/bash
# MacOS create directories

mkdir -p ${HOME}/Applications
mkdir -p ${HOME}/Backups
mkdir -p ${HOME}/Builds
mkdir -p ${HOME}/Documents
mkdir -p ${HOME}/Downloads
mkdir -p ${HOME}/Engineering
mkdir -p ${HOME}/Logs
mkdir -p ${HOME}/Projects
mkdir -p ${HOME}/Personal
mkdir -p ${HOME}/Tmp

