#!/bin/bash
## Osync - two way file syncronization
# http://www.netpower.fr/osync
# https://github.com/deajan/osync

# Variables
BuildDir="${HOME}/Builds"
Name="osync"
Url="https://github.com/deajan/${Name}"

## Install

# Prepare
mkdir -p "${BuildDir}"
pushd "${BuildDir}" || exit

# Clean up any old clone
rm -rf "${Name}"

# Download and install
git clone "${Url}"
cd "${Name}" || exit

# Add pull 254 that fixes MacOS issues
# # https://github.com/deajan/osync/pull/254/commits/22e269902b1a198e2f95972b828f6846791091e4
git fetch origin pull/254/head:pr-254
git checkout pr-254

# Clean up any old install
sudo rm -f /usr/local/bin/osync.sh

sudo bash install.sh

# Exit
popd || exit
exit

# --------------------------
# Below is the old method

# Variables
BuildDir="${HOME}/Builds"
Name="osync"
Url="https://github.com/deajan/${Name}"
Branch="old-stable"

## Install

# Prepare
mkdir -p "${BuildDir}"
pushd "${BuildDir}" || exit

# Download and install
git clone -b "${Branch}" "${Url}"
cd "${Name}" || exit
sudo bash install.sh

# Exit
popd || exit

