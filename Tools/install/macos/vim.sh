#!/bin/bash

# Install Vim from repo
brew install vim

## Vim Markdown support from plasticboy

# Should be run after Vim is installed

# Variables
VimDir="${HOME}/.vim/pack/vendor/start"
Package="vim-markdown"
PackageDir="${VimDir}/${Package}"

# Make directory and move into it
mkdir -p "$PackageDir"
pushd "$PackageDir" || exit

# Download the archive and extract
wget https://github.com/plasticboy/vim-markdown/archive/master.tar.gz
tar --strip=1 -zxf master.tar.gz

# Exit
popd | exit

