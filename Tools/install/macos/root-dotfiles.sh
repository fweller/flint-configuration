#!/bin/bash

# Make sure to confirm the appropriate root directory
RootDir="/var/root"

# Copy my dotfiles to root
sudo cp -f ${HOME}/.bashrc ${RootDir}
sudo cp -f ${HOME}/.bash_profile ${RootDir}
sudo cp -f ${HOME}/.inputrc ${RootDir}
sudo cp -f ${HOME}/.profile ${RootDir}
sudo cp -f ${HOME}/.shellrc ${RootDir}
sudo cp -f ${HOME}/.vimrc ${RootDir}

# Copy Vim directory to root
sudo cp -rf ${HOME}/.vim ${RootDir}

# Copy my tools directory to root
sudo cp -rf ${HOME}/Tools ${RootDir}

