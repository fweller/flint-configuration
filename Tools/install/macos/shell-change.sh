#!/bin/sh
# MacOS Change default shell to Bash

chsh -s /bin/bash

# Change default shell back to Zsh
# chsh -s /bin/zsh

