#!/bin/bash
# https://gitlab.com/fweller/flint-configuration.git


# Assuming we are starting from the unzipped contents of the archive
pwd
cd ${HOME}/Downloads
pwd

# Now we are at the right location
mv flint-configuration/* flint-configuration/.[!.]* flint-configuration/..?* ~  

