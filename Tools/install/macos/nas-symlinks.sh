#!/bin/bash

MountDir="/Volumes"
SymlinkDir="${HOME}/SkyNet"

# Symbolic Links
mkdir -p ${SymlinkDir}
ln -s ${MountDir}/SkyData/ ${SymlinkDir}/SkyData
ln -s ${MountDir}/SkyStore/ ${SymlinkDir}/SkyStore
#ln -s ${MountDir}/SkyPenguin/ ${SymlinkDir}/SkyPenguin
ln -s ${MountDir}/SkyWork/ ${SymlinkDir}/SkyWork
ln -s ${MountDir}/music/ ${SymlinkDir}/SkyMusic
ln -s ${MountDir}/video/ ${SymlinkDir}/SkyVideo

