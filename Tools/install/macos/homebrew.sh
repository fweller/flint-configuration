#!/bin/bash
# MacOS install Homebrew package manager
# https://brew.sh/

/bin/bash -c "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/HEAD/install.sh)"

# Homebrew wants to add this command to the end of .bash_profile
# eval "$(/opt/homebrew/bin/brew shellenv)"


