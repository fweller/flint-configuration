#!/bin/sh
# Installer for HomeBrew installed applications

brew install \
  wget p7zip eza vim shellcheck inxi zlib rsync htop tree \
  fastfetch pv bash iproute2mac

brew install --cask qlmarkdown
brew install --cask veracrypt


# ----------------------------------
# REMOVED

# 2024 exa.  repaced by eza.
# 2024 neofetch.  replced by fastfetch.
# 2024 evernote2md
# 2024 7-zip.  p7zip is better.


# ----------------------------------
# NOTES

# bash
# Apple default bash is the final version to be licensed with GPLv2
# Newer versions of bash are licensed to GPLv3 which is incompatible with Apple
#   The default location is /usr/bash
#   GNU bash, version 3.2.57(1)-release (arm64-apple-darwin23)
#   Copyright (C) 2007 Free Software Foundation, Inc.
# Homebrew bash installs version 5.2.15 or newer and does not care about GPLv3
#   The location is /opt/homebrew/bin/bash

# p7zip
# Call with 7z
# https://formulae.brew.sh/formula/p7zip
# https://github.com/p7zip-project/p7zip
# A new p7zip fork with additional codecs and improvements
# forked from https://sourceforge.net/projects/sevenzip/
# AND https://sourceforge.net/projects/p7zip/

# sevenzip
# AKA 7-zip AKA 7zip
# Call with 7zz
# https://formulae.brew.sh/formula/sevenzip
# https://7-zip.org/
#




