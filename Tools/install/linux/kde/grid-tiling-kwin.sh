#!/bin/bash
# grid-tiling-kwin

# Variables
BuildDir="${HOME}/Builds"
Name="Grid-tiling-kwin"
Url="https://github.com/lingtjien/${Name}"

## Install

# Prepare
mkdir -p "${BuildDir}"
pushd "${BuildDir}" || exit

# Download and install
scriptdir="${HOME}/.local/share/kwin/scripts/grid-tiling"
mkdir -p ~/.local/share/kwin/scripts/grid-tiling
git clone "${Url}"
cd "${Name}" || exit
ln -s "$(pwd)/contents" "$scriptdir"
ln -s "$(pwd)/metadata.json" "$scriptdir"

# Exit
popd || exit

