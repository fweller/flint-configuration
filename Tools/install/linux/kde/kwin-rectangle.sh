#!/bin/bash
# ----------------------------------------
# NAME        : kwin-rectangle.sh
# DESCRIPTION : Installs the kwin-rectangle project
# ----------------------------------------
# kwin-rectangle - Shortcuts to move windows around
# https://github.com/acristoffers/kwin-rectangle

# ----------------------------------------
# Variables

BuildDir="$HOME/Builds"
Name="kwin-rectangle"
Url="https://github.com/acristoffers/$Name"
Branch="main"
Tag="v1.2.11" # Last version before support for Plasma 6
Installer="install.sh"

echo ""
echo "----------------------------------------"
echo "Installing dbux-x11"
sudo apt install dbus-x11


echo ""
echo "----------------------------------------"
echo "Enter the build directory"
mkdir -p "$BuildDir"
pushd "$BuildDir" || exit
pwd # Let the user know where we are

echo ""
echo "----------------------------------------"
echo "Clone $Url"
git clone "$Url"
cd "$Name" || exit
pwd # Let the user know where we are

echo ""
echo "----------------------------------------"
echo "Switch to tag $Tag"
git switch --detach $Tag

echo ""
echo "----------------------------------------"
full_path=$(realpath ./$Installer)
echo "Run the installer $full_path"
echo "If the installer fails, then user must manually install the script"
sudo "$full_path"
if [ $? -eq 0 ]; then
    echo "Installer succeeded."
else
    echo "Installer failed."
fi

echo ""
echo "----------------------------------------"
echo "End of the script"
popd || exit
pwd # Let the user know where we are

