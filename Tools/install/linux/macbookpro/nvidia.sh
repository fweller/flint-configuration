#!/bin/bash
# Install Nvidia GPU drivers on MacBook Pro
# Can only use the Nvidia legacy driver version 470

sudo apt install linux-headers-amd64
sudo apt install firmware-misc-nonfree
sudo apt install nvidia-tesla-470-driver

