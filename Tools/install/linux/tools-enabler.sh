#!/bin/bash
# ----------------------------------------
# NAME        : tools-enabler.sh
# SUMMARY     : Easy installer script.
# DESCRIPTION : This script prepares and runs my tools installer
# ----------------------------------------



# ----------------------------------------
# Download and run the installer, then quit

curl -o ./tools.sh https://gitlab.com/fweller/flint-configuration/-/raw/master/Tools/install/linux/tools-proper.sh
chmod +x ./tools.sh
bash ./tools.sh
exit 0

# ----------------------------------------


# ----------------------------------------
# Notes
#   Sourcing a file
#     . filename    # POSIX (sh)
#     . ./filename  # Bash
#   The source alias for . exists in Bash but not POSIX (sh)


# ----------------------------------------
# Instructions
# 1) Copy SSH Folder to $HOME
# 2) Copy and paste the following line into the terminal
curl https://gitlab.com/fweller/flint-configuration/-/raw/master/Tools/install/linux/tools-enabler.sh | bash

