#!/bin/bash

pushd ${HOME}/Builds

# Manjaro themes are stored here
# https://gitlab.manjaro.org/artwork/themes

git clone https://gitlab.manjaro.org/artwork/themes/breath.git

git clone https://gitlab.manjaro.org/artwork/themes/breath-gtk

popd
