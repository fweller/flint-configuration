#!/bin/bash
# Start installing my tools



#https://gitlab.com/fweller/flint-configuration
#curl -s <em>http://example.com/script.sh</em> | bash

# Variables
DownloadDir="${HOME}/Downloads"
Url="https://gitlab.com/fweller/flint-configuration/-/raw/master/Tools/install/linux/tools.sh"

# Install wget - just in case it is missing
su -c "apt install wget"

# Create the downloads directory and move into it
mkdir -p "${DownloadDir}"
cd "${DownloadDir}"

# Download my script
wget https://gitlab.com/fweller/flint-configuration/-/raw/master/Tools/install/linux/tools.sh
chmod +x tools.sh
./tools.sh
cd "${HOME}/Tools/install/linux"


