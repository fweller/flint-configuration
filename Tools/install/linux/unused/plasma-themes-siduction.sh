#!/bin/bash

pushd ${HOME}/Builds

# SDDM theme
git clone https://github.com/siduction/sddm-theme-siduction.git

# KDE settings
git clone https://github.com/siduction/settings-kde.git
# https://github.com/siduction/settings-kde/tree/MoW/desktoptheme/mow-dark

popd
