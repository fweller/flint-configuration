#!/bin/bash
ScriptName="$0"
ScriptDescription="Install packages"
Arguments=$1 # Arguments for this script
Line="--------------------------------------------------------------------------------"

# Reference
#   http://distrowatch.org/dwres.php?resource=package-management
# Distro agnostic
#   pkcon  = Distro Agnostic
# Distro specific
#   apt    = Debian, Ubuntu, Mint
#   nala   = Upgraded: Debian, Ubuntu, Mint
#   pacman = Arch, Manjaro
#   zypper = OpenSUSE
#   dnf    = Fedora, RHEL
#   urpmi  = Mandriva, Mageia
#   emerge = Gentoo

Command() {
  echo ""
  echo "${ScriptName} COMMAND: $1"
  echo ""
  Output=$($1)
  echo ""
  echo "${ScriptName} RETURN: $Output"
  #eval "$1"
}
# Start
echo ""
echo "${Line}"
echo "${ScriptName} START ${ScriptDescription}"
echo ""

# Only use one package manager
# Configure if/else appropriately for priority

if [ -x "$(command -v nala)" ]; then 
  Command "sudo nala install ${Arguments}"
elif [ -x "$(command -v apt)" ]; then
  Command "sudo apt install ${Arguments}"
elif [ -x "$(command -v pkcon)" ]; then
  Command "sudo pkcon install ${Arguments}"
elif [ -x "$(command -v pacman)" ]; then 
  Command "sudo pacman -U ${Arguments}"
elif [ -x "$(command -v zypper)" ]; then 
  Command "sudo zypper install ${Arguments}"
elif [ -x "$(command -v dnf)" ]; then 
  Command "sudo dnf install ${Arguments}"
elif [ -x "$(command -v urpmi)" ]; then
  Command "sudo urpmi ${Arguments}"
elif [ -x "$(command -v emerge)" ]; then
  Command "sudo emerge ${Arguments}"



# End
echo ""
echo "${ScriptName} END ${ScriptDescription}"
echo "${Line}"
echo ""

