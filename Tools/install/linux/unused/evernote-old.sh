#!/bin/bash

echo "Copying Evernote for Windows to Downloads folder"
Source="${HOME}/SkyNet/SkyData/Software/Multiplatform/Evernote/windows/Evernote_6.25.2.9198.exe"
DestDir="${HOME}/Downloads"
command="cp $Source $DestDir"
echo "$command"
eval "$command"

echo "Installing bottles from flathub"
sudo apt install -y flatpak
sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
sudo flatpak install -y flathub com.usebottles.bottles
sudo flatpak override com.usebottles.bottles --user --filesystem=xdg-data/applications
# flatpak run com.usebottles.bottles

