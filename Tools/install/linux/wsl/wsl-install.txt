WSL Install Instructions

----------------------------
SSH Folder

WINDOWS:

Copy SSH folder to Downloads
Unzip and rename to SSH

WSL:

cp -r /mnt/c/Users/flint/Downloads/SSH .
mv SSH .ssh
cd .ssh
chmod 700 *


----------------------------
Install Tools Project

WSL:

curl https://gitlab.com/fweller/flint-configuration/-/raw/master/Tools/install/linux/tools-enabler.sh | bash


----------------------------
Run Installer

WSL:

cd $HOME/Tools/install/linux
./installer-wsl.sh


----------------------------
Mount NAS

WSL:

cd $HOME /Tools/scripts
./wslmount.sh


----------------------------
End

