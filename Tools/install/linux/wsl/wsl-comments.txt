# WSL Comments

# Network mounts
Do not edit /etc/fstab to add network mounts
use mount command such as that found in /Tools/scripts/wslmount.sh

# Example mount for WSL2
sudo mount -t auto -o credentials=/root/.skynetcredentials,vers=3,rw,uid=flint,gid=flint,iocharset=utf8 //192.168.1.11/SkyWork /mnt/SkyWork
sudo mount -t auto -o credentials=/root/.skynetcredentials,vers=3,rw,uid=flint,gid=flint,iocharset=utf8 //192.168.1.11/SkyData /mnt/SkyData

