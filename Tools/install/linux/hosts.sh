#!/bin/bash
# ----------------------------------------
# NAME        : hosts.sh
# DESCRIPTION : Appends custom entries from a specified hosts file to /etc/hosts.
# ----------------------------------------

# ----------------------------------------
# Variables

# Directory where the source hosts file is located
SourceDir="$(eval pwd)/../../config"

# Name of the source hosts file
SourceFile="hosts.txt"

# Full path to the source hosts file
Source="$SourceDir/$SourceFile"

# ----------------------------------------
# Start of the script

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : $(date) - Updating /etc/hosts with entries from $Source"

# ----------------------------------------
# Check if the source file exists

if [ ! -f "$Source" ]; then
  echo "ERROR   : Source file $Source does not exist. Exiting."
  exit 1
fi

# ----------------------------------------
# Append the source hosts file to /etc/hosts

Command="sudo bash -c 'cat $Source >> /etc/hosts'"
echo "COMMAND : $Command"
eval "$Command"

# Check if the command was successful
if [ $? -ne 0 ]; then
  echo "ERROR   : Failed to update /etc/hosts with $Source"
  exit 1
else
  echo "SUCCESS : /etc/hosts updated successfully with entries from $Source"
fi

# ----------------------------------------
# Finish

echo ""
echo "END     : $(date) - Finished updating /etc/hosts with $Source"
echo "SPACER  : ----------------------------------------"
exit 0 # Success

