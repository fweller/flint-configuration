#!/bin/bash
# ----------------------------------------
# NAME        : installer-wsl.sh
# SUMMARY     : WSL Installer
# DESCRIPTION : Installs all my tools under WSL Linux
# ----------------------------------------


# ----------------------------------------
# Variables


# ----------------------------------------
# If this is Debian, add my user to the Sudoers list

# ./debian-sudoers.sh


# ----------------------------------------
# Create WSL folders

mkdir -p "$HOME/Applications"
mkdir -p "$HOME/Backups"
mkdir -p "$HOME/Builds"
mkdir -p "$HOME/Downloads"
mkdir -p "$HOME/Logs"
mkdir -p "$HOME/Tmp"


# ----------------------------------------
# Create Windows folders

WindowsHome="/mnt/c/Users/flint"
mkdir -p "$WindowsHome/Applications"
mkdir -p "$WindowsHome/Backups"
mkdir -p "$WindowsHome/Builds"
mkdir -p "$WindowsHome/Downloads"
mkdir -p "$WindowsHome/Engineering"
mkdir -p "$WindowsHome/Logs"
mkdir -p "$WindowsHome/Projects"
mkdir -p "$WindowsHome/Personal"
mkdir -p "$WindowsHome/Tmp"
mkdir -p "$WindowsHome/Work"


# ----------------------------------------
# Install Packages

sudo apt install -y \
  htop curl wget p7zip-full bzip2 ncdu zstd \
  git git-lfs shellcheck cifs-utils bind9-dnsutils

sudo apt install -y eza # 2024 replaces exa
sudo apt install -y neofetch # 2024 eol
sudo apt install -y fastfetch # 2024 replaces neofetch

./console/vim.sh

./console/osync.sh


# ----------------------------------------
# NAS Access

./nas-skydata.sh


# ----------------------------------------
# Root Dotfiles

./root-dotfiles.sh


# ----------------------------------------
# Done

