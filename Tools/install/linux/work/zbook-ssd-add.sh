#!/bin/bash
## Adding the second SSD to ZBook

# Create a mountpoint
sudo mkdir /work

# Add entry to fstab
# DO NOT TOUCH ANY TEXT BETWEEN THESE ARROWS ---->>>>
sudo sh -c "cat << EOF >> /etc/fstab

# SSD M.2 2TB WD Blue on /dev/sdb
UUID=0e9a2ead-a410-49b0-8902-2f2a84a95027 /work  ext4    errors=remount-ro 0       1

EOF"
# <<<<---- DO NOT TOUCH ANY TEXT BETWEEN THESE ARROWS

# Reload /etc/fstab
sudo systemctl daemon-reload

# Remount
sudo mount -a

# Change ownership from root to flint
sudo chown -R flint /work
sudo chgrp -R flint /work

