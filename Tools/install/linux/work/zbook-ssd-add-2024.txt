# Add second SSD to ZBook
# 2024 October 28
# My goal is to attach this drive as ext4 on top of LVM

# SuperUser
su

# Confirm status of LVM on the local computer
pvscan
vgscan
vgdisplay
pvdisplay
vgdisplay
lvdisplay

# Identify the SSD
sudo fdisk -l

# Create partition table on the disk
sudo fdisk /dev/sdb
# Create partition 1, full size

# LVM create physical volume
pvcreate /dev/sdb1

# LVM create virtual group that contains the physical volume
vgcreate ZData-vg /dev/sdb1

# LVM create logical volume
lvcreate -l 100%FREE -n zdata ZData-vg

# Create filesystem on the logical volume
mkfs.ext4 -L zdata /dev/ZData-vg/zdata

# Note: If I forgot to add the label name to the above command, I could do the following:
# Add label to filesystem
e2label  /dev/ZData-vg/zdata zdata
# confirm volume label
blkid -s LABEL

# Create mountpoint
mkdir -f /mnt/zdata

# List all the UUIDs
# Identify UUID for /dev/mapper/ZData--vg-zdata:
lsblk -f
blkid

# Add zdata UUID to /etc/fstab
UUID=5d2b9466-b4a4-442a-8fd2-0fd7972cd4d7       /mnt/zdata    ext4    defaults        0       2

# Reload /etc/fstab
systemctl daemon-reload
mount -a

# change permissions of mountpoint
sudo chown -R flint:users /mnt/zdata

# Exit SuperUser
exit

# Create symbolic link in home directory
ln -s /mnt/zdata /home/flint/ZData

