#!/bin/bash

# Create symlinks from my work scripts to my local bin
ln "${HOME}/Tools/scripts/work/startapps.sh" "${HOME}/Bin/startapps"
ln "${HOME}/Tools/scripts/work/startwindows.sh" "${HOME}/Bin/startwindows"
ln "${HOME}/Tools/scripts/work/moveapps.sh" "${HOME}/Bin/moveapps"
ln "${HOME}/Tools/scripts/work/movewindows.sh" "${HOME}/Bin/movewindows"
ln "${HOME}/Tools/scripts/work/archivewindows.sh" "${HOME}/Bin/archivewindows"
ln "${HOME}/Tools/scripts/pointers.sh" "${HOME}/Bin/pointers"
#ln "${HOME}/Tools/scripts/work/closewindows.sh" "${HOME}/Bin/closewindows"
#ln "${HOME}/Tools/scripts/work/codewarrior.sh" "${HOME}/Bin/codewarrior"
#ln "${HOME}/Tools/scripts/work/wireshark.sh" "${HOME}/Bin/wireshark"
#ln "${HOME}/Tools/scripts/work/wslmount.sh" "${HOME}/Bin/wslmount"

