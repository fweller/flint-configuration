#!/bin/bash

## Install info for Siduction on the ZBook

# For the installer: Add a single boot command: nomodeset
# For the first boot of the system: Edit the kernel string by adding one word
# to it: nomodeset
# For the first login: Select X11 instead of Wayland

# Superuser
su

# Edit the repositories
mv /etc/apt/sources.list.d/debian.list /etc/apt/sources.list.d/debian.list.original
cp /home/flint/Tools/reference/debian/sources.list /etc/apt/sources.list.d/debian.list
apt update

## The beginning
apt install linux-image-amd64 linux-headers-amd64
apt install firmware-misc-nonfree
apt install nvidia-driver

exit

# Do things manually

## Enable use of external displays
# add parameter nvidia-drm.modeset=1 to the line GRUB_CMDLINE_LINUX_DEFAULT
vi /etc/default/grub
update-grub

## Reboot and use Debian kernel instead of Siduciton
 
# Remove Siduction kernel
su
apt remove linux-*siduction*

# configure nvidia
nvidia-settings

