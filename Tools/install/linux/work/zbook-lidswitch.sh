#!/bin/bash
## For ZBook and any Laptop
## Instruct SystemD to ignore the lid switch in any context

# This is the file we are working on
FileName="/etc/systemd/logind.conf"
# For Debug the directory must be hardcoded
#FileName="/home/flint/Tmp/logind.conf"

# Details of the file contents can be found here
# https://www.freedesktop.org/software/systemd/man/logind.conf.html

# Make a backup
echo "Making a backup of ${FileName}"
sudo cp "${FileName}" "${FileName}.original"

# Add our special modifications
echo "Updating ${FileName} with Lid Switch modifications"
# DO NOT TOUCH ANY TEXT BETWEEN THESE ARROWS ---->>>>
sudo sh -c "cat << EOF >> ${FileName}

# Flint's ZBook and any Laptop lid switch tweaks
HandleLidSwitch=ignore
HandleLidSwitchDocked=ignore
HandleLidSwitchExternalPower=ignore

EOF"
# <<<<---- DO NOT TOUCH ANY TEXT BETWEEN THESE ARROWS

# Done
echo "Done"

