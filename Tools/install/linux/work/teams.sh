#!/bin/bash
# -----------------------------------------------------------------
# Install Microsoft Teams on Linux
# There have been many methods used by Microsoft over the years
# They are captured here


# -----------------------------------------------------------------
# Method 1 - Repo
# Broken as of 2022
# The official method of installing Teams is through a deb or rpm
# https://learn.microsoft.com/en-us/microsoftteams/get-clients?tabs=Linux
#pushd "${HOME}/Downloads"
#curl https://packages.microsoft.com/keys/microsoft.asc | sudo gpg --dearmor -o /usr/share/keyrings/microsoft-archive-keyring.gpg
#sudo sh -c 'echo "deb [arch=amd64 signed-by=/usr/share/keyrings/microsoft-archive-keyring.gpg] https://packages.microsoft.com/repos/ms-teams stable main" > /etc/apt/sources.list.d/teams.list'
#sudo apt update
#sudo apt install teams
#popdM


# -----------------------------------------------------------------
# Method 2 - Deb download
# Broken as of 2022
# https://www.microsoft.com/en-us/microsoft-teams/download-app
# Download the deb package
#pushd "${HOME}/Downloads"
#wget ....
# sudo apt install ./teams*.deb
#popd


# -----------------------------------------------------------------
# Method 3 - PWA
# Doesn't work well
# As of 2022 Q4, Teams is distributed as a PWA (Progressive Web App)
# https://web.dev/progressive-web-apps/
# Instructions:
# Use a PWA-friendly browser
# Navigate to https://teams.microsoft.com/
# Log in
# An install prompt might appear


# -----------------------------------------------------------------
# Method 4 - Unofficial Teams client teams-for-linux from Ismael Martinez
# Working well since 2022
# https://github.com/IsmaelMartinez/teams-for-linux


# -----------------------------------------------------------------
# Method 4A - Repository
# https://teamsforlinux.de/

# This is the DEB method
Method4A(){
sudo mkdir -p /etc/apt/keyrings
sudo wget -qO /etc/apt/keyrings/teams-for-linux.asc https://repo.teamsforlinux.de/teams-for-linux.asc
echo "deb [signed-by=/etc/apt/keyrings/teams-for-linux.asc arch=$(dpkg --print-architecture)] https://repo.teamsforlinux.de/debian/ stable main" | sudo tee /etc/apt/sources.list.d/teams-for-linux-packages.list
sudo apt update
sudo apt install teams-for-linux

# There is a YUM method
}

# -----------------------------------------------------------------
# Method 4B - Container (Flatpak/Snap)
# sudo flatpak install flathub com.github.IsmaelMartinez.teams_for_linux
# sudo snap install teams-for-linux
# Below is code for automating the process
Method4B(){
Distro_ID=$(lsb_release --id --short | tr "[:upper:]" "[:lower:]")
case $Distro_ID in
  'debian')
    echo "Debian.  Installing flatpak"
    sudo flatpak install flathub com.github.IsmaelMartinez.teams_for_linux
    ;;
  'ubuntu')
    echo "Ubuntu.  Installing snap"
    sudo snap install teams-for-linux
    ;;
  *)
    echo "This distro is called $Distro_ID"
    echo "This is neither Debian nor Ubuntu"
    echo "I will attempt to install the flatpak"
    sudo flatpak install flathub com.github.IsmaelMartinez.teams_for_linux
    ;;
esac
}


# -----------------------------------------------------------------
# Call the preferred method here
Method4A
# Method4B


