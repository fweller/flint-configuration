# VM-Qemu-Shared-Folders

Below are two sections
- On Linux Host
- On Windows Client


## On Linux Host

### Install Samba and Firewall on the Host
```
sudo apt update
sudo apt install samba ufw
```

### Configure Shared Directories
```
sudo chown -R flint:flint /home/flint/Work
sudo chmod -R 0777 /home/flint/Work
sudo chown -R flint:flint /home/flint/VMShare
sudo chmod -R 0777 /home/flint/VMShare
sudo chown -R flint:flint /mnt/zdata
sudo chmod -R 0777 /mnt/zdata
```

### Configure Samba 
Edit the Samba config file
```
sudo vim /etc/samba/smb.conf
```
Add the following section at the end of the file:
```
[Work]
   path = /home/flint/Work
   writable = yes
   browsable = yes
   guest ok = no
   create mask = 0775
   directory mask = 0775
   valid users = flint

[VMShare]
   path = /home/flint/VMShare
   writable = yes
   browsable = yes
   guest ok = no
   create mask = 0775
   directory mask = 0775
   valid users = flint

[ZData]
   path = /mnt/zdata
   writable = yes
   browsable = yes
   guest ok = no
   create mask = 0775
   directory mask = 0775
   valid users = flint
```

### Create a Samba User
Create a Samba user with the following command:
```
sudo smbpasswd -a flint
```
Enter a password when prompted.

### Restart Samba Services
```
sudo systemctl restart smbd
sudo systemctl enable smbd
```

### Allow Samba Traffic Through Firewall (if applicable)
If you have a firewall enabled, allow Samba traffic:
```
sudo ufw allow samba
```

## On Windows Client

### Access the Shared Folder from the Windows Guest
1. Open **File Explorer** in the Windows VM.
2. Type `\\<host-ip>\Work` in the address bar and press Enter.
   - Replace `<host-ip>` with the IP address of your Debian host (use `ip addr` to find it).
3. Enter your Samba username and password when prompted.
4. Optionally, map the network share as a network drive.


