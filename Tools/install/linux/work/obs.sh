#!/bin/bash
# https://obsproject.com/wiki/install-instructions#linux

# OBS officially supports 2 methods of installation
# 1. Flatpak
flatpak install flathub com.obsproject.Studio
# 2. Ubuntu PPA
#sudo add-apt-repository ppa:obsproject/obs-studio
#sudo apt update
#sudo apt install obs-studio

# Unofficial builds
# 3. SnapCraft - last updated in 2021
# https://obsproject.com/wiki/unofficial-linux-builds
# sudo snap install obs-studio


