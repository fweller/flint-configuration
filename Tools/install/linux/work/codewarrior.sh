#!/bin/bash

# This script will install codewarrior and the license file

# Download folder
Downloads="${HOME}/Downloads"

# CodeWarrior Installer
CW_File_Name="CW_ARMv8_v2020.06_b200629GA_Linux_Offline.tar"
CW_File_Folder="${HOME}/Documents/NXP/SoftwareOS/CodeWarrior/CodeWarriorARMv8-Software"

# CodeWarrior License
CW_License_Name="license.dat"
CW_License_Folder="${HOME}/Documents/NXP/SoftwareOS/CodeWarrior/License-DLA-LS-SPLST-NL_CWPKU90593"
CW_License_Install="/opt/Freescale/CW4NET_v2020.06/Common"


# Instructions for downloading the installer for the first time
# Log into NXP
# https://www.nxp.com/security/login
# Navigate to Software Licensing and Support
# https://www.nxp.com/webapp/swlicensing/sso/dologin.sp
# Access to Software, Product List, NXP Software
# CodeWarrior Networked Suite - Specialist Level
# CodeWarrior for QorIQ LS Series ARMv8 ISA, version 11.5.12
# https://nxp.flexnetoperations.com/control/frse/download?element=13904417
# Product Download
#  	CodeWarrior for ARMv8 v11.5.0 b200629 Linux Offline Installer

# Prepare the system
# Note, selecting 'libgcc-s1:i386' instead of 'libgcc1:i386'
sudo apt -y install libc6:i386 libgcc-s1:i386 libstdc++5:i386 libstdc++6:i386 \
  libgtk2.0-0:i386 libxpm4:i386 libusb-0.1-4:i386 libusb-1.0-0:i386 \
  libuuid1:i386 libxcb1:i386 libnspr4:i386 libglib2.0-0:i386 \
  libxdamage1:i386 libxtst6:i386 libtinfo5 python2-minimal libcanberra-gtk-module

# Copy both the installer and the license file to Downloads directory
cp "${CW_File_Folder}/${CW_File_Name}" "${Downloads}"
cp "${CW_License_Folder}/${CW_License_Name}" "${Downloads}"
# Both are now located in $Downloads

pushd "${Downloads}"

# Extract the installer
tar -xf "${Downloads}/${CW_File_Name}" -C "${Downloads}"

# Enter the directory
cd disk1

# Run the installer as root
sudo ./setuplinux.sh

# Back up the existing license file
sudo mv "${CW_License_Install}/license.dat" "${CW_License_Install}/license.dat.bak"

# Copy over the new license file
sudo cp "${Downloads}/${CW_License_Name}" "${CW_License_Install}/license.dat"
sudo chown root "${CW_License_Install}/license.dat"
sudo chown root "${CW_License_Install}/license.dat"
sudo chmod 664 "${CW_License_Install}/license.dat"

# Create symlink
ln "${HOME}/Tools/scripts/work/codewarrior.sh" "${HOME}/Bin/codewarrior"

