#!/bin/bash
## For ZBook
## Tell X11 what the preferred monitor configuration is

# Variables
SourceDir="${HOME}/Tools/install/linux/work/xorg.conf"
DestDir="/etc/X11"
FileName="xorg.conf"
SourceFile="${SourceDir}/${FileName}.3monitors"
DestFile="${DestDir}/${FileName}"

# There are 3 monitor configurations
#   xorg.conf.1monitor
#   xorg.conf.2monitors
#   xorg.conf.3monitors
# I plan on defaulting to the 3monitor config as it seems to handle every
# situation

# Make a backup of the original if there is one
echo "Testing if ${DestFile} exists, and if it does, to make a backup"
test -f "${DestFile}" && sudo mv "${DestFile}" "${DestFile}.backup"

# Copy monitor configuration
echo "Copying ${SourceFile} to ${DestFile}"
sudo cp "${SourceFile}" "${DestFile}"

# Done
echo "Done"

