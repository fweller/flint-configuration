#!/bin/bash
# ----------------------------------------
# NAME        : directories.sh
# DESCRIPTION : Creates a preferred directory structure in the user's $HOME directory.
# ----------------------------------------

# ----------------------------------------
# Function: Command
# Description: Executes a given command and prints it to the console.
Command() {
  echo ""
  echo "COMMAND : $1"
  eval "$1"

  # Check if the command was successful
  if [ $? -ne 0 ]; then
    echo "ERROR   : Command failed - $1"
    exit 1 # Exit if command fails
  fi
}

# ----------------------------------------
# Start of the script

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : $(date) - Creating directory structure in $HOME"

# ----------------------------------------
# Create the preferred directories in $HOME

directories=(
  "Applications"
  "Backups"
  "Builds"
  "Documents"
  "Downloads"
  "Engineering"
  "Logs"
  "Obsidian"
  "Projects"
  "Personal"
  "Tmp"
  "VMs"
  "VMShare"
  "Work"
)

for dir in "${directories[@]}"; do
  Command "mkdir -p $HOME/$dir"
done

# ----------------------------------------
# Finish

echo ""
echo "END     : $(date) - Directory structure created in $HOME"
echo "SPACER  : ----------------------------------------"
exit 0 # Success

