#!/bin/bash
# ----------------------------------------
# NAME        : packages-desktop.sh
# DESCRIPTION : Installs preferred desktop applications.
# ----------------------------------------

# Fedora so far
yakuake vim-X11

# Debian
yakuake vim-gtk3

# ----------------------------------------
# Package name variables
# Try to be cognizant of differences in package names between distros
Packages="git" # Default
PackagesApt=$Packages
PackagesDnf=$Packages
PackagesZypper=$Packages
PackagesPacman=$Packages


# ----------------------------------------
# Function: Command
# Description: Executes a given command, prints it, and captures the output.
Command() {
  echo ""
  echo "COMMAND : $1"
  echo ""
  Output=$($1)
  echo ""
  echo "RETURN  : $Output"
}


# ----------------------------------------
# Install Dependencies

# Detect package manager and install packages
if command -v apt >/dev/null 2>&1; then
    sudo apt update && sudo apt install -y "$PackagesApt"
elif command -v dnf >/dev/null 2>&1; then
    sudo dnf install -y "$PackagesDnf"
elif command -v zypper >/dev/null 2>&1; then
    sudo zypper install -y "$PackagesZypper"
elif command -v pacman >/dev/null 2>&1; then
    sudo pacman -Syu --noconfirm "$PackagesPacman"
else
    echo "Package manager not supported."
    exit 1
fi



# ----------------------------------------
# Function: Installer
# Description: Installs the specified packages using the package installer script.
Installer() {
  Command "$PackageInstaller $1"
}

# ----------------------------------------
# Start of the script

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : $(date) - Installing console applications"


