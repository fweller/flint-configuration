## Instructions

# GitLab Project
https://gitlab.com/fweller/flint-configuration

# Debian
su -c 'apt install wget' && mkdir -p ${HOME}/Downloads && cd ${HOME}/Downloads && wget https://gitlab.com/fweller/flint-configuration/-/raw/master/Tools/install/linux/tools.sh && chmod +x tools.sh && ./tools.sh && cd ${HOME}/Tools/install/linux

# Ubuntu
sudo apt -y install wget && mkdir -p ${HOME}/Downloads && cd ${HOME}/Downloads && wget https://gitlab.com/fweller/flint-configuration/-/raw/master/Tools/install/linux/tools.sh && chmod +x tools.sh && ./tools.sh && cd ${HOME}/Tools/install/linux

# Need to create a script that does this
su -c 'apt install wget'
mkdir -p "${HOME}/Downloads"
cd "${HOME}/Downloads"
wget https://gitlab.com/fweller/flint-configuration/-/raw/master/Tools/install/linux/tools.sh
chmod +x tools.sh
./tools.sh
cd "${HOME}/Tools/install/linux"
./install.sh


