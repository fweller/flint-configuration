#!/bin/bash
# ----------------------------------------
# NAME        : hw-probe.sh
# DESCRIPTION : Probes hardware during installation
# ----------------------------------------


# ----------------------------------------
# Command and Print

Command() {
  echo ""
  echo "COMMAND : $1"
  eval "$1"
}


# ----------------------------------------
# Say hello

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : Running hw-probe"

Command "sudo -E hw-probe -all -upload"

# ----------------------------------------
# Finish

echo ""
echo "END     : Creating directory structure in $HOME"
echo "SPACER  : ----------------------------------------"
exit 0 # Success

