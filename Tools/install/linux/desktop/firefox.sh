#!/bin/bash
# ----------------------------------------
# NAME        : firefox.sh
# DESCRIPTION : Downloads and installs Firefox on a Debian or Ubuntu system.
# ----------------------------------------

cat <<'EOF'
Instructions for Installing Firefox

https://support.mozilla.org/en-US/kb/install-firefox-linux

Debian:
sudo apt install firefox-esr

Ubuntu:
sudo snap install firefox

Any Distro:
sudo flatpak install -y flathub org.mozilla.firefox
EOF


