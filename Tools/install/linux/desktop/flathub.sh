#!/bin/sh
# ----------------------------------------
# NAME        : install-flathub.sh
# DESCRIPTION : Installs Flatpak and Flathub support on a Linux system.
#               Configures the system for decent GTK and Qt theme support.
# ----------------------------------------

# ----------------------------------------
# Function: Command
# Description: Executes a given command, prints it, and handles errors.
Command() {
  echo ""
  echo "COMMAND: $1"
  echo ""
  if ! eval "$1"; then
    echo "ERROR: Command failed - $1"
    exit 1
  fi
}

# ----------------------------------------
# Start of the script

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : $(date) - Installing Flatpak and configuring Flathub"

# ----------------------------------------
# Install Flatpak and backend support for Plasma Discover

Command "sudo apt -y install flatpak plasma-discover-backend-flatpak"

# ----------------------------------------
# Add Flathub as a Flatpak remote repository

Command "flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo"

# ----------------------------------------
# Optional: Install GTK theme support for Flatpak applications

# Uncomment the following lines to install the Breeze GTK theme and apply it to Flatpak applications
# Command "flatpak install -y --noninteractive --system org.gtk.Gtk3theme.Breeze"
# Command "flatpak override --filesystem=xdg-config/gtk-3.0:ro"

# ----------------------------------------
# Finish

echo ""
echo "END     : $(date) - Flathub installation and configuration completed"
echo "SPACER  : ----------------------------------------"

exit 0 # Success

