#!/bin/bash
## Qalculate - Desktop calculator
# https://qalculate.github.io/

Flathub()
{
# Flathub
# Qalculate! (Qt UI)
sudo flatpak -y install flathub io.github.Qalculate.qalculate-qt
# Qalculate! (GTK UI)
# sudo flatpak -y install flathub io.github.Qalculate
}

Snapcraft()
{
# SnapCraft
# Qalculate! (Qt)
sudo snap -y install qalculate-qt
# Qalculate! (GTK GUI)
#sudo snap -y install qalculate
}


CheckDistro()
{
  echo "Is this Debian or Ubuntu?"

  Distro_ID=$(lsb_release --id --short | tr "[:upper:]" "[:lower:]")

  case $Distro_ID in
    'debian')
      echo "This is a Debian distribution."
      Flathub
      ;;
    'ubuntu')
      echo "This is an Ubuntu distribution."
      Snapcraft
      ;;
    *)
      echo "This distro is called $Distro_ID"
      echo "This is neither Debian nor Ubuntu"
      echo "Use at your own risk"
      echo "Continuing..."
      Flathub
      ;;
  esac
}

CheckDistro

