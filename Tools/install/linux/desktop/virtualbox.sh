#!/bin/bash
# ----------------------------------------
# NAME        : install-virtualbox.sh
# DESCRIPTION : Installs VirtualBox on Debian, Ubuntu, or other Linux distributions.
#               Provides instructions for Debian and Ubuntu repositories and handles direct installation from VirtualBox.
# ----------------------------------------
# REFERENCE   :
#               - https://wiki.debian.org/VirtualBox
#               - https://www.virtualbox.org/wiki/Linux_Downloads
# ----------------------------------------

# ----------------------------------------
# Debian Instructions

# Debian has VirtualBox available in:
#   - The Unstable "Sid" repository
#   - The Fast Track for Stable repository
# Debian repo info: https://wiki.debian.org/VirtualBox

# For Testing, add the following line for Sid access to /etc/apt/sources.list.d/debian.list:
#   deb https://deb.debian.org/debian sid main contrib non-free non-free-firmware

# For Stable, add Fast-Track:
#   sudo apt install fasttrack-archive-keyring
# Add Fast-Track to /etc/apt/sources.list.d/debian.list:
#   deb https://fasttrack.debian.net/debian-fasttrack/ bookworm-fasttrack main contrib
#   deb https://fasttrack.debian.net/debian-fasttrack/ bookworm-backports-staging main contrib

# ----------------------------------------
# Ubuntu Instructions

# Ubuntu has VirtualBox available in the Multiverse repository:
#   sudo add-apt-repository multiverse
# More info: https://packages.ubuntu.com/source/noble/virtualbox

# ----------------------------------------
# Installation Command

# To install VirtualBox on Debian/Ubuntu:
#   sudo apt update && sudo apt install virtualbox virtualbox-ext-pack virtualbox-dkms virtualbox-qt

# ----------------------------------------

# ----------------------------------------
# Set up directories in HOME

Command() {
  echo ""
  echo "COMMAND: $1"
  echo ""
  if ! eval "$1"; then
    echo "ERROR: Command failed - $1"
    exit 1
  fi
}

Command "mkdir -p \"${HOME}/VMs\""
Command "mkdir -p \"${HOME}/VMShare\""

# ----------------------------------------
# Exit for Debian, Ubuntu, and Mint

exit 0

# ----------------------------------------
# For other distributions or direct download from VirtualBox, continue
# Note: VirtualBox does not offer the DKMS kernel module in the direct binaries

# Update these 2 variables as needed
vbversion="7.0.10"  # Version number
vbcode="158379"     # Code used with downloading files

# These variables shouldn't need to be updated, but if things break, check here first
vbname="VirtualBox-${vbversion}-${vbcode}-Linux_amd64.run"
vbextname="Oracle_VM_VirtualBox_Extension_Pack-${vbversion}.vbox-extpack"

# ----------------------------------------
# Start Installation

Command "pushd \"${HOME}/Downloads\""

# Clean up earlier installations
Command "rm -f VirtualBox* Oracle*-extpack"

# Download application
echo ""; echo "1. Download the latest binaries from VirtualBox"
echo "   https://www.virtualbox.org/wiki/Linux_Downloads"
Command "wget https://download.virtualbox.org/virtualbox/$vbversion/$vbname"
Command "chmod +x $vbname"

# Download extension pack
echo ""; echo "2. Download the Oracle VM VirtualBox Extension Pack"
echo "   https://www.virtualbox.org/wiki/Downloads"
Command "wget https://download.virtualbox.org/virtualbox/$vbversion/$vbextname"

# Uninstall earlier installations
echo ""; echo "3. Uninstall the previous binary"
Command "sudo ./$vbname uninstall"

# Install application
echo ""; echo "4. Install the binary"
Command "sudo ./$vbname"

# Install extension pack
echo ""; echo "5. Install the Extension Pack"
Command "sudo VBoxManage extpack install --replace $vbextname"

# Done
Command "popd"

# ----------------------------------------
# Finish

echo ""
echo "END     : $(date) - VirtualBox installation completed"
echo "SPACER  : ----------------------------------------"

exit 0 # Success

