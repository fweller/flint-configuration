#!/bin/bash
# ----------------------------------------
# NAME        : install-x2go.sh
# DESCRIPTION : Guides the user through installing X2Go on Debian or Ubuntu.
#               Adds the X2Go PPA for Ubuntu systems.
# ----------------------------------------
# REFERENCE   :
#               - https://wiki.x2go.org/
# ----------------------------------------

# ----------------------------------------
# Function: Command
# Description: Executes a given command, prints it, and handles errors.
Command() {
  echo ""
  echo "COMMAND: $1"
  echo ""
  if ! eval "$1"; then
    echo "ERROR: Command failed - $1"
    exit 1
  fi
}

# ----------------------------------------
# Start of the script

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : $(date) - Configuring X2Go installation"

# ----------------------------------------
# Check if the system is Debian or Ubuntu

echo "Checking if the system is Debian or Ubuntu..."
Distro_ID=$(lsb_release --id --short | tr "[:upper:]" "[:lower:]")

case $Distro_ID in
  'debian')
    echo "This is a Debian distribution."
    ;;
  'ubuntu')
    echo "This is an Ubuntu distribution."
    Command "sudo add-apt-repository -y ppa:x2go/stable"
    ;;
  *)
    echo "WARNING: Unsupported distribution: $Distro_ID"
    echo "This is neither Debian nor Ubuntu. Use at your own risk. Continuing..."
    ;;
esac

# ----------------------------------------
# Update package lists

Command "sudo apt update"

# ----------------------------------------
# Instructions for installing X2Go server and client

echo ""; echo "To install the X2Go server:"
echo "COMMAND: sudo apt install -y x2goserver x2goserver-xsession"

echo ""; echo "To install the X2Go client:"
echo "COMMAND: sudo apt install -y x2goclient"

# ----------------------------------------
# Usage instructions

echo ""; echo "Remember to use the proper command to start the KDE session:"
echo "startplasma-x11"
echo "startplasma-wayland"

# ----------------------------------------
# Finish

echo ""
echo "END     : $(date) - X2Go configuration script completed"
echo "SPACER  : ----------------------------------------"

exit 0 # Success

