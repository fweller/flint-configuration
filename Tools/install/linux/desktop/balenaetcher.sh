#!/bin/bash
# ----------------------------------------
# NAME        : install-balena-etcher.sh
# DESCRIPTION : Downloads and installs Balena Etcher as an AppImage.
#               Balena Etcher is used to flash OS images to SD cards & USB drives.
# ----------------------------------------

# ----------------------------------------
# Variables

DownloadDir="${HOME}/Downloads"
ApplicationDir="${HOME}/Applications"
#Url="https://github.com/balena-io/etcher/releases/download/v1.19.21/balenaEtcher-1.19.21-x64.AppImage"
#https://github.com/balena-io/etcher/releases/download/v1.19.21/balenaEtcher-linux-x64-1.19.21.zip
Url="https://github.com/balena-io/etcher/releases/download/v1.19.21/balena-etcher_1.19.21_amd64.deb"

# ----------------------------------------
# Function: Command
# Description: Executes a given command, prints it, and handles errors.
Command() {
  echo ""
  echo "COMMAND: $1"
  echo ""
  if ! eval "$1"; then
    echo "ERROR: Command failed - $1"
    exit 1
  fi
}

# ----------------------------------------
# Start of the script

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : $(date) - Installing Balena Etcher"

# ----------------------------------------
# Prepare the download and application directories

Command "mkdir -p \"${DownloadDir}\""
Command "mkdir -p \"${ApplicationDir}\""
Command "pushd \"${DownloadDir}\" || exit"

# ----------------------------------------
# Download the Balena Etcher AppImage

Command "wget \"${Url}\""

# ----------------------------------------
# Move the downloaded AppImage to the application directory

#Command "cp balena*.AppImage \"${ApplicationDir}\""

# ----------------------------------------
# Install the Deb package

Command "sudo apt install ./balena*" 

# ----------------------------------------
# Return to the previous directory

Command "popd || exit"

# ----------------------------------------
# Finish

echo ""
echo "END     : $(date) - Balena Etcher installation completed"
echo "SPACER  : ----------------------------------------"

exit 0 # Success

