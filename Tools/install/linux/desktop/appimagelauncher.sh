#!/bin/bash
# ----------------------------------------
# NAME        : install-appimagelauncher.sh
# DESCRIPTION : Downloads and installs AppImageLauncher on a Debian or Ubuntu system.
#               AppImageLauncher integrates AppImages into the system.
# ----------------------------------------
# REFERENCES  :
#               - https://github.com/TheAssassin/AppImageLauncher/
#               - https://assassinate-you.net/tags/appimagelauncher/
# ----------------------------------------

# ----------------------------------------
# Variables

DownloadDir="${HOME}/Downloads"
App="appimagelauncher_2.2.0-travis995.0f91801.bionic_amd64.deb"
Url="https://github.com/TheAssassin/AppImageLauncher/releases/download/v2.2.0/${App}"

# ----------------------------------------
# Function: Command
# Description: Executes a given command, prints it, and handles errors.
Command() {
  echo ""
  echo "COMMAND: $1"
  echo ""
  if ! eval "$1"; then
    echo "ERROR: Command failed - $1"
    exit 1
  fi
}

# ----------------------------------------
# Start of the script

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : $(date) - Installing AppImageLauncher"

# ----------------------------------------
# Prepare the download directory

Command "mkdir -p \"${DownloadDir}\""
Command "pushd \"${DownloadDir}\" || exit"

# ----------------------------------------
# Download the AppImageLauncher package

Command "wget \"${Url}\""

# ----------------------------------------
# Install the downloaded package

Command "sudo apt install -y ./${App}"

# ----------------------------------------
# Return to the previous directory

Command "popd || exit"

# ----------------------------------------
# Finish

echo ""
echo "END     : $(date) - AppImageLauncher installation completed"
echo "SPACER  : ----------------------------------------"

exit 0 # Success

