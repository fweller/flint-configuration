#!/bin/bash
# ----------------------------------------
# NAME        : install-solaar.sh
# DESCRIPTION : Installs and runs Solaar, a Linux device manager for Logitech Unifying Receivers and Devices.
# ----------------------------------------

# ----------------------------------------
# Function: Command
# Description: Executes a given command, prints it, and handles errors.
Command() {
  echo ""
  echo "COMMAND: $1"
  echo ""
  if ! eval "$1"; then
    echo "ERROR: Command failed - $1"
    exit 1
  fi
}

# ----------------------------------------
# Variables

# Log file for Solaar
LogFile="${HOME}/Logs/solaar.log"

# ----------------------------------------
# Start of the script

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : $(date) - Installing and running Solaar"

# ----------------------------------------
# Install Solaar from the repository

Command "sudo apt install -y solaar"

# ----------------------------------------
# Run Solaar and log output

# Ensure the Logs directory exists
Command "mkdir -p \"${HOME}/Logs\""

# Start Solaar and redirect output to the log file
Command "nohup solaar &>> \"${LogFile}\" &"

# ----------------------------------------
# Finish

echo ""
echo "END     : $(date) - Solaar installation and execution completed"
echo "SPACER  : ----------------------------------------"

exit 0 # Success

