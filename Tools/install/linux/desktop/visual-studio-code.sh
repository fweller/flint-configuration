#!/bin/bash
# ----------------------------------------
# NAME        : install-vscode.sh
# DESCRIPTION : Downloads and installs Visual Studio Code on a Debian or Ubuntu system.
#               The script uses the official package from Microsoft.
# ----------------------------------------
# REFERENCE   :
#               - https://code.visualstudio.com/Download
# ----------------------------------------

# ----------------------------------------
# Variables

DownloadDir="${HOME}/Downloads"
Url="https://code.visualstudio.com/sha/download?build=stable&os=linux-deb-x64"
App="code.deb"

# ----------------------------------------
# Function: Command
# Description: Executes a given command, prints it, and handles errors.
Command() {
  echo ""
  echo "COMMAND: $1"
  echo ""
  if ! eval "$1"; then
    echo "ERROR: Command failed - $1"
    exit 1
  fi
}

# ----------------------------------------
# Start of the script

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : $(date) - Installing Visual Studio Code"

# ----------------------------------------
# Prepare the download directory

Command "mkdir -p \"${DownloadDir}\""
Command "pushd \"${DownloadDir}\" || exit"

# ----------------------------------------
# Clean up previous downloads

Command "rm -f \"code*.deb\""

# ----------------------------------------
# Download the Visual Studio Code package

Command "curl -L \"$Url\" -o \"$App\""

# ----------------------------------------
# Install the downloaded package

Command "sudo apt install -y \"./$App\""

# ----------------------------------------
# Return to the previous directory

Command "popd || exit"

# ----------------------------------------
# Finish

echo ""
echo "END     : $(date) - Visual Studio Code installation completed"
echo "SPACER  : ----------------------------------------"

exit 0 # Success

