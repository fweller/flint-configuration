#!/bin/bash
# ----------------------------------------
# NAME        : chrome.sh
# DESCRIPTION : Downloads and installs Google Chrome on a Debian or Ubuntu system.
# ----------------------------------------

# ----------------------------------------
# Variables

DownloadDir="${HOME}/Downloads"
App="google-chrome-stable_current_amd64.deb"
Url="https://dl.google.com/linux/direct/${App}"

# ----------------------------------------
# Function: Command
# Description: Executes a given command, prints it, and handles errors.
Command() {
  echo ""
  echo "COMMAND: $1"
  echo ""
  if ! eval "$1"; then
    echo "ERROR: Command failed - $1"
    exit 1
  fi
}

# ----------------------------------------
# Start of the script

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : $(date) - Installing Google Chrome"

# ----------------------------------------
# Prepare the download directory

Command "mkdir -p \"${DownloadDir}\""
Command "pushd \"${DownloadDir}\" || exit"

# ----------------------------------------
# Download the Google Chrome package

Command "wget \"${Url}\""

# ----------------------------------------
# Install the downloaded package

Command "sudo apt install -y \"./${App}\""

# ----------------------------------------
# Return to the previous directory

Command "popd || exit"

# ----------------------------------------
# Finish

echo ""
echo "END     : $(date) - Google Chrome installation completed"
echo "SPACER  : ----------------------------------------"

exit 0 # Success

