#!/bin/bash
# ----------------------------------------
# NAME        : install-qemu-kvm.sh
# DESCRIPTION : Installs QEMU KVM for GUI-capable virtual machines.
#               The script provides guidance for installing the necessary packages and setting up the environment.
# ----------------------------------------
# REFERENCE   :
#               - https://wiki.debian.org/KVM
#               - https://packages.debian.org/bookworm/qemu-kvm
#               - https://packages.debian.org/trixie/virt-manager
#               - https://packages.debian.org/trixie/virt-viewer
#               - https://packages.debian.org/bookworm/virtinst
#               - https://packages.debian.org/trixie/libvirt-daemon-system
#               - https://packages.debian.org/trixie/libvirt-daemon
#               - https://packages.debian.org/bookworm/libvirt-clients
#               - https://packages.debian.org/trixie/bridge-utils
#               - https://packages.debian.org/bookworm/libosinfo-bin
# ----------------------------------------

# ----------------------------------------
# Warning Message

#echo "WARNING: It is recommended to run the installation manually due to complexity."
#exit 0

# ----------------------------------------
# Function: Command
# Description: Executes a given command, prints it, and handles errors.
Command() {
  echo ""
  echo "COMMAND: $1"
  echo ""
  if ! eval "$1"; then
    echo "ERROR: Command failed - $1"
    exit 1
  fi
}

# ----------------------------------------
# Start of the script

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : $(date) - Installing QEMU KVM and related tools"

# ----------------------------------------
# Install the necessary packages

#Command "sudo apt update"
Command "sudo apt install bridge-utils libvirt-clients libvirt-daemon-system \
  ovmf qemu-kvm qemu-system-gui qemu-utils spice-client-gtk virt-manager virt-viewer"

# bridge-utils: Utilities for configuring network bridge devices, useful for VM networking.
# libvirt-clients: Command-line utilities for managing virtual machines.
# libvirt-daemon-system: The libvirtd service for managing virtual machines.
# ovmf: Provides UEFI firmware support for QEMU virtual machines.
# qemu-kvm: Provides QEMU and KVM support for virtualization.
# qemu-system-gui: Provides GUI modules for GTK and SDL, necessary for graphical output.
# qemu-utils: Includes qemu-img and other utilities for managing QEMU disk images.
# spice-client-gtk: Adds SPICE support, allowing enhanced graphical performance, video streaming, audio, and USB redirection in VMs.
# virt-manager: Graphical interface for managing virtual machines.
# virt-viewer: Lightweight application for connecting to virtual machine consoles, useful if you prefer a dedicated viewer.

#Command "sudo apt install -y qemu-kvm qemu-system virt-manager virt-viewer virtinst libosinfo-bin libvirt-daemon libvirt-daemon-system libvirt-clients bridge-utils"

# ----------------------------------------
# Enable and start the libvirtd service:

Command "sudo systemctl enable libvirtd"
Command "sudo systemctl start libvirtd"

# ----------------------------------------
#Add your user to the libvirt and kvm groups (optional, but recommended for managing VMs without sudo):

Command "sudo usermod -aG libvirt,kvm $USER"

# ----------------------------------------
# Verification


Command "virsh list --all"


# ----------------------------------------
# Add the current user to the libvirt and kvm groups

#Command "sudo usermod -aG libvirt $(whoami)"
#Command "sudo usermod -aG kvm $(whoami)"

# ----------------------------------------
# Enable and start the libvirtd service

#Command "sudo systemctl enable libvirtd"
#Command "sudo systemctl start libvirtd"
#Command "sudo systemctl status libvirtd"

# ----------------------------------------
# Finish

echo ""
echo "END     : $(date) - QEMU KVM installation and configuration completed"
echo "SPACER  : ----------------------------------------"

exit 0 # Success
exit

# ----------------------------------------
# Debian instructions: https://wiki.debian.org/KVM

# qemu-kvm
# https://packages.debian.org/bookworm/qemu-kvm

# virt-manager
# https://packages.debian.org/trixie/virt-manager

# virt-viewer
# https://packages.debian.org/trixie/virt-viewer

# virtinst
# https://packages.debian.org/bookworm/virtinst

# libvirt-daemon-system
# More commonly used
# https://packages.debian.org/trixie/libvirt-daemon-system

# libvirt-daemon
# Only used on Trixie and newer
# https://packages.debian.org/trixie/libvirt-daemon

# libvirt-clients
# https://packages.debian.org/bookworm/libvirt-clients

# bridge-utils
# https://packages.debian.org/trixie/bridge-utils

# libosinfo-bin
# https://packages.debian.org/bookworm/libosinfo-bin

