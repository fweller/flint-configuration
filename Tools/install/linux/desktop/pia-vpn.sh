#!/bin/bash
# ----------------------------------------
# NAME        : install-pia-vpn.sh
# DESCRIPTION : Downloads and installs the PIA (Private Internet Access) VPN client on Linux.
# ----------------------------------------
# REFERENCE   :
#               - https://www.privateinternetaccess.com/download
# ----------------------------------------

# ----------------------------------------
# Variables

DownloadDir="${HOME}/Downloads"
App="pia-linux-3.5.7-08120.run"
Url="https://installers.privateinternetaccess.com/download/${App}"

# ----------------------------------------
# Function: Command
# Description: Executes a given command, prints it, and handles errors.
Command() {
  echo ""
  echo "COMMAND: $1"
  echo ""
  if ! eval "$1"; then
    echo "ERROR: Command failed - $1"
    exit 1
  fi
}

# ----------------------------------------
# Start of the script

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : $(date) - Installing PIA (Private Internet Access) VPN"

# ----------------------------------------
# Prepare the download directory

Command "mkdir -p \"${DownloadDir}\""
Command "pushd \"${DownloadDir}\" || exit"

# ----------------------------------------
# Download the PIA installer

Command "wget \"${Url}\""

# ----------------------------------------
# Make the installer executable

Command "chmod +x \"${App}\""

# ----------------------------------------
# Install the PIA VPN client

#Command "sudo \"./${App}\""
Command "./${App}"

# ----------------------------------------
# Return to the previous directory

Command "popd || exit"

# ----------------------------------------
# Finish

echo ""
echo "END     : $(date) - PIA VPN installation completed"
echo "SPACER  : ----------------------------------------"

exit 0 # Success

