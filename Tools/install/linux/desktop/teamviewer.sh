#!/bin/sh
# Install teamviewer application on Debian Linux

# Variables
DownloadDir="${HOME}/Downloads"
App="teamviewer_amd64.deb"
Url="https://download.teamviewer.com/download/linux/${App}"

# Prepare
mkdir -p "${DownloadDir}"
pushd "${DownloadDir}" || exit

# Download and install
wget "${Url}" || exit
sudo apt install -y "./${App}"

# Exit
popd || exit

# Previous method
# cd "${HOME}/Downloads"
# wget https://download.teamviewer.com/download/linux/teamviewer_amd64.deb
# sudo apt install ./teamviewer_amd64.deb

