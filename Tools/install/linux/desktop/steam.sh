#!/bin/bash
# ----------------------------------------
# NAME        : install-steam.sh
# DESCRIPTION : Downloads and installs Steam on a Debian or Ubuntu system.
#               Includes notes for alternative installation methods.
# ----------------------------------------
# REFERENCE   :
#               - https://store.steampowered.com/about/
#               - https://wiki.debian.org/Steam
# ----------------------------------------

# ----------------------------------------
# Warning Message

echo "WARNING: It is recommended to run this manually until the installation details are finalized."
exit 0

# ----------------------------------------
# Variables

DownloadDir="${HOME}/Downloads"
Url="https://cdn.akamai.steamstatic.com/client/installer/steam.deb"
App="steam.deb"

# ----------------------------------------
# Function: Command
# Description: Executes a given command, prints it, and handles errors.
Command() {
  echo ""
  echo "COMMAND: $1"
  echo ""
  if ! eval "$1"; then
    echo "ERROR: Command failed - $1"
    exit 1
  fi
}

# ----------------------------------------
# Start of the script

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : $(date) - Installing Steam"

# ----------------------------------------
# Prepare the download directory

Command "mkdir -p \"${DownloadDir}\""
Command "pushd \"${DownloadDir}\" || exit"

# ----------------------------------------
# Download the Steam installer

Command "wget \"${Url}\""

# ----------------------------------------
# Install the Steam package

Command "sudo apt install -y \"./${App}\""

# ----------------------------------------
# Return to the previous directory

Command "popd || exit"

# ----------------------------------------
# Finish

echo ""
echo "END     : $(date) - Steam installation completed"
echo "SPACER  : ----------------------------------------"

exit 0 # Success

# ----------------------------------------
# Alternative Installation Methods

# Ubuntu - using snap
# sudo apt install steam

# Debian - detailed steps for setting up Steam on Debian
# Enable multi-arch
# sudo dpkg --add-architecture i386
# sudo apt update
# Install necessary libraries for AMD GPU and 32-bit support
# sudo apt install mesa-vulkan-drivers libglx-mesa0:i386 mesa-vulkan-drivers:i386 libgl1-mesa-dri:i386

# ------------------------------------------------
# end

