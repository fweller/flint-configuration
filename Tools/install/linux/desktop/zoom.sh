#!/bin/bash
# ----------------------------------------
# NAME        : install-zoom.sh
# DESCRIPTION : Downloads and installs Zoom on a Debian or Ubuntu system.
# ----------------------------------------
# REFERENCE   :
#               - https://zoom.us/download?os=linux
# ----------------------------------------

# ----------------------------------------
# Variables

Version="6.0.12.5501"
App="zoom_amd64.deb"
Url="https://zoom.us/client/${Version}/${App}"
DownloadDir="${HOME}/Downloads"

# ----------------------------------------
# Function: Command
# Description: Executes a given command, prints it, and handles errors.
Command() {
  echo ""
  echo "COMMAND: $1"
  echo ""
  if ! eval "$1"; then
    echo "ERROR: Command failed - $1"
    exit 1
  fi
}

# ----------------------------------------
# Start of the script

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : $(date) - Installing Zoom"

# ----------------------------------------
# Prepare the download directory

Command "mkdir -p \"${DownloadDir}\""
Command "pushd \"${DownloadDir}\" || exit"

# ----------------------------------------
# Download the Zoom package

Command "wget \"${Url}\""

# ----------------------------------------
# Install the downloaded package

Command "sudo apt install -y \"./${App}\""

# ----------------------------------------
# Return to the previous directory

Command "popd || exit"

# ----------------------------------------
# Finish

echo ""
echo "END     : $(date) - Zoom installation completed"
echo "SPACER  : ----------------------------------------"

exit

