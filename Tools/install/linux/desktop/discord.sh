#!/bin/bash
# https://discord.com/download
pushd "${HOME}/Downloads"
wget https://discord.com/api/download?platform=linux&format=deb
sudo apt install ./discord*.deb
popd

