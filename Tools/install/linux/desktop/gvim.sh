#!/bin/bash
# ----------------------------------------
# NAME        : install-gvim.sh
# DESCRIPTION : Installs GVim (Vi Improved for desktop) on a Debian or Ubuntu system.
#               GVim provides a graphical interface for Vim.
# ----------------------------------------

# ----------------------------------------
# Function: Command
# Description: Executes a given command, prints it, and handles errors.
Command() {
  echo ""
  echo "COMMAND: $1"
  echo ""
  if ! eval "$1"; then
    echo "ERROR: Command failed - $1"
    exit 1
  fi
}

# ----------------------------------------
# Start of the script

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : $(date) - Installing GVim (Vim with GUI)"

# ----------------------------------------
# Install GVim from the repository

Command "sudo apt install -y vim-gtk3"

# ----------------------------------------
# Finish

echo ""
echo "END     : $(date) - GVim installation completed"
echo "SPACER  : ----------------------------------------"

exit 0 # Success

