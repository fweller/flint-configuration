#!/bin/bash
# ----------------------------------------
# NAME        : install-veracrypt.sh
# DESCRIPTION : Installs VeraCrypt on Debian or Ubuntu systems.
#               This script allows for both console-only and desktop installations.
# ----------------------------------------
# REFERENCES  :
#               - https://www.veracrypt.fr/en/Downloads.html
# ----------------------------------------

# ----------------------------------------
# Variables

DownloadDir="${HOME}/Downloads"
Version="1.26.14"
Console="console-"  # Uncomment to enable console-only installation
# Console=""        # Uncomment to enable desktop installation
UbuntuUrl="https://launchpad.net/veracrypt/trunk/${Version}/+download/veracrypt-${Console}${Version}-Ubuntu-22.04-amd64.deb"
DebianUrl="https://launchpad.net/veracrypt/trunk/${Version}/+download/veracrypt-${Console}${Version}-Debian-12-amd64.deb"
UbuntuFile="veracrypt-${Console}${Version}-Ubuntu-22.04-amd64.deb"
DebianFile="veracrypt-${Console}${Version}-Debian-12-amd64.deb"
Sig=".sig"
PublicKey="https://www.idrix.fr/VeraCrypt/VeraCrypt_PGP_public_key.asc"
PublicKeyFile="VeraCrypt_PGP_public_key.asc"
ExpectedPublicKey="5069A233D55A0EEB174A5FC3821ACD02680D16DE"

# ----------------------------------------
# Function: Command
# Description: Executes a given command, prints it, and handles errors.
Command() {
  echo ""
  echo "COMMAND: $1"
  echo ""
  if ! eval "$1"; then
    echo "ERROR: Command failed - $1"
    exit 1
  fi
}

# ----------------------------------------
# Function: CheckDistro
# Description: Determines if the system is running Debian or Ubuntu.
CheckDistro() {
  echo "Checking if the system is running Debian or Ubuntu..."

  Distro_ID=$(lsb_release --id --short | tr "[:upper:]" "[:lower:]")

  case $Distro_ID in
    'debian')
      echo "This is a Debian distribution."
      Url=$DebianUrl
      File=$DebianFile
      ;;
    'ubuntu')
      echo "This is an Ubuntu distribution."
      Url=$UbuntuUrl
      File=$UbuntuFile
      ;;
    *)
      echo "WARNING: Unsupported distribution: $Distro_ID"
      echo "Use at your own risk. Continuing..."
      Url=$DebianUrl
      File=$DebianFile
      ;;
  esac
}

# ----------------------------------------
# Start of the script

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : $(date) - Installing VeraCrypt"

# Check the distribution and set the appropriate URL and file
CheckDistro

# Prepare the download directory
Command "mkdir -p \"${DownloadDir}\""
Command "pushd \"${DownloadDir}\" || exit"

# Install necessary packages for VeraCrypt
Command "sudo apt install -y exfat-fuse exfatprogs"

# Download and verify the VeraCrypt GPG Public Key
Command "wget \"${PublicKey}\""
Result=$(gpg --show-keys "${PublicKeyFile}" | sed -nr 's/^([ ]+)([0-9A-Z]{40}$)/\2/p')
echo "Downloaded public key: $Result"
echo "Expected public key: $ExpectedPublicKey"

if [ "${Result}" == "${ExpectedPublicKey}" ]; then
  echo "SUCCESS: Public key matches the expected key."
else
  echo "ERROR: Public key does not match the expected key."
  exit 1
fi

# Import the VeraCrypt GPG Public Key
Command "sudo gpg --import \"${PublicKeyFile}\""

# Download the VeraCrypt package and its signature
Command "wget \"${Url}\""
Command "wget \"${Url}${Sig}\""

# Verify the signature
# NOTE: Verification is skipped here, but you may need to sign the VeraCrypt key with your own key before verification
# Command "gpg --verify \"${File}${Sig}\" \"${File}\""

# Install the VeraCrypt package
Command "sudo apt install -y \"./${File}\""

# Return to the previous directory
Command "popd || exit"

# ----------------------------------------
# Finish

echo ""
echo "END     : $(date) - VeraCrypt installation completed"
echo "SPACER  : ----------------------------------------"

exit 0 # Success

