#!/bin/bash
# ----------------------------------------
# NAME        : installer.sh
# DESCRIPTION : Linux installer script
#               This script helps configure a newly installed Linux distribution.
#               It automatically detects the distribution, runs essential setup scripts,
#               and performs necessary configurations for both console and desktop environments.
# ----------------------------------------

# ----------------------------------------
# Variables

# Path to the script that identifies the Linux distribution
WhatDistro="$HOME/Tools/scripts/whatdistro.sh"

# Path to the script that configures sudoers for Debian-based distributions
DebianSudoers="$HOME/Tools/install/linux/debian-sudoers.sh"

# Log file to capture the output of the script
LOGFILE="$HOME/install_log.txt"

# Redirect all output (stdout and stderr) to both the console and the log file
exec > >(tee -a $LOGFILE) 2>&1

# ----------------------------------------
# Function: Command
# Description: Prints and executes a given command.
Command() {
  echo ""
  echo "SPACER  : ----------------------------------------"
  echo "COMMAND : $1"
  echo ""
  eval "$1"
}

# ----------------------------------------
# Function: Run
# Description: Runs a specified script if it exists and handles errors.
Run() {
  Script=$1
  echo ""
  echo "SPACER  : ----------------------------------------"
  echo ""

  if [ -f "$Script" ]; then
    echo "START   : Installer is calling script $Script"
    eval "$Script"
    ExitCode=$?
    if [ $ExitCode -ne 0 ]; then
      echo "ERROR   : Script $Script exited with status $ExitCode"
    else
      echo "END     : Installer successfully completed script $Script"
    fi
  else
    echo "ERROR   : Installer is unable to find and run $Script"
  fi

  echo "SPACER  : ----------------------------------------"
}

# ----------------------------------------
# Function: PrintTools
# Description: Prints instructions for installing additional tools.
PrintTools() {
  echo ""
  echo "SPACER  : ----------------------------------------"
  echo "NOTICE  : If you want the proper tools installation then run:"
  echo "  ./tools-proper.sh"
}

# ----------------------------------------
# Function: PrintRecommended
# Description: Prints a list of recommended scripts to run.
PrintRecommended() {
  echo ""
  echo "SPACER  : ----------------------------------------"
  echo "NOTICE  : Recommended scripts to run"
  echo "  ./packages/printers.sh"
  echo "  ./packages/laptop.sh"
  echo "  ./packages/development.sh"
  echo "  ./desktop/chrome.sh"
  echo "  cat ./desktop/apps-flathub.txt"
  echo "  cat ./desktop/apps-snap.txt"
}

# ----------------------------------------
# Start of the script

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : $(date) - Installer"

# ----------------------------------------
# Check if necessary tools are installed

if ! command -v sudo &> /dev/null; then
  echo "ERROR   : sudo is not installed. Please install sudo and try again."
  exit 1
fi

# ----------------------------------------
# Detect the Linux distribution

Distro=$(sh "$WhatDistro")

echo ""
echo "SPACER  : ----------------------------------------"
echo "NOTICE  : Distro detected as $Distro"

# If this is Debian, add the current user to the sudoers list
if [ "$Distro" == "debian" ]; then
  Run "$DebianSudoers"
fi

# ----------------------------------------
# Run the most critical scripts

echo ""
echo "SPACER  : ----------------------------------------"
echo "NOTICE  : Running the most critical scripts"

Run "./hosts.sh"
Run "./directories.sh"
Run "./packages/setup.sh"
Run "./packages/firmware.sh"
Run "./packages/console.sh"
Run "./nas-skydata.sh"

# ----------------------------------------
# Applications for the Console

echo ""
echo "SPACER  : ----------------------------------------"
echo "NOTICE  : Running console scripts"

Run "./console/vim.sh"
Run "./console/osync.sh"
Run "./console/ssh-server.sh"

# ----------------------------------------
# Only run these scripts if there is a desktop environment available

echo ""
echo "SPACER  : ----------------------------------------"

if [ -z "$DISPLAY" ]; then
  echo "NOTICE  : No GUI detected. Skipping desktop scripts."
else
  echo "NOTICE  : GUI detected. Running desktop scripts"
  Run "./packages/desktop.sh"
  Run "./packages/printers.sh"
  Run "./desktop/appimagelauncher.sh"
  Run "./desktop/gvim.sh"
fi

# ----------------------------------------
# Clean up

echo ""
echo "SPACER  : ----------------------------------------"
echo "NOTICE  : Running clean-up scripts"

Run "./root-dotfiles.sh"
Run "./fix-local-share.sh"

# ----------------------------------------
# Extra

echo ""
echo "SPACER  : ----------------------------------------"
echo "NOTICE  : Running extra scripts"

Run "./hw-probe.sh"

# ----------------------------------------
# Print additional instructions

PrintTools
PrintRecommended

# ----------------------------------------
# Finish

echo ""
echo "END     : $(date) - Installer"
echo "SPACER  : ----------------------------------------"
exit 0 # Success

