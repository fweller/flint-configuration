#!/bin/bash
# ----------------------------------------
# NAME        : tools.sh
# DESCRIPTION : My simplest GitLab tools installer
# ----------------------------------------


# ----------------------------------------
# Variables

Name="flint-configuration"
Project="https://gitlab.com/fweller/flint-configuration.git"
DownloadsDir="${HOME}/Downloads"
InstallDir="${HOME}" # Production
#InstallDir="${HOME}/Downloads/Install" # Test


# ----------------------------------------
# Install Dependencies

sudo apt install git


# ----------------------------------------
# Prepare directories

mkdir -p "$DownloadsDir"
mkdir -p "$InstallDir"


# ----------------------------------------
# Move to Downloads directory and clean up

cd "$DownloadsDir" || exit
rm -rf ${Name}*


# ----------------------------------------
# Clone my project

git clone "$Project"


# ----------------------------------------
# Move my project to the Install directory

echo "NOTICE  : The next line will have an error that it cannot stat....."
echo "NOTICE  : This is expected and normal"
cp -rf ${Name}/* ${Name}/.[!.]* ${Name}/..?* ${InstallDir}


# ----------------------------------------
# Finish

cd $HOME || exit
source .shellrc
exit 0 # Success

