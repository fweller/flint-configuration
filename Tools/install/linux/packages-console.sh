#!/bin/bash
# ----------------------------------------
# NAME        : console.sh
# DESCRIPTION : Installs preferred console applications.
# ----------------------------------------

# Fedora so far
vim fastfetch eza git hw-probe inxi lshw shellcheck


# ----------------------------------------
# Package name variables
# Try to be cognizant of differences in package names between distros
Packages="git" # Default
PackagesApt=$Packages
PackagesDnf=$Packages
PackagesZypper=$Packages
PackagesPacman=$Packages


# ----------------------------------------
# Function: Command
# Description: Executes a given command, prints it, and captures the output.
Command() {
  echo ""
  echo "COMMAND : $1"
  echo ""
  Output=$($1)
  echo ""
  echo "RETURN  : $Output"
}


# ----------------------------------------
# Install Dependencies

# Detect package manager and install packages
if command -v apt >/dev/null 2>&1; then
    sudo apt update && sudo apt install -y "$PackagesApt"
elif command -v dnf >/dev/null 2>&1; then
    sudo dnf install -y "$PackagesDnf"
elif command -v zypper >/dev/null 2>&1; then
    sudo zypper install -y "$PackagesZypper"
elif command -v pacman >/dev/null 2>&1; then
    sudo pacman -Syu --noconfirm "$PackagesPacman"
else
    echo "Package manager not supported."
    exit 1
fi



# ----------------------------------------
# Function: Installer
# Description: Installs the specified packages using the package installer script.
Installer() {
  Command "$PackageInstaller $1"
}

# ----------------------------------------
# Start of the script

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : $(date) - Installing console applications"



7zip # High-compression file archiver with a high compression ratio.
bind9-dnsutils # Clients provided with BIND for querying DNS servers.
bpfcc-tools # Tools for BPF Compiler Collection (BCC) to analyze kernel performance.
bpftrace # High-level tracing language for Linux eBPF.
build-essential # Informational list of packages which are essential for building Debian packages.
bzip2 # High-quality data compressor.
cifs-utils # Tools for mounting and managing CIFS/SMB filesystems.
cpu-checker # Tool to check for the presence of hardware virtualization.
cpufrequtils # Utilities to deal with the CPU frequency scaling.
cpuid # Tool to dump x86 CPUID information about the CPU(s).
curl # Command-line tool for transferring data with URL syntax.
dkms # Dynamic Kernel Module Support Framework.
duc # Suite of tools for inspecting disk usage.
edid-decode # Decode EDID data in human-readable format.
ethtool # Display or change Ethernet device settings.
exa # Modern replacement for 'ls' written in Rust.
eza # A modern, maintained replacement for 'ls' with color support.
fastfetch # Like neofetch, but much faster because written in C.
git # Fast, scalable, distributed revision control system.
git-lfs # Git extension for versioning large files.
htop # Interactive processes viewer for Unix systems.
hw-probe # Probe for hardware and check operability.
hwinfo # Hardware information and diagnostics tool.
inxi # Full-featured system information script.
iproute2 # Networking and traffic control tools.
lm-sensors # Utilities to read temperature/voltage/fan sensors.
lshw # Information about hardware configuration.
msr-tools # Utilities for modifying CPU model-specific registers.
ncdu # NCurses Disk Usage viewer.
neofetch # Command-line system information tool.
nicstat # Network interface statistics reporting tool.
numactl # NUMA policy control.
p7zip-full # 7z and 7za file archivers with high compression ratio.
procinfo # Utilities to display system information from /proc.
procps # Utilities for monitoring your system and processes.
shellcheck # Shell script analysis tool.
sysstat # System performance tools for Linux.
tcpdump # Command-line network traffic analyzer.
tiptop # Performance monitoring for Linux.
trace-cmd # User interface to Ftrace Linux kernel tracer.
tree # Displays directories as trees (with optional color/HTML output).
util-linux # Miscellaneous system utilities.
vim # Vi IMproved - enhanced vi editor.
wget # Retrieves files from the web.
zstd # Fast real-time compression algorithm.



# ----------------------------------------
# Install console applications

Installer "htop curl wget bzip2 ncdu zstd git git-lfs shellcheck inxi tree"
Installer "exa" # Planned removal in 2026
Installer "eza" # Replacement for exa
Installer "neofetch" # Planned removal in 2026
Installer "fastfetch" # Replacement for neofetch
Installer "7zip" # Replacing p7zip-full in 2024

# If 7zip did not install, then fallback to p7zip
if ! command -v 7z &> /dev/null; then
  Installer "p7zip-full" # Planned removal in 2026
fi

Installer "build-essential dkms cifs-utils bind9-dnsutils"
Installer "hw-probe lshw hwinfo procinfo cpufrequtils lm-sensors cpu-checker cpuid edid-decode"
Installer "duc procps util-linux sysstat iproute2 numactl tcpdump bpfcc-tools"
Installer "bpftrace trace-cmd nicstat ethtool tiptop msr-tools"
#Installer "btrfs-progs" # Only for btrfs installations

# ----------------------------------------
# Finish

echo ""
echo "END     : $(date) - Finished installing console applications"
echo "SPACER  : ----------------------------------------"
exit 0 # Success

# -----------------------------------
# Packages that were removed

# 2024 timeshift. System restore utility that I don't need.
# 2024 p7zip-rar. I'm not using it.
# 2024 neovim. I'm not using it.

# -----------------------------------
# Package Info

# What provides this command?
# dpkg -S <full path>
# Example: dpkg -S /usr/bin/7zz

# 7zip
# https://packages.debian.org/trixie/7zip
# 7-Zip file archiver with a high compression ratio
# Provides
#  7z: Full featured with plugin functionality.
#  7za: Major formats/features only.
#  7zr: LZMA (.7z, .lzma, .xz) only. Minimal executable.
#  7zz: Unknown but it works

# p7zip-full
# https://packages.debian.org/bookworm/p7zip-full
# https://p7zip.sourceforge.net/
# Provides
# 7z
# 7za
# Note: p7zip is a port of 7za for POSIX systems

# 7zip-rar
# https://packages.debian.org/trixie/7zip-rar
# Provides a RAR codec module for 7zip to make the 7z command able to extract RAR files.

# NAS Access
# cifs-utils:
# This is the core package that provides the necessary tools to mount and manage CIFS (Common Internet File System) shares, which is essentially the Linux implementation of SMB.
# smbclient:
# This is a command-line utility included in cifs-utils that allows you to directly access and manage files on an SMB share using a simple command-line interface.
# winbind:
# If you need to integrate your Linux system with a Windows Active Directory domain, "winbind" is crucial as it enables user authentication against the domain using the Windows domain controller.
# keyutils:
# This package provides additional cryptographic functions that might be needed for enhanced security when accessing the SMB share, depending on the specific configuration.


