#!/bin/bash
# ----------------------------------------
# NAME        : apt-upgrade.sh
# DESCRIPTION : Fixes a broken apt install, performs an upgrade, and cleans up
# ----------------------------------------

sudo apt update
sudo apt -y --fix-broken install
sudo apt -y --force-confold -upgrade
sudo apt -y autoremove


# https://raphaelhertzog.com/2010/09/21/debian-conffile-configuration-file-managed-by-dpkg/
# #$ apt-get -o Dpkg::Options::="--force-confdef" -o Dpkg::Options::="--force-confold" dist-upgrade
#    --force-confold: do not modify the current configuration file, the new version is installed with a .dpkg-dist suffix. With this option alone, even configuration files that you have not modified are left untouched. You need to combine it with --force-confdef to let dpkg overwrite configuration files that you have not modified.
#    --force-confnew: always install the new version of the configuration file, the current version is kept in a file with the .dpkg-old suffix.
#    --force-confdef: ask dpkg to decide alone when it can and prompt otherwise. This is the default behavior of dpkg and this option is mainly useful in combination with --force-confold.
#    --force-confmiss: ask dpkg to install the configuration file if it’s currently missing (for example because you have removed the file by mistake).

# You can also make those options permanent by creating /etc/apt/apt.conf.d/local:

#Dpkg::Options {
#   "--force-confdef";
#   "--force-confold";
#}

#I also want to keep original config files while doing automatic updates. You can add the following to /etc/apt/apt.conf.d/50unattended-upgrades

#Dpkg::Options {
#   "--force-confdef";
#   "--force-confold";
#};


