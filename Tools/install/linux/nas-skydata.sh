#!/bin/bash
# ----------------------------------------
# NAME        : nas-skydata.sh
# DESCRIPTION : Enables system access to the SkyData NAS by creating necessary directories,
#               updating /etc/fstab, and creating symbolic links.
# ----------------------------------------

# ----------------------------------------
# Variables

# NAS name and configuration
NasName="SkyData"
CredentialsFile="/root/.skydatacredentials"

# Mount directory and symlink destination
MountDir="/mnt"
SymlinkDir="$HOME/$NasName"

# ----------------------------------------
# Function: Command
# Description: Executes a given command and prints it to the console.
Command() {
  echo ""
  echo "COMMAND : $1"
  eval "$1"

  # Check if the command was successful
  if [ $? -ne 0 ]; then
    echo "ERROR   : Command failed - $1"
    exit 1 # Exit if command fails
  fi
}

# ----------------------------------------
# Start of the script

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : $(date) - SkyData NAS Enablement"

# ----------------------------------------
# Create mount directories

echo ""
echo "SPACER  : ----------------------------------------"
echo "NOTICE  : Creating directories in $MountDir"

for dir in SkyData SkyWork SkyStore SkyPenguin SkyMusic SkyVideo; do
  Command "sudo mkdir -p $MountDir/$dir"
done

# ----------------------------------------
# Update /etc/fstab with NAS information

echo ""
echo "SPACER  : ----------------------------------------"
echo "NOTICE  : Updating /etc/fstab with NAS information"

# Add entries to /etc/fstab
sudo sh -c "cat << EOF >> /etc/fstab

#SkyNet SkyData NAS
//SkyData/SkyData          /mnt/SkyData    cifs credentials=$CredentialsFile,vers=3,rw,uid=$USER,gid=users,iocharset=utf8,x-systemd.after=network-online.target,x-systemd.automount,x-systemd.mount-timeout=30,_netdev 0 0
//SkyData/SkyStore         /mnt/SkyStore   cifs credentials=$CredentialsFile,vers=3,rw,uid=$USER,gid=users,iocharset=utf8,x-systemd.after=network-online.target,x-systemd.automount,x-systemd.mount-timeout=30,_netdev 0 0
//SkyData/SkyPenguin       /mnt/SkyPenguin cifs credentials=$CredentialsFile,vers=3,rw,uid=$USER,gid=users,iocharset=utf8,x-systemd.after=network-online.target,x-systemd.automount,x-systemd.mount-timeout=30,_netdev 0 0
//SkyData/SkyWork          /mnt/SkyWork    cifs credentials=$CredentialsFile,vers=3,rw,uid=$USER,gid=users,iocharset=utf8,x-systemd.after=network-online.target,x-systemd.automount,x-systemd.mount-timeout=30,_netdev 0 0
//SkyData/music            /mnt/SkyMusic   cifs credentials=$CredentialsFile,vers=3,rw,uid=$USER,gid=users,iocharset=utf8,x-systemd.after=network-online.target,x-systemd.automount,x-systemd.mount-timeout=30,_netdev 0 0
//SkyData/video            /mnt/SkyVideo   cifs credentials=$CredentialsFile,vers=3,rw,uid=$USER,gid=users,iocharset=utf8,x-systemd.after=network-online.target,x-systemd.automount,x-systemd.mount-timeout=30,_netdev 0 0

EOF"

# ----------------------------------------
# Create the credentials file for NAS access

echo ""
echo "SPACER  : ----------------------------------------"
echo "NOTICE  : Creating credentials file at $CredentialsFile"

sudo sh -c "cat << EOF > $CredentialsFile
username=flint
password=<YourWifeIsWhat?Number?>
domain=SkyData
EOF"

echo ""
echo "SPACER  : ----------------------------------------"
echo "NOTICE  : You must now edit the credentials file to set the correct password."
read -r -n1 -s -p "WAIT   : Press <SPACE> to continue."

sudo vi "$CredentialsFile"

# ----------------------------------------
# Mount the NAS and reload systemd

echo ""
echo "SPACER  : ----------------------------------------"
echo "NOTICE  : Mounting $NasName and reloading systemd"

Command "sudo mount -a"
Command "sudo systemctl daemon-reload"

# ----------------------------------------
# Create symbolic links in the home directory

echo ""
echo "SPACER  : ----------------------------------------"
echo "NOTICE  : Creating symbolic links in $SymlinkDir"

Command "mkdir -p $SymlinkDir"

for dir in SkyData SkyWork SkyStore SkyPenguin SkyMusic SkyVideo; do
  Command "ln -s $MountDir/$dir/ $SymlinkDir/$dir"
done

# ----------------------------------------
# Finish

echo ""
echo "END     : $(date) - SkyData NAS Enablement completed"
echo "SPACER  : ----------------------------------------"
exit 0 # Success

