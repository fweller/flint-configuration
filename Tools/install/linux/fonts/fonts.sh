#!/bin/bash

## TODO
# Clean this up

exit

# Make font removal a separate script
# Remove fonts
#sudo apt remove -y --purge fonts-beng* fonts-de* fonts-g*  fonts-i* \
#  fonts-k* fonts-l* fonts-m* fonts-na* fonts-noto-cjk* \
#  fonts-noto-ui-extra fonts-noto-ui-core  \
#  fonts-o* fonts-p* fonts-s* fonts-t* fonts-y*

# Install fonts seperately
# Install fonts
#sudo apt install -y fonts-lato fonts-noto-core fonts-noto-extra \
#  fonts-roboto-slab fonts-roboto fonts-hack fonts-firacode \
#  fonts-inconsolata fonts-open-sans fonts-stix ttf-mscorefonts-installer
# fonts-ubuntu # not available in debian?

# UPDATE: Just manually install my fonts
echo "---------------------------------------------------"
echo "WHAT FONTS ARE INSTALLED???"
sudo apt list fonts-* | grep installed
echo "---------------------------------------------------"

echo "TO UNINSTALL FONTS RUN THIS COMMAND"
echo "sudo apt purge FONTNAME"
echo "---------------------------------------------------"

echo "MANUALLY INSTALL FONTS NOW"
echo "THEN UPDATE THE FONT CACHES WITH THESE TWO COMMANDS"
# Update the font caches
echo "sudo fc-cache -f -v &&  sudo dpkg-reconfigure fontconfig"
#sudo fc-cache -f -v &&  sudo dpkg-reconfigure fontconfig
echo "sudo dpkg -l fonts\*|grep ^ii|awk '{print $2}'"
#sudo dpkg -l fonts\*|grep ^ii|awk '{print $2}'
echo "---------------------------------------------------"

