# Installer

## Install tools (read only, no git available)

```bash
wget https://gitlab.com/fweller/flint-configuration/-/archive/master/flint-configuration-master.zip
unzip flint-configuration-master.zip
cd flint-configuration-master.zip
```

## Run installer

```bash
cd Tools/install/linux
./installer.sh
```

## Install tools (read only)

```bash
curl -s https://gitlab.com/fweller/flint-configuration/-/raw/master/Tools/install/linux/tools.sh | bash
```

## Install tools properly (read and write)

```bash
curl -s https://gitlab.com/fweller/flint-configuration/-/raw/master/Tools/install/linux/tools-proper.sh | bash
```

