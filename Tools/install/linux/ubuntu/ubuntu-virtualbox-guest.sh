#!/bin/bash

sudo apt update
sudo apt -y install build-essential dkms linux-headers-"$(uname -r)" && \
sudo apt -y install virtualbox-guest-utils virtualbox-guest-x11

# Clean up the packages
sudo apt -y --fix-broken install
sudo apt -y autoremove

# Print our ip address
echo ""; echo ""; echo "--------------------------------------------------"
ip a | grep 'inet '

# One line for use in a VM
#sudo apt install ufw openssh-server; sudo systemctl status ssh; sudo systemctl enable ssh --now; sudo ufw allow ssh; ip a | grep 'inet'

# This line might help when running within the client and needing to access shared folders
#sudo usermod -aG vboxsf $USER

