#!/bin/bash
# disk_setup_luks_ext4.sh

# Description
# This script will create the necessary partitions for a LUKS encryption enabled Linux installation
# This script is dual-boot w/ Microsoft Windows friendly
# I created this script originally to duplicate the Ubuntu 20.04 USB ISO installer design:
#   1  EFI     vFAT
#   2  boot    ext4 with no encryption
#   3  rootfs  ext4 with encription (ext4 sits on top of LUKS)
# I have used this script with installs for Ubuntu & Mint, and it should work fine with
#   any other Debian/Ubuntu/Mint-based distro
# This script is flexible enough to handle a swap partition and a share partition

# Usage
# 1. If dual-boot with MS Windows:
#   A. Install MS Windows
#   B. Ensure that there is an empty space after the last Windows partition of
#       sufficient size for your Linux installation
# 2. Download this file to your installer directory
#   wget https://gitlab.com/fweller/flint-configuration/-/raw/master/Tools/install/linux/disk_setup_luks_ext4.sh
# 3. Edit the variables below as needed for your desired configuration
# 4  Run this script as root: sudo ./disk_setup_luks_ext4.sh
# 5  Run the USB installer for your Linux distro
# 6  Manually select the partitions for install
#     /dev/nvme1n1p1       Call it EFI
#     /dev/nvme1n1p2       Call it /boot and format it
#     /dev/mapper/rootfs   Call it / and format it
#     Specify the bootloader location to be the EFI partition /dev/nvme1n1p1
#    Optional:
#     /dev/nvme1n1p3       Call it swap
#     /dev/nvme1n1p1       Call it BIOS.  Unlikely to be needed.
#
# Additional Information
#   Swap Partition - I believe that Ubuntu uses LVM on top of LUKS and then adds 
#     partitions for a ext4 rootfs and swap on top of that, possibly with a LVM layer in between.
#     One does not need to use this script to set up a swap partition if using the Ubuntu method.
#     I like to add a swap partition for installs where I am able to use hibernation to 
#     the swap partition because booting from the hibernation image is much easier when 
#     the swapfile is not encrypted.  I have successfully used a swap partition to handle 
#     hibernation in Linux Mint, but not Ubuntu.
#   One way to improve this script would be to refer to partitions by /dev/disk/by-label

# Useful commands
#   List all of the filesystems and partitions
#     sudo fdisk -l
#   Print details of one filesystem
#     sudo sgdisk -p /dev/sda

# Need sgdisk which is part of gdisk package
apt install gdisk

# -------------------------------------------------- 
# EDIT THE VARIABLES IN THIS SECTION

# Specify the disk name: sda, sdb, nvme0n1, nvme1n1, etc...
disk_name="sdb"
#disk_name="nvme0n1"

# From the provided disk name we can form the device_name AKA device_path
device_name="/dev/$disk_name" # Do not modify

# If you are using NVMe disks with nvme0n1 type of naming then we need to add
#   an extra character between the disk name and the partition number
#   i.e. nvme1n1 + partition 3 = nvme1n1p3
# If you are using NVMe, then uncomment this next line
# extra_char="p"
# If you are using SATA or similar disks with names such as sda or sdb, then uncomment this next line
extra_char=""

# Get the last sector number
# Command "sgdisk -E $device_name"
# echo "$command"; last_sector=$(eval "$command"); echo "last_sector=$last_sector"

# Will we be supporting dual-boot with Windows
# Uncomment the next line if this is not dual-boot
#dual_boot="true"

# Specify the partition numbers
# Comment out the partition number if the partition is not required

# BIOS Boot partition number is always 1
# No computer manufactured after ~2012 should need this
# WARNING: I have never tested this portion of the script
# Uncomment the next two lines if you need this partition
# bios_partno="1"
# bios_device="$device_name$extra_char$bios_partno"

# EFI partition is always 1 unless there is also a BIOS Boot partition, then it is 2
efi_partno="1"
efi_device="$device_name$extra_char$efi_partno"
efi_size="+550M" # Minimum size 550M, use 1G if supporting systemd-boot
# EFI start sector should always be 2048
efi_start="2048"

# WARNING: If dual-boot, the partition numbers must start after the
#   last windows partition or else bad things will happen

# Boot partition is hightly recommended
boot_partno="2"
boot_device="$device_name$extra_char$boot_partno"
# Boot partition size: 512M for most installs, 768M to be extra safe, 1024M if using an nvidia GPU
boot_size="+512M" # normal
#boot_size="+768M" # extra-safe
#boot_size="+1G" # for nvidia

# Swap partition if desired
swap_partno="3"
swap_device="$device_name$extra_char$swap_partno"
# Swap size (in GiB) should be 125% of RAM size if using hibernation
swap_size="+12G"

# RootFS partition is required
root_partno="4"
root_device="$device_name$extra_char$root_partno"
# Root size should be specified in ###G or -1M to fill the disk
# root_size="+16G"
root_size="-1M"

# Share partition is optional
# share_partno="6"
# share_device="$device_name$extra_char$share_partno"
# Root size should be specified in ###G or -1M to fill the disk
# share_size="-1M"

# -------------------------------------------------- 
# DO NOT EDIT THIS SECTION

# Partition type information, for reference only
# List all types: $ sgdisk -L
# ef00 EFI system partition
# ef01 MBR partition scheme
# ef02 BIOS boot partition
# 8200 Linux swap
# 8300 Linux filesystem
# 8309 Linux LUKS

# Print our comments
Comment () {
  echo ""; echo ""
  echo "--------------------------------------------------"
  echo "$1"
}

# Print the command and then evaluate it
Command () {
  echo ""
  echo "$1"
  eval "$1"
}

Comment "Check for Sudo"
if [[ $UID != 0 ]]; then
  echo "ERROR! This script can only be run as root so make sure to call this script with:"
  echo "sudo $0 $*"
  exit 1
fi


Comment "Verify the disk before our work"
Command "sgdisk -v $device_name"

Comment "Print all the details"
Command "sgdisk -p $device_name"

# If dual-boot then the EFI already exists and we do not want to damage it
if [ ! "$dual_boot" == "true" ]; then
  Comment "This is not a dual-boot setup, so make a new GPT (GUID Partition Table)"
  # -Z Zap everything
  # -o Erase all GPT data structures and create a fresh GPT
  # -g Convert an MBR or BSD disklabel disk to GPT format
  Command "sgdisk -Z $device_name -a 2048"

  # BIOS Boot Partition
  if [ "$bios_partno" ]; then
    Comment "Creating a BIOS Boot Partition"
    # Some name this partition "BIOS Boot Partition" while others use "BIOS"
    Command "sgdisk -n $bios_partno:2048:4095 -a 2048 -I -c $bios_partno:'BIOS' -t $bios_partno:ef02 $device_name"
    efi_start="0" # 0 means default which is the next sector location
    Comment "Creating the BIOS Boot filesystem"
    Command "mkfs.vfat $bios_device"
    Command "fatlabel $bios_device BIOS"
  fi

  # EFI System Partition
  Comment "Creating EFI System Partition"
  # Create first partition which should be the EFI
  # -n Create a new partition'
  # Some name this partition "EFI System Partition" while others use "EFI"
  Command "sgdisk -n $efi_partno:$efi_start:$efi_size -a 2048 -I -c $efi_partno:'EFI' -t $efi_partno:ef00 $device_name"

  Comment "Creating the EFI filesystem"
  Command "mkfs.vfat $efi_device"
  Command "fatlabel $efi_device EFI"
fi

# Boot partition
if [ "$boot_partno" ]; then
  Comment "Creating the boot partition"
  Command "sgdisk -n $boot_partno:0:$boot_size -a 2048 -I -c $boot_partno:'BOOT' -t $boot_partno:8300 $device_name"

  Comment "Creating the boot filesystem"
  Command "mkfs.ext4 $boot_device"
  Command "e2label $boot_device boot"
fi

# Swap partition
if [ "$swap_partno" ]; then
  Comment "Creating the swap partition"
  Command "sgdisk -n $swap_partno:0:$swap_size -a 2048 -I -c $swap_partno:'SWAP' -t $swap_partno:8300 $device_name"
  Comment "Creating the swap space"
  Command "mkswap $swap_device"
  Command "swaplabel -L 'swap' $swap_partno"
fi

# RootFS partition
if [ "$root_partno" ]; then
  Comment "Creating the rootfs partition"
  Command "sgdisk -n $root_partno:0:$root_size -a 2048 -I -c $root_partno:'ROOT' -t $root_partno:8300 $device_name"
fi

# Share partition
if [ "$share_partno" ]; then
  Comment "Creating the share partition"
  Command "sgdisk -n $share_partno:0:$share_size -a 2048 -I -c $share_partno:'SHARE' -t $share_partno:8300 $device_name"
  Comment "Creating the share filesystem"
  Command "mkfs.vfat $share_device"
  Command "fatlabel $share_device share"
fi

# Finished with the partition table
Comment "Inform the system of the partition table update"
Command "partprobe $device_name"
Comment "Verify the disk"
Command "sgdisk -v $device_name"
Comment "Print all the details of the disk"
Command "sgdisk -p $device_name"
Comment "All partitions have been completed.  Continuing..."

# LUKS creation
Comment "Creating the LUKS disk"
echo ""; echo ""; echo "ALERT! You will be asked for a LUKS password below."
Command "cryptsetup luksFormat $root_device"
Comment "Dump LUKS partition info so we can see the details"
Command "cryptsetup luksDump $root_device"
Comment "Extract the UUID from the LUKS partition"
command="blkid -s UUID -o value $root_device"
echo ""; echo "$command"
root_uuid=$(eval "$command")
Comment "The LUKS root partition UUID=$root_uuid"
Comment "Open the LUKS partition"
Command "cryptsetup open $root_device rootfs"

# Rootfs filesystem
Comment "Create ext4 filesystem on top of the LUKS layer, name it rootfs"
Command "mkfs.ext4 /dev/mapper/rootfs"
Command "e2label /dev/mapper/rootfs rootfs"

# Let the installer do its work
Comment "ALERT! The first portion of the install process is complete."
echo "Now is the time for you to use the Linux distro installer GUI"
echo "Manually select each partition for the install."
echo "Make sure to select /dev/mapper/rootfs for the installation and not $root_partno"
echo ""; echo ""
echo "When the installer has completed, return to this window and"
while true; do
  read -p "Type yes to continue this script, type no to exit: " yn
  case $yn in
    [Yy]* ) break;;
    [Nn]* ) exit;;
    * ) echo "Please answer yes or no.";;
  esac
done

# The rest of this script is involved in adding the LUKS UUID to the new filesystem
# and to inform GRUB on how to access it
# This portion of the script is also useful for rescuing a filesystem that isn't booting
# Just make sure that you have the root partition properly defined:  #root_partno="3"

# If we are operating on a filesystem without chroot'ing into it, we have to
#   add the folder hierarchy on the system we are running from
#   for most installs, this would be /mnt
mountpoint="/mnt" # for use before chroot
#mountpoint=""     # for use after chroot

# Mount filesystems
Comment "Mounting rootfs"
# Open the LUKS partition
Command "cryptsetup open $root_device rootfs"
Command "sudo mount /dev/mapper/rootfs /mnt"
Comment "Mount the boot and efi partitions"
Command "mount $boot_device /mnt/boot"
Command "mount $efi_device /mnt/boot/efi"


# Add rootfs UUID to crypttab
filename="$mountpoint/etc/crypttab"
echo ""; echo "Adding rootfs UUID=$root_uuid to $filename"
# DO NOT TOUCH ANY TEXT BETWEEN THESE ARROWS ---->>>>
sudo sh -c "cat << EOF >> $filename
rootfs UUID=$root_uuid none luks,discard
EOF"
# <<<<---- DO NOT TOUCH ANY TEXT BETWEEN THESE ARROWS

# We are now done with adding LUKS UUID to GRUB
# Starting the process of adding Swap UUID for hibernation

# Swap partition
if [ "$swap_partno" ]; then
  Comment "Adding Swap UUID to enable hibernation"
  # Extract the UUID from the swap partition
  command="blkid -s UUID -o value $swap_device"
  echo ""; echo "$command"
  swap_uuid=$(eval "$command")
  Comment "Swap UUID=$swap_uuid"

# Swap & GRUB
filename="$mountpoint/etc/default/grub"
text="

#GRUB_CMDLINE_LINUX_DEFAULT=\"quiet splash\"
GRUB_CMDLINE_LINUX_DEFAULT=\"quiet splash resume=UUID=$swap_uuid\"
"

Comment "Inform GRUB about the hibernation swap partition by editing $filename"
Comment "$text"
# DO NOT TOUCH ANY TEXT BETWEEN THESE ARROWS ---->>>>
sudo sh -c "cat << EOF >> $filename
$text
EOF"
# <<<<---- DO NOT TOUCH ANY TEXT BETWEEN THESE ARROWS

# Swap & Initramfs
filename="$mountpoint/etc/initramfs-tools/conf.d/resume"
Comment "Inform initramfs-tools about resuming from hibernation swap partition by editing $filename"
# DO NOT TOUCH ANY TEXT BETWEEN THESE ARROWS ---->>>>
sudo sh -c "cat << EOF >> $filename
RESUME=UUID=$swap_uuid
EOF"
# <<<<---- DO NOT TOUCH ANY TEXT BETWEEN THESE ARROWS

# Swap Hibernation Button
filename="$mountpoint/etc/polkit-1/localauthority/90-mandatory.d/enable-hibernate.pkla"
Comment "Create hibernate button by editing $filename"
# DO NOT TOUCH ANY TEXT BETWEEN THESE ARROWS ---->>>>
sudo sh -c "cat << EOF >> $filename
[Enable hibernate]
Identity=unix-user:*
Action=org.freedesktop.login1.hibernate;org.freedesktop.login1.handle-hibernate-key;org.freedesktop.login1;org.freedesktop.login1.hibernate-multiple-sessions
ResultActive=yes
EOF"
# <<<<---- DO NOT TOUCH ANY TEXT BETWEEN THESE ARROWS

fi




# Prepare for Chroot
Comment "Prepare for chroot"
# Once we enter chroot, the script will stop running

# DO NOT TOUCH ANY TEXT BETWEEN THESE ARROWS ---->>>>
filename="/mnt/tmp/finish_install.sh"
sudo sh -c "cat << EOF >> $filename
#!/bin/bash

# Apply changes
update-initramfs -k all -c

# Install the GRUB bootloader
grub-install --efi-directory=/boot/efi

# Update GRUB
grub-mkconfig -o /boot/grub/grub.cfg

# Exit chroot
exit
for i in /dev /dev/pts /proc /sys /run; do sudo umount -v /mnt$i; done
sudo umount /mnt/boot/efi
sudo umount /mnt/boot
sudo umount /mnt

EOF"
# <<<<---- DO NOT TOUCH ANY TEXT BETWEEN THESE ARROWS
chmod +x $filename

Comment "Once chroot is complete, this script will stop running"
echo "Continue the script by running"
echo "./tmp/finish_install.sh"

# Chroot time
for i in /dev /dev/pts /proc /sys /run; do sudo mount -B $i /mnt$i; done
echo ""; echo "Performing chroot"
chroot /mnt # Script dies here

exit # We probably won't get to this point

# If the script didn't end, we would have been able to run the following....

# Apply changes
Command "update-initramfs -k all -c"

# Install the GRUB bootloader
Command "grub-install --efi-directory=/boot/efi"

# Update GRUB
Command "grub-mkconfig -o /boot/grub/grub.cfg"

# Exit chroot
Command "exit"
Command "for i in /dev /dev/pts /proc /sys /run; do sudo umount -v /mnt$i; done"
Command "sudo umount /mnt/boot/efi"
Command "sudo umount /mnt/boot"
Command "sudo umount /mnt"

# Done
echo ""; echo ""; echo "All done!  The script is ending"

exit
# For reference, to change the LUKS password
# cryptsetup luksChangeKey $root_partno

# Use these last lines for exiting chroot
for i in /dev /dev/pts /proc /sys /run; do sudo umount -v /mnt$i; done
sudo umount /mnt/boot/efi
sudo umount /mnt/boot
sudo umount /mnt

