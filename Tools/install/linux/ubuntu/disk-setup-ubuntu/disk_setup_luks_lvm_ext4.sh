#!/bin/bash
# disk_setup_luks_lvm_ext4.sh

# Description
# This script will create the necessary partitions for a LUKS encryption enabled Linux installation
# This script is dual-boot w/ Microsoft Windows friendly
# I created this script to create the filesystem setup that Canonical should have done
# This script is flexible enough to handle a swap partition and a share partition
# Tested with Ubuntu 22.04, 22.10
# Hibernation works, as does booting from hibernation
#
#   #    NAME    TYPE   NOTES
#   1    EFI     vFAT   Standard EFI
#   2    boot    ext4   Ubuntu installer requires boot partition
#   3    luks    LUKS2  Full disk encryption
#   3.1  swap    swap   Swap partition for hibertnation
#   3.2  rootfs  ext4   Root filesystem

# Usage
# 1. If dual-boot with MS Windows:
#   A. Install MS Windows
#   B. Ensure that there is an empty space after the last Windows partition of
#       sufficient size for your Linux installation
# 2. Download this file to your installer directory
#   wget https://gitlab.com/fweller/flint-configuration/-/raw/master/Tools/install/linux/disk_setup_luks_lvm_ext4.sh
# 3. Edit the variables below as needed for your desired configuration
# 4  Run this script as root: sudo ./disk_setup_luks_lvm_ext4.sh
# 5  Run the USB installer for your Linux distro
# 6  Manually select the partitions for install


# Recommendations for swap size to ram size
# RedHat
#   Amount of RAM in the system 	Recommended swap space 	    Recommended swap space if allowing for hibernation
#   ⩽ 2 GB 	                      2 times the amount of RAM 	3 times the amount of RAM
#   > 2 GB – 8 GB 	              Equal to the amount of RAM 	2 times the amount of RAM
#   > 8 GB – 64 GB 	              At least 4 GB 	            1.5 times the amount of RAM
#   > 64 GB 	                    At least 4 GB 	            Hibernation not recommended 
# Ubuntu
#        RAM   No hibernation    With Hibernation  Maximum
#        1GB              1GB                 2GB      2GB
#        2GB              1GB                 3GB      4GB
#        3GB              2GB                 5GB      6GB
#        4GB              2GB                 6GB      8GB
#        5GB              2GB                 7GB     10GB
#        6GB              2GB                 8GB     12GB
#        8GB              3GB                11GB     16GB
#       12GB              3GB                15GB     24GB
#       16GB              4GB                20GB     32GB
#       24GB              5GB                29GB     48GB
#       32GB              6GB                38GB     64GB
#       64GB              8GB                72GB    128GB
#      128GB             11GB               139GB    256GB
#      256GB             16GB               272GB    512GB

# -------------------------------------------------- 
# EDIT THE VARIABLES IN THIS SECTION

# Specify the disk name: sda, sdb, nvme0n1, nvme1n1, etc...
disk_name="sda"
#disk_name="nvme0n1"

# From the provided disk name we can form the device_name AKA device_path
device_name="/dev/$disk_name" # Do not modify

# If you are using NVMe disks with nvme0n1 type of naming then we need to add
#   an extra character between the disk name and the partition number
#   i.e. nvme1n1 + partition 3 = nvme1n1p3
# If you are using NVMe, then uncomment this next line
# extra_char="p"
# If you are using SATA or similar disks with names such as sda or sdb, then uncomment this next line
extra_char=""

# Get the last sector number
# Command "sgdisk -E $device_name"
# echo "$command"; last_sector=$(eval "$command"); echo "last_sector=$last_sector"

# Will we be supporting dual-boot with Windows
# Uncomment the next line if this is not dual-boot
#dual_boot="true"

# Specify the partition numbers
# Comment out the partition number if the partition is not required

# EFI partition is always 1 unless there is also a BIOS Boot partition, then it is 2
efi_partno="1"
efi_device="$device_name$extra_char$efi_partno" # partition device
efi_size="+1G" # Minimum size 550M, use 1G if supporting systemd-boot
# EFI start sector should always be 2048
efi_start="2048"

# WARNING: If dual-boot, the partition numbers must start after the
#   last windows partition or else bad things will happen

# Boot partition is required by Ubuntu, but shouldn't be
boot_partno="2"
boot_device="$device_name$extra_char$boot_partno"
# Boot partition size: 512M for most installs, 768M to be extra safe, 1024M if using an nvidia GPU
boot_size="+1G" # 512M Minimal, 768M Recommended, 1G Extra-safe when Nvidia is involved


# Luks partition is required
luks_partno="3"
luks_device="$device_name$extra_char$luks_partno" # partition device
# luks size should be specified in ###G or -1M to fill the disk
# luks_size="+16G"
luks_size="-1M"
luks_name="${disk_name}${extra_char}${luks_partno}_crypt"
#echo "luks_name=$luks_name"
luks_decrypted_device="/dev/mapper/${luks_name}"

# Share partition is optional
# share_partno="3"
# share_device="$device_name$extra_char$share_partno"
# Root size should be specified in ###G or -1M to fill the disk
# share_size="-1M"


# LVM variables
lvm_vg="vglinux"
#boot_size="1G"
#boot_name="boot"
#boot_device="/dev/${lvm_vg}/${boot_name}"
swap_size="40G"
swap_name="swap"
swap_device="/dev/${lvm_vg}/${swap_name}"
root_size="100%FREE"
root_name="root"
root_device="/dev/${lvm_vg}/${root_name}"

# -------------------------------------------------- 
# DO NOT EDIT THIS SECTION

# Partition type information, for reference only
# List all types: $ sgdisk -L
# ef00 EFI system partition
# ef01 MBR partition scheme
# ef02 BIOS boot partition
# 8200 Linux swap
# 8300 Linux filesystem
# 8309 Linux LUKS

# Print divider line so its easier to read the console
Divider () {
  echo ""; echo ""
  echo "##################################################"
  echo "$1"
}

# Print our comments
Comment () {
  echo ""; echo ""
  echo "--------------------------------------------------"
  echo "$1"
}

# Print the command and then evaluate it
Command () {
  echo ""
  echo "$1"
  eval "$1"
}

RetCommand () {
  echo ""
  echo "$1"
  retval=$(eval "$1")
  echo $retval
}

# Wait for user input before continuing
WaitUser () {
  echo ""; echo ""
  echo "--------------------------------------------------"
  while true; do
    read -p "Type yes to continue this script, type no to exit: " yn
    case $yn in
      [Yy]* ) break;;
      [Nn]* ) exit;;
      * ) echo "Please answer yes or no.";;
    esac
  done
}

Divider
Comment "Check for Sudo"
if [[ $UID != 0 ]]; then
  echo "ERROR! This script can only be run as root so make sure to call this script with:"
  echo "sudo $0 $*"
  exit 1
fi

# Install packages
Divider
Comment "Installing cryptsetup and lvm2"
Command "apt -y install cryptsetup lvm2"

# Print some info we may find useful
Divider
Comment "Verify the disk before our work"
Command "sgdisk -v $device_name"
Comment "Print all the details"
Command "sgdisk -p $device_name"
WaitUser

# If dual-boot then the EFI already exists and we do not want to damage it
if [ ! "$dual_boot" == "true" ]; then
  Divider
  # New GPT
  Comment "This is not a dual-boot setup, so make a new GPT (GUID Partition Table)"
  Command "sgdisk -Z $device_name -a 2048"
  # EFI System Partition
  Comment "Creating EFI System Partition"
  Command "sgdisk -n $efi_partno:$efi_start:$efi_size -a 2048 -c $efi_partno:'EFI' -t $efi_partno:ef00 $device_name"
fi

# Boot partition
if [ "$boot_partno" ]; then
  Comment "Creating the boot partition"
  Command "sgdisk -n $boot_partno:0:$boot_size -a 2048 -c $boot_partno:'BOOT' -t $boot_partno:8300 $device_name"
fi

# LUKS partition
#if [ "$luks_partno" ]; then
Comment "Creating the rootfs partition"
Command "sgdisk -n $luks_partno:0:$luks_size -a 2048 -c $luks_partno:'ROOT' -t $luks_partno:8309 $device_name"
#fi

# Print some info we may find useful
Divider
Comment "Partitioning complete.  Verify the disk before we continue."
Command "sgdisk -v $device_name"
Comment "Print all the details"
Command "sgdisk -p $device_name"
WaitUser

# LUKS creation
Divider
Comment "LUKS Start."
echo ""; echo ""; echo "ALERT! You will be asked for a LUKS password below."
Command "cryptsetup luksFormat $luks_device"
Comment "Dump LUKS partition info so we can see the details"
Command "cryptsetup luksDump $luks_device"
Comment "Extract the UUID from the LUKS partition"
#command="blkid -s UUID -o value $luks_device"
#echo ""; echo "$command"
#luks_uuid=$(eval "$command")
RetCommand "blkid -s UUID -o value $luks_device"
luks_uuid=$retval
Comment "The LUKS root partition UUID=$luks_uuid"
Comment "Open the LUKS partition"
Command "cryptsetup open $luks_device $luks_name"

# Share partition is optional
if [ "$share_partno" ]; then
  Comment "Creating the share partition"
  Command "sgdisk -n $share_partno:0:$share_size -a 2048 -c $share_partno:'SHARE' -t $share_partno:8300 $device_name"
fi

# Finished with the partition table
Divider
Comment "LUKS Complete."
Comment "Inform the system of the partition table update"
Command "partprobe $device_name"
Comment "Verify the disk"
Command "sgdisk -v $device_name"
Comment "Print all the details of the disk"
Command "sgdisk -p $device_name"



# LVM setup
Divider
Comment "LVM Start."

# pvcreate PV
# note to add -y flag when I am confident
Comment "Create one LVM PV (Physical Volume)"
Command "pvcreate $luks_decrypted_device"

# vgcreate VG_new PV
# note to add -y flag when I am confident
Comment "Create one LVM VG (Volume Group)"
Command "vgcreate $lvm_vg $luks_decrypted_device"


# lvcreate option_args position_args
# lvcreate -L|--size Size[m|UNIT] VG
#       -l|--extents Number[PERCENT]
# note to add -y flag when I am confident
Comment "Create multiple LVM LV (Logical Volume)"
#Command "lvcreate -L $boot_size -n $boot_name $lvm_vg"
Command "lvcreate -L $swap_size -n $swap_name $lvm_vg"
Command "lvcreate -l $root_size -n $root_name $lvm_vg"


Command "lvscan"
Comment "LVM Complete."
WaitUser #DEBUG

# Filesystem creation
Divider
Comment "Filesystem creation start."

if [ ! "$dual_boot" == "true" ]; then
  Comment "Creating the EFI filesystem"
  Command "mkfs.vfat $efi_device"
  Command "fatlabel $efi_device EFI"
fi

# Boot partition
if [ "$boot_partno" ]; then
  Comment "Creating the boot filesystem"
  Command "mkfs.ext4 $boot_device"
  Command "e2label $boot_device boot"
fi


# Boot partition
#if [ "$boot_size" ]; then
#Comment "Creating the boot filesystem"
#Command "mkfs.ext4 $boot_device"
#Command "e2label $boot_device $boot_name"
#RetCommand "blkid -s UUID -o value $boot_device"
#boot_uuid=$retval
#Comment "The boot partition UUID=$boot_uuid"
#fi

# Swap partition
#if [ "$swap_size" ]; then
Comment "Creating the swap space"
Command "mkswap $swap_device"
Command "swaplabel -L $swap_name $swap_device"
RetCommand "blkid -s UUID -o value $swap_device"
swap_uuid=$retval
Comment "The swap partition UUID=$swap_uuid"
#fi

# Rootfs filesystem
Comment "Creating the root filesystem"
Command "mkfs.ext4 $root_device"
Command "e2label $root_device $root_name"
RetCommand "blkid -s UUID -o value $root_device"
root_uuid=$retval
Comment "The root partition UUID=$root_uuid"


# Share partition is optional
if [ "$share_partno" ]; then
  Comment "Creating the share filesystem"
  Command "mkfs.vfat $share_device"
  Command "fatlabel $share_device share"
fi



Divider
Command "lsblk -o NAME,FSTYPE,MOUNTPOINT $device_name"
Comment "Filesystem creation complete."
WaitUser


# Let the installer do its work
Divider
Comment "ALERT! The first portion of the install process is complete."
echo "Now is the time for you to use the Linux distro installer GUI"
echo "Manually select each partition for the install."
echo "Make sure to select:"
echo "$root_device for / 'rootfs'"
echo "$boot_device for /boot"
echo "$efi_device for /boot/efi"
echo ""; echo ""
echo "When the installer has completed, return to this window"
WaitUser


# The rest of this script is involved in adding the LUKS UUID to the new filesystem
# and to inform GRUB on how to access it
# This portion of the script is also useful for rescuing a filesystem that isn't booting
# Just make sure that you have the root partition properly defined:  #root_partno="3"

# If we are operating on a filesystem without chroot'ing into it, we have to
#   add the folder hierarchy on the system we are running from
#   for most installs, this would be /mnt
mountpoint="/mnt" # for use before chroot
#mountpoint=""     # for use after chroot

# Mount filesystems
Divider
Comment "Mounting rootfs"
# Open the LUKS partition
Command "cryptsetup open $luks_device $luks_name"
Command "pvscan"
Command "vgscan"
Command "lvscan"
Command "mount $root_device $mountpoint"
#Command "mkdir -p ${mountpoint}/boot" # unnecessary
#Command "mount $boot_device ${mountpoint}/boot"
Command "mount $boot_device /mnt/boot"
#Command "mkdir -p ${mountpoint}/boot/efi" # unnecessary
Command "mount $efi_device ${mountpoint}/boot/efi"

# Deal with efivars.  This is new.
Command "mkdir ${mountpoint}/efivars-temp"
Command "mount --bind /sys/firmware/efi/efivars ${mountpoint}/efivars-temp"



# Add luks UUID to crypttab
Command "mkdir -p ${mountpoint}/etc"
filename="$mountpoint/etc/crypttab"
echo ""; echo "Adding luks UUID=$luks_uuid to $filename"
# DO NOT TOUCH ANY TEXT BETWEEN THESE ARROWS ---->>>>
sudo sh -c "cat << EOF >> $filename
$luks_name UUID=$luks_uuid none luks,discard
EOF"
# <<<<---- DO NOT TOUCH ANY TEXT BETWEEN THESE ARROWS

# We are now done with adding LUKS UUID to GRUB
# Starting the process of adding Swap UUID for hibernation

# Swap Partition Start
Comment "Adding Swap UUID to enable hibernation"
# Extract the UUID from the swap partition
#command="blkid -s UUID -o value $swap_device"
#echo ""; echo "$command"
#swap_uuid=$(eval "$command")
Comment "Swap UUID=$swap_uuid"

# Swap & GRUB
filename="$mountpoint/etc/default/grub"
text="

#GRUB_CMDLINE_LINUX_DEFAULT=\"quiet splash\"
GRUB_CMDLINE_LINUX_DEFAULT=\"quiet splash resume=UUID=$swap_uuid\"
"

Comment "Inform GRUB about the hibernation swap partition by editing $filename"
Comment "$text"
# DO NOT TOUCH ANY TEXT BETWEEN THESE ARROWS ---->>>>
sudo sh -c "cat << EOF >> $filename
$text
EOF"
# <<<<---- DO NOT TOUCH ANY TEXT BETWEEN THESE ARROWS

# Swap & Initramfs
filename="$mountpoint/etc/initramfs-tools/conf.d/resume"
Comment "Inform initramfs-tools about resuming from hibernation swap partition by editing $filename"
# DO NOT TOUCH ANY TEXT BETWEEN THESE ARROWS ---->>>>
sudo sh -c "cat << EOF >> $filename
RESUME=UUID=$swap_uuid
EOF"
# <<<<---- DO NOT TOUCH ANY TEXT BETWEEN THESE ARROWS

# Swap Hibernation Button
filename="$mountpoint/etc/polkit-1/localauthority/90-mandatory.d/enable-hibernate.pkla"
Comment "Create hibernate button by editing $filename"
# DO NOT TOUCH ANY TEXT BETWEEN THESE ARROWS ---->>>>
sudo sh -c "cat << EOF >> $filename
[Enable hibernate]
Identity=unix-user:*
Action=org.freedesktop.login1.hibernate;org.freedesktop.login1.handle-hibernate-key;org.freedesktop.login1;org.freedesktop.login1.hibernate-multiple-sessions
ResultActive=yes
EOF"
# <<<<---- DO NOT TOUCH ANY TEXT BETWEEN THESE ARROWS

# Swap partition end



# Prepare for Chroot
Divider
Comment "Prepare for chroot"
# Once we enter chroot, the script will stop running

# DO NOT TOUCH ANY TEXT BETWEEN THESE ARROWS ---->>>>
filename="${mountpoint}/tmp/finish_install.sh"
sudo sh -c "cat << EOF >> $filename
#!/bin/bash

# Mount the temp efivars directory
mount --bind /efivars-temp /sys/firmware/efi/efivars

# Apply changes
update-initramfs -k all -c

# Install the GRUB bootloader
grub-install --efi-directory=/boot/efi

# Update GRUB
grub-mkconfig -o /boot/grub/grub.cfg

# Unmount the efivars directory
umount /sys/firmware/efi/efivars

# Exit chroot
exit
for i in /dev /dev/pts /proc /sys /run; do sudo umount -v ${mountpoint}$i; done
cd /
sudo umount ${mountpoint}/efivars-temp
sudo rmdir ${mountpoint}/efivars-temp
sudo umount -l ${mountpoint}/boot/efi
sudo umount -l ${mountpoint}/boot
sudo umount -l ${mountpoint}

EOF"
# <<<<---- DO NOT TOUCH ANY TEXT BETWEEN THESE ARROWS
chmod +x "$filename"

Comment "Once chroot is complete, this script will stop running"
echo "Continue the script by running"
echo "./tmp/finish_install.sh"

# Chroot time
#for i in /dev /dev/pts /proc /sys /run; do sudo mount -B $i "${mountpoint}$i"; done
# -B for bind, -R for rbind
for i in /dev /dev/pts /proc /sys /run /etc/resolv.conf ;
  do sudo mount -R $i "${mountpoint}$i"; done
echo ""; echo "Performing chroot"
chroot "${mountpoint}"
# Script dies here

exit # We probably won't get to this point

# If the script didn't end, we would have been able to run the following....

# Mount the temp efivars directory
Command "mount --bind /efivars-temp /sys/firmware/efi/efivars"

# Apply changes
Command "update-initramfs -k all -c"

# Install the GRUB bootloader
Command "grub-install --efi-directory=/boot/efi"

# Update GRUB
Command "grub-mkconfig -o /boot/grub/grub.cfg"

# Unmount the efivars directory
Command "umount /sys/firmware/efi/efivars"

# Exit chroot
Command "exit"
Command "for i in /dev /dev/pts /proc /sys /run; do sudo umount -v ${mountpoint}$i; done"
Command "cd /"
Command "umount ${mountpoint}/efivars-temp" # Unmount the temp efivars directory
Command "rmdir ${mountpoint}/efivars-temp"  # Remove the temp dir
Command "umount -l ${mountpoint}/boot/efi"
Command "umount -l ${mountpoint}/boot"
Command "umount -l ${mountpoint}"

# Done
echo ""; echo ""; echo "All done!  The script is ending"

exit
# For reference, to change the LUKS password
# cryptsetup luksChangeKey $root_partno

# Use these last lines for exiting chroot
for i in /dev /dev/pts /proc /sys /run; do sudo umount -v ${mountpoint}$i; done
sudo umount ${mountpoint}/boot/efi
sudo umount ${mountpoint}/boot
sudo umount ${mountpoint}
sudo rmdir ${mountpoint}/efivars-temp

