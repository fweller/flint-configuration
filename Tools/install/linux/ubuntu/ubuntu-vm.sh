#!/bin/bash

# ubuntu iso installers suck
# here are some methods to improve usability as a vm client

# Edit the username for the appropraite ubuntu flavor
flavor="ubuntu"

# change the login password for the user account
sudo usermod -G root "${flavor}"; echo "${flavor}:${flavor}" | sudo chpasswd
# ignore the BAD PASSWORD response

# install virtualbox guest editions as best as we can
sudo apt update
#sudo apt -y install build-essential dkms linux-headers-"$(uname -r)" && \
sudo apt -y install virtualbox-guest-utils virtualbox-guest-x11

# enable ssh and report the ip addresses
sudo apt update; sudo apt -y install ufw openssh-server; sudo systemctl enable ssh --now; sudo ufw allow ssh; ip a | grep 'inet'

exit

# Tips for downloading this file to an ubuntu iso
# Easiest: Firefox: navigate to flintweller.com, click on scripts, download entire zip, unzip, run tools
export ubuntuvm="https://gitlab.com/fweller/flint-configuration/-/raw/master/Tools/install/linux/ubuntu-vm.sh"
export tools="https://gitlab.com/fweller/flint-configuration/-/raw/master/Tools/install/linux/tools.sh"
# Command to download only this file
wget "$ubuntuvm"
# Command to download all of my tools instead
wget "$tools"
