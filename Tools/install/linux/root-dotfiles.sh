#!/bin/bash
# ----------------------------------------
# NAME        : root-dotfiles.sh
# DESCRIPTION : Copy my preferred dotfiles to the root account
# ----------------------------------------


# ----------------------------------------
# Issue command

Command() {
  echo ""
  echo "COMMAND : $1"
  eval "$1"
}


# ----------------------------------------
# Say hello

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : Copying dotfiles and other configuration to /root"


# ----------------------------------------
# Copy my dotfiles to root

echo ""
echo "----------------------------------------"
echo "NOTICE  : Copying my dotfiles to /root"

Command "sudo -S cp -f $HOME/.bashrc /root"
Command "sudo -S cp -f $HOME/.bash_profile /root"
Command "sudo -S cp -f $HOME/.inputrc /root"
Command "sudo -S cp -f $HOME/.profile /root"
Command "sudo -S cp -f $HOME/.shellrc /root"
Command "sudo -S cp -f $HOME/.vimrc /root"


# ----------------------------------------
# Copy Vim directory to root

echo ""
echo "----------------------------------------"
echo "NOTICE  : Copying my Vim directory to /root"

Command "sudo -S cp -rf $HOME/.vim /root"


# ----------------------------------------
# Copy my tools directory to root

echo ""
echo "----------------------------------------"
echo "NOTICE  : Copying my Tools directory to /root"

Command "sudo -S cp -rf ${HOME}/Tools /root"


# ----------------------------------------
# Finish

echo ""
echo "END     : Copying dotfiles and other configuration to /root"
echo "SPACER  : ----------------------------------------"
exit 0 # Success

