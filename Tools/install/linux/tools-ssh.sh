#!/bin/bash
# ----------------------------------------
# NAME        : tools-ssh.sh
# DESCRIPTION : Install my tools read-write over ssh
# ----------------------------------------


# ----------------------------------------
# Variables

Name="flint-configuration"
Project="https://gitlab.com/fweller/flint-configuration.git"
DownloadsDir="$HOME/Downloads"
InstallDir="$HOME" # Production
#InstallDir="$HOME/Downloads/Install" # Test
GitConfigFile="$HOME/.gitconfig"


# ----------------------------------------
# Package name variables
# Try to be cognizant of differences in package names between distros
Packages="git" # Default
PackagesApt=$Packages
PackagesDnf=$Packages
PackagesZypper=$Packages
PackagesPacman=$Packages


# ----------------------------------------
# Wait for user input

WaitForKeyPress() {
  echo ""
  echo "--------------------------------------------------------------------------------"
  read -r -p "REQUEST  : When you are ready, press <SPACE> to continue, or press <CTRL> \"C\" to exit." -n1 -s
  echo ""
}


# ----------------------------------------
# Comment nicely

Comment() {
  echo ""
  echo "--------------------------------------------------------------------------------"
  echo "$1"
  echo ""
}


# ----------------------------------------
# Install Dependencies

# Detect package manager and install packages
if command -v apt >/dev/null 2>&1; then
    sudo apt update && sudo apt install -y "$PackagesApt"
elif command -v dnf >/dev/null 2>&1; then
    sudo dnf install -y "$PackagesDnf"
elif command -v zypper >/dev/null 2>&1; then
    sudo zypper install -y "$PackagesZypper"
elif command -v pacman >/dev/null 2>&1; then
    sudo pacman -Syu --noconfirm "$PackagesPacman"
else
    echo "Package manager not supported."
    exit 1
fi


# ----------------------------------------
# Prepare directories

mkdir -p "$DownloadsDir"
mkdir -p "$InstallDir"


# ----------------------------------------
# Move to Downloads directory and clean up

Comment "Cleaning up download directory : $DownloadsDir"
cd "$DownloadsDir" || { echo "Cannot change directory to $DownloadsDir"; exit 1; }
rm -rf "$Name"*


# ----------------------------------------
# Configure git if neeed

if [ ! -f "$GitConfigFile" ]; then
        cat << EOF >> "$GitConfigFile"
[user]
        name = Flint Weller
        email = flint.weller@gmail.com
[color]
        ui = auto
EOF
    Comment "Git configuration created at $GitConfigFile."
else
    Comment "Git configuration already exists."
fi


# ----------------------------------------
# Confirming git settings.

Comment "Confirming git settings:"
git config -l || { echo "Failed to list Git settings."; exit 1; }
WaitForKeyPress


# ----------------------------------------
# Test

Comment "Testing SSH to GitLab. You should see something like: Welcome to GitLab, @fweller!  "
ssh -T git@gitlab.com || { echo "SSH test to GitLab failed."; exit 1; }
WaitForKeyPress


# ----------------------------------------
# Clone my project

if ! git clone "$Project"; then
    echo "Failed to clone the repository."
    exit 1
fi

# ----------------------------------------
# Move my project to the Install directory

#echo "NOTICE  : The next line will have an error that it cannot stat....."
#echo "NOTICE  : This is expected and normal"
#cp -rf "$Name/" "$InstallDir/" && cp -rf "$Name/." "$InstallDir/"
#cp -rf $Name/* $Name/.[!.]* $Name/..?* $InstallDir


Comment "Moving the project to the Install directory."
if ! cp -rf "$Name/" "$InstallDir/" && cp -rf "$Name/." "$InstallDir/"; then
    echo "Failed to copy project files to Install directory."
    exit 1
fi

# ----------------------------------------
# Source .shellrc

cd "$HOME" || { echo "Cannot change directory to $HOME"; exit 1; }
if [ -f ".shellrc" ]; then
    source .shellrc
else
    echo "WARNING: .shellrc not found."
fi

# ----------------------------------------
# End

exit 0 # Success

