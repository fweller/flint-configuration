#!/bin/bash
# ----------------------------------------
# NAME        : setup_sudo_access.sh
# DESCRIPTION : Adds the current user to the sudo group and the sudoers file on a Debian-based Linux distribution.
# ----------------------------------------

# ----------------------------------------
# Variables
UserName="$USER"

# ----------------------------------------
# Start of the script

echo ""
echo "----------------------------------------"
echo "START   : $(date) - Sudo Setup Script"

# ----------------------------------------
# Check if the script is being run without sudo

if [ "$(id -u)" -eq 0 ]; then
  echo "ERROR: This script should not be run as root. Please run it as a regular user."
  exit 1
fi

# ----------------------------------------
# Add the user to the sudo group

echo ""
echo "NOTICE  : Adding $UserName to the sudo group."
su -c "/usr/sbin/usermod -aG sudo $UserName"

# Check if the command was successful
if [ $? -ne 0 ]; then
  echo "ERROR   : Failed to add $UserName to the sudo group."
  exit 1
else
  echo "SUCCESS : $UserName has been added to the sudo group."
fi

# ----------------------------------------
# Check if the user is already in the sudoers file

echo ""
echo "NOTICE  : Checking if $UserName is already in the sudoers file."

if su -c "grep -q '^$UserName ' /etc/sudoers"; then
  echo "NOTICE  : $UserName is already in the sudoers file. No need to add again."
else
  echo "NOTICE  : $UserName is not in the sudoers file. Adding now."
  su -c "echo '$UserName ALL=(ALL:ALL) ALL' | tee -a /etc/sudoers > /dev/null"

  # Check if the command was successful
  if [ $? -ne 0 ]; then
    echo "ERROR   : Failed to add $UserName to the sudoers file."
    exit 1
  else
    echo "SUCCESS : $UserName has been added to the sudoers file."
  fi
fi

# ----------------------------------------
# Finish

echo ""
echo "END     : $(date) - Sudo Setup Script"
exit 0


