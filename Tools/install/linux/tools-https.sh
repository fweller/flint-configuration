#!/bin/bash
# ----------------------------------------
# NAME        : tools-https.sh
# DESCRIPTION : Install my tools read-only over https
# ----------------------------------------


# ----------------------------------------
# Variables

Name="flint-configuration"
Project="https://gitlab.com/fweller/flint-configuration.git"
DownloadsDir="$HOME/Downloads"
InstallDir="$HOME" # Production
#InstallDir="$HOME/Downloads/Install" # Test

# ----------------------------------------
# Package name variables
# Try to be cognizant of differences in package names between distros
Packages="git" # Default
PackagesApt=$Packages
PackagesDnf=$Packages
PackagesZypper=$Packages
PackagesPacman=$Packages


# ----------------------------------------
# Install Dependencies

# Detect package manager and install packages
if command -v apt >/dev/null 2>&1; then
    sudo apt update && sudo apt install -y "$PackagesApt"
elif command -v dnf >/dev/null 2>&1; then
    sudo dnf install -y "$PackagesDnf"
elif command -v zypper >/dev/null 2>&1; then
    sudo zypper install -y "$PackagesZypper"
elif command -v pacman >/dev/null 2>&1; then
    sudo pacman -Syu --noconfirm "$PackagesPacman"
else
    echo "Package manager not supported."
    exit 1
fi


# ----------------------------------------
# Prepare directories

mkdir -p "$DownloadsDir"
mkdir -p "$InstallDir"


# ----------------------------------------
# Move to Downloads directory and clean up

cd "$DownloadsDir" || { echo "Cannot change directory to $DownloadsDir"; exit 1; }
rm -rf "$Name*"


# ----------------------------------------
# Clone my project

if ! git clone "$Project"; then
    echo "Failed to clone the repository."
    exit 1
fi

# ----------------------------------------
# Move my project to the Install directory

echo "NOTICE  : The next line will have an error that it cannot stat....."
echo "NOTICE  : This is expected and normal"
cp -rf "$Name/" "$InstallDir/" && cp -rf "$Name/." "$InstallDir/"
#cp -rf $Name/* $Name/.[!.]* $Name/..?* $InstallDir


# ----------------------------------------
# Source .shellrc

cd "$HOME" || { echo "Cannot change directory to $HOME"; exit 1; }
if [ -f ".shellrc" ]; then
    source .shellrc
else
    echo "WARNING: .shellrc not found."
fi

# ----------------------------------------
# End

exit 0 # Success

