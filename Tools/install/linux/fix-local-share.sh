#!/bin/bash
# ----------------------------------------
# NAME        : fix-local-share.sh
# DESCRIPTION : Fix local share directory permissions
# ----------------------------------------

# ----------------------------------------
# Work

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : Fixing a possible permissions issue in $HOME/.local/share"

pushd "$HOME/.local/share" || exit

Command="sudo chown -R $USER *"
echo ""
echo "COMMAND : $Command"
eval "$Command"

Command="sudo chgrp -R $USER *"
echo ""
echo "COMMAND : $Command"
eval "$Command"

popd || exit


# ----------------------------------------
# Finish

echo ""
echo "END     : Fixing a possible permissions issue in $HOME/.local/share"
echo "SPACER  : ----------------------------------------"
exit 0 # Success

