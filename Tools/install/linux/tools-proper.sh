#!/bin/bash
# ----------------------------------------
# NAME        : tools-proper.sh
# SUMMARY     : My proper GitLab tools installer.
# DESCRIPTION : This script assumes that the SSH folder has been installed.
# ----------------------------------------


# ----------------------------------------
# Instructions

# 1) Copy SSH Folder to $HOME
# 2) Copy and paste the following line into the terminal
# curl https://gitlab.com/fweller/flint-configuration/-/raw/master/Tools/install/linux/tools-enabler.sh | bash


# ----------------------------------------
# Variables

Name="flint-configuration"
Project="git@gitlab.com:fweller/flint-configuration.git"
DownloadsDir="$HOME/Downloads"
InstallDir="$HOME"
GitConfigFile="$HOME/.gitconfig"
SSHDir="$HOME/.ssh"
ToolsDir="$InstallDir/Tools"


# ----------------------------------------
# Say hello

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : Proper Tools installation"

# ----------------------------------------
# Start

echo ""
echo "SPACER  : ----------------------------------------"
echo "NOTICE  : This script assumes that the SSH folder has been installed already."
read -r -n1 -s -p "WAIT   : Press <SPACE> to continue."

# ----------------------------------------
# Check for SSH folder

if [ ! -d "$SSHDir" ]; then
  echo ""
  echo "SPACER  : ----------------------------------------"
  echo "ERROR   : $SSHDir has not been installed."
  exit 2 # No such file or directory
fi


# ----------------------------------------
# Install Dependencies

sudo apt install git


# ----------------------------------------
# Configure git

if [ ! -f "$GitConfigFile" ]; then
  cat << EOF >> "$GitConfigFile"
[user]
        name = Flint Weller
        email = flint.weller@gmail.com
[color]
        ui = auto
EOF
fi


# ----------------------------------------
# Confirm git settings

echo ""
echo "SPACER  : ----------------------------------------"
echo "NOTICE  : Check for proper git configuration."
echo ""
git config -l
read -r -n1 -s -p "WAIT   : Press <SPACE> to continue."


# ----------------------------------------
# Test SSH to GitLab

echo ""
echo "SPACER  : ----------------------------------------"
echo "NOTICE  : Testing SSH to GitLab."
echo ""
ssh -T git@gitlab.com
read -r -n1 -s -p "WAIT   : Press <SPACE> to continue."


# ----------------------------------------
# Prepare directories

mkdir -p "$DownloadsDir"
mkdir -p "$InstallDir"


# ----------------------------------------
# Move to Downloads directory and clean up

cd "$DownloadsDir" || exit 2 # No such file or directory
rm -rf ${Name}*


# ----------------------------------------
# Clone my project

git clone ${Project}


# ----------------------------------------
# Move my project to the Install directory

echo ""
echo "SPACER  : ----------------------------------------"
echo "NOTICE  : The following error message is expected and normal:"
echo "          cp: cannot stat 'flint-configuration/..?*': No such file or directory"
cp -rf ${Name}/* ${Name}/.[!.]* ${Name}/..?* "${InstallDir}"


# ----------------------------------------
# Finish

echo ""
#echo "SPACER  : ----------------------------------------"

if [ -d "$InstallDir" ]; then
  echo "NOTICE  : Tools have successfully been installed."
  cd "$HOME" || exit 2 # No such file or directory
  source "$HOME/.bashrc"
  echo "END     : Proper Tools installation"
  echo "SPACER  : ----------------------------------------"
  exit 0 # Success
else
  echo "ERROR   : Tools have not been properly installed."
  cd "$HOME" || exit 2 # No such file or directory
  echo "END     : Proper Tools installation"
  echo "SPACER  : ----------------------------------------"
  exit 2 # No such file or directory
fi

