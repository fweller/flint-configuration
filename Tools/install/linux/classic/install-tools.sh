#!/bin/bash
# install-tools.bash
# Call once in order to set up my tools from GitLab
# NOTE: Bash is required for using pushd, popd, and read.

# Installing for the first time?
# wget https://gitlab.com/fweller/flint-configuration/-/raw/master/Tools/install/linux/install-tools.sh


# Variables used in this script
ScriptName="install-tools.bash"
TaskCounter=0
CommandCounter=0
HomeDir=""
ProjectsDir=""
GitConfigFile=""
TestAppResult=""

Usage() {
echo "Usage:"
echo "$ bash $ScriptName run    # Run the installer"
echo "$ bash $ScriptName debug  # Debug this script"
exit 1
}

InstallInfo() {
TaskCounter=$((TaskCounter + 1))
CommandCounter=0
echo "------------------------------------------------------------"
echo "------------------------------------------------------------"
echo "$TaskCounter-$CommandCounter: $1"
echo ""
}

Command() {
CommandCounter=$((CommandCounter + 1))
echo ""
echo "------------------------------------------------------------"
echo "$TaskCounter-$CommandCounter: $1"
echo ""
eval "$1"
}

WaitForKeyPress() {
echo "------------------------------------------------------------"
read -p "When you are ready, press <SPACE> to continue, or press <CTRL> \"C\" to exit." -n1 -s
echo ""
echo "------------------------------------------------------------"
}

TestApp() {
  TestApp=$1
  TestAppCmd="command -v $TestApp"
  TestAppResult=$($TestAppCmd)
  echo "Checking if $1 is installed: $TestAppResult"
}


InstallGit() {
InstallInfo "NOTICE: Making sure that git is installed, and if not, install it"
TestApp "git"
if [ ! "$TestAppResult" ]; then
  echo "NOTICE: git is not installed.  I am installing it now"
	Command "sudo apt update && sudo apt install git"
else
  echo "NOTICE: git is installed.  I will not try to install it again."
fi
InstallInfo "NOTICE: Making sure that git is configured with $GitConfigFile"
if [ ! -f "$GitConfigFile" ]; then
	cat << EOF >> "$GitConfigFile"
[user]
	name = Flint Weller
	email = flint.weller@gmail.com
[color]
	ui = auto
EOF
else
  echo "NOTICE: git is already configured."
fi
InstallInfo "Confirming git settings."
Command "git config -l"
WaitForKeyPress
}

InstallSSH() {
InstallInfo "Testing if ssh is set up, and if not, set it up."
SSHFile="$HOME/.ssh/id_rsa"
echo "NOTICE: Checking for existance of $SSHFile"
if [ ! -f "$SSHFile" ]; then
  echo "NOTICE: SSH is not configured.  I will attempt to configure it now."
	Command "ssh-keygen -t rsa -b 4096 -C \"flint.weller@gmail.com\""
	Command "ssh-add ${HOME}/.ssh/id_rsa"
else
  echo "NOTICE: SSH is already configured."
fi
}

ConfigureGitlab() {
InstallInfo "Configure GitLab"
echo "INSTRUCTION: Navigate to GitLab to log in."
echo "https://gitlab.com/users/sign_in"
WaitForKeyPress
echo "INSTRUCTION: Navigate to GitLab profile to add SSH keys."
echo "https://gitlab.com/-/profile/keys"
WaitForKeyPress
echo "NOTICE: Displaying the public key:"
Command "cat $HOME/.ssh/id_rsa.pub"
echo "INSTRUCTION: Copy the above public key to the clipboard and add entry in GitLab SSH keys"
WaitForKeyPress
echo "NOTICE: Testing SSH to GitLab. You should see something like: Welcome to GitLab, @fweller!  "
Command "ssh -T git@gitlab.com"
WaitForKeyPress
}

InstallTools() {
InstallInfo "Installing the Tools"
Command "pushd $ProjectsDir"
Pwd=$(pwd)
echo "NOTICE: Cloning the project into $Pwd"
Command "git clone git@gitlab.com:fweller/flint-configuration.git"
echo "NOTICE: Installing our project."
#  The following error is expected: mv: target '~  ' is not a directory "
echo "EXPECTED RESULT: cp: cannot stat 'flint-configuration/..?*': No such file or directory"

Command "cp -r flint-configuration/* flint-configuration/.[!.]* flint-configuration/..?* $HomeDir"

InstallInfo "Testing the system"
Command "git pull"

InstallInfo "Cleaning up"
Command "rm $HomeDir/README.md $HomeDir/LICENSE.md"
Command "git ls-files --deleted -z | git update-index --assume-unchanged -z --stdin"
Command "popd"  #$HOME
}

echo "------------------------------------------------------------"

# Parse the Command line parameters
if [ $# -eq 0 ]; then
  Usage
fi
for arg do
  case $arg in
    debug|--debug)
      DEBUG="true"
      ;;
    run|--run)
      # Run normally
      ;;
    *)
      Usage
      ;;
  esac
done

# Configure for Debug or Run
if [ -n "$DEBUG" ]; then
  HomeDir="$HOME/tmp/home"
  echo "$ScriptName DEBUG Enabled."
else
  HomeDir="$HOME"
  echo "$ScriptName RUN Enabled."
fi

GitConfigFile="$HomeDir/.gitconfig"
ProjectsDir="$HomeDir/Projects"
echo "    HomeDir       = $HomeDir"
echo "    ProjectsDir   = $ProjectsDir"
echo "    GitConfigFile = $GitConfigFile"

Command "mkdir -p $HomeDir"
Command "mkdir -p $ProjectsDir"

# Call above functins here
InstallGit
InstallSSH
ConfigureGitlab
InstallTools

echo "------------------------------------------------------------"
echo ""
echo "Done!"
exit 0
