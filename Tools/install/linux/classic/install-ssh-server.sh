#!/bin/sh
# install-ssh-server.sh
sudo apt update && sudo apt install -y openssh-server
sudo systemctl start ssh
sudo ufw allow ssh
sudo systemctl status ssh
