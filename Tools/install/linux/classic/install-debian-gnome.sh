#!/bin/bash
# install-debian-gnome.sh
# Purpose: Configure Debian-based Gnome for my needs
# NOTE: Bash is required for using pushd, popd, and read.

# Variables used in this script
ScriptName="install-debian-gnome.sh"
TaskCounter=0
CommandCounter=0
HomeDir=""
DownloadDir=""
ProjectsDir=""
ConfigDir=""
#InstallURL=""
InstallDir=""
HomeDir=""

Usage() {
echo "Usage:"
echo "$ bash $ScriptName run    # Run the installer"
echo "$ bash $ScriptName debug  # Debug this script"
exit 1
}

InstallInfo() {
TaskCounter=$((TaskCounter + 1))
CommandCounter=0
echo "------------------------------------------------------------"
echo "------------------------------------------------------------"
echo "$TaskCounter-$CommandCounter: $1"
echo ""
}

Command() {
CommandCounter=$((CommandCounter + 1)) echo ""
echo "------------------------------------------------------------"
echo "$TaskCounter-$CommandCounter: $1"
echo ""
eval "$1"
}

InstallPackages() {
echo "------------------------------------------------------------"
echo "Installing packages"
sudo apt update
if [ -z "$DEBUG" ]; then
  sudo apt install vim-gtk3 gnome-tweaks chrome-gnome-shell \
    gnome-shell-extensions dconf-editor indicator-multiload \
    gnome-shell-extension-dash-to-panel gir1.2-wnck-3.0
  # gnome-shell-extension-arc-menu
fi
}

InstallGnomePutWindows() {
InstallInfo "Install Gnome Put-Windows"
InstallDir="$ProjectsDir/putWindow@clemens.lab21.org"
LinkDir="$HomeDir/.local/share/gnome-shell/extensions"
echo "------------------------------------------------------------"
echo "NOTICE: To retreive the put-windows user settings, issue this command:"
echo "dconf dump /org-lab21-putwindow/ > gnome-dconf-putwindow-config1.ini"
echo "dconf dump /org/gnome/shell/extensions/org-lab21-putwindow/ > gnome-dconf-putwindow-config2.ini"
echo "------------------------------------------------------------"
Command "pushd $DownloadDir"
Pwd=$(pwd); echo "NOTICE: Downloading to location $Pwd"
Command "wget https://github.com/negesti/gnome-shell-extensions-negesti/releases/download/v32/putWindow@clemens.lab21.org.zip"
echo "NOTICE: Installing to location $InstallDir"
Command "unzip putWindow@clemens.lab21.org.zip -d $InstallDir"
Command "mkdir -p $LinkDir"
echo "NOTICE: Creating symbolic link $InstallDir $LinkDir"
Command "ln -s $InstallDir $LinkDir"
echo "NOTICE: Adding user settings to gnome using dconf"
if [ -z "$DEBUG" ]; then
  Command "dconf load /org-lab21-putwindow/ < $ConfigDir/gnome-dconf-putwindow-config1.ini"
  Command "dconf load /org/gnome/shell/extensions/org-lab21-putwindow/ < $ConfigDir/gnome-dconf-putwindow-config2.ini"
fi
Command "popd"
}

InstallTopIcons-Plus() {
#TODO Where are the configurations stored?
InstallInfo "Install Gnome TopIcons-Plus"
InstallDir="$ProjectsDir/TopIcons-plus"
echo "------------------------------------------------------------"
echo "NOTICE: There are no dconf settings for TopIcons-Plus."
Command "pushd $ProjectsDir"
Command "git clone https://github.com/phocean/TopIcons-plus.git"
Command "pushd $InstallDir"
if [ -z "$DEBUG" ]; then Command "make install"; fi
Command "popd"
Command "popd"
}

InstallDashToPanel() {
  echo "TODO - Complete this"
}

InstallFreon() {
  echo "TODO - Complete this"
}


InstallSpotify() {
Command "pushd $DownloadDir"
Command "curl -sS https://download.spotify.com/debian/pubkey_5E3C45D7B312C643.gpg | sudo apt-key add - "
#Command "echo \"deb http://repository.spotify.com stable non-free\" | sudo tee /etc/apt/sources.list.d/spotify.list"
#Then you can install the Spotify client:
Command "sudo apt-get update && sudo apt-get install spotify-client"
Command "popd"
}

echo "------------------------------------------------------------"

# Parse the Command line parameters
if [ $# -eq 0 ]; then
  Usage
fi
for arg do
  case $arg in
    debug|--debug)
      DEBUG="true"
      ;;
    run|--run)
      # Run normally
      ;;
    *)
      Usage
      ;;
  esac
done

# Configure for Debug or Run
if [ -n "$DEBUG" ]; then
  HomeDir="${HOME}/tmp"
  #RootDir="$HomeDir/root"
  echo "$ScriptName DEBUG Enabled."
else
  HomeDir="${HOME}"
  #RootDir=""
  echo "$ScriptName RUN Enabled."
fi

DownloadDir="${HOME}/Downloads"
ConfigDir="${HOME}/Tools/config/linux"
ProjectsDir="$HomeDir/Projects"
echo "    HomeDir       = $HomeDir"
echo "    DownloadDir   = $DownloadDir"
echo "    ProjectsDir   = $ProjectsDir"
echo "    ConfigDir     = $ConfigDir"
echo "    HomeDir       = $HomeDir"
Command "mkdir -p $DownloadDir"
Command "mkdir -p $ProjectsDir"
Command "mkdir -p $HomeDir"

# This is where we call the above functions to run
InstallPackages
InstallGnomePutWindows
InstallTopIcons-Plus
InstallDashToPanel
InstallFreon
InstallSpotify


echo "------------------------------------------------------------"
echo "INSTRUCTION: Enable gnome extensions in the tweaks app"

echo "------------------------------------------------------------"
echo ""
echo "Done!"
exit 0
