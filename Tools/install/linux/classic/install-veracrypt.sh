#!/bin/sh
#install-veracrypt.sh
#usage: sh ./install-veracrypt.sh



echo "-------------------------------------------------"
echo "install-veracrypt.sh"
echo "Go here for help https://github.com/veracrypt/VeraCrypt"
echo "-------------------------------------------------"

echo "install building apps"
sudo apt install g++ libfuse-dev pkg-config yasm libwxbase3.0-dev

echo "install wxwidgets"
sudo apt install -y libwx*

echo "install makeself"
sudo apt install -y makeself

echo "install fuse"
sudo apt install -y libfuse-dev fuse

echo "-------------------------------------------------"
echo "clone"
mkdir ${HOME}/Projects
cd ${HOME}/Projects
git clone https://github.com/veracrypt/VeraCrypt.git
cd VeraCrypt

#will not build package properly without the right tag selected
git checkout VeraCrypt_1.25.9

cd src
pwd

echo "-------------------------------------------------"
echo "build"
make clean
make

echo "-------------------------------------------------"
echo "build package"
make package

echo "-------------------------------------------------"
echo "install manually"
cd Setup/Linux
echo "Run installer application called veracrypt-1.25.9-setup-gui-x64"
pwd
ls

echo "-------------------------------------------------"
