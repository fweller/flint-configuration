#!/bin/bash
# install-root.sh
# copy my dotfiles to root so root has some settings i like
# to do: clean this up so that it doesn't rely on my tools

# --------------------------------------------------
# Check for Sudo
if [[ $UID != 0 ]]; then
    echo "Please run this script with sudo:"
    echo "sudo bash $0 $*"
    exit 1
fi
# --------------------------------------------------

# Copy my dotfiles to root
cp /home/flint/.bashrc /root
cp /home/flint/.bash_profile /root
cp /home/flint/.inputrc /root
cp /home/flint/.profile /root
cp /home/flint/.profile-flint /root
cp /home/flint/.vimrc /root

# Copy my dockerfile aliases dotfile to root
cp /home/flint/Tools/dockerfiles/dotfiles/aliases /root/.aliases

# Copy my tools directory to root
cp -r /home/flint/tools /root/tools


echo "Done with installation"
# END


