#!/bin/bash

mkdir -p "${HOME}/Downloads"
mkdir -p "${HOME}/Builds"

echo "Is this Debian or Ubuntu?"
Distro_ID=$(lsb_release --id --short | tr "[:upper:]" "[:lower:]")
case $Distro_ID in
  'debian')
    echo "This is a Debian distribution."
    Debian=true
    ;;
  'ubuntu')
    echo "This is an Ubuntu distribution."
    Ubuntu=true
    ;;
  *)
    echo "This distro is called $Distro_ID"
    echo "This is neither Debian nor Ubuntu"
    echo "Use at your own risk"
    echo "Continuing..."
    ;;
esac

veracrypt(){
  if [ "$Ubuntu" ]; then
    # VeraCrypt
    # https://www.veracrypt.fr/en/Downloads.html
    pushd "${HOME}/Downloads"
    wget https://launchpad.net/veracrypt/trunk/1.25.9/+download/veracrypt-1.25.9-Ubuntu-22.04-amd64.deb
    sudo apt install -y ./veracrypt*.deb
    popd
  else # debian or other
    # VeraCrypt
    # https://www.veracrypt.fr/en/Downloads.html
    pushd "${HOME}/Downloads"
    wget https://launchpad.net/veracrypt/trunk/1.25.9/+download/veracrypt-1.25.9-Debian-11-amd64.deb
    #sudo dpkg -i veracrypt*.deb
    sudo apt install -y ./veracrypt*.deb
    popd
  fi
}

chrome(){
  # Google Chrome
  pushd "${HOME}/Downloads"
  wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb
  #sudo dpkg -i google-chrome-stable_current_amd64.deb
  sudo apt install -y ./google-chrome-stable_current_amd64.deb
  popd
}

osync(){
  # Osync
  pushd "${HOME}/Builds"
  #git clone -b "stable" https://github.com/deajan/osync
  git clone -b "old-stable" https://github.com/deajan/osync
  cd osync
  sudo bash install.sh
  popd
}

appimagelauncher(){
  # https://github.com/TheAssassin/AppImageLauncher
  pushd "${HOME}/Downloads"
  wget https://github.com/TheAssassin/AppImageLauncher/releases/download/v2.2.0/appimagelauncher_2.2.0-travis995.0f91801.bionic_amd64.deb
  #sudo dpkg -i appimagelauncher*.deb
  sudo apt install -y ./appimagelauncher*.deb
  popd
}

balenaetcher(){
  # Balena Etcher
  # https://github.com/TheAssassin/AppImageLauncher
  pushd "${HOME}/Downloads"
  wget https://github.com/balena-io/etcher/releases/download/v1.14.3/balenaEtcher-1.14.3-x64.AppImage
  cp balena*.AppImage "${HOME}/Applications"
  popd
}


# Specify packages to install
# Comment / Uncomment as needed
veracrypt
chrome
osync
appimagelauncher
balenaetcher

# Clean up the packages
sudo apt update
sudo apt --fix-broken install -y
sudo apt autoremove -y

# Run the disto-specific installers
#./apps-snap.sh
#./apps-flathub.sh

