#!/bin/bash
# https://github.com/TheAssassin/AppImageLauncher

sudo apt install software-properties-common
sudo add-apt-repository ppa:appimagelauncher-team/stable
sudo apt update
sudo apt install appimagelauncher
mkdir -p ~/Applications

#move appimage application into ~/Applications

