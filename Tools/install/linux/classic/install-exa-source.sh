#!/bin/sh
#install-exa-source.sh
# Description: This will build exa from source on any platform
#              Should work on any x86, ARM, RISC-V


sudo apt update

# Install cargo
sudo apt install -y cargo
#alternative: curl --proto '=https' --tlsv1.2 -sSf https://sh.rustup.rs | sh

# Create ~/Projects and move there
mkdir -p ${HOME}/Projects
pushd ${HOME}/Projects

# Clone and build
git clone https://github.com/ogham/exa.git
cd exa
cargo build --release

# Install
#does not work: sudo cargo install --path /usr/local/bin
#pwd should be: ~/Projects/exa/target/release
sudo cp ./target/release/exa /usr/local/bin

# Test
echo "Testing exa: you should see a directory listed below"
exa

# End
popd
exit 0
