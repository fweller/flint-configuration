#!/bin/sh
# install-nas.sh
# Purpose: Set up my personal home NAS
# NOTE: Bash is required for using pushd, popd, and read.

# Variables used in this script
ScriptName="install-nas.sh"
TaskCounter=0
CommandCounter=0
InstallInfo=""
HomeDir=""
MountDir=""
TargetDir=""
HostsFile=""
FstabFile=""
CredentialsFile=""
Softlinks=""

Usage() {
echo "Usage:"
echo "$ bash $ScriptName run    # Run the installer"
echo "$ bash $ScriptName debug  # Debug this script"
exit 1
}

InstallInfo() {
TaskCounter=$((TaskCounter + 1))
CommandCounter=0
echo "------------------------------------------------------------"
echo "------------------------------------------------------------"
echo "$TaskCounter-$CommandCounter: $1"
echo ""
}

Command() {
CommandCounter=$((CommandCounter + 1))
echo ""
echo "------------------------------------------------------------"
echo "$TaskCounter-$CommandCounter: $1"
echo ""
eval "$1"
}

InstallMountpoints() {
InstallInfo "Making our mountpoints"
Command "sudo mkdir -p $MountDir/SkyData"
Command "sudo mkdir -p $MountDir/SkyStore"
Command "sudo mkdir -p $MountDir/SkyPenguin"
Command "sudo mkdir -p $MountDir/SkyWork"
Command "sudo mkdir -p $MountDir/SkyMusic"
Command "sudo mkdir -p $MountDir/SkyVideo"
}

InstallHosts() {
InstallInfo "Updating $HostsFile with NAS information"
echo "SPECIAL: Will execute this command: sudo sh -c cat << EOF >> $HostsFile"
# DO NOT TOUCH ANY TEXT BETWEEN THESE ARROWS ---->>>>
sudo sh -c "cat << EOF >> $HostsFile

192.168.1.11   SkyNet
EOF"
# <<<<---- DO NOT TOUCH ANY TEXT BETWEEN THESE ARROWS
}

InstallFstab() {
InstallInfo "Updating $FstabFile with NAS information"
echo "SPECIAL: Will execute this command: sudo sh -c cat << EOF >> $FstabFile"
# DO NOT TOUCH ANY TEXT BETWEEN THESE ARROWS ---->>>>
sudo sh -c "cat << EOF >> $FstabFile

#SkyNet SkyData NAS
SkyNet:/volume1/SkyPenguin $MountDir/SkyPenguin nfs
//SkyNet/SkyData	$MountDir/SkyData	  cifs credentials=/root/.skynetcredentials,vers=3,rw,uid=flint,gid=users,iocharset=utf8 0 0
//SkyNet/SkyStore	$MountDir/SkyStore	cifs credentials=/root/.skynetcredentials,vers=3,rw,uid=flint,gid=users,iocharset=utf8 0 0
//SkyNet/SkyWork	$MountDir/SkyWork	  cifs credentials=/root/.skynetcredentials,vers=3,rw,uid=flint,gid=users,iocharset=utf8 0 0
//SkyNet/music		$MountDir/SkyMusic	cifs credentials=/root/.skynetcredentials,vers=3,rw,uid=flint,gid=users,iocharset=utf8 0 0
//SkyNet/video		$MountDir/SkyVideo	cifs credentials=/root/.skynetcredentials,vers=3,rw,uid=flint,gid=users,iocharset=utf8 0 0
EOF"
# <<<<---- DO NOT TOUCH ANY TEXT BETWEEN THESE ARROWS
}

InstallCredentials() {
InstallInfo "Creating our credentials file in location $CredentialsFile"
echo "SPECIAL: Will execute this command: cat << EOF >> $CredentialsFile"
# DO NOT TOUCH ANY TEXT BETWEEN THESE ARROWS ---->>>>
sudo sh -c "cat << EOF >> "$CredentialsFile"
username=flint
password=<YourWifeIsWhat?Number?>
EOF"
# <<<<---- DO NOT TOUCH ANY TEXT BETWEEN THESE ARROWS
InstallInfo "Edit the credentials file by adding the password"
sudo vi "$CredentialsFile"
}

InstallMount() {
InstallInfo "Mounting the NAS now."
Command "sudo mount -a"
}

InstallSoftlinks() {
InstallInfo "Creating softlinks in home directory"
Command "ln -s $MountDir/SkyData/ ${Softlinks}/SkyData"
Command "ln -s $MountDir/SkyStore/ ${Softlinks}/SkyStore"
Command "ln -s $MountDir/SkyPenguin/ ${Softlinks}/SkyPenguin"
Command "ln -s $MountDir/SkyWork/ ${Softlinks}/SkyWork"
Command "ln -s $MountDir/SkyMusic/ ${Softlinks}/SkyMusic"
Command "ln -s $MountDir/SkyVideo/ ${Softlinks}/SkyVideo"
}


echo "------------------------------------------------------------"

# Parse the command line parameters
if [ $# -eq 0 ]; then
  Usage
fi
for arg do
  case $arg in
    debug|--debug)
      DEBUG="true"
      ;;
    run|--run)
      # Run normally
      ;;
    *)
      Usage
      ;;
  esac
done

# Configure for Debug or Run
if [ ! -z "$DEBUG" ]; then
  HomeDir="${HOME}/tmp/install-nas-testing"
  RootDir="$HomeDir/root"
  CredentialsFile="$HomeDir/.skynetcredentials"
  echo "$ScriptName DEBUG Enabled."
else
  HomeDir="${HOME}"
  RootDir=""
  CredentialsFile="/root/.skynetcredentials"
  echo "$ScriptName RUN Enabled."
fi

MountDir="$RootDir/mnt"
HostsFile="$RootDir/etc/hosts"
FstabFile="$RootDir/etc/fstab"
Softlinks="$HomeDir/sky"

echo ""
echo "    RootDir         = $RootDir"
echo "    HostsFile       = $HostsFile"
echo "    FstabFile       = $FstabFile"
echo "    MountDir        = $MountDir"
echo "    HomeDir         = $HomeDir"
echo "    CredentialsFile = $CredentialsFile"
echo "    Softlinks       = $Softlinks"
echo ""

InstallInfo "Making directories if needed."
if [ -n "$DEBUG" ]; then
  Command "mkdir -p $HomeDir"
  Command "mkdir -p $RootDir"
  Command "mkdir -p $MountDir"
  Command "mkdir -p $( dirname $HostsFile )"
  Command "mkdir -p $( dirname $FstabFile)"
  Command "mkdir -p $( dirname $CredentialsFile)"
  Command "sudo touch $HostsFile"
  Command "sudo touch $FstabFile"
fi
Command "mkdir -p $Softlinks"

# This is where we call the individual installers
InstallMountpoints
InstallHosts
InstallFstab
InstallCredentials
InstallMount
InstallSoftlinks

echo "------------------------------------------------------------"
echo ""
echo "Done!"
exit 0
