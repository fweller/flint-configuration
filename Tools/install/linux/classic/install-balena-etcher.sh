#!/bin/bash
# install-balena-etcher.sh
# Install Balena-Etcher
# TODO Clean this up someday


ScriptName="install-balena-etcher.sh"
ApplicationName="Balena-Etcher"
RawApplicationName="balenaEtcher"
StoreIn="Projects"
InstallTo="bin"
ApplicationURL="https://github.com/balena-io/etcher/releases/download/v1.7.9/balena-etcher-electron-1.7.9-linux-x64.zip"
CommandCounter=0

Usage() {
echo "Usage:"
echo "$ bash $ScriptName run    # Run the installer"
echo "$ bash $ScriptName debug  # Debug this script"
exit 1
}


Command() {
CommandCounter=$((CommandCounter + 1)) echo ""
echo "------------------------------------------------------------"
echo "$CommandCounter: $1"
echo ""
eval "$1"
}


echo "------------------------------------------------------------"

# Parse the Command line parameters
if [ $# -eq 0 ]; then
  Usage
fi
for arg do
  case $arg in
    debug|--debug)
      DEBUG="true"
      ;;
    run|--run)
      # Run normally
      ;;
    *)
      Usage
      ;;
  esac
done

# Configure for Debug or Run
if [ -n "$DEBUG" ]; then
  HomeDir="${HOME}/tmp"
  echo "$ScriptName DEBUG E/abled."
else
  HomeDir="${HOME}"
  echo "$ScriptName RUN Enabled."
fi

StoreInDir="$HomeDir/$StoreIn"
InstallToDir="$HomeDir/$InstallTo"
ApplicationDir="$StoreInDir/$ApplicationName"
ApplicationLocation="$ApplicationDir/$ApplicationName"
echo "    HomeDir             = $HomeDir"
echo "    StoreInDir          = $StoreInDir"
echo "    ApplicationDir      = $ApplicationDir"
echo "    ApplicationLocation = $ApplicationLocation"
echo "    InstallToDir        = $InstallToDir"

# For DEBUG, doesn't affect RUN
Command "mkdir -p $InstallToDir"
Command "mkdir -p $StoreInDir"
#Command "rm -r $ApplicationDir 2> /dev/null"

Command "pushd $StoreInDir" #Go to the StoreInDir
#Command "wget -O balena.zip $ApplicationURL"
#Command "unzip balena.zip -d $ApplicationName"
Command "pushd $ApplicationName"

# This is just for testing
Command "ls -al"

# Extract the filename that is specific to this version
FileName=$(ls | grep $RawApplicationName)
FileNameFullPath=$(readlink -f $FileName)

# Create a symbolic link
#Command "ln -s $FileNameFullPath $ApplicationName"

# Clean up
Command "popd"
#Command "rm balena.zip"
Command "popd"

# Now create a hard link from /bin to the script
Command "ln ${HOME}/Tools/scripts/balena-etcher.sh ${HOME}/Bin//etcher"



echo "------------------------------------------------------------"
echo ""
echo "Done!"
exit 0

