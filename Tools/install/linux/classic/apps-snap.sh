#!/bin/bash

# Using Canonical's products means accepting the Snap store instead of Flathub

Distro_ID=$(lsb_release --id --short | tr "[:upper:]" "[:lower:]")
case $Distro_ID in
  'ubuntu')
    echo "This is an Ubuntu distribution."
    ;;
  *)
    echo "This distro is called $Distro_ID"
    echo "This is not Ubuntu so I will not install Snaps."
    echo "Exiting"
    exit
    ;;
esac

exit # Remove to install my applications


#--------------------------------------------------
# Install the Snap daemon and the Snap Store
# sudo apt -y install snapd       # Do not need to reinstall snap on Ubuntu
# sudo snap install snap-store    # Unnecessary, should already be present

#--------------------------------------------------
# Remove package that I will install as snaps
sudo apt purge vlc
sudo apt purge thunderbird
sudo apt purge firefox
sudo apt purge firefox-esr
sudo apt purge libreoffice*

#--------------------------------------------------
# Install the core of my favorite Snaps
sudo snap install firefox
sudo snap install spotify
sudo snap install libreoffice
sudo snap install vlc

exit # Remove to install all of my favorite Snaps

#--------------------------------------------------
# Install all of my Snaps
sudo snap install sublime-text --classic
sudo snap install code --classic
sudo snap install gitkraken --classic
sudo snap install thunderbird
sudo snap install remmina

exit # Do not remove or else bad things happen

#--------------------------------------------------
# Extra stuff
sudo snap refresh
sudo snap update
sudo snap upgrade

