#!/bin/bash
# install-debian.sh
# Purpose: Configure Debian for my needs
# NOTE: Bash is required for using pushd, popd, and read.

# Variables used in this script
ScriptName="install-debian.sh"
TaskCounter=0
CommandCounter=0
HomeDir=""
DownloadDir=""
ProjectsDir=""
ConfigDir=""
InstallURL=""
InstallDir=""
HomeDir=""

Usage() {
echo "Usage:"
echo "$ bash $ScriptName run    # Run the installer"
echo "$ bash $ScriptName debug  # Debug this script"
exit 1
}

InstallInfo() {
TaskCounter=$((TaskCounter + 1))
CommandCounter=0
echo "------------------------------------------------------------"
echo "------------------------------------------------------------"
echo "$TaskCounter-$CommandCounter: $1"
echo ""
}

Command() {
CommandCounter=$((CommandCounter + 1)) echo ""
echo "------------------------------------------------------------"
echo "$TaskCounter-$CommandCounter: $1"
echo ""
eval "$1"
}

InstallPackagesStandard() {
InstallInfo "Installing standard software packages"
sudo apt update
if [ -z "$DEBUG" ]; then
  sudo apt install vim neofetch git caffeine cifs-utils nfs-common \
    hw-probe shellcheck python3 python3-pip git pv ufw htop gtkhash \
    fio tio cpufrequtils inxi hwinfo procinfo \
    p7zip-full p7zip-rar bzip2 lzip rzip lrzip lz4 lzma xz-utils \
    pigz pbzip2 lbzip2 plzip lrzip pixz
fi
sudo apt --fix-broken install
}

InstallPackagesDesktop() {
InstallInfo "Installing packages for the desktop"
sudo apt update
if [ -z "$DEBUG" ]; then
  sudo apt install flameshot linssid vlc
fi
sudo apt --fix-broken install
}


InstallPackagesDevelopment() {
InstallInfo "Installing packages for development"
sudo apt update
if [ -z "$DEBUG" ]; then
  sudo apt install iozone3 iperf2 iperf3 tmux 
fi
sudo apt --fix-broken install
}


InstallChrome() {
InstallInfo "Install Chrome"
Command "pushd $DownloadDir"
Pwd=$(pwd); echo "NOTICE: Downloading to location $Pwd"
Command "wget https://dl.google.com/linux/direct/google-chrome-stable_current_amd64.deb"
if [ -z "$DEBUG" ]; then Command "sudo dpkg -i google-chrome-stable_current_amd64.deb"; fi
Command "popd"
}

InstallExa() {
InstallInfo "Install Exa"
InstallDir="$ProjectsDir/Exa"
Command "pushd $DownloadDir"
Pwd=$(pwd); echo "NOTICE: Downloading to location $Pwd"
Command "wget https://github.com/ogham/exa/releases/download/v0.10.1/exa-linux-x86_64-v0.10.1.zip"
Command "unzip exa-linux-x86_64-v0.10.1.zip -d $InstallDir"
Command "popd"
#start install
Command "pushd $InstallDir"
#binary
Command "pushd bin"
CopyDir="$RootDir/usr/local/bin/"
Command "sudo mkdir -p $CopyDir"
Command "sudo cp exa $CopyDir"
Command "popd"
#completions
Command "pushd completions"
CopyDir="$RootDir/etc/bash_completion.d/"
Command "sudo mkdir -p $CopyDir"
Command "sudo cp exa.bash $CopyDir"
Command "popd"
#manpage
Command "pushd man"
CopyDir="$RootDir/usr/share/man/man1/"
Command "sudo mkdir -p $CopyDir"
Command "sudo cp * $CopyDir"
Command "popd"
#finish
Command "popd"
}

InstallOsync() {
InstallInfo "Install Osync"
InstallDir="$ProjectsDir/Exa"
Command "pushd $ProjectsDir"
Pwd=$(pwd); echo "NOTICE: Downloading to location $Pwd"
Command "git clone -b \"stable\" https://github.com/deajan/osync"
Command "pushd osync"
if [ -z "$DEBUG" ]; then Command "sudo bash install.sh"; fi
Command "popd"
Command "popd"
}


RunHardwareProbe() {
InstallInfo "Run Hardware Probe"
if [ -z "$DEBUG" ]; then Command "sudo -E hw-probe -all -upload"; fi
}

InstallSSHServer() {
InstallInfo "Installing SSH server"
if [ -z "$DEBUG" ]; then Command "sudo apt install openssh-server"; fi
sudo apt --fix-broken install
Command "sudo service ssh status"
Command "sudo ufw allow ssh"
Command "hostname -I"
}

#TODO find a way to install this sodftware without using snap store
#0ad       
#boa                  
#firefox    
#standard-notes
#audacity  
#canonical-livepatch  
#code      
#flameshot  


echo "------------------------------------------------------------"

# Parse the Command line parameters
if [ $# -eq 0 ]; then
  Usage
fi
for arg do
  case $arg in
    debug|--debug)
      DEBUG="true"
      ;;
    run|--run)
      # Run normally
      ;;
    *)
      Usage
      ;;
  esac
done

# Configure for Debug or Run
if [ -n "$DEBUG" ]; then
  HomeDir="${HOME}/tmp"
  RootDir="$HomeDir/root"
  echo "$ScriptName DEBUG Enabled."
else
  HomeDir="${HOME}"
  RootDir=""
  echo "$ScriptName RUN Enabled."
fi

DownloadDir="${HOME}/Downloads"
ConfigDir="${HOME}/Tools/config/linux"
ProjectsDir="$HomeDir/Projects"
echo "    HomeDir       = $HomeDir"
echo "    DownloadDir   = $DownloadDir"
echo "    ProjectsDir   = $ProjectsDir"
echo "    ConfigDir     = $ConfigDir"
echo "    HomeDir       = $HomeDir"
Command "mkdir -p $DownloadDir"
Command "mkdir -p $ProjectsDir"
Command "mkdir -p $HomeDir"

# This is where we call the above functions to run
InstallPackagesStandard
#InstallPackagesDesktop
#InstallPackagesDevelopment
#InstallChrome
#InstallExa
InstallOsync
RunHardwareProbe
#InstallSSHServer

echo "------------------------------------------------------------"
echo ""
echo "Done!"
exit 0
