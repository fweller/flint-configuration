#!/bin/bash
# install-grub.sh
# Purpose: Install grub configuration
# NOTE: Bash is required for using pushd, popd, and read.

# Variables used in this script
ScriptName="install-grub.sh"
InstallInfo=""
CommandCounter=0
HomeDir=""
TargetDir=""
ConfigDir=""

Usage() {
echo "Usage:"
echo "$ bash $ScriptName run    # Run the installer"
echo "$ bash $ScriptName debug  # Debug this script"
exit 1
}

Command() {
CommandCounter=$((CommandCounter + 1))
echo ""
echo "------------------------------------------------------------"
echo "$CommandCounter: $InstallInfo"
echo "$CommandCounter: $1"
echo ""
eval "$1"
}


echo "------------------------------------------------------------"

# Parse the command line parameters
if [ $# -eq 0 ]; then
  Usage
fi
for arg do
  case $arg in
    debug|--debug)
      DEBUG="true"
      ;;
    run|--run)
      # Run normally
      ;;
    *)
      Usage
      ;;
  esac
done

# Configure for Debug or Run
if [ ! -z "$DEBUG" ]; then
  HomeDir="${HOME}/tmp"
  TargetDir="${HOME}/tmp"
  echo "$ScriptName DEBUG Enabled."
  touch "$TargetDir/grub"
else
  HomeDir="${HOME}"
  TargetDir="/etc/default"
  echo "$ScriptName RUN Enabled."
fi

ConfigDir="${HOME}/Tools/config/linux"
echo ""
echo "    HomeDir     = $HomeDir"
echo "    TargetDir   = $TargetDir"
echo "    ClonfigDir  = $ConfigDir"
echo ""

InstallInfo="Creating TargetDir"
Command "mkdir -p $TargetDir"

InstallInfo="Backing up the original grub config"
Command "sudo mv $TargetDir/grub $TargetDir/grub.original"

InstallInfo="Installing grub configuration"
Command "sudo cp $ConfigDir/grub $TargetDir/grub"

InstallInfo="Updating grub"
if [ -z "$DEBUG" ]; then
  Command "sudo os-prober"
  Command "sudo grub-mkconfig -o /boot/grub/grub.cfg"
fi

echo "------------------------------------------------------------"
echo ""
echo "Done!"
exit 0
