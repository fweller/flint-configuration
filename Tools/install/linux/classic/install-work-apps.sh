#!/bin/sh
# install-work-apps.sh
# Purpose: Create links from scripts to ~/bin

Command() {
  echo "$1"
  eval "$1"
}

Command "ln ${HOME}/Tools/scripts/work-codewarrior.sh ${HOME}/Bin//codewarrior"
Command "ln ${HOME}/Tools/scripts/work-startapps.sh ${HOME}/Bin//startapps"
Command "ln ${HOME}/Tools/scripts/work-startwindows.sh ${HOME}/Bin//startwindows"
Command "ln ${HOME}/Tools/scripts/work-wireshark.sh ${HOME}/Bin//wireshark"
Command "ln ${HOME}/Tools/scripts/balena-etcher.sh ${HOME}/Bin//etcher"
Command "ln ${HOME}/Tools/scripts/work-moveapps.sh ${HOME}/Bin//moveapps"


exit 0
