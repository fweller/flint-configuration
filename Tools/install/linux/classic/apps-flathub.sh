#!/bin/bash

Distro_ID=$(lsb_release --id --short | tr "[:upper:]" "[:lower:]")
case $Distro_ID in
  'ubuntu')
    echo "This is an Ubuntu distribution."
    echo "I will not install all these flathub application on Ubuntu."
    echo "Exiting"
    exit
    ;;
  *)
    echo "This distro is called $Distro_ID"
    ;;
esac


exit # Remove this to install the basics

#--------------------------------------------------
# Remove packages that I will install as flatpaks
sudo apt -y purge vlc thunderbird firefox firefox-esr libreoffice* firefox* vlc* thunderbird*

#--------------------------------------------------
# Install flatpak and flathub
sudo apt install -y flatpak plasma-discover-backend-flatpak
sudo flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo

#--------------------------------------------------
# Install the basic flatpaks
sudo flatpak install -y flathub org.mozilla.firefox
sudo flatpak install -y flathub org.libreoffice.LibreOffice
sudo flatpak install -y flathub org.videolan.VLC
sudo flatpak install -y flathub com.spotify.Client
#sudo flatpak install -y flathub com.github.tchx84.Flatseal
./fix-local-share.sh
exit # Remove this to continue installing all my applications

#--------------------------------------------------
# Install the rest of my favorite flatpaks
sudo flatpak install -y flathub com.sublimetext.three
#sudo flatpak install -y flathub io.github.celluloid_player.Celluloid
sudo flatpak install -y flathub com.visualstudio.code
#sudo flatpak install -y flathub com.axosoft.GitKraken
#sudo flatpak install -y flathub com.play0ad.zeroad
#sudo flatpak install -y flathub org.darktable.Darktable

exit # Do not remove this or bad things happen

#--------------------------------------------------
# The following is for reference
sudo flatpak repair
sudo flatpak uninstall --all --delete-data
sudo apt remove --autoremove flatpak

