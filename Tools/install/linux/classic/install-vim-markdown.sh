#!/bin/bash
# install-vim-markdown
# Installs the vim-markdown script from plasticboy
# https://vimawesome.com/plugin/markdown-syntax
# https://github.com/plasticboy/vim-markdown/

CommandCounter=0

Command() {
  CommandCounter=$((CommandCounter + 1))
  echo ""
  echo "------------------------------------------------------------"
  echo "$CommandCounter: $1"
  echo ""
  eval "$1"
}

Command "mkdir -p ${HOME}/.vim/pack/vendor/start/vim-markdown"
Command "cd ${HOME}/.vim/pack/vendor/start/vim-markdown"
Command "wget https://github.com/plasticboy/vim-markdown/archive/master.tar.gz"
Command "tar --strip=1 -zxf master.tar.gz"

#mkdir -p ${HOME}/.vim/pack/vendor/start/vim-markdown
#cd ${HOME}/.vim/pack/vendor/start/vim-markdown
#wget https://github.com/plasticboy/vim-markdown/archive/master.tar.gz
#tar --strip=1 -zxf master.tar.gz

exit 0
