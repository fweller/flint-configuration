# install-nas.txt
# Purpose: Set up my personal home NAS


# Create the mountpoints
sudo mkdir -p /mnt/SkyData
C
sudo mkdir -p /mnt/SkyStore
sudo mkdir -p /mnt/SkyPenguin
sudo mkdir -p /mnt/SkyWork
sudo mkdir -p /mnt/SkyMusic
sudo mkdir -p /mnt/SkyVideo

# Edit the hosts file
sudo vim /etc/hosts

# Add this line
192.168.1.11   SkyNet

# Edit the fstab file
sudo vim /etc/fstab

# Add these lines
SkyNet:/volume1/SkyPenguin /mnt/SkyPenguin nfs
//SkyNet/SkyData	/mnt/SkyData	  cifs credentials=/root/.skynetcredentials,vers=3,rw,uid=flint,gid=users,iocharset=utf8 0 0
//SkyNet/SkyStore	/mnt/SkyStore	cifs credentials=/root/.skynetcredentials,vers=3,rw,uid=flint,gid=users,iocharset=utf8 0 0
//SkyNet/SkyWork	/mnt/SkyWork	  cifs credentials=/root/.skynetcredentials,vers=3,rw,uid=flint,gid=users,iocharset=utf8 0 0
//SkyNet/music		/mnt/SkyMusic	cifs credentials=/root/.skynetcredentials,vers=3,rw,uid=flint,gid=users,iocharset=utf8 0 0
//SkyNet/video		/mnt/SkyVideo	cifs credentials=/root/.skynetcredentials,vers=3,rw,uid=flint,gid=users,iocharset=utf8 0 0

# Create the credentials file
sudo vim /root/.skynetcredentials

# Add these lines, make sure to edit
username=flint
password=<YourWifeIsWhat?Number?>

# Mount the NAS
sudo mount -a

# Create symbolic links
mkdir -p ${HOME}/sky
pushd ${HOME}/sky
ln -s /mnt/SkyData/ ${HOME}/SkyData
ln -s /mnt/SkyStore/ ${HOME}/SkyStore
ln -s /mnt/SkyPenguin/ ${HOME}/SkyPenguin
ln -s /mnt/SkyWork/ ${HOME}/SkyWork
ln -s /mnt/SkyMusic/ ${HOME}/SkyMusic
ln -s /mnt/SkyVideo/ ${HOME}/SkyVideo
popd

# End
