# install-tools.txt
# Call once in order to set up my tools from GitLab

# Make sure that git is installed

# Is git configured?
git config -l
cat ${HOME}/.gitconfig

# if not, configure it
git config --global user.name "Flint Weller"
git config --global user.email "flint.weller@gmail.com"
git config --global color.ui true
git config --list

# is SSH configured?
cat ${HOME}/.ssh/id_rsa

# if not, configure it
ssh-keygen -t rsa -b 4096 -C "flint.weller@gmail.com"
ssh-add ${HOME}/.ssh/id_rsa

# Display public key
cat ${HOME}/.ssh/id_rsa.pub

# Sign into Gitlab
https://gitlab.com/users/sign_in

# Navigate to GitLab profile to add SSH keys
https://gitlab.com/-/profile/keys

# Test SSH connection to Gitlab
ssh -T git@gitlab.com

# Clone the directory, move files, and test
mkdir -p ${HOME}/tmp
pushd ${HOME}/tmp
git clone git@gitlab.com:fweller/flint-configuration.git
cp -r flint-configuration/* flint-configuration/.[!.]* flint-configuration/..?* ${HOME}
popd
cd ${HOME}
git pull

# Get rid of annoying files
rm ${HOME}/README.md ${HOME}/LICENSE.md
git ls-files --deleted -z | git update-index --assume-unchanged -z --stdin

# End
