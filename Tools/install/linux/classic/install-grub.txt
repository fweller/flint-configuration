# install-grub.txt
# Purpose: Install grub configuration

# Back up the original grub
sudo cp /etc/default/grub.cfg /etc/default/grub.original

# Copy my preferred grub to the folder
sudo cp ${HOME}/Tools/config/linux/grub /etc/default/grub.preferred

# Compare the files
sudo vimdiff /etc/default/grub.preferred /etc/default/grub.cfg

# When ready, copy the preferred grub over the original and then edit
sudo cp /etc/default/grub.preferred /etc/default/grub.cfg
sudo vim /etc/default/grub.cfg

# Probe for other OSs
sudo os-prober

# Update the grub config file
sudo grub-mkconfig -o /boot/grub/grub.cfg

# End
