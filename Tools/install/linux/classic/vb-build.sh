#!/bin/bash

mkdir -p "${HOME}/VMs"
mkdir -p "${HOME}/VMShare"
mkdir -p "${HOME}/Builds"

# Update these 2 variables as needed
vbversion="7.0.10"  # version number
vbcode="158379"     # code used with downloading files

# These variable shouldn't need to be updated - but if things break, look here first
vbname="VirtualBox-${vbversion}-${vbcode}-Linux_amd64.run"
vbextname="Oracle_VM_VirtualBox_Extension_Pack-${vbversion}.vbox-extpack"
vbsourcename="VirtualBox-${vbversion}"
vbsourceurl="https://download.virtualbox.org/virtualbox/${vbversion}/${vbsourcename}.tar.bz2"

# https://www.virtualbox.org/wiki/Linux%20build%20instructions
#

sudo nala install acpica-tools chrpath doxygen g++-multilib libasound2-dev libcap-dev \
  libcurl4-openssl-dev libdevmapper-dev libidl-dev libopus-dev libpam0g-dev \
  libpulse-dev libqt5opengl5-dev libqt5x11extras5-dev qttools5-dev libsdl1.2-dev libsdl-ttf2.0-dev \
  libssl-dev libvpx-dev libxcursor-dev libxinerama-dev libxml2-dev libxml2-utils \
  libxmu-dev libxrandr-dev make nasm python3-dev python-dev qttools5-dev-tools \
  texlive texlive-fonts-extra texlive-latex-extra unzip xsltproc \
  default-jdk libstdc++5 libxslt1-dev linux-kernel-headers makeself \
  mesa-common-dev subversion yasm zlib1g-dev glslang-tools \
  ia32-libs libc6-dev-i386 lib32gcc1 lib32stdc++6 yasm libxml2-dev libidl-dev \
  libvpx-dev libpng-dev libsdl-dev libxmu-dev qtbase5-dev qt5-qmake qtbase5-dev-tools \
  libdevmapper-dev libcap-dev



# Special tweaks for 64 bit binaries
# Building on a 64bit host still requires 32bit libraries and build tools as the Guest Additions which are part of the build process are 32bit. Note that on 64bit Ubuntu systems some links to shared libraries are missing. This can be fixed with

sudo ln -s libX11.so.6    /usr/lib32/libX11.so
sudo ln -s libXTrap.so.6  /usr/lib32/libXTrap.so
sudo ln -s libXt.so.6     /usr/lib32/libXt.so
sudo ln -s libXtst.so.6   /usr/lib32/libXtst.so
sudo ln -s libXmu.so.6    /usr/lib32/libXmu.so
sudo ln -s libXext.so.6   /usr/lib32/libXext.so



# Start
pushd "${HOME}/Builds"
wget ${vbsourceurl}
tar xvf ${vbsourcename}.tar.bz2
cd ${vbsourcename}
cd 

