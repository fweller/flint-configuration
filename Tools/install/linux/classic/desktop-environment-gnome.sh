#!/bin/bash


ConfigDir="${HOME}/Tools/config/linux"

Command() {
echo "------------------------------------------------------------"
echo ""
eval "$1"
}

InstallPackages() {
sudo apt update && \
sudo apt -y install gnome-tweaks chrome-gnome-shell \
  gnome-shell-extensions dconf-editor indicator-multiload \
  gnome-shell-extension-dash-to-panel gir1.2-wnck-3.0 \
  nautilus-admin
  #timeshift-gtk
  # gnome-shell-extension-arc-menu
}

InstallGnomePutWindows() {
InstallDir="${HOME}/Builds/putWindow@clemens.lab21.org"
LinkDir="${HOME}/.local/share/gnome-shell/extensions"
# "NOTICE: To retreive the put-windows user settings, issue this command:"
# "dconf dump /org-lab21-putwindow/ > gnome-dconf-putwindow-config1.ini"
# "dconf dump /org/gnome/shell/extensions/org-lab21-putwindow/ > gnome-dconf-putwindow-config2.ini"
Command "pushd ${HOME}/Downloads"
Command "wget https://github.com/negesti/gnome-shell-extensions-negesti/releases/download/v32/putWindow@clemens.lab21.org.zip"
Command "unzip putWindow@clemens.lab21.org.zip -d $InstallDir"
Command "mkdir -p $LinkDir"
Command "ln -s $InstallDir $LinkDir"
Command "dconf load /org-lab21-putwindow/ < $ConfigDir/gnome-dconf-putwindow-config1.ini"
Command "dconf load /org/gnome/shell/extensions/org-lab21-putwindow/ < $ConfigDir/gnome-dconf-putwindow-config2.ini"
Command "popd"
}

InstallTopIcons-Plus() {
InstallDir="${HOME}/Builds/TopIcons-plus"
# "NOTICE: There are no dconf settings for TopIcons-Plus."
Command "pushd ${HOME}/Builds"
Command "git clone https://github.com/phocean/TopIcons-plus.git"
Command "cd $InstallDir"
Command "make install"
Command "popd"
}

# This is where we call the above functions to run
InstallPackages
InstallGnomePutWindows
InstallTopIcons-Plus

echo "Still need to install DashToPanel"
echo "Still need to install Freon"

