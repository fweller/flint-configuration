#!/bin/bash
# ----------------------------------------
# NAME        : wsl-nas-skydata.sh
# DESCRIPTION : Configure WSL2 to access my SkyData NAS
# USAGE       : wsl-nas-skydata.sh
# NOTE        : WSL2 does not use /etc/fstab
# ----------------------------------------


# ----------------------------------------
# Variables

NasName="SkyNet"
MountDir="/mnt"
CredentialsFile="/root/.skynetcredentials"
SymlinkDir="$HOME/$NasName"


# ----------------------------------------
# Command and Print

Command() {
  echo ""
  echo "COMMAND : $1"
  eval "$1"
}


# ----------------------------------------
# Say hello

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : SkyData NAS Enablement"


# ----------------------------------------
# Create Directories

echo ""
echo "SPACER  : ----------------------------------------"
echo "NOTICE  : Creating folders in $MountDir"

Command "sudo -S mkdir -p $MountDir/SkyData"
Command "sudo -S mkdir -p $MountDir/SkyWork"
Command "sudo -S mkdir -p $MountDir/SkyStore"
Command "sudo -S mkdir -p $MountDir/SkyPenguin"
Command "sudo -S mkdir -p $MountDir/SkyMusic"
Command "sudo -S mkdir -p $MountDir/SkyVideo"


# ----------------------------------------
# Credentials File for NAS Access

echo ""
echo "SPACER  : ----------------------------------------"
echo "NOTICE  : Creating our credentials file in $CredentialsFile"

# DO NOT TOUCH ANY TEXT BETWEEN THESE ARROWS ---->>>>
sudo sh -c "cat << EOF >> $CredentialsFile
username=flint
password=<YourWifeIsWhat?Number?>
domain=SkyNet
EOF"
# <<<<---- DO NOT TOUCH ANY TEXT BETWEEN THESE ARROWS

echo ""
echo "SPACER  : ----------------------------------------"
echo "NOTICE  : The next step will be to edit the credentials file"

read -r -n1 -s -p "WAIT   : Press <SPACE> to continue."

sudo vi "$CredentialsFile"


# ----------------------------------------
# Mount the NAS

echo ""
echo "SPACER  : ----------------------------------------"
echo "NOTICE  : Attempting to mount $NasName"
bash "$HOME/Tools/scripts/wslmount.sh"



# ----------------------------------------
# Symbolic Links

echo ""
echo "SPACER  : ----------------------------------------"
echo "NOTICE  : Creating symbolic links in folder $SymlinkDir"

Command "mkdir -p $SymlinkDir"
Command "ln -s $MountDir/SkyData/ $SymlinkDir/SkyData"
Command "ln -s $MountDir/SkyWork/ $SymlinkDir/SkyWork"
Command "ln -s $MountDir/SkyStore/ $SymlinkDir/SkyStore"
Command "ln -s $MountDir/SkyPenguin/ $SymlinkDir/SkyPenguin"
Command "ln -s $MountDir/SkyMusic/ $SymlinkDir/SkyMusic"
Command "ln -s $MountDir/SkyVideo/ $SymlinkDir/SkyVideo"


# ----------------------------------------
# Finish

echo ""
echo "END     : SkyData NAS Enablement"
echo "SPACER  : ----------------------------------------"
exit 0 # Success

