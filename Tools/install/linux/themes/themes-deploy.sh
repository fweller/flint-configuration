#!/bin/sh
# Deploy themes from local build dir to Linux OS

ArchiveName="linuxthemes"
#NetworkDir="${HOME}/SkyNet/SkyData/Computers/Linux/"
BuildDir="${HOME}/Builds"
Folder="manual"
DeployDir="/usr/share/"

# Debug
#DeployDir="${HOME}/Tmp/asdf"
#mkdir -p "$DeployDir"

# Change permissions of local directory
sudo chown -R root "${BuildDir}/${ArchiveName}/${Folder}"
sudo chgrp -R root "${BuildDir}/${ArchiveName}/${Folder}"
sudo chmod -R 755 "${BuildDir}/${ArchiveName}/${Folder}"

# Copy all directories and files to deploy directory
sudo cp -rT "${BuildDir}/${ArchiveName}/${Folder}" "${DeployDir}"

# Change permissions back
sudo chown -R flint "${BuildDir}/${ArchiveName}/${Folder}"
sudo chgrp -R flint "${BuildDir}/${ArchiveName}/${Folder}"
sudo chmod -R 775 "${BuildDir}/${ArchiveName}/${Folder}"

