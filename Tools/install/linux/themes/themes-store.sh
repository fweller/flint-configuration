#!/bin/sh
# Copy themes from local build dir to NAS

ArchiveName="linuxthemes"
NetworkDir="${HOME}/SkyNet/SkyData/Computers/Linux/"
BuildDir="${HOME}/Builds/"

# Archive local directory
7z a "${BuildDir}/${ArchiveName}.7z" "${BuildDir}/${ArchiveName}"

# Copy archive to network
rsync "${BuildDir}/${ArchiveName}.7z" "${NetworkDir}"

# Remove local copy of archive
rm "${BuildDir}/${ArchiveName}.7z"

