#!/bin/sh
# Copy themes from NAS to local build directory

ArchiveName="linuxthemes"
NetworkDir="${HOME}/SkyNet/SkyData/Computers/Linux/"
BuildDir="${HOME}/Builds/"

# Remove local copy
rm -rf "${BuildDir}/${ArchiveName}"

# Copy archive from network to local
rsync "${NetworkDir}/${ArchiveName}.7z" "${BuildDir}"

# Extract archive to local directory
7z x "${BuildDir}/${ArchiveName}.7z" -o"${BuildDir}"

# Remove archive
rm "${BuildDir}/${ArchiveName}.7z"

