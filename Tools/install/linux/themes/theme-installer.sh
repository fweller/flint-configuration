#!/bin/bash
# Theme installer

#Configure
#- global theme orchis dark
#- application style breeze
#- plasma style orchis-dark
#- colors orchisdark or materia breath dark
#- window decorations orchis-dark
#- fonts
#- icons papirus-dark
#- cursors ocygen matrix green (or green emerald)
#- splash screen orchis-dark


# 2024 October KDE Plasma 5
sudo apt install \
  orchis-gtk-theme \
  materia-gtk-theme materia-kde \
  arc-kde papirus-icon-theme \
  qt5-style-kvantum qt5-style-kvantum-l10n qt5-style-kvantum-themes


# Future KDE Plasma 6
#kde-style-oxygen-qt6
#qt6-gtk-platformtheme
#qt6-style-kvantum
#qt6-style-kvantum-l10n

# Orchis-kde
# Install from source https://github.com/vinceliuice/Orchis-kde
pushd ~/Builds
git clone https://github.com/vinceliuice/Orchis-kde.git
cd Orchis-kde
./install.sh
popd

# Cursors
# Install from source
pushd ~/Builds
mkdir -p "${HOME}/.icons"
git clone https://github.com/wo2ni/Oxygen-Cursors.git
cd Oxygen-Cursors
cp -avr Oxygen* "${HOME}"/.icons
gtk-update-icon-cache
popd




