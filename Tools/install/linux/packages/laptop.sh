#!/bin/bash
# ----------------------------------------
# NAME        : laptop.sh
# DESCRIPTION : Installs laptop-specific applications and optimizations.
# ----------------------------------------

# ----------------------------------------
# Variables

# Path to the package installer script
PackageInstaller="$HOME/Tools/scripts/package-installer.sh"

# ----------------------------------------
# Function: Command
# Description: Executes a given command, prints it, and captures the output.
Command() {
  echo ""
  echo "COMMAND : $1"
  echo ""
  Output=$($1)
  echo ""
  echo "RETURN  : $Output"
}

# ----------------------------------------
# Function: Installer
# Description: Installs the specified package using the package installer script.
Installer() {
  Command "$PackageInstaller $1"
}

# ----------------------------------------
# Start of the script

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : $(date) - Laptop applications installer"

# ----------------------------------------
# Install TLP to optimize Linux laptop battery life
# TLP Homepage: https://linrunner.de/tlp/index.html

Installer "tlp"
# Do not install tlp-rdw as it may prevent wifi from working again after sleep

# Start TLP and display battery status
Command "sudo systemctl enable tlp"
Command "sudo tlp start"
Command "sudo tlp-stat -s" # Displays the status of TLP
Command "sudo tlp-stat -b" # Displays the battery status

# ----------------------------------------
# Finish

echo ""
echo "END     : $(date) - Laptop applications installer"
echo "SPACER  : ----------------------------------------"

exit 0 # Success

