#!/bin/bash
# ----------------------------------------
# NAME        : console.sh
# DESCRIPTION : Installs preferred console applications.
# ----------------------------------------

# ----------------------------------------
# Variables

# Path to the package installer script
PackageInstaller="$HOME/Tools/scripts/package-installer.sh"

# ----------------------------------------
# Function: Command
# Description: Executes a given command, prints it, and captures the output.
Command() {
  echo ""
  echo "COMMAND : $1"
  echo ""
  Output=$($1)
  echo ""
  echo "RETURN  : $Output"
}

# ----------------------------------------
# Function: Installer
# Description: Installs the specified packages using the package installer script.
Installer() {
  Command "$PackageInstaller $1"
}

# ----------------------------------------
# Start of the script

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : $(date) - Installing console applications"

# ----------------------------------------
# Install console applications

Installer "htop curl wget bzip2 ncdu zstd git git-lfs shellcheck inxi tree"
Installer "exa" # Planned removal in 2026
Installer "eza" # Replacement for exa
Installer "neofetch" # Planned removal in 2026
Installer "fastfetch" # Replacement for neofetch
Installer "7zip" # Replacing p7zip-full in 2024

# If 7zip did not install, then fallback to p7zip
if ! command -v 7z &> /dev/null; then
  Installer "p7zip-full" # Planned removal in 2026
fi

Installer "build-essential dkms cifs-utils bind9-dnsutils"
Installer "hw-probe lshw hwinfo procinfo cpufrequtils lm-sensors cpu-checker cpuid edid-decode"
Installer "duc procps util-linux sysstat iproute2 numactl tcpdump bpfcc-tools"
Installer "bpftrace trace-cmd nicstat ethtool tiptop msr-tools"
#Installer "btrfs-progs" # Only for btrfs installations

# ----------------------------------------
# Finish

echo ""
echo "END     : $(date) - Finished installing console applications"
echo "SPACER  : ----------------------------------------"
exit 0 # Success

# -----------------------------------
# Packages that were removed

# 2024 timeshift. System restore utility that I don't need.
# 2024 p7zip-rar. I'm not using it.
# 2024 neovim. I'm not using it.

# -----------------------------------
# Package Info

# What provides this command?
# dpkg -S <full path>
# Example: dpkg -S /usr/bin/7zz

# 7zip
# https://packages.debian.org/trixie/7zip
# 7-Zip file archiver with a high compression ratio
# Provides
#  7z: Full featured with plugin functionality.
#  7za: Major formats/features only.
#  7zr: LZMA (.7z, .lzma, .xz) only. Minimal executable.
#  7zz: Unknown but it works

# p7zip-full
# https://packages.debian.org/bookworm/p7zip-full
# https://p7zip.sourceforge.net/
# Provides
# 7z
# 7za
# Note: p7zip is a port of 7za for POSIX systems

# 7zip-rar
# https://packages.debian.org/trixie/7zip-rar
# Provides a RAR codec module for 7zip to make the 7z command able to extract RAR files.

# NAS Access
# cifs-utils:
# This is the core package that provides the necessary tools to mount and manage CIFS (Common Internet File System) shares, which is essentially the Linux implementation of SMB.
# smbclient:
# This is a command-line utility included in cifs-utils that allows you to directly access and manage files on an SMB share using a simple command-line interface.
# winbind:
# If you need to integrate your Linux system with a Windows Active Directory domain, "winbind" is crucial as it enables user authentication against the domain using the Windows domain controller.
# keyutils:
# This package provides additional cryptographic functions that might be needed for enhanced security when accessing the SMB share, depending on the specific configuration.


