#!/bin/bash
# ----------------------------------------
# NAME        : desktop.sh
# DESCRIPTION : Install desktop applications
# ----------------------------------------


PackageInstaller="$HOME/Tools/scripts/package-installer.sh"

Command() {
  echo ""
  echo "COMMAND : $1"
  echo ""
  Output=$($1)
  echo ""
  echo "RETURN  : $Output"
}

Installer() {
  Command "$PackageInstaller $1"
}

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : Desktop apps installer"

Installer "caffeine wmctrl linssid gtkhash recoll ark okular"

echo ""
echo "END     : Desktop apps installer"
echo "SPACER  : ----------------------------------------"

exit 0 # Success


# ----------------------------------------
# Packages that I removed from above list

# kde-spectacle
# arandr
# meld
# flameshot
# timeshift-gtk
# neovim-qt

