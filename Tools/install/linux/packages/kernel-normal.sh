#!/bin/bash
# ----------------------------------------
# NAME        : kernel-normal.sh
# DESCRIPTION : Install the normal Kernel
# ----------------------------------------

PackageInstaller="$HOME/Tools/scripts/package-installer.sh"

Command() {
  echo ""
  echo "COMMAND : $1"
  echo ""
  eval "$1"
}

Installer() {
  Command "$PackageInstaller $1"
}

Installer "linux-image-amd64 linux-headers-amd64"
echo "NOTICE  : Restart computer and then select normal kernel"

# Run this command after next boot
# sudo apt remove linux-*siduction*

