#!/bin/bash
# ----------------------------------------
# NAME        : ubuntu-repos.sh
# DESCRIPTION : Configures Ubuntu repositories by adding universe, multiverse, restricted,
#               partner, and backports, and installs ppa-purge for managing PPAs.
# ----------------------------------------

# ----------------------------------------
# Function: Command
# Description: Executes a given command, prints it, and captures the output.
Command() {
  echo ""
  echo "COMMAND : $1"
  echo ""
  Output=$($1)
  echo ""
  echo "RETURN  : $Output"
}

# ----------------------------------------
# Start of the script

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : $(date) - Configuring Ubuntu repositories"

# ----------------------------------------
# Add Ubuntu universe, multiverse, restricted, partner, and backports repositories

echo "NOTICE  : Adding Ubuntu repositories: universe, multiverse, restricted, partner, and backports"

Command "sudo -S add-apt-repository -y universe"
Command "sudo -S add-apt-repository -y multiverse"
Command "sudo -S add-apt-repository -y restricted"
Command "sudo -S add-apt-repository -y partner"
Command "sudo -S add-apt-repository -y backports"

# ----------------------------------------
# Install ppa-purge for managing PPAs

echo "NOTICE  : Installing ppa-purge for PPA management"

Command "sudo -S apt install -y ppa-purge"

# ----------------------------------------
# Finish

echo ""
echo "END     : $(date) - Finished configuring Ubuntu repositories"
echo "SPACER  : ----------------------------------------"

exit 0 # Success

