#!/bin/bash
# ----------------------------------------
# NAME        : debian-mirrors.sh
# DESCRIPTION : Configure Debian mirrors
# NOTE        : This is an unnecessary step
#               Remove this script
# ----------------------------------------

echo "DEBIAN BY DEFAULT USES MIRRORS SINCE STRETCH"
echo "WE DO NOT NEED TO MANUALLY SPECIFY MIRRORS"
echo "THIS SCRIPT SHOULD BE REMOVED"
exit 0

