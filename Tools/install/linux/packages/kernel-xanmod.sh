#!/bin/bash
# ----------------------------------------
# NAME        : kernal-xanmod.sh
# DESCRIPTION : Install the Xanmod Kernel
# ----------------------------------------

DownloadDir="$HOME/Downloads"
Name="check_x86-64_psabi.sh"
Url="https://dl.xanmod.org/$Name"


Installer() {

  echo "Installing...."
  wget -qO - https://dl.xanmod.org/archive.key | sudo gpg --dearmor -o /usr/share/keyrings/xanmod-archive-keyring.gpg

  echo 'deb [signed-by=/usr/share/keyrings/xanmod-archive-keyring.gpg] http://deb.xanmod.org releases main' | sudo tee /etc/apt/sources.list.d/xanmod-release.list

  sudo apt update && sudo apt install linux-xanmod-x64v3
  sudo update-grub && sudo grub-install

  echo "Done with the install"
}


TestScript()
{
  pushd "$DownloadDir" || exit

  wget "$Url"
  chmod +x "$Name"
  sudo "./$Name"

  popd || exit
}


TestScript

echo "If the above says \"CPU supports x86-64-v3\" then we are good to go with the remainder of these commands"

read -r -p "Are you sure? [y/N] " response

case "$response" in
    [yY][eE][sS]|[yY])
      Installer
        ;;
    *)
       exit
        ;;
esac

exit 0

