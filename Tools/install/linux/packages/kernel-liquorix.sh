#!/bin/bash
# ----------------------------------------
# NAME        : kernel-liquorix.sh
# DESCRIPTION : Install Liquorix Kernel
# ----------------------------------------

DownloadDir="$HOME/Downloads"

pushd "$DownloadDir" || exit

curl -s 'https://liquorix.net/install-liquorix.sh' | sudo bash

popd || exit

