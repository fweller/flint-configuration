#!/bin/bash
# ----------------------------------------
# NAME        : firmware.sh
# DESCRIPTION : Installs and updates firmware using fwupd.
# ----------------------------------------

# ----------------------------------------
# Function: Command
# Description: Executes a given command, prints it, and captures the output.
Command() {
  echo ""
  echo "COMMAND : $1"
  echo ""
  Output=$($1)
  echo ""
  echo "RETURN  : $Output"
}

# ----------------------------------------
# Start of the script

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : $(date) - Firmware installation and updates"

# ----------------------------------------
# Check for and install firmware updates

Command "sudo fwupdmgr get-devices"  # Lists all devices that can be updated
Command "sudo fwupdmgr get-updates"  # Checks for available firmware updates
Command "sudo fwupdmgr update"       # Applies the firmware updates

# ----------------------------------------
# Finish

echo ""
echo "END     : $(date) - Firmware installation and updates completed"
echo "SPACER  : ----------------------------------------"

exit 0 # Success

