#!/bin/bash
# ----------------------------------------
# NAME        : setup.sh
# DESCRIPTION : Prepare system to install packages
#               This script sets up the package management environment.
# ----------------------------------------

# ----------------------------------------
# Reference
#
# http://distrowatch.org/dwres.php?resource=package-management
#
# Distro agnostic package managers
#   pkcon  = Distro Agnostic
#
# Distro specific package managers
#   apt    = Debian, Ubuntu, Mint
#   nala   = Upgraded: Debian, Ubuntu, Mint
#   pacman = Arch, Manjaro
#   zypper = OpenSUSE
#   dnf    = Fedora, RHEL
#   urpmi  = Mandriva, Mageia
#   emerge = Gentoo
# ----------------------------------------

# ----------------------------------------
# Variables

PackageInstaller="${HOME}/Tools/scripts/package-installer.sh"
PackageUpdater="${HOME}/Tools/scripts/package-updater.sh"
DebianFirmware="${HOME}/Tools/scripts/debian-firmware.sh"
UbuntuRepos="${HOME}/Tools/install/linux/packages/ubuntu-repos.sh"
WhatDistro="${HOME}/Tools/scripts/whatdistro.sh"

Line="----------------------------------------"

# ----------------------------------------
# Function: Command
# Description: Executes a given command, prints it, and captures the output.
Command() {
  echo ""; echo "COMMAND: $1"; echo ""
  Output=$($1)
  echo ""; echo "RETURN: $Output"
}

# ----------------------------------------
# Function: Installer
# Description: Calls the package installer script with the provided argument.
Installer() {
  Command "${PackageInstaller} ${1}"
}

# ----------------------------------------
# Function: Updater
# Description: Calls the package updater script.
Updater() {
  Command "${PackageUpdater}"
}

# ----------------------------------------
# Package Manager Setup Functions

Setup_pkcon(){
  echo ""; echo "${Line}"; echo "Setup_pkcon: START - Update package list and optimize mirrors"; echo ""

  Command "sudo -S pkcon refresh"

  echo ""; echo "Setup_pkcon: END"; echo "${Line}"; echo ""
}

Setup_apt(){
  echo ""; echo "${Line}"; echo "Setup_apt: START - Update package list and optimize mirrors"; echo ""

  if [ -n "${Ubuntu}" ]; then
    Command "bash ${UbuntuRepos}"
  fi

  echo ""; echo "Setup_apt: END"; echo "${Line}"; echo ""
}

Setup_nala(){
  echo ""; echo "${Line}"; echo "Setup_nala: START - Update package list and optimize mirrors"; echo ""

  Command "sudo -S nala update"

  echo ""; echo "Setup_nala: END"; echo "${Line}"; echo ""
}

Setup_pacman(){
  echo ""; echo "${Line}"; echo "Setup_pacman: START - Update package list and optimize mirrors"; echo ""

  Command "sudo -S pacman -Sy"

  echo ""; echo "Setup_pacman: END"; echo "${Line}"; echo ""
}

Setup_zypper(){
  echo ""; echo "${Line}"; echo "Setup_zypper: START - Update package list and optimize mirrors"; echo ""

  Command "sudo -S zypper refresh"

  echo ""; echo "Setup_zypper: END"; echo "${Line}"; echo ""
}

Setup_dnf(){
  echo ""; echo "${Line}"; echo "Setup_dnf: START - Update package list and optimize mirrors"; echo ""

  Command "sudo -S dnf refresh"

  echo ""; echo "Setup_dnf: END"; echo "${Line}"; echo ""
}

Setup_urpmi(){
  echo ""; echo "${Line}"; echo "Setup_urpmi: START - Update package list and optimize mirrors"; echo ""

  Command "sudo -S urpmi.update -a"

  echo ""; echo "Setup_urpmi: END"; echo "${Line}"; echo ""
}

Setup_emerge(){
  echo ""; echo "${Line}"; echo "Setup_emerge: START - Update package list and optimize mirrors"; echo ""

  Command "sudo -S emerge --sync"

  echo ""; echo "Setup_emerge: END"; echo "${Line}"; echo ""
}

# ----------------------------------------
# Function: PackageManagerSetup
# Description: Decides which package manager setup functions to run.
PackageManagerSetup() {
  echo ""; echo "${Line}"; echo "PackageManagerSetup: START - Configuring package managers"; echo ""

  if [ -x "$(command -v nala)" ]; then Setup_nala; fi
  if [ -x "$(command -v apt)" ]; then Setup_apt; fi
  if [ -x "$(command -v pkcon)" ]; then Setup_pkcon; fi
  if [ -x "$(command -v pacman)" ]; then Setup_pacman; fi
  if [ -x "$(command -v zypper)" ]; then Setup_zypper; fi
  if [ -x "$(command -v dnf)" ]; then Setup_dnf; fi
  if [ -x "$(command -v urpmi)" ]; then Setup_urpmi; fi
  if [ -x "$(command -v emerge)" ]; then Setup_emerge; fi

  echo ""; echo "PackageManagerSetup: END"; echo "${Line}"; echo ""
}

# ----------------------------------------
# Function: DebPackages
# Description: Installs the first set of packages for Debian/Ubuntu/Mint.
DebPackages() {
  echo ""; echo "${Line}"; echo "DebPackages: START - Installing base packages for Debian/Ubuntu/Mint"; echo ""

  Installer "software-properties-common synaptic aptitude nala tasksel"

  echo ""; echo "DebPackages: END"; echo "${Line}"; echo ""
}

# ----------------------------------------
# Function: DistroSetup
# Description: Handles distribution-specific package setup.
DistroSetup() {
  echo ""; echo "${Line}"; echo "DistroSetup: START - Running distro-specific package setup"; echo ""

  Distro=$(sh "${WhatDistro}")
  echo "DistroSetup: Distro detected as ${Distro}"; echo ""

  case $Distro in
    'debian')
      DebPackages
      Command "sh $DebianFirmware"
      ;;
    'ubuntu')
      DebPackages
      Command "sh $UbuntuRepos"
      ;;
    'mint')
      DebPackages
      ;;
    *)
      echo "DistroSetup: Nothing special required for ${Distro}"
      ;;
  esac

  echo ""; echo "DistroSetup: END"; echo "${Line}"; echo ""
}

# ----------------------------------------
# Start of the script

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : Prepare system to install packages"

# Uncomment the following lines to activate the setup functions
# Updater        # Refresh all package managers
# DistroSetup    # Install distro-specific packages and configurations
# PackageManagerSetup  # Configure each package manager

echo ""
echo "END     : Prepare system to install packages"
echo "SPACER  : ----------------------------------------"

exit 0 # Success

