#!/bin/bash
# ----------------------------------------
# NAME        : debian-firmware.sh
# DESCRIPTION : Installs necessary Debian firmware packages.
# ----------------------------------------

# ----------------------------------------
# Variables

# Path to the package installer script
PackageInstaller="$HOME/Tools/scripts/package-installer.sh"

# ----------------------------------------
# Function: Command
# Description: Executes a given command, prints it, and captures the output.
Command() {
  echo ""
  echo "COMMAND : $1"
  echo ""
  Output=$($1)
  echo ""
  echo "RETURN  : $Output"
}

# ----------------------------------------
# Function: Installer
# Description: Installs the specified package using the package installer script.
Installer() {
  Command "$PackageInstaller $1"
}

# ----------------------------------------
# Start of the script

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : $(date) - Debian Firmware Installer"

# Install the non-free firmware package for Debian
Installer "firmware-linux-nonfree"

# ----------------------------------------
# Finish

echo ""
echo "END     : $(date) - Debian Firmware Installer"
echo "SPACER  : ----------------------------------------"

exit 0 # Success

