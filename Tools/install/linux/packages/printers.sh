#!/bin/bash
# ----------------------------------------
# NAME        : desktop.sh
# DESCRIPTION : Installs printer tools for systems that didn't install them by default.
# ----------------------------------------

# ----------------------------------------
# Variables

# Path to the package installer script
PackageInstaller="$HOME/Tools/scripts/package-installer.sh"

# ----------------------------------------
# Function: Command
# Description: Executes a given command, prints it, and captures the output.
Command() {
  echo ""
  echo "COMMAND : $1"
  echo ""
  Output=$($1)
  echo ""
  echo "RETURN  : $Output"
}

# ----------------------------------------
# Function: Installer
# Description: Installs the specified packages using the package installer script.
Installer() {
  Command "$PackageInstaller $1"
}

# ----------------------------------------
# Start of the script

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : $(date) - Printer tools installer"

# ----------------------------------------
# Install printer tools

Installer "cups system-config-printer print-manager"
# Note: cups-pdf is usually unnecessary to install

# ----------------------------------------
# Finish

echo ""
echo "END     : $(date) - Printer tools installer"
echo "SPACER  : ----------------------------------------"

exit 0 # Success

