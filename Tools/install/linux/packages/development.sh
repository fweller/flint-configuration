#!/bin/bash
# ----------------------------------------
# NAME        : development.sh
# DESCRIPTION : Install development applications
# ----------------------------------------


PackageInstaller="$HOME/Tools/scripts/package-installer.sh"

Command() {
  echo ""
  echo "COMMAND : $1"
  echo ""
  Output=$($1)
  echo ""
  echo "RETURN  : $Output"
}

Installer() {
  Command "$PackageInstaller $1"
}

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : Development apps installer"

Installer "python3 python3-pip cmake extra-cmake-modules"
Installer "gettext tpm2-tools gcc make perl software-properties-common"
Installer "apt-transport-https pv tmux tio telnet"

echo ""
echo "END     : Development apps installer"
echo "SPACER  : ----------------------------------------"

exit 0 # Success

