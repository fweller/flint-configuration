#!/bin/sh
# ----------------------------------------
# NAME        : dotfiles.sh
# DESCRIPTION : Installs dotfiles to the $HOME directory by creating symbolic links.
# ----------------------------------------

# ----------------------------------------
# Variables

# Path to the directory containing the dotfiles
SOURCE="$HOME/Tools/scripts/dotfiles"

# Destination directory for the symbolic links
DESTINATION="$HOME"

# ----------------------------------------
# Start of the script

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : $(date) - Installing dotfiles from $SOURCE to $DESTINATION"

# ----------------------------------------
# Check if the SOURCE directory exists

if [ ! -d "$SOURCE" ]; then
    echo "ERROR   : Source directory '$SOURCE' does not exist."
    exit 1 # Exit with an error if the source directory is missing
fi

# ----------------------------------------
# Check if the SOURCE directory is not empty

if [ -z "$(ls -A "$SOURCE")" ]; then
    echo "ERROR   : Source directory '$SOURCE' is empty."
    exit 1 # Exit with an error if the source directory is empty
fi

# ----------------------------------------
# Create symbolic links for each file in the SOURCE directory

for FILE in "$SOURCE"/*; do
    BASENAME=$(basename "$FILE")
    ln -sf "$FILE" "$DESTINATION/.$BASENAME"
    if [ $? -eq 0 ]; then
        echo "SUCCESS : Linked $FILE to $DESTINATION/.$BASENAME"
    else
        echo "ERROR   : Failed to link $FILE to $DESTINATION/.$BASENAME"
    fi
done

# ----------------------------------------
# Finish

echo ""
echo "END     : $(date) - Dotfiles installation completed"
echo "SPACER  : ----------------------------------------"
exit 0 # Success

