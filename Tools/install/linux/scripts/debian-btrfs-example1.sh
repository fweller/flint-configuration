sudo apt install gparted vim debootstrap arch-install-scripts

sudo su
## After setting up your partitions:

## efi
	mkfs.fat -F32 /dev/nvme0n1p1

# swap
	mkswap /dev/nvme0n1p2
	swapon /dev/nvme0n1p2

# btrfs
	mkfs.btrfs /dev/nvme0n1p3 -f
	
# mount
	mount -t btrfs -o subvolid=5 /dev/nvme0n1p3 /mnt

btrfs subvolume create /mnt/@
btrfs subvolume create /mnt/@home
btrfs subvolume create /mnt/@opt
btrfs subvolume create /mnt/@root
btrfs subvolume create /mnt/@tmp
btrfs subvolume create /mnt/@var-cache
btrfs subvolume create /mnt/@var-log
btrfs subvolume create /mnt/@var-tmp
btrfs subvolume create /mnt/@snapshots

umount /mnt

mount -o noatime,compress=zstd:1,subvol=@ /dev/nvme0n1p3 /mnt

mkdir -p /mnt/{boot,home,opt,root,tmp,.snapshots} ; \
mkdir -p /mnt/var/{cache,log,tmp} ; \
mkdir -p /mnt/boot/efi ; \
ls -al /mnt/

mount /dev/nvme0n1p1 /mnt/boot/efi
mount -o noatime,compress=zstd:1,subvol=@home        /dev/nvme0n1p3 /mnt/home
mount -o noatime,compress=zstd:1,subvol=@opt         /dev/nvme0n1p3 /mnt/opt
mount -o noatime,compress=zstd:1,subvol=@root        /dev/nvme0n1p3 /mnt/root
mount -o noatime,compress=zstd:1,subvol=@tmp         /dev/nvme0n1p3 /mnt/tmp
mount -o noatime,compress=zstd:1,subvol=@var-cache   /dev/nvme0n1p3 /mnt/var/cache
mount -o noatime,compress=zstd:1,subvol=@var-log     /dev/nvme0n1p3 /mnt/var/log
mount -o noatime,compress=zstd:1,subvol=@var-tmp     /dev/nvme0n1p3 /mnt/var/tmp
mount -o noatime,compress=zstd:1,subvol=@snapshots   /dev/nvme0n1p3 /mnt/.snapshots

## Install debian SID
debootstrap --include linux-image-amd64,grub-efi-amd64,vim,btrfs-progs,systemd-timesyncd,locales --arch amd64 sid /mnt

## Install debian stable
debootstrap --include linux-image-amd64,grub-efi-amd64,vim,btrfs-progs,systemd-timesyncd,locales --arch amd64 bookworm /mnt

mount -t proc proc /mnt/proc
mount -t sysfs sys /mnt/sys
mount -o bind /dev /mnt/dev
mount -t devpts pts /mnt/dev/pts/
mount -o bind /etc/resolv.conf /mnt/etc/resolv.conf

genfstab -U /mnt >> /mnt/etc/fstab

chroot /mnt

mount -t efivarfs efivarfs /sys/firmware/efi/efivars

dpkg-reconfigure tzdata
dpkg-reconfigure locales

echo "debsid" > /etc/hostname

echo "
127.0.0.1	debsid
" >> /etc/hosts

vim /etc/apt/sources.list

# Sid Repos
deb http://deb.debian.org/debian sid main contrib non-free non-free-firmware
deb http://deb.debian.org/debian-debug/ sid-debug main contrib non-free non-free-firmware
deb-src http://deb.debian.org/debian/ sid main contrib non-free non-free-firmware

dpkg --add-architecture i386 ; \
apt update ; \
apt install linux-headers-amd64 firmware-iwlwifi firmware-linux firmware-linux-nonfree sudo vim bash-completion command-not-found plocate systemd-timesyncd usbutils hwinfo v4l-utils zram-tools plymouth-themes zstd efibootmgr network-manager xdg-utils pipewire git curl wget debian-goodies openssh-server openssh-client debian-keyring mesa-utils debconf btrfs-progs

passwd

useradd username -m -c "User Name" -s /bin/bash  ; \
usermod -aG sudo,disk,input,kvm,adm,dialout,cdrom,floppy,audio,dip,video,plugdev,users,netdev username  ; \
passwd username

## Install with KDE
apt install git curl wget wireshark kde-full kwin-addons plasma-workspace-wayland libqt6multimediaquick6 qml6-module-qtquick-dialogs xdg-desktop-portal-kde qt6-xdgdesktopportal-platformtheme menu-xdg gstreamer1.0-qt5 wayland-utils dolphin-plugins plymouth-themes flatpak zram-tools steam

grub-install --target=x86_64-efi --efi-directory=/boot/efi --bootloader-id=GRUB

## Exit and reboot the pc
update-grub
exit 
umount -a
reboot now
