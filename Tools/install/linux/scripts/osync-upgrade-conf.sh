#!/bin/bash
# This script upgrades all osync log files to v1.3
# only need to run this once

# Variables
BuildDir="${HOME}/Builds"
Name="osync"
LogDir="${HOME}/Logs"
ScriptDir="${HOME}/Tools/osync"
Script="${BuildDir}/${Name}/upgrade-v1.0x-v1.3x.sh"

# Do things only after printing to screen
Command() {
  echo ""; echo "COMMAND: $1"; echo ""
  eval "$1"
}

# Print comments
Comment() {
  echo ""; echo "COMMENT: $1"; echo ""
}

if [ ! -f $Script ]; then
  Comment "$Script does not exist"
  exit
fi

if [ ! -d $ScriptDir ]; then
  Comment "$ScriptDir does not exist"
  exit
fi

Comment "Beginning mass conversion"

for file in ${ScriptDir}/*.conf ; do
  Command "bash $Script $file"
done

