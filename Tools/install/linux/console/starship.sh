#!/bin/bash
# https://starship.rs/

pushd "${HOME}/Builds" || exit
git clone https://github.com/starship/starship
cd starship || exit
cd install || exit
sudo install.sh
popd

