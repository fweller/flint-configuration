#!/bin/bash
# ----------------------------------------
# NAME        : install-multithreaded-compression.sh
# DESCRIPTION : Installs multi-threaded compression tools and creates symlinks to prioritize them over single-threaded options.
#               Ensures that /usr/local/bin versions are executed instead of the default /usr/bin versions.
# ----------------------------------------

# ----------------------------------------
# Function: Command
# Description: Executes a given command, prints it, and handles errors.
Command() {
  echo ""
  echo "COMMAND: $1"
  echo ""
  if ! eval "$1"; then
    echo "ERROR: Command failed - $1"
    exit 1
  fi
}

# ----------------------------------------
# Start of the script

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : $(date) - Installing multi-threaded compression tools"

# ----------------------------------------
# Install multi-threaded compression tools

Command "sudo apt install -y zstd lbzip2 pigz"

# ----------------------------------------
# Create symlinks to replace single-threaded tools with multi-threaded versions

Command "pushd /usr/local/bin || exit"
Command "sudo ln -sf /usr/bin/lbzip2 bzip2"
Command "sudo ln -sf /usr/bin/lbzip2 bunzip2"
Command "sudo ln -sf /usr/bin/lbzip2 bzcat"
Command "sudo ln -sf /usr/bin/pigz gzip"
Command "sudo ln -sf /usr/bin/pigz gunzip"
Command "sudo ln -sf /usr/bin/pigz gzcat"
Command "sudo ln -sf /usr/bin/pigz zcat"
Command "popd || exit"

# ----------------------------------------
# Finish

echo ""
echo "END     : $(date) - Multi-threaded compression tools installation and symlink setup completed"
echo "SPACER  : ----------------------------------------"

exit 0 # Success

