#!/bin/bash
## Glances is a cross-platform system monitoring tool written in Python.

## TODO
# Use git to checkout the latest version instead of downloading a specific
# version

## Documentation
# https://nicolargo.github.io/glances/
# https://github.com/nicolargo/glances
# https://glances.readthedocs.io/
# Releases: https://github.com/nicolargo/glances/releases

## Variables
BuildDir="${HOME}/Builds"
Version="3.4.0.2"
Source="https://github.com/nicolargo/glances/archive/refs/tags/v${Version}.tar.gz"

## Install from source

# Download project and move into it
pushd "$BuildDir" || exit
wget "$Source" -O- | tar xz
cd glances-$Version || exit

# Build
sudo python3 setup.py install

# Exit
popd || exit

# Uninstall
#


