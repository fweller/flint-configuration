#!/bin/bash
# btop++  A monitor of resources 
# https://github.com/aristocratos/btop

# Variables
BuildDir="${HOME}/Builds"

# Dependencies
sudo apt install coreutils sed git build-essential gcc-11 g++-11

## Install from source

# Download project and move into it
pushd "$BuildDir" || exit
git clone --recursive https://github.com/aristocratos/btop.git
cd btop || exit

# Compile
make

# Install
sudo make install

# Make btop always run as root
sudo make setuid

# Exit
popd || exit

# Uninstall
# sudo make uninstall

