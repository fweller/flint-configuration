#!/bin/bash
# --------------------------------------------------------
# NAME    : install-docker.sh
# PURPOSE : Install Docker Engine on Debian or Ubuntu.
#         : This script is meant to simplify the installation process.
# USAGE   : ./install-docker.sh
# AUTHOR  : Flint Weller
# DATE    : 2023 December 04
# --------------------------------------------------------
# ADDITIONAL INFORMATION
# Docker Install https://docs.docker.com/engine/install/
# Docker Post-Install https://docs.docker.com/engine/install/linux-postinstall/
# Debian https://docs.docker.com/engine/install/debian/
# Ubuntu https://docs.docker.com/engine/install/ubuntu/
# --------------------------------------------------------


# --------------------------------------------------------
# Clean up previous Docker installs

echo "Clean up previous Docker installs"

# Disable Docker start with systemd
echo "Disable Docker start with systemd"
sudo systemctl disable docker.service
sudo systemctl disable containerd.service

# Uninstall old versions
echo "Uninstall old versions"
for pkg in docker.io docker-doc docker-compose podman-docker containerd runc; do sudo apt remove $pkg; done
sudo apt remove -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

# Remove docker keyring
echo "Remove docker keyring"
sudo rm -f /etc/apt/keyrings/docker.gpg

# Remove docker repo source list
echo "Remove docker repo source list"
sudo rm -f /etc/apt/sources.list.d/docker.list

# Remove user config directory
echo "Remove user config directory"
sudo rm -rf "${HOME}/.docker"


# --------------------------------------------------------
# Start the install process

echo "Start the install process"

# Get the Distro ID from lsb_release
Distro_ID=$(lsb_release --id --short | tr "[:upper:]" "[:lower:]")
echo $Distro_ID

# Update the apt package index and install packages to allow apt to use a repository over HTTPS
echo "Update the apt package index and install packages to allow apt to use a repository over HTTPS"
sudo apt update
sudo apt -y install ca-certificates curl gnupg lsb-release

# Install the keyring directory in case it is not already there
echo "Install the keyring directory in case it is not already there"
sudo install -m 0755 -d /etc/apt/keyrings

# Add Dockers official GPG key:
echo "Add Dockers official GPG key:"
case $Distro_ID in
  'debian')
    curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
    ;;
  'ubuntu')
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
    ;;
  *)
    exit
    ;;
esac

# Make the file read-only
echo "Make the file read-only"
sudo chmod a+r /etc/apt/keyrings/docker.gpg

# Set up the repository
echo "Set up the repository:"
case $Distro_ID in
  'debian')
    echo \
      "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
      "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
      sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    ;;
  'ubuntu')
    echo \
      "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
      "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
      sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    ;;
  *)
    exit
    ;;
esac

# Install Docker Engine, containerd, and Docker Compose
echo "Install Docker Engine, containerd, and Docker Compose"
sudo apt update
sudo apt -y install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

# Verify that the Docker Engine installation is successful
echo "-------------------------------------------------------------------------"
echo "Verify that the Docker Engine installation is successful"
echo "If you see \"Hello from Docker!\" then installation is successful"
echo "-------------------------------------------------------------------------"
sudo docker run hello-world

# --------------------------------------------------------
# Post-Install

# Create docker group
echo "Create docker group"
sudo groupadd -f docker

# Add your user to the docker group
echo "Add your user to the docker group"
sudo usermod -aG docker $USER

# Configure Docker to start on boot with systemd
echo "Configure Docker to start on boot with systemd"
sudo systemctl enable docker.service
sudo systemctl enable containerd.service

# Fix potential permissions issue
echo "Fix potential permissions issue"
sudo chown "$USER":"$USER" /home/"$USER"/.docker -R
sudo chmod g+rwx "${HOME}/.docker" -R

# Log out and back in for change to groups to take effect
echo ""
echo "-------------------------------------------------------------------------"
echo "Log out and back in again for changes to groups to take effect"
echo "And then run: docker run hello-world"


# Activate changes to groups
: '
echo "Activate changes to groups"
# Note: newgrp opens a new shell, so we have to pass commands to it
newgrp docker << EOF

# Verify that you can run docker commands without sudo
echo "-------------------------------------------------------------------------"
echo "If you see \"Hello from Docker!\" then everything is configured properly"
echo "-------------------------------------------------------------------------"
docker run hello-world
EOF
'

# End
