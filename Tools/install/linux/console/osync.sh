#!/bin/bash
## Osync - two way file syncronization
# http://www.netpower.fr/osync
# https://github.com/deajan/osync



# This installer is only for Linux and WSL
# Do not use this installer for MacOS


# Variables
BuildDir="${HOME}/Builds"
Name="osync"
Url="https://github.com/deajan/${Name}"

## Install

# Prepare
mkdir -p "${BuildDir}"
pushd "${BuildDir}" || exit

# Download and install
git clone "${Url}"
cd "${Name}" || exit
sudo bash install.sh

# Exit
popd || exit
exit

# --------------------------
# Below is the old method





# ----------------------------------------
# NAME        : osync-setup.sh
# DESCRIPTION : Installs and configures Osync, a two-way file synchronization tool.
#               Switches from an older version of Osync to the latest stable version.
# ----------------------------------------

# ----------------------------------------
# Variables

BuildDir="${HOME}/Builds"
LogDir="${HOME}/Logs"
Name="osync"
Url="https://github.com/deajan/${Name}"
Branch="master"
Tag="v1.3"
OldVersionDir="${BuildDir}/${Name}-old"

# ----------------------------------------
# Function: Command
# Description: Executes a given command, prints it, and handles errors.
Command() {
  echo ""
  echo "COMMAND: $1"
  echo ""
  if ! eval "$1"; then
    echo "ERROR: Command failed - $1"
    exit 1
  fi
}

# ----------------------------------------
# Function: Comment
# Description: Prints a comment to the console.
Comment() {
  echo ""
  echo "COMMENT: $1"
  echo ""
}

# ----------------------------------------
# Uninstall old version if present

Comment "Checking if a previous installation of Osync exists"

if [ -d "${BuildDir}/${Name}" ]; then
  Comment "Previous installation of Osync detected. Uninstalling..."

  Command "pushd ${BuildDir}/${Name} || exit"
  Command "sudo bash install.sh --remove"
  Command "cd .."
  Command "mv -f ${BuildDir}/${Name} ${OldVersionDir}"
  Command "popd || exit"

  if [ -d "$LogDir" ]; then
    Comment "Renaming old log files"
    for file in ${LogDir}/osync-*; do
      Command "mv -f $file ${file/osync/osync-old}"
    done
    Comment "Finished renaming old log files"
  fi

  Comment "Uninstallation of the previous version completed"
fi

# ----------------------------------------
# Install new version

Comment "Installing the new version of Osync"

# Prepare build directory
Command "mkdir -p ${BuildDir}"
Command "pushd ${BuildDir} || exit"

# Clone the repository and switch to the specified tag
Command "git clone -b ${Branch} ${Url}"
Command "cd ${Name} || exit"
Command "git switch --detach $Tag"

# Run the installation script
Command "sudo bash install.sh"

# Return to the previous directory
Command "popd || exit"

# ----------------------------------------
# Finish

Comment "Osync installation and configuration complete"
exit 0 # Success

