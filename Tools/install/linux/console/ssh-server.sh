#!/bin/bash
# ----------------------------------------
# NAME        : install-ssh-server.sh
# DESCRIPTION : Installs and configures the SSH server on a Linux system.
#               Also configures the firewall to allow SSH connections if running on Ubuntu.
# ----------------------------------------

# ----------------------------------------
# Function: Command
# Description: Executes a given command, prints it, and handles errors.
Command() {
  echo ""
  echo "COMMAND: $1"
  echo ""
  if ! eval "$1"; then
    echo "ERROR: Command failed - $1"
    exit 1
  fi
}

# ----------------------------------------
# Start of the script

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : $(date) - Installing and configuring SSH server"

# ----------------------------------------
# Install SSH server from repository

Command "sudo apt -y install openssh-server"

# Check SSH server status and enable it to start on boot
Command "sudo systemctl status ssh"
Command "sudo systemctl enable ssh --now"
Command "sudo systemctl status ssh"

# ----------------------------------------
# Configure firewall (UFW) if running on Ubuntu

if grep -qi 'ubuntu' /etc/os-release; then
  echo "NOTICE  : Ubuntu detected. Installing and configuring UFW to allow SSH."
  Command "sudo apt -y install ufw"
  Command "sudo ufw allow ssh"
  Command "sudo ufw enable"
  Command "sudo ufw status"
else
  echo "NOTICE  : Not running on Ubuntu, skipping UFW configuration."
fi

# ----------------------------------------
# Finish

echo ""
echo "END     : $(date) - Finished installing and configuring SSH server"
echo "SPACER  : ----------------------------------------"

exit 0 # Success

