#!/bin/bash
# Install bottom
# https://github.com/ClementTsang/bottom
# https://clementtsang.github.io/bottom/

# Variables
Version="0.9.6"
Filename="bottom_${Version}_amd64.deb"
UrlBase="https://github.com/ClementTsang/bottom/releases/download/${Version}/"
Url="${UrlBase}${Filename}"
DownloadDir="${HOME}/Downloads"

echo "Filename=${Filename}"
echo "Url=${Url}"

pushd "${DownloadDir}" || exit
curl -LO "${Url}"
sudo dpkg -i "${Filename}"
popd || exit

