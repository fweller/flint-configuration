#!/bin/bash
# rickslab-gpu-utils - a set of utilities for monitoring GPU performance
# https://github.com/Ricks-Lab/gpu-utils
# https://packages.debian.org/source/sid/rickslab-gpu-utils

## TODO
# Need to run this script and test it on Turing and ZBook

## Install from repos
sudo apt install rickslab-gpu-utils
exit

# Variables
# BuildDir="${HOME}/Builds"

# Dependencies

## Build from source

# Download the project and move into it
# pushd $BuildDir || exit

# Compile

# Install

# Exit
# popd || exit

# Uninstall

