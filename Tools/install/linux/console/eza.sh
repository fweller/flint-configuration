#!/bin/bash
# ----------------------------------------
# NAME        : install-eza.sh
# DESCRIPTION : Installs eza, a modern replacement for 'ls' on Debian-based systems.
#               The script adds the Gierens repository and installs eza.
# ----------------------------------------
# REFERENCE   :
#               - https://eza.rocks/
#               - https://github.com/eza-community/eza
#               - https://packages.debian.org/sid/eza
# ----------------------------------------

# ----------------------------------------
# Function: Command
# Description: Executes a given command, prints it, and handles errors.
Command() {
  echo ""
  echo "COMMAND: $1"
  echo ""
  if ! eval "$1"; then
    echo "ERROR: Command failed - $1"
    exit 1
  fi
}

# ----------------------------------------
# Start of the script

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : $(date) - Installing eza"

# ----------------------------------------
# Navigate to the Downloads directory

Command "pushd \"${HOME}/Downloads\" || exit"

# ----------------------------------------
# Update package lists and install GPG

Command "sudo apt update"
Command "sudo apt install -y gpg"

# ----------------------------------------
# Create the keyrings directory if it doesn't exist

Command "sudo mkdir -p /etc/apt/keyrings"

# ----------------------------------------
# Download and add the GPG key for the Gierens repository

Command "wget -qO- https://raw.githubusercontent.com/eza-community/eza/main/deb.asc | sudo gpg --dearmor -o /etc/apt/keyrings/gierens.gpg"

# ----------------------------------------
# Add the Gierens repository to the sources list

Command "echo \"deb [signed-by=/etc/apt/keyrings/gierens.gpg] http://deb.gierens.de stable main\" | sudo tee /etc/apt/sources.list.d/gierens.list"

# ----------------------------------------
# Set appropriate permissions for the key and sources list

Command "sudo chmod 644 /etc/apt/keyrings/gierens.gpg /etc/apt/sources.list.d/gierens.list"

# ----------------------------------------
# Update package lists and install eza

Command "sudo apt update"
Command "sudo apt install -y eza"

# ----------------------------------------
# Return to the previous directory

Command "popd || exit"

# ----------------------------------------
# Finish

echo ""
echo "END     : $(date) - eza installation completed"
echo "SPACER  : ----------------------------------------"

exit 0 # Success

