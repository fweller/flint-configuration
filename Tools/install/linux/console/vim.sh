#!/bin/bash
# ----------------------------------------
# NAME        : vim.sh
# DESCRIPTION : Installs Vim packages and plugins
#               This script should be run after Vim is installed.
# ----------------------------------------

# ----------------------------------------
# Variables

# Directory where Vim plugins are stored
VimDir="${HOME}/.vim/pack/vendor/start"

# Name of the package to install
Package="vim-markdown"

# Full path to the package directory
PackageDir="${VimDir}/${Package}"

# URL to the vim-markdown GitHub repository
ArchiveUrl="https://github.com/plasticboy/vim-markdown/archive/master.tar.gz"

# ----------------------------------------
# Start of the script

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : $(date) - Installing Vim Markdown support"

# ----------------------------------------
# Create the package directory if it doesn't exist and move into it

echo "NOTICE  : Creating directory $PackageDir"
mkdir -p "$PackageDir"
pushd "$PackageDir" || { echo "ERROR   : Failed to navigate to $PackageDir"; exit 1; }

# ----------------------------------------
# Download and extract the vim-markdown package

echo "NOTICE  : Downloading $Package from $ArchiveUrl"
if wget "$ArchiveUrl"; then
  echo "NOTICE  : Extracting the archive"
  tar --strip=1 -zxf master.tar.gz
  rm master.tar.gz # Clean up the downloaded archive
else
  echo "ERROR   : Failed to download $ArchiveUrl"
  popd || exit
  exit 1
fi

# ----------------------------------------
# Return to the previous directory

popd || { echo "ERROR   : Failed to return to the previous directory"; exit 1; }

# ----------------------------------------
# Finish

echo ""
echo "END     : $(date) - Finished installing Vim Markdown support"
echo "SPACER  : ----------------------------------------"
exit 0 # Success

