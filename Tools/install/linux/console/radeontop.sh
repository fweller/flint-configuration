#!/bin/bash
# ----------------------------------------
# NAME        : install-radeontop.sh
# DESCRIPTION : Installs radeontop, a tool to view AMD GPU utilization.
#               The script supports installation from the repository or building from source.
# ----------------------------------------
# REFERENCE   :
#               - https://github.com/clbr/radeontop
# ----------------------------------------

# ----------------------------------------
# TODO
# This script needs to be tested on Turing.
# ----------------------------------------

# ----------------------------------------
# Function: Command
# Description: Executes a given command, prints it, and handles errors.
Command() {
  echo ""
  echo "COMMAND: $1"
  echo ""
  if ! eval "$1"; then
    echo "ERROR: Command failed - $1"
    exit 1
  fi
}

# ----------------------------------------
# Start of the script

echo ""
echo "SPACER  : ----------------------------------------"
echo "START   : $(date) - Installing radeontop"

# ----------------------------------------
# Update package lists and fix broken dependencies

Command "sudo apt update"
Command "sudo apt --fix-broken install -y"

# ----------------------------------------
# Install radeontop from the repository

Command "sudo apt install -y radeontop"

# ----------------------------------------
# Usage information

echo ""; echo "To use radeontop, run the following command:"
echo "COMMAND: radeontop -c"

# ----------------------------------------
# Finish the script

exit 0 # Success

# ----------------------------------------
# Build from Source

# Variables
BuildDir="${HOME}/Builds"

# ----------------------------------------
# Download the project and move into the build directory

Command "mkdir -p \"${BuildDir}\""
Command "pushd \"${BuildDir}\" || exit"
Command "git clone https://github.com/clbr/radeontop"
Command "cd \"radeontop\" || exit"

# ----------------------------------------
# Compile the project

Command "make amdgpu=1 xcb=1"

# ----------------------------------------
# Return to the previous directory

Command "popd || exit"

# ----------------------------------------
# Finish

echo ""
echo "END     : $(date) - radeontop installation from source completed"
echo "SPACER  : ----------------------------------------"

exit 0 # Success

# ----------------------------------------
# Uninstall Instructions
# To uninstall, remove the binaries and the cloned repository.

