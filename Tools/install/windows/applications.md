# Windows Applications

## Essential

- Firefox
- [Aquasnap](https://www.nurgo-software.com/pricing/aquasnap) PAID
- [PureText](https://stevemiller.net/puretext/)
- Obsidian
- 7Zip (or NanaZip)
- Vim
- inSSIDer
- Adobe Acrobat PDF viewer
- WSL2
  - VM [Hyper-V](https://techcommunity.microsoft.com/t5/educator-developer-blog/step-by-step-enabling-hyper-v-for-use-on-windows-11/ba-p/3745905) is already built in
  - WSL2 Ubuntu or Debian
  - MS Terminal

## Good to have

- OneCommander
  - Or QT Tab Bar
- Agent Ransack (file search)
- Spotify
- Chrome
- Microsoft Office
- XPS Viewer
- Logitech Elements (or whatever configures the mouse)
- Kensington Works
- Astro Command Center for headphones
- FoxIt PDF viewer
- Zoom
- WebEX

## Games

- Steam (For nearly all games)
- Epic (Crysis Remastered)
- Battle (Call of Duty)
- UbiSoft (Assassins Creed, Far Cry)

## PC Testing

- HWiNFO
- GeekBench

## Development

- NotePad++
- MS Visual Studio Code
- MS Powertoys
- TeraTerm
- Putty
- WinSCP
- Treesize
- TFTPD64
- Wireshark
- Hex Editor Neo
- Git
- FileZilla
- Bitvise SSH client
- Balena Etcher

## Rarely Used

- Q-Dir
- Midnight Commander
- TeamViewerQS
- LTSPICE
- Irfanview
- VM VirtualBox
- VM [QEMU](https://qemu.weilnetz.de/w64/2024/)

