# Adobe Acrobat Configuration

Adobe Acrobat Reader
    Menu: Edit / Preferences
    Documents
        Restore last view setting when reopening documents
        Remember current state of the tools pane
    Page Display
        Page Layout - single page
        Zoom - Fit Page
    Security (Enhanced) - fix the issues w/ AquaSnap
        Disable Enable Protected Mode at startup.
    Restart Adobe Reader

