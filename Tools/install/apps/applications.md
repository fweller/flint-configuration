# Applications GUI

## Introduction

- This is a list of general usage applications that I employ.
  - OS-specific applications are not listed here.
  - Development-specific applications are not listed here.
- Each category is ranked in decreasing order of preference.
- If an application is not cross-platform, it will be noted.

## Web Browsers

- Firefox
- Chrome
- Opera
- Safari (MacOS) default
- Edge (Windows) default

## Notes

- Obsidian

## Email

- Thunderbird (Linux)
- AirMail (MacOS)
- Outlook (Work Windows)

## PDF Viewer

- Okular (Linux)
- Adobe Reader (Windows, MacOS)
- Foxit (Windows)

## Word Processors

- Word (Windows, MacOS)
- LibreOffice (Linux)

## Spreadsheets

- Excel (Windows, MacOS)
- LibreOffice (Linux)

## Presentations

- Powerpoint (Windows, MacOS)
- LibreOffice (Linux)

## Document Search

- Recoll (Linux)

## Music

- Spotify
- Elisa (Linux)

## Video

- VLC
- SMPlayer
- MPV Media Player

## Compression

- 7-Zip (Windows)
- Keka (MacOS)

## Encryption

- VeraCrypt

## Wi-Fi Scanner

- LinSSID (Linux)
- WiFi Explorer (MacOS)
- inSSIDer (Windows)

## Pointers (Mouse, Trackball, Trackpad)

- Solaar (Linux)
- Logi Options (Windows)
- KensingtonWorks (Windows)

## Conferencing

- Teams
- Zoom

## Chat

- WhatsApp

## Text Editors

- Visual Studio Code
- GVim
- Notepad++ (Windows)
- [See my notes on editors](./editors.md)

## Games

- Steam

## Diff Viewers

- More research required
- Meld (Linux)

## Hex Editors

- Hex Editor Neo (Windows)

## Screen Capture

- Spectacle (Linux)
- Flameshot (Linux)
- Snipping Tool (Windows)

