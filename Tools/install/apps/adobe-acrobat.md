# Adobe Acrobat Reader

Settings for Windows and MacOS below.
I don't know how to prevent the bookmarks panel from opening every time.

## Preferences

- Documents
    - Select "Restore last view settings when reopening documents"
    - Select "Always use filename as document title"
    - Select "Remember last state of All tools pane when opening documents"
        - This should get rid of the annoying tools pane.
- General
    - Unselect "Open PDFs automatically in Reader when they are downloaded in Chrome browser"
    - Unselect "Show me messages when I launch Adobe Acrobat Reader"
- Page Display
    - Page Layout: Single Page Continuous
    - Zoom: Fit Page
    - Resolution: Use system setting
- Search
    - Select "Always show more options in advanced search"
- Security (Enhanced)
    - Unselect "Enable Protected Mode at startup"

