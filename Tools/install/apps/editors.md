# Source Code Editors and Integration Development Environments

## Popularity Charts

- [Stack Overflow 2023](https://survey.stackoverflow.co/2023/#section-most-popular-technologies-integrated-development-environment)

## Visual Studio

- [Visual Studio](https://visualstudio.microsoft.com/) Microsoft Windows-only platform
- [Visual Studio Code](https://code.visualstudio.com/) Microsoft multi-OS platform
- [Code-OSS](https://github.com/microsoft/vscode) is the open-source codebase for VS Code
- [VSCodium](https://vscodium.com/) is a community-driven derivative of Code-OSS

## JetBrains

- [JetBrains](https://www.jetbrains.com/)
- [JetBrains Fleet](https://www.jetbrains.com/fleet/)
- [IntelliJ IDEA](https://www.jetbrains.com/idea/)

## Other Modern IDE

- [Sublime Text](https://www.sublimetext.com/)
- [Eclipse IDE](https://eclipseide.org/)
- [Xcode](https://developer.apple.com/xcode/) Apple MacOS only

## Vim

- [Vim](https://www.vim.org/)
- [NeoVim](https://neovim.io/)
- [SpaceVim](https://spacevim.org/)

## Emacs

- [EMACS](https://www.gnu.org/software/emacs/)
- [Doom Emacs](https://github.com/doomemacs/doomemacs)

## EOL

- [Atom](https://atom-editor.cc/) is not longer developed
