#!/bin/bash
# Install all of my favorite fonts
# Should work on any Debian APT Linux distro
# Will need modification for use with MacOS

# final install dir
finaldestdir="/usr/share/fonts"
finalttfdir="$finaldestdir/truetype"
finalotfdir="$finaldestdir/opentype"

# temporary install dir
destdir="${HOME}/Tmp/usrsharefonts"
ttfdir="$destdir/truetype"
otfdir="$destdir/opentype"
mkdir -p "$ttfdir"
mkdir -p "$otfdir"

# temporary download dir
srcdir="${HOME}/Tmp/fonts"
mkdir -p "$srcdir"

font=""
install=""
pushd "$srcdir" || exit 1
pwd



copyotf() {
find "./$font" -name "*.otf" | while read -r line; do
    echo "Copying $line to $install"
    cp -- "$line" "$install"
done
}


copyttf() {
find "./$font" -name "*.ttf" | while read -r line; do
    echo "Copying $line to $install"
    cp -- "$line" "$install"
done
}




google-download() {
# download all of the google fonts at once
wget https://github.com/google/fonts/archive/main.zip
unzip main.zip
}




noto() {
#git clone https://github.com/googlefonts/noto-fonts.git
install="$ttfdir/noto"
mkdir -p "$install"
font="fonts-main/ofl/notosans"
copyttf
font="fonts-main/ofl/notosansdisplay"
copyttf
font="fonts-main/ofl/notoserif"
copyttf
font="fonts-main/ofl/notosansmono"
copyttf
font="fonts-main/ofl/notosansrunic"
copyttf
font="fonts-main/ofl/notosansmath"
copyttf
}



ubuntu() {
font="fonts-main/ufl"
install="$ttfdir/ubuntu"
mkdir -p "$install"
copyttf
}



redhat() {
font="RedHatFont"
install="$otfdir/redhat"
mkdir -p "$install"
git clone https://github.com/RedHatOfficial/RedHatFont.git
copyotf
}



roboto() {
#git clone https://github.com/googlefonts/roboto
install="$ttfdir/roboto"
mkdir -p "$install"
font="fonts-main/ofl/robotoserif"
copyttf
font="fonts-main/ofl/robotoflex"
copyttf
font="fonts-main/apache/roboto"
copyttf
font="fonts-main/apache/robotomono"
copyttf
font="fonts-main/apache/robotoslab"
copyttf
}



libre() {
install="$otfdir/libre"
mkdir -p "$install"
# Baskerville
font="Libre-Baskerville"
git clone https://github.com/impallari/Libre-Baskerville.git
copyotf
# Caslon Text
font="Libre-Caslon-Text"
git clone https://github.com/impallari/Libre-Caslon-Text.git
copyotf
# Caslon Display
font="Libre-Caslon-Display"
git clone https://github.com/impallari/Libre-Caslon-Display.git
copyotf
}



merriweather() {
install="$otfdir/merriweather"
mkdir -p "$install"
# merriweather
font="Merriweather"
git clone https://github.com/SorkinType/Merriweather.git
copyotf
# merriweather sans
font="Merriweather-Sans"
git clone https://github.com/SorkinType/Merriweather-Sans.git
copyotf
}




opensans() {
#git clone https://github.com/googlefonts/opensans.git
install="$ttfdir/opensans"
mkdir -p "$install"
font="fonts-main/ofl/opensans"
copyttf
font="fonts-main/apache/opensanscondensed"
copyttf
}



montserrat() {
font="Montserrat"
install="$otfdir/montserrat"
mkdir -p "$install"
git clone https://github.com/JulietaUla/Montserrat.git
copyotf
}



lato() {
echo "lato not installing"
git clone https://github.com/google/fonts/tree/main/ofl/lato.git
font="fonts-main/ofl/lato"
install="$ttfdir/lato"
mkdir -p "$install"
copyttf
}



stix() {
#echo "stix not installing"
git clone https://github.com/stipub/stixfonts.git
font="/stixfonts/fonts/static_otf"
install="$otfdir/stix"
mkdir -p "$install"
copyotf
}


hack() {
font="Hack"
install="$ttfdir/hack"
mkdir -p "$install"
git clone https://github.com/source-foundry/Hack.git
copyttf
}


intel-one-mono() {
font="intel-one-mono"
install="$ttfdir/intel-one-mono"
mkdir -p "$install"
git clone https://github.com/intel/intel-one-mono.git
copyttf
}


officecodepro() {
font="Office-Code-Pro/Fonts/Office Code Pro/OTF"
install="$otfdir/officecodepro"
mkdir -p "$install"
git clone https://github.com/nathco/Office-Code-Pro.git
copyotf
}



computermodern() {
echo "computermodern not installing"
# Computer Modern Unicode Fonts
# Do not install unless I really need these
# wget https://mirrors.ctan.org/fonts/cm-unicode.zip
# or go here https://cm-unicode.sourceforge.io/download.html
}

# Do everything here
google-download
noto
ubuntu
redhat
roboto
libre
merriweather
opensans
montserrat
lato
#stix
hack
#officecodepro
#computermodern


echo "---------------------------------------"
echo "$ttfdir"
ls -al "$ttfdir"
echo "---------------------------------------"
echo "$otfdir"
ls -al "$otfdir"
echo "---------------------------------------"
echo "Debian/Ubuntu Linux"
echo "When you are happy with the temporary install, run the following:"
echo "sudo cp -r $ttfdir/* $finalttfdir"
echo "sudo cp -r $otfdir/* $finalotfdir"
echo "sudo fc-cache -f -v"
echo "---------------------------------------"
echo "Debian/Ubuntu Linux"
echo "Recommended to run these install first, and then manually install the missing fonts"
echo "sudo apt install fonts-noto fonts-noto-hinted fonts-noto-unhinted"
echo "sudo apt install fonts-roboto fonts-roboto-hinted fonts-roboto-slab fonts-roboto-unhinted fonts-roboto-fontface"
echo "sudo apt install fonts-lato"
echo "sudo apt install fonts-open-sans"
echo "sudo apt install fonts-stix"
echo "---------------------------------------"
echo "MacOS and other OS's: just manually move fonts"
echo "---------------------------------------"


popd || exit 0
exit 0
