#!/bin/bash
# ----------------------------------------
# NAME        : .bash_profile
# DESCRIPTION : Bash Profile
#               This shell script is an entry point for:
#               - MacOS Terminal
#               - WSL Terminal
#               - Linux remote shells such as for SSH
# ----------------------------------------

NAME=".bash_profile"

# Handle debug
if [ -f "$HOME/.debug" ]; then
  eval 'date "+%Y-%m-%d %H:%M:%S-%N %Z $NAME" >> "$HOME/Logs/dotfiles.log" '
  #eval 'date "+%Y-%m-%d %H:%M:%S-%N %Z $(basename "${BASH_SOURCE[0]}")" >> "$HOME/Logs/dotfiles.log" '

fi


# ----------------------------------------
# Bash settings

# Append history instead of overwriting
shopt -s histappend

# Save all lines of a multi-line command in the same history entry
shopt -s cmdhist

# History configuration
export HISTTIMEFORMAT="%Y-%m-%d %T "
export HISTCONTROL="ignoredups:ignorespace" # Ignore duplicates and lines starting with space
export HISTSIZE=1024
export HISTFILESIZE=32768
export HISTFILE="$HOME/.bash_history"

# Time formatting
export TIMEFORMAT='real:%lR user:%lU sys:%lS'


# ----------------------------------------
# MacOS

# Set up environment for MacOS Homebrew package manager
HostOS=$($HOME/Tools/scripts/whatos.sh)
case $HostOS in
  'macos')
    export BASH_SILENCE_DEPRECATION_WARNING=1 # Suppress the Catalina ZSH Message
    # Homebrew configuration
    if [ -f /usr/local/bin/brew ]; then # Intel Macs
      eval "$(/usr/local/bin/brew shellenv)"
    elif [ -f /opt/homebrew/bin/brew ]; then # Apple Silicon Macs
      eval "$(/opt/homebrew/bin/brew shellenv)"
    fi
    ;;
  *)
    ;;
esac


# ----------------------------------------
# Chain-loading

# The next dotfile to chain-load
nextfile="$HOME/.profile"

# Check if the shell is interactive before sourcing the next file
if [[ $- == *i* ]] && [ -f "$nextfile" ]; then
  . "$nextfile"
fi

