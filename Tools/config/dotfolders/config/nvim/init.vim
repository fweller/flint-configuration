" NeoVim Init

" --------------------------------------------------
"  Just use my .vim configurations and .vimrc
" --------------------------------------------------
set runtimepath^=~/.vim runtimepath+=~/.vim/after
let &packpath=&runtimepath
source ~/.vimrc

