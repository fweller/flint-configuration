# ------------------------------------------------------------
# NAME        : dpkg.cfg
# DESCRIPTION : dpkg configuration file
# LOCATION    : Location: /etc/dpkg/dpkg.conf
# ------------------------------------------------------------
# This file controls the behavior of dpkg when installing, removing,
# and upgrading packages. Uncomment and adjust the options as needed.
# ------------------------------------------------------------


# ------------------------------------------------------------
# GENERAL SETTINGS
# For more information, visit: https://manpages.debian.org/bullseye/dpkg/dpkg.1.en.html
# ------------------------------------------------------------

# Verbose output
# Enabling this will make dpkg provide more detailed output during operations.
# Useful for debugging or when you want more insight into what dpkg is doing.
# Recommended for users who want to see more details during package management.
# Uncomment to enable.
# verbose=1

# Force dpkg to ignore dependency problems. Be very cautious with this option,
# as it can result in an unstable or broken system. Typically, this is used
# only in very controlled environments or during recovery operations.
# Not recommended for most users, so this remains commented out by default.
# force-depends

# Specify the architecture of the system. Normally, dpkg will detect this
# automatically, so setting it is usually unnecessary. However, you can
# manually specify the architecture if needed.
# Uncomment and adjust as necessary.
# architecture=amd64

# This option is useful when using a custom directory for the dpkg database,
# such as when working in a chroot environment or testing packages. The default
# directory is /var/lib/dpkg.
# Uncomment and adjust as necessary.
# admindir=/custom/path/to/dpkg-db

# The directory where dpkg should install packages. This option is useful
# for testing or custom installations. By default, dpkg installs packages
# to the root filesystem.
# Uncomment and adjust as necessary.
# instdir=/custom/install/directory

# Enable dry-run mode. This mode simulates package installations or removals
# without making any changes. It's useful for testing what dpkg will do
# without actually applying the changes.
# Uncomment to enable.
# no-act

# ------------------------------------------------------------
# CONFIGURATION FILE HANDLING
# For more information, visit: https://manpages.debian.org/bullseye/dpkg/dpkg.1.en.html#CONFIGURATION_FILES
# ------------------------------------------------------------

# Control how dpkg handles configuration files during package upgrades.
# These options allow you to specify whether to keep the old configuration,
# use the new one, or apply default behavior.

# Automatically use the default action if one is suggested during a package
# upgrade. This helps avoid unnecessary prompts and can speed up upgrades.
# Recommended for most users.
force-confdef

# Always keep the old version of a configuration file when a package upgrade
# introduces a new version. This prevents dpkg from overwriting your existing
# configurations.
# Recommended for most users to avoid losing custom configurations.
force-confold

# Alternatively, you can always install the new version of the configuration
# file during an upgrade, overwriting the old one. Use this if you want to
# ensure that you are always using the latest configuration provided by the
# package maintainer.
# Not recommended by default; only use if you know you want to override old configs.
# force-confnew

# If a configuration file is missing during an upgrade, this option will force
# dpkg to install the new configuration file.
# Uncomment if you want dpkg to ensure that missing configuration files
# are replaced with the package defaults.
# force-confmiss

# ------------------------------------------------------------
# PACKAGE INSTALLATION/REMOVAL OPTIONS
# For more information, visit: https://manpages.debian.org/bullseye/dpkg/dpkg.1.en.html#PACKAGE_INSTALLATION/REMOVAL
# ------------------------------------------------------------

# Skip installation of a package if the same version is already installed.
# This can help avoid unnecessary reinstallation of packages during upgrades.
# Uncomment to enable.
# skip-same-version

# Prevent dpkg from downgrading packages. This ensures that only newer versions
# of packages are installed, protecting against accidental downgrades.
# Uncomment to enable.
# refuse-downgrade

# Automatically deconfigure packages that depend on a package being removed.
# This option is useful for avoiding broken dependencies when removing packages.
# Uncomment to enable.
# auto-deconfigure

# This option can help when removing packages that require reinstallation.
# It forces dpkg to remove the package despite the reinstallation requirement.
# Uncomment to enable if you frequently encounter issues with packages
# needing reinstallation before removal.
# force-remove-reinstreq

# ------------------------------------------------------------
# SYSTEM-WIDE SETTINGS (ADVANCED)
# For more information, visit: https://manpages.debian.org/bullseye/dpkg/dpkg.1.en.html#ENVIRONMENT_VARIABLES
# ------------------------------------------------------------

# Automatically install recommended packages. dpkg itself doesn't manage
# dependencies (this is usually done by apt), but this option can be useful
# in specific dpkg-based workflows.
# Uncomment to enable if you want dpkg to automatically handle recommends.
# install-recommends

# Log status information to a specified file. This is useful for auditing or
# troubleshooting package management activities.
# Uncomment and adjust the path if you want to enable logging.
# status-log=/var/log/dpkg-status.log

# Direct dpkg to send status change information to a specific file descriptor.
# This is advanced and typically only used in scripted environments.
# Uncomment and specify the file descriptor if needed.
# status-fd=2

# Enable debug mode with a specified debug level. Higher numbers produce more
# verbose debugging information. This is useful for developers or when
# troubleshooting complex dpkg issues.
# Uncomment and adjust the debug level as necessary.
# debug=2

# Set dpkg to automatically create backup files of old configuration files
# when installing a new version. This is a safety feature that allows recovery
# of old configurations if needed.
# Uncomment to enable.
# backup

# Do not enable debsig-verify by default; since the distribution is not using
# embedded signatures, debsig-verify would reject all packages.
# debsig-verify is a tool that verifies the embedded signatures of Debian
# packages. Since Debian Bookworm does not use these signatures, enabling this
# option would cause all packages to be rejected. It is therefore disabled by default.
# This is recommended to remain disabled unless you have a specific use case.
no-debsig

# Log status changes and actions to a file.
# This option ensures that all actions taken by dpkg, such as package installations,
# removals, and upgrades, are logged to a specified file. This is useful for auditing
# and tracking changes to the system.
# Recommended for system administrators who need to keep detailed logs.
log /var/log/dpkg.log


