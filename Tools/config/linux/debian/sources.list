# ------------------------------------------------------------
# NAME        : debian.list
# DESCRIPTION : Debian repository sources list
# LOCATION    : /etc/apt/sources.list.d/debian.list
# ------------------------------------------------------------

# Information:
# - Debian sources list : https://wiki.debian.org/SourcesList
# - Debian releases     : https://wiki.debian.org/DebianReleases
# - Debian Testing      : https://wiki.debian.org/DebianTesting
# - Debian Unstable     : https://wiki.debian.org/DebianUnstable

# Format:
# - Binary Packages     : deb URI SUITES COMPONENTS
# - Source Packages     : deb-src URI SUITES COMPONENTS

# URI (Uniform Resource Identifier):
# - Typically           : https://deb.debian.org/debian

# Suite Base Names:
# - Aliases: oldoldstable, oldstable, stable, testing, unstable
# - Codenames: stretch, buster, bullseye, bookworm, trixie, forky, sid (always unstable)
# - Recommendation: Use the codename instead of aliases for stable releases to ensure 
#   consistency when new stable versions are released.

# Components:
# - main = Debian packages that rely only on packages in main
# - contrib = Packages that comply with Debian DFSG but may rely on non-free components
# - non-free = Packages that do not comply with DFSG
# - non-free-firmware = Firmware that does not comply with DFSG
# - stable-security  : https://www.debian.org/security/
# - stable-updates   : https://wiki.debian.org/StableUpdates
# - stable-backports : https://wiki.debian.org/Backports
# - fast-track       : https://fasttrack.debian.net/
# - stable-proposed-updates   : https://wiki.debian.org/StableProposedUpdates

# Debian Version to Codename Mapping:
# - 9 Stretch   (2017)
# - 10 Buster   (2019)
# - 11 Bullseye (2021)
# - 12 Bookworm (2023)
# - 13 Trixie   (2025)
# - 14 Forky    (2027)

# --------------------------------------------
# Stable Branches

# Stable (Bookworm)
deb https://deb.debian.org/debian bookworm main contrib non-free non-free-firmware
# deb-src https://deb.debian.org/debian bookworm main contrib non-free non-free-firmware

# Stable-Security
# Note: security.debian.org may be phased out in favor of deb.debian.org/debian-security
deb https://security.debian.org/debian-security bookworm-security main contrib non-free non-free-firmware
# deb-src https://security.debian.org/debian-security bookworm-security main contrib non-free non-free-firmware

# Stable-Updates
deb https://deb.debian.org/debian bookworm-updates main contrib non-free non-free-firmware
# deb-src https://deb.debian.org/debian bookworm-updates main contrib non-free non-free-firmware

# Stable-Proposed-Updates
# deb https://deb.debian.org/debian bookworm-proposed-updates main contrib non-free non-free-firmware
# deb-src https://deb.debian.org/debian bookworm-proposed-updates main contrib non-free non-free-firmware

# Stable-Backports
deb https://deb.debian.org/debian bookworm-backports main contrib non-free non-free-firmware
# deb-src https://deb.debian.org/debian bookworm-backports main contrib non-free non-free-firmware

# Fast Track
# Install the fasttrack-archive-keyring before enabling these repositories:
#   sudo apt install fasttrack-archive-keyring
# Uncomment the following lines to enable Fast Track:
# deb https://fasttrack.debian.net/debian-fasttrack/ bookworm-fasttrack main contrib
# deb https://fasttrack.debian.net/debian-fasttrack/ bookworm-backports-staging main contrib

# --------------------------------------------
# Testing Branch

# Testing
# deb https://deb.debian.org/debian testing main contrib non-free non-free-firmware
# deb-src https://deb.debian.org/debian testing main contrib non-free non-free-firmware

# Note: The 'testing-security' repository exists but is currently unused.
# It is advisable not to rely on it for security updates.
# deb https://security.debian.org/debian-security testing-security main contrib non-free non-free-firmware
# deb-src https://security.debian.org/debian-security testing-security main contrib non-free non-free-firmware

# --------------------------------------------
# Unstable (Sid) Branch

# Unstable (Sid)
# deb https://deb.debian.org/debian unstable main contrib non-free non-free-firmware
# deb-src https://deb.debian.org/debian unstable main contrib non-free non-free-firmware

# --------------------------------------------

