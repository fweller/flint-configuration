# i3wm
# load: copy settings from usable location to storage location
cp ${HOME}/.config/i3/config ${HOME}/Tools/config/linux/i3wm.i3.config
# store: copy settings from storage location to usable location
cp ${HOME}/Tools/config/linux/i3wm.i3.config ${HOME}/.config/i3/config 
