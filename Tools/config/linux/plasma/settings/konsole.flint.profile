[Appearance]
ColorScheme=GreenOnBlack
Font=Noto Sans Mono,12,-1,5,50,0,0,0,0,0
TabColor=27,30,32,0
UseFontLineChararacters=true

[Cursor Options]
CustomCursorColor=0,255,0
UseCustomCursorColor=true

[General]
Environment=TERM=xterm-256color,COLORTERM=truecolor
LocalTabTitleFormat=%#
Name=Flint
Parent=FALLBACK/
RemoteTabTitleFormat=%#

[Interaction Options]
CopyTextAsHTML=false
TrimLeadingSpacesInSelectedText=true
TrimTrailingSpacesInSelectedText=true

[Scrolling]
HistoryMode=2
HistorySize=9999

[Terminal Features]
BellMode=3
BlinkingCursorEnabled=true
