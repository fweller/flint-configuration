#!/bin/bash
## Configure Yakuake to autostart in KDE Plasma

# Storage Variables
StoreDir="${HOME}/Tools/config/linux/plasma" # Plasma configurations
SettingsDir="${StoreDir}/settings" # Config files are stored here

# Target Variables
FileName="org.kde.yakuake.desktop" # Yakuake desktop
ConfigDir="${HOME}/.config/autostart" # Plasma autostart

# Copy the Yakuake autostart file into the KDE .config autostart location
cp "${SettingsDir}/${FileName}" "${ConfigDir}"

