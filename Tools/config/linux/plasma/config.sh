#!/bin/bash
## Load my KDE Plasma settings into this computer

## Storage Variables
ConfigDir="${HOME}/Tools/config/linux/plasma" # Plasma configuration root
SourceDir="${ConfigDir}/settings" # Settings and Config files are stored here

## Target Variables

# Konsole
KonsoleTargetDir="${HOME}/.local/share/konsole" # Konsole deployed here
KonsoleTargetName="Flint.profile" # What it's called when deployed
KonsoleSourceName="konsole.flint.profile" # What it's called when stored

KonsoleLoad() {
  cp "${SourceDir}/$KonsoleSourceName" "${KonsoleTargetDir}/${KonsoleTargetName}"
}

KonsoleStore() {
  cp "${KonsoleTargetDir}/${KonsoleTargetName}" "${SourceDir}/$KonsoleSourceName"
}

# Load all configurations into local system
Load() {
  KonsoleLoad
}

# Store configurations in Tools
Store() {
  KonsoleStore
}

# Handle input commands
for arg do
  case $arg in
    load |--load)
      Load
      ;;
    store |--store)
      Store
      ;;
    *)
      echo "USAGE:"
      echo "config.sh load  # Load settings into local system"
      echo "config.sh store # Store settings into tools"
      ;;
  esac
done

