#!/bin/sh
# ----------------------------------------
# NAME        : KensingtonExpertMouse.sh
# DESCRIPTION : Modifications in Linux X11 for the Kensington Expert Mouse Trackball
# USAGE       : KensingtonExpertMouse.sh [debug]
# WORKS ON    : Linux
# ----------------------------------------
# NOTES
# - To see a list of all devices: xinput --list
# - To see a list of all features of one device: xinput --list-props ID#
# - To see a live view of all activities of the pointer: xinput test ID#
# - Changes made with xinput are not persistent across reboots.
#   To make them permanent, add the full xinput --set-prop command to your .xsessionrc file
# - Excellent article about the Input Coordinate Transformation Matrix:
#   https://wiki.ubuntu.com/X/InputCoordinateTransformation
# - Call this script with "debug" as the parameter and it will print info to stdout
# - lsusb : ID 047d:1020 Kensington Expert Mouse Trackball
# - hwinfo --usb
#   Model: "Kensington Expert Mouse Trackball"
#   Vendor: usb 0x047d "Kensington"
#   Device: usb 0x1020 "Expert Mouse Trackball"
# ----------------------------------------

NAME="KensingtonExpertMouse.sh"

# ----------------------------------------
# Handle X11 vs Wayland

if [ "$XDG_SESSION_TYPE" = "x11" ]; then
  # Okay to continue running this script
  X11="TRUE"
  # echo "Running in X11 environment"
elif [ "$XDG_SESSION_TYPE" = "wayland" ]; then
  # Not possible to run this script....yet
  Wayland="TRUE"
  exit # Exit silently
  #if [ "$XWAYLAND" = "1" ]; then
    #XWayland="TRUE"
    #echo "Running in Xwayland environment"
  #else
    #XWayland="FALSE"
    #echo "Running in native Wayland environment"
else
  #echo "Not in an X11 or Wayland environment"
  exit # Exit silently
fi

# ----------------------------------------
# Handle debug logging

if [ -f "$HOME/.debug" ]; then
  # Append the formatted date and script name to the log file
  date "+%Y-%m-%d %H:%M:%S-%N %Z $NAME" >> "$HOME/Logs/dotfiles.log"
fi

# -----------------------------------------------------
# Variables

# Pointer (Mouse) name
PointerName="Kensington      Kensington Expert Mouse"

# Speed
SpeedName="Coordinate Transformation Matrix"
Speed="1.3" # User can edit this variable to update SpeedValue
SpeedValue="$Speed, 0, 0, 0, $Speed, 0, 0, 0, 1"

# Acceleration
AccelerationName="libinput Accel Speed"
AccelerationValue="1"

# -----------------------------------------------------
# Functions

# Print message to console if debug is enabled
Print() {
  if [ "$DEBUG" ]; then
    echo "-----------------------------------------------------"
    echo "$1"
    echo ""
  fi
}

# Change a property value
ChangeSetting() {
  Property="$1"
  Print "Issuing command: $Property"
  eval "$Property"
}

# -----------------------------------------------------
# Main Execution

# Enable debug mode if argument "debug" is passed
if [ "$1" = "debug" ]; then
  DEBUG=yes
  Print "Debug mode enabled"
fi

# Check if the pointer device is present
PointerPresent=$(xinput | grep -F "$PointerName")
Print "PointerPresent=$PointerPresent"

if [ -z "$PointerPresent" ]; then
  Print "Pointer '${PointerName}' is not present in this system"
  exit 1
else
  # Get all Pointer IDs (handles multiple instances of the same device)
  PointerIDs=$(echo "$PointerPresent" | grep -oP '(?<=id=)\d+')
  if [ -z "$PointerIDs" ]; then
    Print "Failed to extract Pointer IDs for '${PointerName}'"
    exit 1
  fi

  # Iterate over each Pointer ID and apply settings
  for PointerID in $PointerIDs; do
    Print "Applying settings to PointerID=$PointerID"
    Print "Issue this command to see properties: xinput --list-props $PointerID"

    # Change properties
    ChangeSetting "xinput --set-prop $PointerID '$SpeedName' --type=float $SpeedValue"
    ChangeSetting "xinput --set-prop $PointerID '$AccelerationName' --type=float $AccelerationValue"
  done
fi

