#!/bin/bash
# ----------------------------------------
# NAME        : wslmount.sh
# DESCRIPTION : Mount network storage on WSL2
# USAGE       : wslmount.sh
# WORKS ON    : WSL2 (Windows Subsystem for Linux 2)
# ----------------------------------------

# Exit immediately if a command exits with a non-zero status.
set -e


# ------------------------------------------------
# Variables

MOUNT_TYPE="-t auto"
MOUNT_OPTIONS="-o credentials=/root/.skynetcredentials,vers=3,rw,uid=flint,gid=flint,iocharset=utf8"
NAS_SERVER="192.168.0.11"


# ------------------------------------------------
# NAS Mounts

declare -a mount_points=(
  SkyData
  SkyStore
  SkyWork
  SkyPenguin
  Video
  Music
)

declare -a net_mounts=(
  /mnt/SkyData
  /mnt/SkyStore
  /mnt/SkyWork
  /mnt/SkyPenguin
  /mnt/SkyVideo
  /mnt/SkyMusic
)



# ------------------------------------------------
# Function to mount a list of network shares

mount_nas_shares() {

  for i in "${!mount_points[@]}"; do
    local share="${mount_points[$i]}"
    local mount_point="${net_mounts[$i]}"

    # Check if the mount point directory exists
    if [[ ! -d "$mount_point" ]]; then
      echo "ERROR    : Mount point $mount_point does not exist. Creating it now."
      sudo mkdir -p "$mount_point"
    fi

    # Check if the share is already mounted
    if mountpoint -q "$mount_point"; then
      echo "INFO     : $mount_point is already mounted."
    else
      local mount_command="sudo mount $MOUNT_TYPE $MOUNT_OPTIONS //${NAS_SERVER}/${share} ${mount_point}"
      printf "ATTEMPT  : %s\n" "$mount_command"
      eval "$mount_command"
    fi
  done
}


# ------------------------------------------------
# Say hello

echo "START    : Running WSL Mount Script"


# ------------------------------------------------
# Only run under WSL

HOST_OS=$("$HOME"/Tools/scripts/whatos.sh)

if [ "$HOST_OS" != "wsl" ]; then
  echo "ERROR    : This script can only be run under WSL (Windows Subsystem for Linux)"
  exit 1 # Operation not permitted
fi


# ------------------------------------------------
# Mount

mount_nas_shares


# ------------------------------------------------
# End

echo "END      : Running WSL Mount Script"
exit 0 # Success

