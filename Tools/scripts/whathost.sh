#!/bin/sh
# ----------------------------------------
# NAME        : whathost.sh
# DESCRIPTION : Return a sanitized hostname
# USAGE       : whathost.sh
# WORKS ON    : Linux, MacOS, WSL2, Cygwin, BSD
# ----------------------------------------

# Use hostname and then strip out the .local from it, this is the harder way vs hostname -s
# HNameExtra=$(hostname)
# HNameRaw="${HNameExtra%%.*}"

# Get the hostname and strip out the domain part
HNameRaw=$(hostname | cut -d '.' -f 1)

case $HNameRaw in
  'Tesla')
    HName="tesla"
    ;;
  'Archimedes')
    HName="archimedes"
    ;;
  'DaVinci')
    HName="davinci"
    ;;
  'Turing'|'TuringWin')
    HName="turing"
    ;;
  'ZBook')
    HName="zbook"
    ;;
  'USSJS17NBA31295'|'ushom00nb031295')
    HName="avnet"
    ;;
  *)
    HName="unknown"
    ;;
esac

echo $HName
exit 0

