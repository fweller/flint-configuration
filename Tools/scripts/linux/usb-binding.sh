#!/bin/bash
# Unbind and then Rebind xHCI USB devices


# 20230717 ZBook reporting this error in DMESG
# usb 4-1.3-port1: Cannot enable. Maybe the USB cable is bad?

# Must run as root
# sudo -s
if [[ $EUID != 0 ]] ; then
  echo This must be run as root!
  exit 1
fi

# Move to directory
pushd /sys/bus/pci/drivers/xhci_hcd/ || exit

# Unbind, then rebind everthing
for file in ????:??:??.? ; do
 echo -n "$file" > unbind
 echo -n "$file" > bind
done

# Return
popd || exit

