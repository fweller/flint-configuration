#!/bin/bash

# Description: This script deletes duplicate files identified by a number in parentheses in the filename.

# Usage: ./delete_duplicates.sh /path/to/directory

if [ -z "$1" ]; then
    echo "Please provide a directory."
    exit 1
fi

directory="$1"

# Check if the directory exists
if [ ! -d "$directory" ]; then
    echo "Directory does not exist: $directory"
    exit 1
fi

# Finding and deleting duplicate files
find "$directory" -type f -name '*([0-9])*' -exec echo Deleting: {} \; -exec rm {} \;

echo "Duplicate files have been deleted."

