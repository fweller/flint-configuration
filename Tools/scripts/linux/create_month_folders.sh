#!/bin/bash

# ----------------------------------------
# NAME        : create_month_folders.sh
# DESCRIPTION : Creates 12 folders named as 01-Jan, 02-Feb, etc., in the current directory.
# USAGE       : ./create_month_folders.sh
# ----------------------------------------

# Array of month names
months=("Jan" "Feb" "Mar" "Apr" "May" "Jun" "Jul" "Aug" "Sep" "Oct" "Nov" "Dec")

# Loop through the array and create folders
for i in $(seq 1 12); do
    # Format the number with leading zero
    folder_name=$(printf "%02d-%s" $i "${months[$i-1]}")
    # Create the folder
    mkdir -p "$folder_name"
    echo "Created folder: $folder_name"
done

echo "All 12 folders have been created successfully."
