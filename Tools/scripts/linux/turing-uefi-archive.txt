## For use on Turing only

# Must run as root

# Mount the EFI directories
mkdir -p /media/0; mount  -t vfat /dev/nvme0n1p1 /media/0; \
mkdir -p /media/1; mount  -t vfat /dev/nvme1n1p1 /media/1; \
mkdir -p /media/2; mount  -t vfat /dev/nvme2n1p1 /media/2; \
mkdir -p /media/3; mount  -t vfat /dev/nvme3n1p1 /media/3;

# Archive the EFI directories
7za a /home/flint/Downloads/0.7z /media/0; \
7za a /home/flint/Downloads/1.7z /media/1; \
7za a /home/flint/Downloads/2.7z /media/2

# Unmount the EFI directories
umount /media/3; umount /media/2; umount /media/1; umount /media/0; \
rmdir /media/3; rmdir /media/2; rmdir /media/1; rmdir /media/0

# Change permissions of the archives
cd /home/flint/Downloads; \
sudo chown flint *.7z; \
sudo chgrp flint *.7z


