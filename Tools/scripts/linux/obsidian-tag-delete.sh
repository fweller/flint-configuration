#!/bin/bash

# ----------------------------------------
# Script Name: obsidian-tag-delete.sh
# Description: This script recursively searches for markdown files from a specified directory.
#              It checks each markdown file for a YAML block within the first ten lines.
#              If found, the script prints the file name and YAML block contents, including the closing "---".
#              If the `--delete` flag is provided during execution, the script will delete the entire YAML block.
# Requirements:
#   - Executable in a Linux environment.
#   - Takes one mandatory argument: the starting directory ($StartingDir).
#   - ReallyDelete mode controlled by a command-line flag (--delete).
#   - YAML blocks starting and ending within the first ten lines are considered.
# Expected Behavior:
#   - Recursively searches all sub-directories under $StartingDir for .md files.
#   - Checks for a YAML block starting within the first three lines and ending within the first ten lines.
#   - Prints the file name and YAML block contents if found.
#   - Removes the YAML block, including the "---" lines, if --delete flag is used.
# Safety Features:
#   - Includes a confirmation step when the --delete flag is used to prevent accidental data loss.
# ----------------------------------------

# Process command-line options
ReallyDelete="FALSE"
for arg in "$@"; do
  if [[ "$arg" == "--delete" ]]; then
    ReallyDelete="TRUE"
  fi
done

# Check for directory argument
if [ -z "$1" ] || [[ "$1" == "--"* ]]; then
  echo "Usage: $0 <Starting Directory> [--delete]"
  exit 1
fi

StartingDir="$1"

# Function to process each Markdown file
process_file() {
    local file=$1
    local delete_flag=$2
    local start_line=0
    local end_line=0
    local in_yaml_block=false
    local line_count=0
    local content=()

    while IFS= read -r line; do
        ((line_count++))
        if [[ $line_count -le 10 && "$line" == "---" ]]; then
            if [[ $in_yaml_block == false ]]; then
                in_yaml_block=true
                start_line=$line_count
                content+=("$line")
                echo "File: $file"
                echo "YAML block:"
            elif [[ $in_yaml_block == true ]]; then
                content+=("$line")
                end_line=$line_count
                printf '%s\n' "${content[@]}"
                break
            fi
        elif [[ $in_yaml_block == true ]]; then
            content+=("$line")
        fi
    done < "$file"

    # If ReallyDelete is TRUE and YAML block was found
    if [[ $delete_flag == "TRUE" && $in_yaml_block == true ]]; then
        awk "NR<$start_line || NR>$end_line" "$file" > "$file.tmp" && mv "$file.tmp" "$file"
    fi
}

export -f process_file

# Find markdown files and process them
find "$StartingDir" -type f -name "*.md" -exec bash -c 'process_file "$0" "$1"' {} $ReallyDelete \;

