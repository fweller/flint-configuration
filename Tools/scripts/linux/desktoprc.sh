#!/bin/bash
# DesktopRC = Desktop RunCom = Desktop Run Commands
# Run this shell script after the desktop loads
test -f "${HOME}/.debug" && \
  eval 'date "+%Y-%m-%d %H:%M:%S-%N %Z $(basename "${BASH_SOURCE[0]}")" >> "${HOME}/Logs/dotfiles.log" '
# Call pointer configuration files
Trackball="${HOME}/Tools/pointers/kensington_expert_mouse.sh"
test -f "${Trackball}" && . "${Trackball}"
Trackpad="${HOME}/Tools/pointers/apple_magic_trackpad2.sh"
test -f "${Trackpad}" && . "${Trackpad}"

