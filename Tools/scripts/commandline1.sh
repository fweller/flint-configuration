#!/bin/sh
# ----------------------------------------
# NAME        : commandline1.sh
# DESCRIPTION : Command Line PS1 Configuration and Coloring
# WORKS ON    : Linux, MacOS, WSL2, Cygwin, BSD
# ----------------------------------------

# Standard (Not Bold)
SBK="\[\033[0;30m\]" #Black
SRD="\[\033[0;31m\]" #Red
SGR="\[\033[0;32m\]" #Green
SYE="\[\033[0;33m\]" #Yellow
SBL="\[\033[0;34m\]" #Blue
SMG="\[\033[0;35m\]" #Magenta
SCY="\[\033[0;36m\]" #Cyan
SWT="\[\033[0;37m\]" #White
 RS="\[\033[0m\]"    #Reset

# Bold
BBK="\[\033[1;30m\]" #Black
BRD="\[\033[1;31m\]" #Red
BGR="\[\033[1;32m\]" #Green
BYE="\[\033[1;33m\]" #Yellow
BBL="\[\033[1;34m\]" #Blue
BMG="\[\033[1;35m\]" #Magenta
BCY="\[\033[1;36m\]" #Cyan
BWT="\[\033[1;37m\]" #White

# Useful Bash special characters for PS1
# \h hostname
# \u username
# \w pwd
# \n newline
# \r carriage return

# Line 1: Cyan [ White username Cyan @ Red hostname Cyan ] Green $ Reset
export PS1="$BCY[$BWT\u$BCY@$BRD\h$BCY] $BGR$ $RS"

