#!/bin/sh
# ----------------------------------------
# NAME        : whatos.sh
# DESCRIPTION : Return the host operating system type
# USAGE       : whatos.sh
# WORKS ON    : Linux, MacOS, WSL2, Cygwin, BSD
# ----------------------------------------

# Get the operating system name
OSName=$(uname -s)

# Microsoft WSL returns Linux, so need to check /proc/version also

case $OSName in
  Linux)
    if grep -iq Microsoft /proc/version 2>/dev/null; then
      OS="wsl"
    else
      OS="linux"
    fi
    ;;
  Darwin)
    OS="macos"
    ;;
  CYGWIN*)
    OS="cygwin"
    ;;
  *BSD)
    OS="bsd"
    ;;
  *)
    OS="unknown"
    ;;
esac

echo $OS
exit 0

