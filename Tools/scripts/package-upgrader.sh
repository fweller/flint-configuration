#!/bin/sh
# ----------------------------------------
# NAME        : package-upgrader.sh
# DESCRIPTION : Upgrade packages using the most preferred package manager
# USAGE       : package-upgrader.sh
# WORKS ON    : Linux, WSL2, MacOS
# ----------------------------------------

#Arguments=$@

Command() {
  echo ""
  echo "COMMAND: $1"
  echo ""
  eval "$1"
}


# Detections performed in order from most to least preferred
# nala   = Upgraded: Debian, Ubuntu, Mint
# apt    = Debian, Ubuntu, Mint
# brew   = MacOS HomeBrew
# pkcon  = Distro Agnostic
# pacman = Arch, Manjaro
# zypper = OpenSUSE
# dnf    = Fedora, RHEL
# urpmi  = Mandriva, Mageia
# emerge = Gentoo

if [ -x "$(command -v nala)" ]; then 
  Command "sudo nala upgrade"
elif [ -x "$(command -v apt)" ]; then
  Command "sudo apt upgrade"
elif [ -x "$(command -v brew)" ]; then
  Command "sudo apt upgrade"
elif [ -x "$(command -v pkcon)" ]; then
  Command "sudo pkcon upgrade"
elif [ -x "$(command -v pacman)" ]; then
  Command "sudo pacman -Su"
elif [ -x "$(command -v zypper)" ]; then
  Command "sudo zypper update"
elif [ -x "$(command -v dnf)" ]; then
  Command "sudo dnf update"
elif [ -x "$(command -v urpmi)" ]; then
  Command "sudo urpmi --auto-select"
elif [ -x "$(command -v emerge)" ]; then
  Command "sudo emerge -NuDa world"
else
  echo ""; "ERROR  : Failed to find a suitable package manager"
fi

exit 0

