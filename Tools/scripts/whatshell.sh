#!/bin/sh
# ----------------------------------------
# NAME        : whatshell.sh
# DESCRIPTION : Return the default user shell type
# USAGE       : whatshell.sh
# WORKS ON    : Linux, MacOS, WSL2, Cygwin, BSD
# ----------------------------------------


# Check if getent is available
if command -v getent > /dev/null 2>&1; then
  # Get the current user's default shell using getent
  default_shell=$(getent passwd "$USER" | cut -d: -f7)
else
  # Fallback to checking /etc/passwd directly
  default_shell=$(grep "^$USER:" /etc/passwd | cut -d: -f7)
fi

# Extract the shell name from the full path
shell_name=$(basename "$default_shell")

echo $shell_name
exit 0

