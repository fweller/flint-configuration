#!/bin/sh
# ----------------------------------------
# NAME        : whatdistro.sh
# DESCRIPTION : Returns the host Linux distro name
# USAGE       : whatdistro.sh
# WORKS ON    : Linux, MacOS, WSL2, Cygwin, BSD
# ----------------------------------------

# Function to check if a command exists
command_exists() {
    command -v "$1" >/dev/null 2>&1
}

# Get distro name based on available commands
if command_exists lsb_release; then
    # lsb_release is available, use it to get the distribution ID
    Distro_ID=$(lsb_release --id --short | tr "[:upper:]" "[:lower:]")
elif [ -f /etc/os-release ]; then
    # Fall back to reading /etc/os-release if lsb_release is not available
    Distro_ID=$(grep '^ID=' /etc/os-release | cut -d= -f2 | tr -d '"' | tr "[:upper:]" "[:lower:]")
elif [ -f /etc/issue ]; then
    # For older systems, try reading /etc/issue
    Distro_ID=$(head -n 1 /etc/issue | cut -d' ' -f1 | tr "[:upper:]" "[:lower:]")
elif command_exists uname; then
    # For systems like MacOS or BSD, use uname
    Distro_ID=$(uname -s | tr "[:upper:]" "[:lower:]")
else
    # If all else fails, return unknown
    Distro_ID="unknown"
fi

echo "$Distro_ID"
exit 0

