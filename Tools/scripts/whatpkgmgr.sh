#!/bin/sh
# ----------------------------------------
# NAME        : whatpkgmgr.sh
# DESCRIPTION : Return the most preferred of available package managers
# USAGE       : whatpkgmgr.sh
# WORKS ON    : Linux, WSL2, MacOS
# ----------------------------------------

# Detections performed in order from most to least preferred
# nala   = Upgraded: Debian, Ubuntu, Mint
# apt    = Debian, Ubuntu, Mint
# brew   = MacOS HomeBrew
# pkcon  = Distro Agnostic
# pacman = Arch, Manjaro
# zypper = OpenSUSE
# dnf    = Fedora, RHEL
# urpmi  = Mandriva, Mageia
# emerge = Gentoo

for pkg_mgr in nala apt brew pkcon pacman zypper dnf urpmi emerge; do
    if command -v "$pkg_mgr" > /dev/null 2>&1; then
        echo "$pkg_mgr"
        exit 0
    fi
done


echo "ERROR" # Should never reach this line
exit 1

# ----------------------------------------
# Bash version
# package_managers=("nala" "apt" "pkcon" "pacman" "zypper" "dnf" "urpmi" "emerge")
# for pkg_mgr in "${package_managers[@]}"; do
#    if command -v "$pkg_mgr" > /dev/null 2>&1; then
#        echo "$pkg_mgr"
#        exit 0
#    fi
# done

# ----------------------------------------
# Previous shell version
#if [ -x "$(command -v nala)" ]; then RetVal="nala"
#elif [ -x "$(command -v apt)" ]; then RetVal="apt"
#elif [ -x "$(command -v pkcon)" ]; then RetVal="pkcon"
#elif [ -x "$(command -v pacman)" ]; then RetVal="pacman"
#elif [ -x "$(command -v zypper)" ]; then RetVal="zypper"
#elif [ -x "$(command -v dnf)" ]; then RetVal="dnf"
#elif [ -x "$(command -v urpmi)" ]; then RetVal="urpmi"
#elif [ -x "$(command -v emerge)" ]; then RetVal="emerge"
#else RetVal="ERROR"
#fi

