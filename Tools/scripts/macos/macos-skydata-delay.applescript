delay 5
tell application "Finder"
	try
		mount volume "smb://SkyNet/SkyData"
		mount volume "smb://SkyNet/SkyStore"
		mount volume "smb://SkyNet/SkyWork"
		mount volume "smb://SkyNet/SkyTM1"
		mount volume "smb://SkyNet/SkyTM2"
		mount volume "smb://SkyNet/music"
		mount volume "smb://SkyNet/video"
	on error number n
		display alert ("Error " & n & " on network share mount")
	end try
end tell
