#!/bin/sh
# Start the new evernote AppImage

# Evernote has killed the application
exit

# TODO
# Run from this location: /home/flint/.local/share/applications
# KDE: kioclient exec <path-to-desktop-file>
# Gnome: gio launch ./foo.desktop or gtk-launch app-name

# Get the name of the AppImage
AppImageName=$(ls "${HOME}/Applications" | grep Evernote)
#echo $AppImageName

# Define how we will call the AppImage
AppImageCmd="${HOME}/Applications/${AppImageName} --no-sandbox %U"
#echo $AppImageCmd

# Run the AppImage and redirect output to logfile
$AppImageCmd >> "${HOME}/Logs/evernote.log" 2>&1 &

