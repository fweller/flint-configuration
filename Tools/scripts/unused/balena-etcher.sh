#!/bin/sh
# balena-etcher.sh
# balena
# etcher
# Purpose: Start Balena-Etcher properly
# Assuming that this file exists: ~/Projects/Balena-Etcher/Balena-Etcher
# Install: ln ${HOME}/Tools/scripts/balena-etcher.sh ${HOME}/Bin//etcher

TargetFile="${HOME}/Projects/Balena-Etcher/Balena-Etcher"
if [ -f $TargetFile ]; then
  Command="$TargetFile > /dev/null 2>&1 &"
  eval $Command
else
  echo "Unfortunately, $TargetFile does not exist, so I am unable to continue."
  exit 1
fi
exit 0
