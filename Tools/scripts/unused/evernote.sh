#!/bin/sh

# Purpose: Start evernote from the command line

# Install:
# ln ${HOME}/Projects/Evernote/Evernote-Beta-10.41.5-linux-ddl-beta-3540-fc7ed4329d.AppImage ${HOME}/Tools/scripts/evernote.AppImage
# ln ${HOME}/Tools/scripts/evernote.sh ${HOME}/Bin//evernote

${HOME}/Tools/scripts/evernote.AppImage >> ${HOME}/logs/evernote.log 2>&1 &

exit 0
