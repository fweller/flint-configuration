#!/usr/bin/env bash
# system
# What computer system is this script running on?
# Detects Linux, MacOS, Cygwin, WSL (Windows Subsystem for Linux)
# Will also detect host name
#
# Flint Weller
# 20210712
# ------------------------------------------------------------



# Operating Systems
Linux=false
MacOS=false
Cygwin=false
WSL=false
UnrecognizedOS=false

# Machines
Archimedes=false
Tesla=false
Turing=false
DaVinci=false
ZBook=false
Avnet=false
UnrecognizedMachine=false

# Get the operating system name
OSName=$(uname -s)
echo "OS Name is $OSName"

# Detect the host operating system
if [[ $OSName =~ "Linux" ]]; then
  Linux=true
  echo "Operating System is GNU/Linux."
elif [[ $OSName =~ "Darwin" ]]; then
  MacOS=true
  echo "Operating System is BSD/MacOS"
elif [[ $OSName =~ "CYGWIN_NT-10.0" ]]; then
  Cygwin=true
  echo "Operating System is Cygwin64 on Windows10."
else
  UnrecognizedOS=true
  echo "Operating System is unrecognized"
  #exit 0
fi

# Detect if we are running on Microsoft WSL
if [[ $Linux == true ]]; then
  if grep -q Microsoft /proc/version; then
    WSL=true
    echo "Running on WSL (Windows Subsystem for Linux)"
  else
    WSL=false
    echo "Running on native Linux, not WSL"
  fi
fi

# Get the name of this machine
HName=$HOSTNAME
echo "Hostname is $HName"

# Identify this machine
if [[ $HOSTNAME =~ "Archimedes" ]]; then
  Archimedes=true
  echo "Host computer is Archimedes"
elif [[ $HOSTNAME =~ "DaVinci" ]]; then
  DaVinci=true
  echo "Host computer is DaVinci"
elif [[ $HOSTNAME =~ "Tesla" ]]; then
  Tesla=true
  echo "Host computer is Tesla"
elif [[ $HOSTNAME =~ "Turing" ]]; then
  Turing=true
  echo "Host computer is Turing"
elif [[ $HOSTNAME =~ "ZBook" ]]; then
  ZBook=true
  echo "Host computer is ZBook"
elif [[ $HOSTNAME =~ "USSJS17NBA31295" ]]; then
  #HName="Avnet"
  Avnet=true
  echo "Host computer is Avnet"
else
  UnrecognizedMachine=True
  echo "Host computer is unrecognized"
  #exit 0
fi

# End
