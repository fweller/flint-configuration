#!/bin/sh
# Start the old Evernote using WINE as implemented in bottles

# TODO
# run from this location: /home/flint/.local/share/applications
# KDE: kioclient exec <path-to-desktop-file>
# Gnome: gio launch ./foo.desktop or gtk-launch app-name

# Define how we will run the command
#Cmd="flatpak run --command=bottles-cli com.usebottles.bottles run -p Evernote -b 'Evernote' -- %u"
#Cmd="flatpak run --command=bottles-cli com.usebottles.bottles run -p Evernote -b Evernote -- %u"
#echo "$Cmd"

# Run the command and redirect the output to a log file
#$Cmd >> "${HOME}/Logs/evernote-old.log" 2>&1 &

flatpak run --command=bottles-cli com.usebottles.bottles run -p Evernote-Old -b Evernote-Old -- %u >> "${HOME}/Logs/evernote-old.log" 2>&1 &
