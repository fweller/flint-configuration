#!/bin/bash
# Install Docker
# This file includes many functions, only one of which needs to be run

## Docker Engine
# This is what I usually want to install
# Info: https://docs.docker.com/engine/
# Install Docker Engine: https://docs.docker.com/engine/install/

## Docker Desktop
# This is something more advanced that is also more of a hassle
# I rarely or never want to install this
# Info: https://www.docker.com/products/docker-desktop/
# Install Docker Desktop: https://docs.docker.com/desktop/install/linux-install/
docker_desktop_version="4.17.0"

## Docker Hub
# I never need this
# Info https://hub.docker.com/


# ------------------------------------------------------
# Docker Repository Setup
# ------------------------------------------------------
docker-repo-setup(){
# Add the Docker repository which is required for all modern install methods
# These instructions work for both Debian and Ubuntu, and with modification
# should also work for Mint

# Information https://docs.docker.com/engine/install/
# Debian https://docs.docker.com/engine/install/debian/
# Ubuntu https://docs.docker.com/engine/install/ubuntu/

# Update the apt package index and install packages to allow apt to use a repository over HTTPS:
sudo apt update
sudo apt -y install ca-certificates curl gnupg lsb-release

# Get the Distro ID from lsb_release
Distro_ID=$(lsb_release --id --short | tr "[:upper:]" "[:lower:]")

# Add Docker’s official GPG key:
sudo rm -f /etc/apt/keyrings/docker.gpg # Remove, just in case it was already there
sudo install -m 0755 -d /etc/apt/keyrings
case $Distro_ID in
  'debian')
    curl -fsSL https://download.docker.com/linux/debian/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
    ;;
  'ubuntu')
    curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg
    ;;
  *)
    exit
    ;;
esac
sudo chmod a+r /etc/apt/keyrings/docker.gpg

#sudo mkdir -m 0755 -p /etc/apt/keyrings # Make directory, probably unnecessary
#curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /etc/apt/keyrings/docker.gpg

# Set up the repository
sudo rm -f /etc/apt/sources.list.d/docker.list # Remove, just in case it was already there
case $Distro_ID in
  'debian')
    echo \
      "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian \
      "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
      sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    ;;
  'ubuntu')
    echo \
      "deb [arch="$(dpkg --print-architecture)" signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/ubuntu \
      "$(. /etc/os-release && echo "$VERSION_CODENAME")" stable" | \
      sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
    ;;
  *)
    exit
    ;;
esac

# DEBIAN NOTE: Some derivative distros may require the following text in the above command to be substituted
# $(. /etc/os-release && echo "$VERSION_CODENAME")

# UBUNTU NOTE: Some derivative distros (such as Mint) may require substituting
# UBUNTU_CODENAME instead of VERSION_CODENAME

} #docker-repo-setup()


# ------------------------------------------------------
# Docker Engine Installation - 2023
# ------------------------------------------------------
docker-engine-2023(){
# As of early 2023 this is the recommended method from Docker
# These instructions work for both Debian and Ubuntu, and with modification
# should also work for Mint

# Information https://docs.docker.com/engine/install/
# Debian https://docs.docker.com/engine/install/debian/
# Ubuntu https://docs.docker.com/engine/install/ubuntu/

# Uninstall old versions
#sudo apt -y remove docker docker-engine docker.io containerd runc
for pkg in docker.io docker-doc docker-compose podman-docker containerd runc; do sudo apt-get remove $pkg; done
sudo apt remove -y docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

# Set up Docker repository
docker-repo-setup

# Install Docker Engine, containerd, and Docker Compose
sudo apt update
sudo apt -y install docker-ce docker-ce-cli containerd.io docker-buildx-plugin docker-compose-plugin

# Verify that the Docker Engine installation is successful
sudo docker run hello-world
} #docker-engine-2023()



# ------------------------------------------------------
# Docker Engine Installation - 2020
# ------------------------------------------------------
docker-engine-2020(){
# This is how I installed Docker in Ubuntu 20.04 in late 2020
# As of early 2023 this is no longer the recommended method from Docker

# Uninstall old versions of Docker
sudo apt -y remove docker docker-engine docker.io containerd runc

# Update repo sources
sudo apt update

# Install prerequisite packages
sudo apt -y install apt-transport-https ca-certificates curl gnupg-agent software-properties-common

# Add Docker GPG key to our system
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -

# Add Docker repo to apt sources
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu focal stable"

# Update repo sources
sudo apt update

# Tell apt to use docker repo and not ubuntu
apt-cache policy docker-ce

# Install Docker
sudo apt -y install docker-ce

# Check that Docker is running
sudo systemctl status docker

#Configuring Docker so we don't need to use sudo for every command

# Add our username to the docker group
sudo usermod -aG docker "${USER}"

# Apply the new group membership
su - "${USER}"

# Confirm that our user is now added to the docker group
id -nG

# Requested by NXP LSDK
sudo gpasswd -a "${USER}" docker

# Restart
sudo service docker restart

# May need to log out of account and back in for the change to be effective without doing the first two commands again.

# Confirm that my username is in the docker group
groups
} #docker-engine-2020()


# ------------------------------------------------------
# Docker Desktop - 2023
# ------------------------------------------------------
docker-desktop-2023(){
# As of early 2023 this is the recommended method from Docker Desktop
# This will also install docker-cli

# If not running Gnome DE, install gnome-terminal
# Safe to run this in Gnome because it would already be installed
sudo apt update
sudo apt -y install gnome-terminal

# If docker-desktop was installed previously, clean it up
sudo apt -y remove docker-desktop
rm -r "$HOME/.docker/desktop"
sudo rm /usr/local/bin/com.docker.cli
sudo apt -y purge docker-desktop

# Set up Docker repository
docker-repo-setup

# Download the DEB package
pushd "${HOME}/Downloads" || exit
wget "https://desktop.docker.com/linux/main/amd64/docker-desktop-${docker_desktop_version}-amd64.deb"
# Install the package
sudo apt -y install -f "./docker-desktop-${docker_desktop_version}-amd64.deb"
popd || exit
} #docker-desktop-2023()


# ------------------------------------------------------
# Run one of the above functions
# ------------------------------------------------------
# Only uncomment one of the following three lines
docker-engine-2023  # Command line as of 2023
#docker-engine-2020  # Command line as of 2020
#docker-desktop-2023 # Desktop as of 2023 - Unnecessary



