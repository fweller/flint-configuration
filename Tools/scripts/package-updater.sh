#!/bin/sh
# ----------------------------------------
# NAME        : package-updater.sh
# DESCRIPTION : Update packages using the most preferred package manager
# USAGE       : package-updater.sh
# WORKS ON    : Linux, WSL2, MacOS
# ----------------------------------------

Command() {
  echo ""
  echo "COMMAND: $1"
  echo ""
  eval "$1"
}

# Detections performed in order from most to least preferred
# nala   = Upgraded: Debian, Ubuntu, Mint
# apt    = Debian, Ubuntu, Mint
# brew   = MacOS HomeBrew
# pkcon  = Distro Agnostic
# pacman = Arch, Manjaro
# zypper = OpenSUSE
# dnf    = Fedora, RHEL
# urpmi  = Mandriva, Mageia
# emerge = Gentoo

if [ -x "$(command -v nala)" ]; then
  Command "sudo nala update"
fi

if [ -x "$(command -v apt)" ]; then
  Command "sudo apt update"
  #Command "sudo apt --fix-broken install -y"
fi

if [ -x "$(command -v brew)" ]; then
  Command "brew update"
fi

if [ -x "$(command -v pkcon)" ]; then
  Command "sudo pkcon refresh"
fi

if [ -x "$(command -v pacman)" ]; then
  Command "sudo pacman -Sy"
fi

if [ -x "$(command -v zypper)" ]; then
  Command "sudo zypper refresh"
fi

if [ -x "$(command -v dnf)" ]; then
  Command "sudo dnf refresh"
fi

if [ -x "$(command -v urpmi)" ]; then
  Command "sudo urpmi.update -a"
fi

if [ -x "$(command -v emerge)" ]; then
  Command "sudo emerge --sync"
fi

exit 0

