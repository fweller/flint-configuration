#!/bin/sh
# ----------------------------------------
# NAME        : startapps
# SUMMARY     : Start my work applications
# DESCRIPTION : From the command line
# USAGE       : startapps
# INSTALL     : ln "${HOME}/Tools/scripts/work/startapps.sh" "${HOME}/Bin/startapps"
# TODO        : Rely on a dotfile to know which apps to open
# ----------------------------------------



# ----------------------------------------
# Variables

# Sleep for this amount of time for apps to finish opening
Delay="30s" # Good for apps and opening windows VM
#Delay="15s" # Good for apps only, no windows VM


# ----------------------------------------
# Notes

# Need to use "command" when calling application from within a function
#   "command" will open a subshell, while "exec" will not
# Do not put the commands into a function or else things get strange


# ----------------------------------------
# Nautilus: Gnome file browser

#if command -v nautilus
#then
#  nautilus "${HOME}/Downloads" >> "${HOME}/Logs/nautilus.log" 2>&1 &
#fi


# ----------------------------------------
# Dolphin: KDE Plasma browser

#if command -v dolphin
#then
#  dolphin >> "${HOME}/Logs/dolphin.log" 2>&1 &
#fi


# ----------------------------------------
# Konsole : KDE Plasma Terminal

#if command -v konsole
#then
#  konsole >> "${HOME}/Logs/konsole.log" 2>&1 &
#fi


# ----------------------------------------
# Google Chrome

#if command -v google-chrome
#then
#  google-chrome & >> "${HOME}/Logs/google-chrome.log" 2>&1 &
#fi


# ----------------------------------------
# Firefox

#if command -v firefox
#then
#  firefox >> "${HOME}/Logs/firefox.log" 2>&1 &
#else
#  flatpak run org.mozilla.firefox >> "${HOME}/Logs/firefox.log" 2>&1 &
#fi


# ----------------------------------------
# Microsoft Teams

if command -v teams
then
  teams >> "${HOME}/Logs/teams.log" 2>&1 &
else
  flatpak run com.github.IsmaelMartinez.teams_for_linux >> "${HOME}/Logs/teams.log" 2>&1 &
fi


# ----------------------------------------
# Spotify

#if command -v spotify
# then
#  spotify >> "${HOME}/Logs/spotify.log" 2>&1 &
#else
#  flatpak run com.spotify.Client >> "${HOME}/Logs/spotify.log" 2>&1 &
#fi


# ----------------------------------------
# Obsidian

if command -v obsidian
 then
  obsidian >> "${HOME}/Logs/obsidian.log" 2>&1 &
else
  flatpak run md.obsidian.Obsidian >> "${HOME}/Logs/obsidian.log" 2>&1 &
fi


# ----------------------------------------
# Start Windows VM

#Cmd="${HOME}/Tools/scripts/work/startwindows.sh"
#test -f "$Cmd" && sh "$Cmd" &


# ----------------------------------------
# Wait for everything to open

echo "Will now sleep for ${Delay}"
sleep "${Delay}"


# ----------------------------------------
# Position the applications on the monitors

eval "${HOME}/Tools/scripts/work/moveapps.sh"
# eval "${HOME}/Tools/scripts/work/movewindows.sh"


# ----------------------------------------
# Finish

exit 0 # Success


