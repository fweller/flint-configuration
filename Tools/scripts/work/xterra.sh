#!/bin/bash
# xterra.sh
# This script will pull my favorite repos

# Location for all these repos to reside on local machine
LocalDir="${HOME}/Avnet"
Remote="git@xterra2.avnet.com"


# List of all repositories that I want to install
declare -a Repos=(
"nxp/internal/weller/security.git"
"nxp/mpu/docker.git"
"nxp/mpu/mx8mplus-usb-audio-gadget-prototype.git"
"nxp/maaxboard/meta-maaxboard.git"
"nxp/internal/git.git"
"nxp/internal/latex.git"
"nxp/mpu/layerscape/lstfa.git"
) # Repos

# Calculate array length for the loop
ArrayLength=${#Repos[@]}

# Create directory and move into it
mkdir -p "${LocalDir}"
pushd "${LocalDir}" || exit


# Loop through every repo in the array and clone it
for (( i=0; i < ArrayLength ; i++ )); do
  git clone "${Remote}:${Repos[$i]}"
done


# Finish
popd || exit

