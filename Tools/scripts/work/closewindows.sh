#!/bin/sh
# ----------------------------------------
# NAME        : closewindows
# SUMMARY     : Closes my Windows VM client
# DESCRIPTION : Run this command if an emergency occurs
# USAGE       : closewindows
# INSTALL     : ln "${HOME}/Tools/scripts/work/closewindows.sh" "${HOME}/Bin/closewindows"
# ----------------------------------------


# ----------------------------------------
# Variables

VMName="Win10ProAvnet"  # Default, will be overwritten
VMName1="WinVM05"       # Used on ZBook
VMName2="WinVM04"       # Used on Turing
VMName3="WinVM01"       # Used on Hedy


# ----------------------------------------
# List running VMs

echo "----------------------------------------"
echo "Listing any running VMs:"
vboxmanage list runningvms


# ----------------------------------------
# Detect which VM is present

test -f "${HOME}/VMs/$VMName1/$VMName1.vdi" && VMName=$VMName1
test -f "${HOME}/VMs/$VMName2/$VMName2.vdi" && VMName=$VMName2
test -f "${HOME}/VMs/$VMName3/$VMName3.vdi" && VMName=$VMName3


# ----------------------------------------
# Close the VM while saving the state

echo "----------------------------------------"
echo "Closing $VMName"
vboxmanage controlvm "$VMName" savestate


# ----------------------------------------
# Finish

echo "----------------------------------------"
echo "Done"
exit 0 # Success

