#!/bin/sh

# Purpose: Start CodeWarrior from the command line
# Install:
# ln "${HOME}/Tools/scripts/work/codewarrior.sh" "${HOME}/Bin/codewarrior"

# When CodeWarrior is installed using sudo
sh /opt/Freescale/CW4NET_v2020.06/CW_ARMv8/fsl_eclipse.sh >> ${HOME}/Logs/codewarrior.log 2>&1 &

# When CodeWarrior is installed without sudo
# sh ${HOME}/Freescale/CW4NET_v2020.06/CW_ARMv8/fsl_eclipse.sh >> ${HOME}/Logs/codewarrior.log 2>&1 &

