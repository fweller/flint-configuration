#!/bin/bash

# Build NXP UUU Manufacturing Tools
# https://github.com/nxp-imx/mfgtools

# Change the release tag as needed
Version="uuu_1.5.21"

# Change the build directory as needed
BuildDir="${HOME}/Builds"

# Install required packages
# Works for Ubuntu and Debian.  Will probably work for Mint and PopOS.
sudo apt update
sudo apt -y install libusb-1.0-0-dev libbz2-dev libzstd-dev pkg-config cmake libssl-dev g++

# Move to the build directory
mkdir -p "$BuildDir"
pushd "$BuildDir" || exit

# Clone the project and move into it
git clone https://github.com/NXPmicro/mfgtools.git
cd mfgtools || exit

# Check out the specified tag
git checkout "tags/$Version"

# Build
cmake . && make

# Remove old versions of UUU
sudo rm -f /usr/local/sbin/uuu
sudo rm -f /usr/local/bin/uuu

# Install UUU so that both root and users can access it
sudo cp "$BuildDir/mfgtools/uuu/uuu" /usr/local/bin/uuu
sudo ln -s /usr/local/bin/uuu /usr/local/sbin/uuu

# Exit
popd || exit

