#!/bin/bash

## NXP iMX BCU (Board Remote Control Utilities)
#  This script will install BCU in Linux
#  NXP runs this with Ubuntu 18 and newer
#  Flint runs this with Debian 12 Bookworm

## -----------------------------------------------
## Variables
BcuGitTag="bcu_1.1.68" # Update this as newer versions are released
BuildDir="${HOME}/Builds"
FtdiRules="/etc/udev/rules.d/xx-ftdi.rules"

echo ""; echo ""
echo "-----------------------------------------------"
echo "BCU Git Tag is: $BcuGitTag"

echo ""; echo ""
echo "-----------------------------------------------"
echo "Install required packages"

echo "Update repo"
sudo apt update

echo "Install packages required to run BCU"
sudo apt install libftdi1-2 libyaml-dev openssl libssl-dev libconfuse-dev

echo "Install packages required to build BCU as specified by NXP"
sudo apt install libftdi1-dev libyaml-dev openssl pkgconf libncurses5-dev libssl-dev libconfuse-dev

echo "Install packages reguired to build"
sudo apt install git make cmake

# Ubuntu distros may also require (if not already installed)
# $ sudo apt install lsb_release


echo ""; echo ""
echo "-----------------------------------------------"
echo "Building"

echo "Enter the build directory"
pushd "$BuildDir" || exit

echo "Create build directory in case it doesn't already exist"
mkdir -p "$BuildDir"

echo "Clone the project"
git clone https://github.com/NXPmicro/bcu.git

echo "Move into the directory"
cd bcu || exit

echo "Check out the specific tag"
git checkout tags/$BcuGitTag

echo "Checkout required submodules"
git submodule update --init

echo "Cmake and then Make"
cmake .
make

echo "Leave the build direcotry"
popd || exit



echo ""; echo ""
echo "-----------------------------------------------"
echo "Installing"

echo "Remove old versions of BCU"
sudo rm -f /usr/local/sbin/bcu
sudo rm -f /usr/local/bin/bcu

echo "Install BCU so that both root and users can access it"
sudo cp "$BuildDir/bcu/bcu" /usr/local/bin/bcu
sudo ln -s /usr/local/bin/bcu /usr/local/sbin/bcu


echo ""; echo ""
echo "-----------------------------------------------"
echo "To avoid running BCU as root in Linux, set rules for the system"

# DO NOT TOUCH ANY TEXT BETWEEN THESE ARROWS ---->>>>
sudo sh -c "cat << EOF > $FtdiRules
SUBSYSTEMS=="usb", ATTRS{idVendor}=="0403", ATTRS{idProduct}=="6011", GROUP="dialout", MODE="0660"
EOF"
# <<<<---- DO NOT TOUCH ANY TEXT BETWEEN THESE ARROWS

echo ""; echo ""
echo "-----------------------------------------------"
# Show the file
echo ""; echo ""; echo "--------------------------------"
echo "The contents of the file $FtdiRules are:"
echo ""
cat $FtdiRules
echo ""


