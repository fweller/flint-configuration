#!/bin/bash
# ----------------------------------------
# NAME        : archivewindows
# SUMMARY     : Archive my Windows VM
# DESCRIPTION : Stores the archive on the NAS
# USAGE       : archivewindows
# INSTALL     : ln "${HOME}/Tools/scripts/work/archivewindows.sh" "${HOME}/Bin/archivewindows"
# ----------------------------------------


# ----------------------------------------
# Variables used in this script

ScriptName="archivewindows"
VMName="Win10ProAvnet"  # Default, will be overwritten
VMName1="WinVM05"       # Used on ZBook
VMName2="WinVM04"       # Used on Turing
VMName3="WinVM01"       # Used on Hedley
VMDir="${HOME}/VMs"
LocalArchiveDir="${HOME}/Backups"
RemoteArchiveDir="/mnt/SkyWork/Backups"
RemoteOVADir="/mnt/SkyData/VMs/Windows"


# ----------------------------------------
# Print usage

Usage() {
  echo "Usage:"
  echo "bash $ScriptName debug  # Debug this script"
  exit 1 # Operation not permitted
}


# ----------------------------------------
# Detect which VM is present

VMDetect() {
  echo ""; echo ""; echo "VM NAME DETECT START"
  test -f "${HOME}/VMs/$VMName1/$VMName1.vdi" && VMName=$VMName1
  test -f "${HOME}/VMs/$VMName2/$VMName2.vdi" && VMName=$VMName2
  test -f "${HOME}/VMs/$VMName3/$VMName3.vdi" && VMName=$VMName3
  if [ "$DEBUG" ]; then echo "VMDetect() VMName=$VMName"; fi
  echo ""; echo ""; echo "VM NAME DETECT ${VMName}"
  echo ""; echo ""; echo "VM NAME DETECT END"
}


# ----------------------------------------
# Detect if VM is already running, abort if it is

VMRunning() {
  echo ""; echo ""; echo "VM RUNNING DETECTION START"
  running=$(vboxmanage list runningvms | grep $VMName)
  if [ "$DEBUG" ]; then echo "VMRunning() $running"; fi
  if [ -n "$running" ]; then 
    echo "WARNING: $VMName is running.  Exiting..."
    exit
  else
    if [ "$DEBUG" ]; then echo "VMRunning() $VMName is not running."; fi
  fi
  echo ""; echo ""; echo "VM RUNNING DETECTION END"
}


# ----------------------------------------
# Prepare VM for archival, then archive it

VMArchive() {
  echo ""; echo ""; echo "ARCHIVING START"

  # Remove existing archives
  echo ""; echo ""; echo "STEP 1 - Removing existing archives"
  mv "${LocalArchiveDir}/${VMName}.7z" "${LocalArchiveDir}/${VMName}.7z.old"
  mv "${LocalArchiveDir}/${VMName}.ova" "${LocalArchiveDir}/${VMName}.ova.old"

  # Compact the virtual disk
  echo ""; echo ""; echo "STEP 2 - Compacting the virtual disk ${VMDir}/${VMName}"
  pushd "${VMDir}/${VMName}" || exit
  VBoxManage modifymedium --compact "${VMName}.vdi"
  popd || exit


  # Archive the entire VM directory
  echo ""; echo ""; echo "STEP 3 - Archiving the entire VM directory ${LocalArchiveDir}/${VMName}"
  7za a "${LocalArchiveDir}/${VMName}.7z" "${VMDir}/${VMName}"

  # Copy file to the NAS in a separate shell
  echo ""; echo ""; echo "STEP 4 - Copying the 7z archive to the NAS"
  rsync --archive --verbose --progress "${LocalArchiveDir}/${VMName}.7z" "${RemoteArchiveDir}" &

  # Export OVA
  echo ""; echo ""; echo "STEP 5 - Exporting the VM to OVA format"
  VBoxManage export "${VMName}" --ovf20 --output "${LocalArchiveDir}/${VMName}.ova"

  # Copy file to the NAS in a separate shell
  echo ""; echo ""; echo "STEP 6 - Copying the OVA to the NAS"
  rsync --archive --verbose --progress "${LocalArchiveDir}/${VMName}.ova" "${RemoteOVADir}" &


  echo ""; echo ""; echo "ARCHIVING END"
}


# ----------------------------------------
# Parse the Command line parameters

if [ $# -eq 0 ]; then
  echo "" #Do nothing really
fi

for arg do
  case $arg in
    debug|--debug)
      DEBUG="true"
      ;;
    *)
      Usage
      ;;
  esac
done


# ----------------------------------------
# Call the above functions

VMDetect  # Detect VM name
VMRunning # Shut down a running VM
VMArchive # Perform the archival


# ----------------------------------------
# Finish
exit 0 # Success

