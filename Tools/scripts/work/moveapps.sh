#!/bin/sh
# ----------------------------------------
# NAME        : moveapps
# SUMMARY     : Moves my application windows to my preferred location
# DESCRIPTION :
# USAGE       : moveapps
# NOTE        : This script never worked right
# INSTALL     : ln "${HOME}/Tools/scripts/work/moveapps.sh" "${HOME}/Bin/moveapps"
# ----------------------------------------



# ----------------------------------------
# Force 4 workspaces

wmctrl -n 4

Output () {
  echo "$1"
  bash -c "$1"
}


# ----------------------------------------
# Workspace 1 - General Purpose

# MONITOR CENTER

# Dolphin
#Output "wmctrl -r 'Dolphin' -e 0,3840,53,1920,2163"
#Output "wmctrl -r 'Dolphin' -e 0,0,877,960,2063"
#Output "wmctrl -r 'Dolphin' -e 0,0,0,960,2063"
#Output "wmctrl -r 'Dolphin' -t 0"

# Firefox
#Output "wmctrl -r 'Firefox' -e 0,1894,-23,1972,2168"
#Output "wmctrl -r 'Firefox' -e 0,960,877,1920,2063"
#Output "wmctrl -r 'Firefox' -e 0,960,0,1920,2063"
#Output "wmctrl -r 'Firefox' -t 0"

# Konsole
#Output "wmctrl -r Konsole -t 0 -e 0,1920,0,1920,2030"
#Output "wmctrl -r 'Konsole' -e 0,2880,877,960,2063"
#Output "wmctrl -r 'Konsole' -e 0,2880,0,960,2063"
#Output "wmctrl -r 'Konsole' -t 0"

# MONITOR RIGHT

# Obsidian
#Output "wmctrl -r 'Obsidian' -e 0,3840,0,2160,1853"
Output "wmctrl -r 'Obsidian' -t -2" # move to all desktops

# Teams
#Output "wmctrl -r 'Microsoft Teams' -e 0,5760,53,1920,2063"
#Output "wmctrl -r 'Microsoft Teams' -e 0,3840,1890,2160,1853"
Output "wmctrl -r 'Microsoft Teams' -t -2" # Move to all desktops



# ----------------------------------------
# Workspace 2 - Windows VM

# The movewindows script will take care of this


# ----------------------------------------
# Workspace 3 - Working

# Konsole
#Output "wmctrl -r 'Konsole' -e 0,3840,53,1920,2062"
#Output "wmctrl -r 'Konsole' -t 2"

# Firefox


# ----------------------------------------
# Workspace 4 - Extras

# Spotify
#Output "wmctrl -r 'Spotify' -e 0,0,53,1920,2163"
#Output "wmctrl -r 'Spotify' -e 0,1920,877,1920,2063"
#Output "wmctrl -r 'Spotify' -t 3"

# Solaar
#Output "wmctrl -r 'solaar' -e 0,3840,53,1920,2062"
#Output "wmctrl -r 'solaar' -t 2"


# ----------------------------------------
# Notes

# Run this command to see the exact position to specify
# wmctrl -lG

# Run this command to see what extra frame boundary is associated with the window
# xprop | grep EXTENT
# <click the GEdit window>

# Using this command to move things around
# wmctrl -r NAME -e gravity,X,Y,width,height

# ----------------------------------------

