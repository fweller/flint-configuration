#!/bin/sh
# ----------------------------------------
# NAME        : movewindows
# SUMMARY     : Moves my Windows VM Client window
# DESCRIPTION : 
# USAGE       : movewindows
# NOTE        : This script never worked right
# INSTALL     : ln "${HOME}/Tools/scripts/work/movewindows.sh" "${HOME}/Bin/movewindows"
# ----------------------------------------



# ----------------------------------------
# Print command and then do it

Output () {
  echo $1
  eval $1
}


# ----------------------------------------
# 2020-2022 Attempts

#Output "wmctrl -r 'VirtualBox' -e 0,0,53,3840,2160"
#0x04a00017  1 1919 53   3840 2061 Turing WinVM04 [Running] - Oracle VM VirtualBox
#0x0480000d  1 3840 53   3840 2062 Turing WinVM04 [Running] - Oracle VM VirtualBox
#Output "wmctrl -r 'VirtualBox' -e 0,1919,53,3840,2061"
#Output "wmctrl -r 'VirtualBox' -e 0,3840,53,3840,2062"
#Output "wmctrl -r 'VirtualBox' -t 1"


# ----------------------------------------
# 2023 start maximized

#Output "wmctrl -r 'Oracle VM VirtualBox' -e 0,0,877,3840,2063"
#Output "wmctrl -r 'Oracle VM VirtualBox' -t 1"


# ----------------------------------------
# 2024-02 start smaller if possible

Output "wmctrl -r 'Oracle VM VirtualBox' -e 0,0,877,1920,1200"
Output "wmctrl -r 'Oracle VM VirtualBox' -t 1"


# ----------------------------------------
# Finish

exit 0 # Success

