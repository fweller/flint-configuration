#!/bin/sh

# Purpose: run wireshark as built on my workstation
# Install:
# ln "${HOME}/Tools/scripts/work/wireshark.sh" "${HOME}/Bin/wireshark"

sudo ${HOME}/Wireshark/build/run/wireshark >> ${HOME}/Logs/wireshark.log 2>&1 &

