#!/bin/bash
# nxp-flex-installer.sh
# Written by Flint Weller
# This script will install NXP Flex-Installer

# Variables for GitHub project, directory, branch, and tag
# Update as needed
Account="https://github.com/nxp-imx" # Account name nxp-imx
Project="meta-nxp-desktop"           # Project meta-nxp-desktop
Directory="scripts"                  # /scripts
Branch="lf-6.1.22-2.0.0-mickledore"  # git branch
Tag="rel_lf_6.1.22_2.0.0_ldp"        # git tag
GitName="flex-installer_1.14.2110.lf"  # The filename as stored in github
BuildDir="${HOME}/Builds"  # The is the local build directory
FileName="flex-installer"  # This is the installed name of the flex-installer script

# Move to the build directory
mkdir -p "$BuildDir"
pushd "$BuildDir" || exit

# Delete previous cloned project, if it exists
rm -rf "${Project}"

# Clone the project and move into it
git clone "${Account}/${Project}" -b "${Branch}"
cd "${Project}" || exit

# Check out the specified tag
git checkout "tags/${Tag}"

# Remove old versions of UUU
sudo rm -f "/usr/local/sbin/${FileName}"
sudo rm -f "/usr/local/bin/${FileName}"

# Install UUU for user access
sudo cp "${BuildDir}/${Project}/${Directory}/${GitName}" "/usr/local/bin/${FileName}"

# Change permissions
sudo chmod a+x "/usr/local/bin/${FileName}"

# Add (unnecessary) symbolic link for root
sudo ln -s "/usr/local/bin/${FileName}" "/usr/local/sbin/${FileName}"

# Exit
popd || exit

