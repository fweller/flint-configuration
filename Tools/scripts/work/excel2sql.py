#!/usr/bin/env python3
# ----------------------------------------
# Name        : excel2sql
# Description : Import Excel XLSX spreadsheet into a SQLite database
# Usage       : excel2sql.py SpreadsheetName.xlsx DatabaseName.db
# ----------------------------------------
# Install Packages: sudo pip3 install openpyxl
# View database with command: sqlitebrowser drms.db
# ----------------------------------------


# ----------------------------------------
import sys
import sqlite3
import openpyxl
from openpyxl import load_workbook
import re


# ----------------------------------------
def slugify(text, lower=True):
    if lower:
        text = text.strip().lower()
    text = re.sub(r'[^\w _-]+', '', text)
    text = re.sub(r'[- ]+', '_', text)
    return text


# ----------------------------------------
def unique_column_names(columns):
    seen = set()
    unique_columns = []
    for col in columns:
        new_col = col
        count = 1
        while new_col in seen:
            new_col = f"{col}_{count}"
            count += 1
        seen.add(new_col)
        unique_columns.append(new_col)
    return unique_columns


# ----------------------------------------
def main():
    if len(sys.argv) != 3:
        print("Usage: excel2sql.py SpreadsheetName.xlsx DatabaseName.db")
        sys.exit(1)

    programname, inputfile, outputfile = sys.argv
    print(f"{programname} {inputfile} {outputfile}")

    try:
        con = sqlite3.connect(outputfile)
    except sqlite3.Error as e:
        print(f"Error connecting to database: {e}")
        sys.exit(1)

    try:
        wb = load_workbook(filename=inputfile)
    except Exception as e:
        print(f"Error loading workbook: {e}")
        sys.exit(1)

    sheets = wb.sheetnames

    for sheet in sheets:
        ws = wb[sheet]

        columns = []
        for row in next(ws.rows):
            columns.append(slugify(row.value))

        columns = unique_column_names(columns)
        query = f"CREATE TABLE {slugify(sheet)} (ID INTEGER PRIMARY KEY AUTOINCREMENT"
        for col in columns:
            query += f", {col} TEXT"
        query += ");"

        try:
            con.execute(query)
        except sqlite3.Error as e:
            print(f"Error creating table: {e}")
            continue

        data = []
        for i, rows in enumerate(ws.iter_rows(min_row=2)):
            data.append(tuple(str(cell.value).strip() if cell.value is not None else '' for cell in rows))

        if data:
            insQuery1 = f"INSERT INTO {slugify(sheet)} ("
            insQuery1 += ', '.join(columns) + ") VALUES ("
            insQuery2 = ', '.join(['?'] * len(columns)) + ")"
            insQuery = insQuery1 + insQuery2

            try:
                con.executemany(insQuery, data)
                con.commit()
            except sqlite3.Error as e:
                print(f"Error inserting data: {e}")

    con.close()


# ----------------------------------------
if __name__ == "__main__":
    main()

