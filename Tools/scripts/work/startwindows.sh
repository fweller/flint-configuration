#!/bin/sh
# ----------------------------------------
# NAME        : startwindows
# SUMMARY     : Start my Windows VM client
# DESCRIPTION : From the command line
# USAGE       : startwindows
# INSTALL     : ln "${HOME}/Tools/scripts/work/startwindows.sh" "${HOME}/Bin/startwindows"
# ----------------------------------------


# ----------------------------------------
# Variables used in this script

ScriptName="startwindows"
VMName="Win10ProAvnet"  # Default, will be overwritten
VMName1="WinVM05"       # Used on ZBook
VMName2="WinVM04"       # Used on Turing
VMName3="WinVM01"       # Used on Hedy
SleepTime="10s"         # How many seconds to sleep before adjusting window size


# ----------------------------------------
# Usage

Usage() {
  echo "Usage:"
  echo "$ bash $ScriptName debug  # Debug this script"
  exit 1
}


# ----------------------------------------
# Detect which VM is present

VMDetect() {
  test -f "${HOME}/VMs/$VMName1/$VMName1.vdi" && VMName=$VMName1
  test -f "${HOME}/VMs/$VMName2/$VMName2.vdi" && VMName=$VMName2
  test -f "${HOME}/VMs/$VMName3/$VMName3.vdi" && VMName=$VMName3
  if [ "$DEBUG" ]; then echo "VMDetect() VMName=$VMName"; fi
}


# ----------------------------------------
# Detect if VM is already running, abort if it is

VMRunning() {
  running=$(vboxmanage list runningvms | grep $VMName)
  if [ "$DEBUG" ]; then echo "VMRunning() $running"; fi
  if [ -n "$running" ]; then 
    echo "WARNING: $VMName is already running.  Exiting..."
    exit
  else
    if [ "$DEBUG" ]; then echo "VMRunning() $VMName is not already running."; fi
  fi
}


# ----------------------------------------
# Start the VM, and then set the resolution

VMStart() {
  # First, start the VM
  VBoxManage startvm "$VMName" >> "${HOME}/Logs/virtualbox-${VMName}.log" 2>&1 &
  # Wait a few seconds for the VM to start running
  sleep "$SleepTime"
  # Second, specify the window size
  VBoxManage controlvm "$VMName" setvideomodehint 3840 1998 32
}


# ----------------------------------------
# Parse the Command line parameters

if [ $# -eq 0 ]; then
  echo "" # Do nothing really
fi

for arg do
  case $arg in
    debug|--debug)
      DEBUG="true"
      ;;
    *)
      Usage
      ;;
  esac
done


# ----------------------------------------
# Call the above functions

VMDetect
VMRunning
VMStart


# ----------------------------------------
# Sleep a few seconds for the window to open

sleep 5


# ----------------------------------------
# Move to second desktop

wmctrl -r 'Oracle VM VirtualBox' -t 1

# ----------------------------------------
# Finish

exit 0 # Success


# ----------------------------------------
# Notes

# Resize window to HD1080 size
# wmctrl -r 'Oracle VM VirtualBox' -e 0,1100,500,1920,1080
# Sleep longer so windows can completely load
# sleep 30
# Finally move the window to the proper location
# movewindows

# ----------------------------------------

