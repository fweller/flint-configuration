#!/usr/bin/env python3
# ----------------------------------------
# Name        : drms-branch.sh
# Description : Export SQLite database to formatted XLSX spreadsheet
# Usage       : drms-branch.sh DatabaseName.db SpreadsheetName.xlsx
# ----------------------------------------
# Install Packages: sudo pip3 install XlsxWriter
# ----------------------------------------

import sys
import os
import sqlite3
import xlsxwriter

def print_usage():
    print("Usage: drms-branch DatabaseName.db SpreadsheetName.xlsx")

def validate_file_path(file_path, file_type):
    if not os.path.isfile(file_path):
        print(f"Error: The specified {file_type} '{file_path}' does not exist.")
        sys.exit(1)

def main():
    # Handle argument passing
    if len(sys.argv) != 3:
        print_usage()
        sys.exit(1)

    programname, inputfile, outputfile = sys.argv
    validate_file_path(inputfile, "database")

    # Open the database
    try:
        conn = sqlite3.connect(inputfile)
    except sqlite3.Error as e:
        print(f"Error: Cannot connect to database '{inputfile}'. {e}")
        sys.exit(1)

    # Create a workbook and add a worksheet
    try:
        workbook = xlsxwriter.Workbook(outputfile)
    except Exception as e:
        print(f"Error: Cannot create Excel file '{outputfile}'. {e}")
        sys.exit(1)

    worksheet = workbook.add_worksheet()

    # Define formats
    header_format = workbook.add_format({
        'font_name': 'Consolas',
        'font_size': 10,
        'bold': True,
        'text_wrap': True,
        'align': 'center',
        'valign': 'top'
    })

    text_format = workbook.add_format({
        'font_name': 'Consolas',
        'font_size': 10,
        'align': 'left'
    })

    numerical_format = workbook.add_format({
        'font_name': 'Consolas',
        'font_size': 10,
        'num_format': '###,##0'
    })

    date_format = workbook.add_format({
        'font_name': 'Consolas',
        'font_size': 10,
        'num_format': 'YYYY-MM-DD'
    })

    month_format = workbook.add_format({
        'font_name': 'Consolas',
        'font_size': 10,
        'num_format': 'YYYY-MM'
    })

    # Start from the first cell. Rows and columns are zero indexed.
    row = 0
    col = 0

    try:
        c = conn.cursor()
        c.execute("SELECT * FROM abc")
        rows = c.fetchall()
        col_names = [description[0] for description in c.description]

        # Write headers
        for col_num, col_name in enumerate(col_names):
            worksheet.write(row, col_num, col_name, header_format)

        # Write data
        for row_num, row_data in enumerate(rows, start=1):
            for col_num, value in enumerate(row_data):
                worksheet.write(row_num, col_num, value, text_format)

    except sqlite3.Error as e:
        print(f"Error: Cannot read from table. {e}")
        sys.exit(1)
    finally:
        conn.close()
        workbook.close()

if __name__ == "__main__":
    main()
b
