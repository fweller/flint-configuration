#!/bin/sh
# ----------------------------------------
# NAME        : whatuse.sh
# DESCRIPTION : Returns usage for host (work or personal)
# USAGE       : whatuse.sh
# WORKS ON    : Linux, MacOS, WSL2, Cygwin, BSD
# ----------------------------------------

# assume that this is for personal use unless explicitly tested
HostName=$($HOME/Tools/scripts/whathost.sh)

case $HostName in
  'tesla')
    Usage="personal"
    ;;
  'archimedes')
    Usage="personal"
    ;;
  'davinci')
    Usage="personal"
    ;;
  'turing')
    Usage="personal"
    ;;
  'zbook')
    Usage="work"
    ;;
  'avnet')
    Usage="work"
    ;;
  *)
    Usage="personal"
    ;;
esac

echo $Usage
exit 0

