# openfolders.ps1
# Windows 11 PowerShell Script
# This script will open multiple tabs in the same File Explorer window
# Each tab will be focused on a specific directory
# The tabs will be ordered properly
# Note that as of 2024 November it is not possible to force new directories to open as tabs in the same window


# Define the folders to open in File Explorer
$folders = @(
    "C:\Users\031295\Downloads",
    "C:\Users\031295\Tmp",
    "W:\Avnet",
    "W:\FAE",
    "W:\NXP",
    "W:\NXP\Processing\iMX",
    "W:\NXP\SoftwareOS",
    "W:\Customers",
    "W:\Suppliers",
    "W:\Projects"
)

# Open each folder in a new File Explorer tab
foreach ($folder in $folders) {
    Start-Process explorer.exe $folder
}
