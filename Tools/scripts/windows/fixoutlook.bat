@echo off
REM ----------------------------------------
REM NAME        : scanpst_rescan_all
REM DESCRIPTION : Scans all .pst files in a specified directory using SCANPST.EXE with specified parameters.
REM USAGE       : Run in a Windows Command Prompt with administrator privileges if required.
REM ----------------------------------------

REM Set the directory containing .pst files
set "pstDir=C:\Users\031295\Work\Outlook"
REM Set the path to SCANPST.EXE
set "scanpstExe=C:\Program Files (x86)\Microsoft Office\root\Office16\SCANPST.EXE"

REM Check if the directory exists
if not exist "%pstDir%" (
    echo Directory %pstDir% does not exist. Exiting.
    exit /b 1
)

REM Loop through all files in the specified directory
for %%f in ("%pstDir%\*.pst") do (
    echo Processing file: %%f
    REM Run SCANPST.EXE with the specified parameters
    "%scanpstExe%" -rescan 10 -force -file "%%f"
    if %errorlevel% neq 0 (
        echo Error processing file %%f. Check if SCANPST.EXE completed successfully.
    )
)
echo All .pst files processed.

