REM openfolders.bat
REM Windows 11 Batch File
REM This script will call a PowerShell script
REM That script will open multiple tabs in the same File Explorer window
REM Each tab will be focused on a specific directory
REM The tabs will be ordered properly
REM Note that as of 2024 November it is not possible to force new directories to open as tabs in the same window
@echo off
powershell.exe -NoProfile -ExecutionPolicy Bypass -File "C:\Users\031295\Scripts\openfolders.ps1"
