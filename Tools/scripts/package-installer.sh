#!/bin/sh
# ----------------------------------------
# NAME        : package-installer.sh
# DESCRIPTION : Install packages using the most preferred package manager
# USAGE       : package-installer.sh <Package Names>
# WORKS ON    : Linux, WSL2, MacOS
# ----------------------------------------

Arguments=$@

Command() {
  echo ""
  echo "COMMAND: $1"
  echo ""
  eval "$1"
}

# Detections performed in order from most to least preferred
# nala   = Upgraded: Debian, Ubuntu, Mint
# apt    = Debian, Ubuntu, Mint
# pkcon  = Distro Agnostic
# pacman = Arch, Manjaro
# zypper = OpenSUSE
# dnf    = Fedora, RHEL
# urpmi  = Mandriva, Mageia
# emerge = Gentoo

if [ -x "$(command -v nala)" ]; then 
  Command "sudo nala install -y ${Arguments}"
elif [ -x "$(command -v apt)" ]; then
  Command "sudo apt install -y ${Arguments}"
elif [ -x "$(command -v pkcon)" ]; then
  Command "sudo pkcon install -y ${Arguments}"
elif [ -x "$(command -v pacman)" ]; then
  Command "sudo pacman -U ${Arguments}"
elif [ -x "$(command -v zypper)" ]; then
  Command "sudo zypper install ${Arguments}"
elif [ -x "$(command -v dnf)" ]; then
  Command "sudo dnf install ${Arguments}"
elif [ -x "$(command -v urpmi)" ]; then
  Command "sudo urpmi ${Arguments}"
elif [ -x "$(command -v emerge)" ]; then
  Command "sudo emerge ${Arguments}"
else
  echo ""; "${ScriptName} Failed to find a suitable package manager"
fi

exit 0

