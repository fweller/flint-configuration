#!/bin/sh
# ----------------------------------------
# NAME        : decryptdir
# DESCRIPTION : Decrypt and decompress 7z archives into directories.
# USAGE       : decryptdir archive1 [archive2 ...]
# ----------------------------------------


# Define directories for logs and discarded files
LOGDIR="$HOME/Logs"
TMPDIR="$HOME/Tmp"
DISCARD_DIR="$TMPDIR/Discard"

# Create directories if they don't exist
mkdir -p "$LOGDIR" || { echo "Failed to create $LOGDIR. Exiting."; exit 1; }
mkdir -p "$DISCARD_DIR" || { echo "Failed to create $DISCARD_DIR. Exiting."; exit 1; }
mkdir -p "$TMPDIR" || { echo "Failed to create $TMPDIR. Exiting."; exit 1; }

# Set log file
LOGFILE="$LOGDIR/decryptdir.log"

# Function to log messages
log_message() {
    echo "$(date +%Y%m%d-%H%M:%S) $1" | tee -a "$LOGFILE"
}

# Prompt user for a password in a cross-platform compatible way
echo "Enter password for decryption:"
stty -echo
IFS= read -r PASSWORD
stty echo
echo

# Process each archive provided as input
for ARCHIVE in "$@"; do
    # Check if the archive exists
    if [ ! -f "$ARCHIVE" ]; then
        log_message "ERROR: Archive $ARCHIVE does not exist. Skipping."
        continue
    fi
    
    # Extract the base directory name from the archive
    DIR="${ARCHIVE%.7z}"

    # Check if the directory already exists
    if [ -d "$DIR" ]; then
        log_message "ERROR: Directory $DIR already exists. Skipping."
        continue
    fi

    log_message "INFO: Decrypting and extracting $ARCHIVE to $DIR"

    # Decrypt and decompress the archive
    7z x -p"$PASSWORD" "$ARCHIVE" 2>&1 | tee -a "$LOGFILE"

    # Verify directory creation
    if [ -d "$DIR" ]; then
        TIMESTAMP=$(date +%Y%m%d-%H%M%S)
        mv "$ARCHIVE" "$DISCARD_DIR/${ARCHIVE%.7z}-$TIMESTAMP.7z"
        log_message "SUCCESS: Archive $ARCHIVE extracted and moved to $DISCARD_DIR/${ARCHIVE%.7z}-$TIMESTAMP.7z"
    else
        log_message "ERROR: Failed to extract archive $ARCHIVE."
    fi
done

# Script complete
log_message "INFO: Decryption and extraction completed."
