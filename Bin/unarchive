#!/bin/sh
# ----------------------------------------
# NAME        : unarchive
# SUMMARY     : Decompresses various archive formats.
# DESCRIPTION : Extracts files from a variety of archive formats using appropriate tools.
#
# USAGE       : unarchive <ArchiveFile>
#
# SUPPORTED FORMATS:
#   - .tar.xz, .7z, .zst, .zip, .tar.gz, .tgz, .tar.bz2, .tbz, .tar
#
# COMPATIBILITY:
#   - Linux, macOS, Windows Subsystem for Linux (WSL)
#
# REQUIREMENTS:
#   - tar, gzip, bzip2, unzip, 7z, zstd, pv
# ----------------------------------------

set -e  # Exit on any command failure

# Function to check if required commands are available
check_command() {
  for cmd in "$@"; do
    if ! command -v "$cmd" >/dev/null 2>&1; then
      echo "Error: $cmd is not installed. Please install it and try again."
      exit 1
    fi
  done
}

# Function to decompress tar.xz files
decompress_tar_xz() {
  archive_path="$1"
  check_command tar xz pv

  # Determine archive size (compatible with Linux and macOS)
  archive_size=$(stat -c%s "$archive_path" 2>/dev/null || stat -f%z "$archive_path")

  if [ -n "$archive_size" ]; then
    pv -s "$archive_size" "$archive_path" | tar -xJf -
    echo "Decompressed $archive_path"
  else
    echo "Error: Could not determine archive size for $archive_path."
    exit 1
  fi
}

# Function to decompress 7z files
decompress_7z() {
  archive_path="$1"
  check_command 7z
  7z x "$archive_path"
  echo "Decompressed $archive_path"
}

# Function to decompress zstd files
decompress_zstd() {
  archive_path="$1"
  check_command pv unzstd

  input_size=$(du -sk "$archive_path" | cut -f 1)
  unzstd -c "$archive_path" | pv -p -s "${input_size}k" | tar xf -
  echo "Decompressed $archive_path"
}

# Function to decompress zip files
decompress_zip() {
  archive_path="$1"
  check_command unzip
  unzip -o "$archive_path"
  echo "Decompressed $archive_path"
}

# Function to decompress gz files
decompress_gz() {
  archive_path="$1"
  check_command tar gzip
  tar -xzf "$archive_path"
  echo "Decompressed $archive_path"
}

# Function to decompress bz2 files
decompress_bz2() {
  archive_path="$1"
  check_command tar bzip2
  tar -xjf "$archive_path"
  echo "Decompressed $archive_path"
}

# Function to decompress tar files
decompress_tar() {
  archive_path="$1"
  check_command tar
  tar -xf "$archive_path"
  echo "Decompressed $archive_path"
}

# Check if input parameter is provided
if [ -z "$1" ]; then
  echo "Usage: $0 <archive>"
  exit 1
fi

archive_path="$1"

# Check if the input archive file exists
if [ ! -f "$archive_path" ]; then
  echo "Error: '$archive_path' does not exist."
  exit 1
fi

# Determine the file extension and call the appropriate function
case "$archive_path" in
  *.tar.xz)
    decompress_tar_xz "$archive_path"
    ;;
  *.7z)
    decompress_7z "$archive_path"
    ;;
  *.zst)
    decompress_zstd "$archive_path"
    ;;
  *.zip)
    decompress_zip "$archive_path"
    ;;
  *.tar.gz|*.tgz)
    decompress_gz "$archive_path"
    ;;
  *.tar.bz2|*.tbz)
    decompress_bz2 "$archive_path"
    ;;
  *.tar)
    decompress_tar "$archive_path"
    ;;
  *)
    echo "Error: Unsupported file format."
    echo "Supported formats: .tar.xz, .7z, .zst, .zip, .tar.gz, .tgz, .tar.bz2, .tbz, .tar"
    exit 1
    ;;
esac

# Finish
echo "----------------------------------------"
echo ""
echo "Extraction completed successfully for '$archive_path'."
echo ""
echo "----------------------------------------"
exit 0
