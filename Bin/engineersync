#!/bin/sh
# --------------------------------------------------
# engineersync
# Bidirectional sync with NAS using Osync
#
# Usage (standard) : engineersync <PARAMETERS>
#     clean         : Clean remote directory in addition to standard sync
#     clobberlocal  : Overwrite local data with remote data using Rsync
#     clobberremote : Overwrite remote data with local data using Rsync
#     debug         : Does the debug thing
#
# --------------------------------------------------


# --------------------------------------------------
# Variables

AppName="engineersync"    # Specific to this application
LocalFolderName="Engineering"   # Specific to this application
RemoteFolderName="Engineering"  # Specific to this application
LocalBase="$HOME"         # Common for all except WSL and Cygwin
RemoteBase="/mnt/SkyData" # Common for all except MacOS
ExcludeList="$HOME/Tools/osync/exclude.list" # Rsync exclude list


OsyncParams="--stats --summary --verbose --log-conflicts --no-resume"
# --summary = Output list of transferred and deleted files.
# --stats   =
# --verbose = Display which files and attrs are actually synchronized and with are to be soft deleted or an in conflict.
# --silent  = Output --verbose only to the log file, not the screen.
# --log-conflicts = Output list of conflicted files.
# --no-resume = do not try to resume a failed run.

RsyncParams="--archive --progress --stats --delete-before --ignore-errors --no-links --exclude-from='$ExcludeList'"
OsyncConfigDir="$HOME/Tools/osync"        # Common for all
LocalDir="$LocalBase/$LocalFolderName"    # Default, will be overwritten
RemoteDir="$RemoteBase/$RemoteFolderName" # Common for all cases except MacOS
ClobberMode="false"      # Default is to not clobber
ClobberLocal="false"     # Clobber the LocalDir
ClobberRemote="false"    # Clobber the RemoteDir
Clean="false"            # Default is not to clean remote directory


# --------------------------------------------------
# Output seconds counter with a message

Output() {
  Message=$1
  NowTime="$(date -u +%s)"
  Elapsed="$((NowTime - StartTime))"
  ElapsedFormatted=$(printf "%06d" $Elapsed)
  echo "TIME: $ElapsedFormatted - $Message"
}


# --------------------------------------------------
# Check if the local directory is available

IsLocalMounted() {
  # Check the local directory
  Output "TESTING : Is local directory $LocalDir available?"
  if [ -d "$LocalDir" ]; then
    Output "SUCCESS : Local directory $LocalDir exists, continuing"
  else
    Output "ERROR   : Local directory $LocalDir does not exist, exiting"
    exit 2 # No such file or directory
  fi
}


# --------------------------------------------------
# Detect if the network directory exists
# Note: It is important to test for a sub-directory of the mountpoint because
#       the mountpoint may already exist even though the NAS isn't mounted

IsRemoteMounted() {
  # Check the remote network directory
  Output "TESTING : Is $RemoteDir mounted?"
  if [ -d "$RemoteDir" ]; then
    Output "SUCCESS : Mountpoint $RemoteDir exists, continuing"
    RemoteMounted="true"
  else
    Output "ERROR   : Mountpoint $RemoteDir does not exist"
    RemoteMounted="false"
  fi
}


# --------------------------------------------------
# Configurations based on host usage and host OS

config_work_linux()
{
  ConfigFile="osync-engineer-linux.conf"
  LocalDir="$LocalBase/$LocalFolderName"
  CommandLine="osync.sh $OsyncConfigDir/$ConfigFile $OsyncParams"
}

config_work_wsl()
{
  ConfigFile="osync-engineer-wsl-work.conf"
  LocalDir="/mnt/c/Users/031295/$LocalFolderName"
  #CommandLine="osync.sh $OsyncConfigDir/$ConfigFile $OsyncParams"
  CommandLine="osync.sh $OsyncConfigDir/$ConfigFile"
}

config_work_cygwin()
{
  # Unsupported
  Output "ERROR   : Cygwin support does not exist yet"
  exit 1 # Operation not permitted
  ConfigFile="osync-engineer-cygwin.conf" # Does not exist
  LocalDir="/cygdrive/c/Users/031295/$LocalFolderName"
  CommandLine="osync.sh $OsyncConfigDir/$ConfigFile $OsyncParams"
  # CommandLine="osync.sh /cygdrive/c/Users/031295/Tools/osync/osync-engineer-cygwin.conf $OsyncParams"
}

config_personal_linux()
{
  ConfigFile="osync-engineer-linux.conf"
  LocalDir="$LocalBase/$LocalFolderName"
  CommandLine="osync.sh $OsyncConfigDir/$ConfigFile $OsyncParams"
}

config_personal_wsl()
{
  ConfigFile="osync-engineer-wsl-personal.conf"
  LocalDir="/mnt/c/Users/flint/$LocalFolderName"
  CommandLine="osync.sh $OsyncConfigDir/$ConfigFile $OsyncParams"
}

config_personal_macos()
{
  ConfigFile="osync-engineer-macos.conf"
  LocalDir="$LocalBase/$LocalFolderName"
  RemoteBase="/Volumes/SkyData" # Different from other OSs
  RemoteDir="$RemoteBase/$RemoteFolderName"
  CommandLine="osync.sh $OsyncConfigDir/$ConfigFile $OsyncParams"
}


# --------------------------------------------------
# Create a seconds counter

StartTime="$(date -u +%s)"
Output "------------------------------------------------------------"
Output "START   : $(date +"%Y-%m-%dT%H:%M:%S%:z")"
Output "START   : Running $AppName"


# --------------------------------------------------
# Handle input commands

for arg do
  case $arg in
    clean|--clean)
      Output "NOTICE  : Cleaning of remote NAS drive selected"
      Clean="true"
      ;;
    clobberlocal|--clobberlocal)
      Output "NOTICE  : ClobberLocal mode initiated.  Will overwrite local files."
      ClobberMode="true"
      ClobberLocal="true"
      ;;
    clobberremote|--clobberremote)
      Output "NOTICE  : ClobberRemote mode initiated.  Will overwrite remote files."
      ClobberMode="true"
      ClobberRemote="true"
      ;;
    debug|--debug)
      Output "NOTICE  : Debug mode initiated"
      Debug="true"
      ;;
    *)
      #Do nothing
      ;;
  esac
done

# Catch error if both clobberlocal and clobberremote are enabled
if [ "$ClobberLocal" = "true" ] && [ "$ClobberRemote" = "true" ]; then
  Output "ERROR   : Using both clobberlocal and clobberremote is not permitted."
  exit 1 # Operation not permitted
fi


# --------------------------------------------------
# Detect Host OS, Host Name, and Host usage

HostOS=$("$HOME"/Tools/scripts/whatos.sh)
HostName=$("$HOME"/Tools/scripts/whathost.sh)
HostUse=$("$HOME"/Tools/scripts/whatuse.sh)
Output "NOTICE  : Host computer is normally used for $HostUse"
Output "NOTICE  : Host operating system is $HostOS"
Output "NOTICE  : Host computer name is $HostName"


# --------------------------------------------------
# Make configuration decisions based on:
# - Host OS (Linux, MacOS, Windows, WSL, Cygwin)
# - Host Name (if needed)
# - Host usage (Work or Personal)

case $HostUse in
  'work')
    case $HostOS in
      'linux')
        config_work_linux
        ;;
      'wsl')
        config_work_wsl
        ;;
      'cygwin')
        config_work_cygwin
        ;;
      *)
        Output "ERROR   : Unrecognized host operating system $HostOS"
        exit 1 # Operation not permitted
        ;;
    esac
    ;;
  'personal')
    case $HostOS in
      'linux')
        config_personal_linux
        ;;
      'wsl')
        config_personal_wsl
        ;;
      'macos')
        config_personal_macos
        ;;
      *)
        Output "ERROR   : Unrecognized host operating system $HostOS"
        exit 1 # Operation not permitted
        ;;
    esac
    ;;
  *)
    Output "ERROR   : Unrecognized host usage $HostUse"
    exit 1 # Operation not permitted
    ;;
esac


# --------------------------------------------------
# Check if local directory exists

IsLocalMounted


# --------------------------------------------------
# Check if remote directory is mounted

IsRemoteMounted


# --------------------------------------------------
# Mount remote directory if it is not mounted yet

if [ "$RemoteMounted" = "false" ]; then

  # First, try to mount the remote directory
  Output "NOTICE  : Will attempt to mount $RemoteDir"
  case $HostOS in
    'linux')
      Output "NOTICE  : Run the following command, and then run $AppName again"
      Output "sudo mount -a"
      exit 2 # No such file or directory
      ;;
    'wsl')
      # Output "NOTICE  : Run the following command, and then run $AppName again"
      # Note: Double backslash must replace single
      bash "${HOME}/Tools/scripts/wslmount.sh"
      ;;
    'macos')
      eval "${HOME}/Tools/scripts/macos/macos-skydata.sh"
      ;;
    *)
      exit 2 # No such file or directory
      ;;
  esac

  # Second, wait a few seconds for the mount to occur
  sleep 5

  # Third, try again
  IsRemoteMounted

  # Fourth, test again
  if [ "$RemoteMounted" = "false" ]; then
    Output "ERROR   : $RemoteDir is still not mounted, exiting."
    exit 2 # No such file or directory
  else
    Output "NOTICE  : $RemoteDir is mounted, continuing"
  fi
fi


# --------------------------------------------------
# If ClobberMode initiated then configure Rsync instead of Osync

if [ "$ClobberMode" = "true" ]; then
  if [ "$ClobberRemote" = "true" ]; then
    #if [ "$HostOS" != "linux" ]; then
    #  Output "ERROR   : ClobberRemote may only be run on a Linux host."
      # Need to correct this, can run from MacOS as well
    #  exit 1 # Operation not permitted
    #fi

    # -------------------------------
    # Clobber Remote
    # Rsync from LocalDir to RemoteDir
    # Delete remote osync folder

    # Note slash after source directory to avoid rsync creating repeating directory structure
    RsyncCmd="rsync $RsyncParams $LocalDir/ $RemoteDir"
    #CommandLine="rsync -a --progress --stats --exclude-from='$ExcludeList' --delete-after --ignore-errors $LocalDir/ $RemoteDir"
    DeleteCmd="rm -rf $RemoteDir/.osync_workdir"
    CommandLine="$RsyncCmd && $DeleteCmd"

  elif [ "$ClobberLocal" = "true" ]; then

    # -------------------------------
    # Clobber Local
    # Rsync from RemoteDir to LocalDir
    # Delete local osync folder
    # Do not delete remote osync folder

    # Note slash after source directory to avoid rsync creating repeating directory structure
    RsyncCmd="rsync $RsyncParams $RemoteDir/ $LocalDir"
    #CommandLine="rsync -a --progress --stats --exclude-from='$ExcludeList' --delete-after --ignore-errors $RemoteDir/ $LocalDir"
    DeleteCmd="rm -rf $LocalDir/.osync_workdir"
    CommandLine="$RsyncCmd && $DeleteCmd"

  else
    Output "ERROR   : ClobberMode specified but not ClobberRemote or ClobberLocal"
    exit 1 # Operation not permitted
  fi
fi


# --------------------------------------------------
# Delete all the crap files in the local directory structure

Output "START   : Removing crap files from the local directory"
Command="rmcrap --dotfiles $LocalDir"
Output "COMMAND : $Command"
eval "$Command"
Output "END     : Removing crap files from the local directory"


# --------------------------------------------------
# Delete Apple .DS_Store files so they don't pollute the NAS
# This is faster than running rmcrap

#if [ "$HostOS" = "macos" ] && [ "$ClobberMode" = "false" ]; then
#  DelCmd="find $LocalDir -name .DS_Store -type f -delete"
#  Output "START   : Removing Apple .DS_Store files from the local directory $LocalDir"
#  Output "START   : Running $DelCmd"
#  Output "END     : Running $DelCmd"
#  Output "END     : Removing Apple .DS_Store files from the local directory $LocalDir"
#fi


# --------------------------------------------------
# If Clean was selected, then delete all the crap files in the remote directory structure

if [ "$Clean" = "true" ]; then
  Output "START   : Removing crap files from the remote directory $RemoteDir"
  Command="rmcrap --dotfiles $RemoteDir"
  Output "COMMAND : $Command"
  eval "$Command"
  Output "END     : Removing crap files from the remote directory $RemoteDir"
fi


# --------------------------------------------------
# Start the action!

Output "START   : Running $CommandLine"
Output "------------------------------------------------------------"
if [ -z "$Debug" ]; then
  eval "$CommandLine"
fi


# --------------------------------------------------
# Finisher

Output "------------------------------------------------------------"
Output "END     : Running $CommandLine"
Output "END     : Running $AppName"
Output "END     : $(date +"%Y-%m-%dT%H:%M:%S%:z")"
Output "------------------------------------------------------------"

# --------------------------------------------------
# End

exit 0 # Success

