#!/bin/sh
# ----------------------------------------
# NAME        : size
# DESCRIPTION : Calculates the total size of a directory.
# USAGE       : size <directory>
# ----------------------------------------

# Ensure the script is called with one parameter
if [ "$#" -ne 1 ]; then
    echo "Usage: $0 <directory>"
    exit 1
fi

# Assign the first parameter to a variable
TARGET_DIR="$1"

# Check if the directory exists and is accessible
if [ ! -d "$TARGET_DIR" ]; then
    echo "Error: '$TARGET_DIR' is not a valid directory."
    exit 1
fi

# Calculate the total size in bytes
TOTAL_SIZE=$(find "$TARGET_DIR" -type f -exec du -b {} + 2>/dev/null | awk '{total += $1} END {print total}')

# If the total size could not be calculated, default to 0
if [ -z "$TOTAL_SIZE" ]; then
    TOTAL_SIZE=0
fi

# Convert scientific notation to a plain integer
TOTAL_SIZE=$(printf "%.0f" "$TOTAL_SIZE")

# Function to convert bytes to a human-readable format
format_size() {
    SIZE=$1
    if [ "$SIZE" -ge 1073741824 ]; then
        # Convert to GB
        printf "%.2f GB\n" "$(echo "$SIZE / 1073741824" | bc -l)"
    elif [ "$SIZE" -ge 1048576 ]; then
        # Convert to MB
        printf "%.2f MB\n" "$(echo "$SIZE / 1048576" | bc -l)"
    elif [ "$SIZE" -ge 1024 ]; then
        # Convert to KB
        printf "%.2f KB\n" "$(echo "$SIZE / 1024" | bc -l)"
    else
        # Keep as bytes
        printf "%d bytes\n" "$SIZE"
    fi
}

# Format the size
FORMATTED_SIZE=$(format_size "$TOTAL_SIZE")

# Output the total size
echo "Total size of '$TARGET_DIR': $FORMATTED_SIZE"
