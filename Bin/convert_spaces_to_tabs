#!/bin/bash

# Function to process a single file
process_file() {
    local file="$1"
    sed -i.bak -e 's/  /\t/g' "$file"  # Replace every 2 spaces with a tab and create a backup file
    rm "${file}.bak"  # Remove the backup file
}

# Function to process all files in a directory recursively
process_directory() {
    local dir="$1"
    find "$dir" -type f | while read -r file; do
        process_file "$file"
    done
}

# Main script logic
if [ -z "$1" ]; then
    target="."
else
    target="$1"
fi

if [ -f "$target" ]; then
    process_file "$target"
elif [ -d "$target" ]; then
    process_directory "$target"
else
    echo "Error: $target is neither a file nor a directory"
    exit 1
fi

echo "Processing complete."

