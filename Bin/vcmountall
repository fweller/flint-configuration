#!/bin/bash
# ----------------------------------------
# NAME        : vcmountall
# DESCRIPTION : VeraCrypt mount all encrypted containers in PWD.
#               Runs in Linux, MacOS, WSL2.
# USAGE       : vcmountall
# ----------------------------------------

set -e  # Exit immediately on any error

hist_disable() {
  if [ -n "$HISTFILE" ]; then
    unset HISTFILE
  fi
}

hist_enable() {
  if [ -z "$HISTFILE" ]; then
    HISTFILE=~/.sh_history  # Adjust to your default history file
  fi
}

hist_disable # Disable history

# ----------------------------------------
# Check if vcmount command is available
if ! command -v vcmount > /dev/null 2>&1; then
  echo "vcmount command not found. Please ensure it is installed and in your PATH."
  hist_enable
  exit 1
fi

# ----------------------------------------
# Get the password securely
echo "Enter the VeraCrypt password:"
read -s -r Password

# ----------------------------------------
# Check and mount all valid containers
success_count=0
fail_count=0

for FILE in *; do
  if [ -f "$FILE" ]; then
    case "$FILE" in
      *.hc|*.vc|*.tc|*.zip)  # Only process known VeraCrypt extensions
        echo "Mounting $FILE..."
        if vcmount "$FILE" "$Password"; then
          echo "Mounted $FILE successfully."
          success_count=$((success_count + 1))
        else
          echo "Failed to mount $FILE."
          fail_count=$((fail_count + 1))
        fi
        ;;
      *)
        echo "Skipping $FILE (not a VeraCrypt container)"
        ;;
    esac
  else
    echo "Skipping $FILE (not a regular file)"
  fi
done

# Clear password variable
unset Password

# ----------------------------------------
# Summary report
echo ""
echo "Mount Summary:"
echo "  Successfully mounted: $success_count"
echo "  Failed to mount: $fail_count"

hist_enable # Enable history

# Set exit status
if [ "$fail_count" -gt 0 ]; then
  exit 1
else
  exit 0
fi

