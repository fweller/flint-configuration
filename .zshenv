#!/bin/zsh
# ----------------------------------------
# NAME        : .zshenv
# DESCRIPTION : ZshEnv = Z Shell Environment
# ----------------------------------------
# .zshenv is always sourced
# It often contains exported variables that should be available to other programs.
# For example, $PATH, $EDITOR, and $PAGER are often set in .zshenv.
# Also, you can set $ZDOTDIR in .zshenv to specify an alternative location for the rest of your zsh configuration.
# Boot Order
#  1 .zshenv always
#  2 .zprofile if login
#  3 .zshrc if interactive
#  4 .zlogin if login
#  5 .zlogout sometimes
# ----------------------------------------

NAME=".zshenv"

# Handle debug
if [ -f "$HOME/.debug" ]; then
  date "+%Y-%m-%d %H:%M:%S-%N %Z $NAME" >> "$HOME/Logs/dotfiles.log"
fi


