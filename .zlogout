#!/bin/zsh
# ----------------------------------------
# NAME        : .zlogout
# DESCRIPTION : ZLogout = Z Shell Logout
# ----------------------------------------
# zlogout is sometimes used to clear and reset the terminal. 
# It is called when exiting, not when opening.
# ----------------------------------------

NAME=".zlogout"

# Handle debug
if [ -f "$HOME/.debug" ]; then
  date "+%Y-%m-%d %H:%M:%S-%N %Z $NAME" >> "$HOME/Logs/dotfiles.log"
fi

# when leaving the console clear the screen to increase privacy
if [ "$SHLVL" = 1 ]; then
    [ -x /usr/bin/clear_console ] && /usr/bin/clear_console -q
fi

