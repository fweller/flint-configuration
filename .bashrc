#!/bin/bash
# ----------------------------------------
# NAME        : .bashrc
# DESCRIPTION : BashRC = Bash RunCom = Bash Run Commands
#               This shell script is an entry point for:
#               - Linux Terminal
# ----------------------------------------

NAME=".bashrc"

# ----------------------------------------
# Handle debug

if [ -f "$HOME/.debug" ]; then
  #eval 'date "+%Y-%m-%d %H:%M:%S-%N %Z $(basename "${BASH_SOURCE[0]}")" >> "$HOME/Logs/dotfiles.log" '
  eval 'date "+%Y-%m-%d %H:%M:%S-%N %Z $NAME" >> "$HOME/Logs/dotfiles.log" '
fi


# ----------------------------------------
# Chain-loading

# The next dotfile to chain-load
nextfile="$HOME/.bash_profile"

# Load .bash_profile for interactive shells if it exists
if [[ $- == *i* ]] && [ -f "$nextfile" ]; then
  . "$nextfile"
fi

