#!/bin/sh
# ----------------------------------------
# NAME        : .profile
# DESCRIPTION : Profile must be compatible with Bash, POSIX Shell, Dash, Ksh, etc...
# WARNING     : If this script outputs anything, KDE Plasma will hang
# ----------------------------------------

NAME=".profile"

# ----------------------------------------
# Handle debug

if [ -f "$HOME/.debug" ]; then
  # Append the formatted date and script name to the log file
  date "+%Y-%m-%d %H:%M:%S-%N %Z $NAME" >> "$HOME/Logs/dotfiles.log"
fi


# ----------------------------------------
# Chain-loading

# The next dotfile to chain-load
nextfile="$HOME/.shellrc"

# Chain-load the next file if this is an interactive shell
if [ "${-#*i}" != "$-" ] && [ -f "$nextfile" ]; then
  . "$nextfile"
fi

