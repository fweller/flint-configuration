#!/bin/zsh
# ----------------------------------------
# NAME        : .zshrc
# DESCRIPTION : ZshRC = ZSH RunCom = Z Shell Run Commands
# ----------------------------------------
# This script is for interactive shells
# ----------------------------------------

NAME=".zshrc"

# ----------------------------------------
# Handle debug

if [ -f "$HOME/.debug" ]; then
  # Append the formatted date and script name to the log file
  date "+%Y-%m-%d %H:%M:%S-%N %Z $NAME" >> "$HOME/Logs/dotfiles.log"
fi

